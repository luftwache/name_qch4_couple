import datetime
import gzip
import h5py
import netCDF4
import numpy as np
import pandas as pd
import re
import sys


def r_netcdf(fpath, key):
    r"""Read variable in NetCDF

    Parameters
    ----------
    fpath : path-like
        Path of the data file.
    key : str
        Variable name.

    Returns
    -------
    f : ndarray
        Output data.

    """
    with netCDF4.Dataset(ifile, 'r') as idata:
        odata = idata[key][:]
    return odata


def r_name(fpath):
    r"""Read dosage csv file within .gz

    Parameters
    ----------
    fpath : path-like
        Path of the data file.

    Returns
    -------
    f : ndarray
        Output data.

    """
    with gzip.open(fpath, 'r') as f_ext:
        f = pd.read_csv(f_ext, sep=',', header=23, skipinitialspace=True)
    return np.array(f.iloc[:, :-1])


def r_name_particles(fpath):
    r"""Read particle end location fixed width file within .gz

    Parameters
    ----------
    fpath : path-like
        Path of the data file.

    Returns
    -------
    f : ndarray
        Output data.

    """
    with gzip.open(fpath, 'r') as f_ext:
        f = pd.read_csv(f_ext, sep=' ', header=0, skipinitialspace=True)
    return np.array(f)


def r_name_met(fpath):
    r"""Read met output of name within .gz

    Parameters
    ----------
    fpath : path-like
        Path of the data file.

    Returns
    -------
    odata : DataFrame
        Output data.

    """
    with gzip.open(fpath, 'r') as f_ext:
        odata = pd.read_csv(
            f_ext, sep=',',
            skipinitialspace=True, skip_blank_lines=False,
            header=22, skiprows=np.arange(23, 39),
            index_col='datetime', parse_dates={'datetime': [0]},
            date_parser=(
                lambda x: datetime.datetime.strptime(x, '%d/%m/%Y %H:%M %Z')
                )
            )
    return odata


def r_intem_posQ(fpath):
    r"""Read InTEM posterior emission fixed width file in .gz

    Parameters
    ----------
    fpath : path-like
        Path of the data file.

    Returns
    -------
    odata : DataFrame
        Output data.

    """
    with gzip.open(fpath, 'r') as f_ext:
        odata = pd.read_fwf(
                f_ext, header=7,
                widths=[4, 4, 14, 14, 4, 14, 14, 14, 12],
                skipinitialspace=True
                )
    return odata


def r_intem_base(fpath, res):
    r"""Read InTEM baseline fixed width file within .gz

    Parameters
    ----------
    fpath : path-like
        Path of the data file.
    res : {'Y', 'M', 'D'}
        Temporal resolution of the data

    Returns
    -------
    odata : DataFrame
        Output data.

    """
    if res == 'D':
        widths=[5, 3, 3, 3, 12, 12]
        parse_dates={'Datetime': ['Yr', 'Mt', 'Dy', 'Hr']}
    elif res == 'M':
        widths=[5, 3, 12, 12, 5]
        parse_dates={'Datetime': ['Yr', 'Mt']}
    elif res == 'Y':
        widths=[5, 3, 12, 12, 5]
        parse_dates={'Datetime': ['Yr']}
    with gzip.open(fpath, 'r') as f_ext:
        odata = pd.read_fwf(
                f_ext, header=0, skipinitialspace=True,
                widths=widths, parse_dates=parse_dates, index_col='Datetime',
                na_values=[-9.9000]
                )
    return odata


def r_instaar(fpath, dtype):
    r"""Read INSTAAR file

    Parameters
    ----------
    fpath : path-like
        Path of the data file.
    dtype : {'event', 'month'}
        Event file or monthly baseline.

    Returns
    -------
    odata : DataFrame
        Output data.

    """
    with open(fpath) as fline:
        line = next(fline)
        header_no = int(line.split(':')[1].strip())
        for i, line in enumerate(fline):
            if i == header_no - 2:
                col_name = line
                break
    col_name = re.sub('# data_fields: ', '', col_name)
    col_name = [name.strip() for name in col_name.split(' ')]
    idata = pd.read_csv(
                fpath,
                sep=r'\s+',
                skiprows=header_no,
                header=None,
                names=col_name
                )
    if dtype == 'month':
        idata['date'] = pd.to_datetime((idata['year']*100 +
                                        idata['month']).astype('i8'),
                                       format='%Y%m')
        idata.set_index('date', inplace=True)
        date = pd.date_range(idata.index[0], idata.index[-1], freq='MS')
        odata = idata['value'].reindex(date, fill_value=np.nan).interpolate()
    else:
        idata = idata[idata['analysis_flag'].str.match('\.(.){2}')]
        idata['date'] = pd.to_datetime(
                (idata['sample_year']*1e10
                    + idata['sample_month'].astype('i8')*1e8
                    + idata['sample_day'].astype('i8')*1e6
                    + idata['sample_hour'].astype('i8')*1e4
                    + idata['sample_minute'].astype('i8')*1e2
                    + idata['sample_seconds'].astype('i8')
                    ).astype('i8').astype(str),
                format='%Y%m%d%H%M%S'
                )
        idata.set_index('date', inplace=True)
        odata = idata['analysis_value'].groupby('date').mean()
    return odata


def r_decc(fpath, cols=['ch4_c', 'ch4_stdev']):
    r"""Read DECC csv file (gcwerks export)

    Parameters
    ----------
    fpath : path-like
        Path of the data file.

    Returns
    -------
    odata : Series
        Output data.

    """
    odata = pd.read_csv(
                fpath,
                usecols=lambda x: x.lower() in ['time', *cols],
                index_col=['time'],
                skipinitialspace=True,
                parse_dates=['time']
                ).dropna()
    odata.columns = odata.columns.str.lower()
    return odata


def r_unfccc(fpath):
    r"""Read UNFCCC csv file

    Parameters
    ----------
    fpath : path-like
        Path of the data file.

    Returns
    -------
    odata : DataFrame
        Output data.

    """
    # Get header and nrows
    with open(fpath) as f:
        head = False
        foot = False
        header_no = 0
        nrows = 0
        for i, line in enumerate(f):
            if line[:6] == ' ,Year':
                years = [i.strip() for i in line.split(',')[2:]]
                years[-1] = re.sub(r'.*\(([0-9]+)\).*', r'\g<1>', years[-1])
            elif line.split(',')[0].strip() == 'Party':
                header_no = i+1
                head = True
                col_name = line.split('\\')[0].strip().split(',') + years
            elif re.match(r'^\W+$', line) and (head) and (not foot):
                nrows = i - header_no
                foot = True
    # Read
    odata = pd.read_csv(
                fpath,
                skiprows=header_no,
                header=None,
                names=col_name,
                nrows=nrows,
                na_values=['NO'],
                )
    return odata


def w_nc(dim, var, ofile):
    r"""Create netCDF4 file

    Parameters
    ----------
    dim : dict
        {key: [values, kwargs1, kwargs2, kwattr]}
        where keys include (time, lat, lon).
        kwargs1 is for the createDimension().
        kwargs2 is for the createVariable().
    var : dict
        Values.
        {key: [values, kwargs, kwattr]}
    ofile : path_like
        Output file.

    """
    with netCDF4.Dataset(ofile, 'w', format='NETCDF4') as ncfile:
        for k, v in dim.items():
            idata_dim = ncfile.createDimension(dimname=k, **v[1])
            idata = ncfile.createVariable(varname=k, dimensions=(k,), **v[2])
            idata[:] =  v[0]
            for k1, v1 in v[3].items():
                setattr(idata, k1, v1)
        for k, v in var.items():
            idata = ncfile.createVariable(varname=k, **v[1])
            idata[:] = v[0]
            for k1, v1 in v[2].items():
                setattr(idata, k1, v1)

