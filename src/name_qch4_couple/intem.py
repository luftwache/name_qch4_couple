import numpy as np


def txt2array(idata, dim, cols, fill=np.nan):
    r"""

    Convert [lon, lat, value] data to array for given columns

    Parameters
    ----------
    idata : ndarray
        Data from fields file.
    dim : list of int
        [nlat, nlon]
    cols : list-like
        Hours of the daily data to read.
        First hour is the 0th hour.
    fill : float
        Value for empty data points. Defaults to np.nan.

    Returns
    -------
    odata : ndarray
        Counts of number of particles leaving each domain borders

    """
    offset = 1
    odata = np.full((len(cols), dim[0], dim[1]), fill)
    x = idata[:, 0].astype(int) - offset
    y = idata[:, 1].astype(int) - offset
    for i, col in enumerate(cols):
        odata[i, y, x] = idata[:, col]
    return odata

