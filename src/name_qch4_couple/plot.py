import cartopy.crs as ccrs
import cartopy.feature as cfeature
import cartopy.io as cio
import functools
import matplotlib as mpl
import matplotlib.colors
import matplotlib.dates as mdates
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
import numpy as np
import sys


def rgetattr(obj, attr, *args):
    def _getattr(obj, attr):
        return getattr(obj, attr, *args)
    return functools.reduce(_getattr, [obj] + attr.split('.'))


def geographical(
        fig,
        idata, grid,
        label='',
        projection=ccrs.PlateCarree(),
        texts=[],
        plot_type='pcolormesh',
        loc_plot=[0.1/6, 0.3/3, 5/6, 2.5/3],
        loc_bar=[5.3/6, 0.3/3, 0.1/6, 1.5/3],
        bbox=[-98.076, 39.556, 10.672, 79.174],
        shapefiles = ['shape_files/CNTR_BN_60M_2016_4326.shp'],
        tick_params = {'labelsize': 6, 'pad': 0.05},
        colour='viridis_r',
        vlim=[
            1e-9, 1e-4,
            np.power(10., np.linspace(-9, -4, 11)),
            np.power(10., np.linspace(-9., -4, 6))
            ],
        gridlines=[np.arange(-180, 190, 10), np.arange(-90, 100, 10)],
        scale='levels', extend='max'
        ):
    r"""Contour plot over map (non-interpolated)

    Parameters
    ----------
    idata : ndarray
        2d lat x lon, float
        Geographical data from gridded data.
        Can accept ungridded data.
    grid : list
        List of longitudes and latitudes of grid box.
        Length depends on plot_type.
            pcolormesh : idata.shape = (len(grid[1])-1, len(grid[0])-1)
            contourf : idata.shape = (len(grid[1]), len(grid[0]))
    label : str, optional
        Name of the Axes object.
    projection : Cartopy.crs proejction
        Map projection.
    texts : list of dict
        Each dict is kwargs for fig.text object, i.e. fig.text(**kwargs).
    plot_type : {'pcolormesh', 'contourf'}
        Plot type.
            pcolormesh : uninterpolated grid cells
            contourf : interpolated grid points
    loc_plot, loc_bar : list of floats
        Location of main plot area/colorbar.
        [x0, y0, width, height]
    bbox : list of floats
        Map extent.
        [lon0, lon1, lat0, lat1]
    shapefiles : list of path-likes
        Path of shapefiles to draw.
    tick_params : dict
        Kwargs for tick labels in colorbar.
    colour : str
        Name of colour scheme in matplotlib
    vlim : list
        [vmin, vmax, ticks, labels]
    gridlines : list of array-likes
        [longitudes, latitudes]
    scale : {'levels', 'log', 'linear'}
        Discrete levels, Log scale or linear scale.
    extend : {'max'', 'min', 'both', neither'}
        Plot outside range.

    Returns
    -------
    fig, ax : object
        Figure and axis objects.

    """
    ax = fig.add_axes(loc_plot, projection=projection, label=label)
    # Set LonLat
    lons, lats = grid
    lon, lat = np.meshgrid(lons, lats)
    ax.set_extent(bbox, crs=projection)
    # Color levels
    if scale == 'log':
        n_level = 256
    elif scale == 'linear':
        n_level = 256
    else:
        if extend == 'neither':
            n_level = len(vlim[2]) - 1
        elif extend == 'both':
            n_level = len(vlim[2]) + 1
        else:
            n_level = len(vlim[2])
    cmap_colour = plt.get_cmap(colour)(np.linspace(0., 1., n_level))
    if extend == 'neither':
        cmap = mpl.colors.ListedColormap(cmap_colour)
    elif extend == 'both':
        cmap = mpl.colors.ListedColormap(cmap_colour[1:-1])
    elif extend == 'min':
        cmap = mpl.colors.ListedColormap(cmap_colour[1:])
    elif extend == 'max':
        cmap = mpl.colors.ListedColormap(cmap_colour[:-1])
    if extend in ['both', 'max']:
        cmap.set_over(cmap_colour[-1])
    else:
        cmap.set_over('#FFFFFFFF')
    if extend in ['both', 'min']:
        cmap.set_under(cmap_colour[0])
    else:
        cmap.set_under('#FFFFFFFF')
    if scale == 'log':
        norm = matplotlib.colors.LogNorm(vmin=vlim[0], vmax=vlim[1])
    elif scale == 'linear':
        norm = None
    else:
        norm = matplotlib.colors.BoundaryNorm(
                boundaries=vlim[2], ncolors=cmap.N, clip=False
                )
    # Data
    if plot_type == 'pcolormesh':
        contour = ax.pcolormesh(
                lon, lat, idata,
                cmap=cmap,
                vmin=vlim[0], vmax=vlim[1],
                norm=norm
                )
    else:
        contour = ax.contourf(
                lon, lat, idata,
                cmap=cmap,
                vmin=vlim[0], vmax=vlim[1], extend=extend,
                norm=norm
                )
    # Eye candies
    for shapefile in shapefiles:
        ax.add_feature(cfeature.ShapelyFeature(
            cio.shapereader.Reader(shapefile).geometries(),
            projection,
            edgecolor='black',
            linewidth=0.5,
            facecolor='none'
            ))
    gridline = ax.gridlines(
            crs=projection, draw_labels=False, linestyle='-'
            )
    gridline.xlocator = mticker.FixedLocator(gridlines[0])
    gridline.ylocator = mticker.FixedLocator(gridlines[1])
    # Colorbars
    if loc_bar:
        cax = fig.add_axes(loc_bar)
        orientation = 'vertical' if loc_bar[3] > loc_bar[2] else 'horizontal'
        colorbar = fig.colorbar(
                contour, cax, orientation=orientation, fraction=0.1, pad=0,
                format='%6.1e', ticks=vlim[3], extend=extend
                )
        colorbar.ax.tick_params(**tick_params)
    # Texts
    for text in texts:
        if text:
            ax.text(**text)
    return ax


def generic(
        fig,
        idata, xlim, ylim,
        projection=None,
        label='',
        texts=[], yticks=[],
        loc_plot=[0.15, 0.1, 0.84, 0.88],
        tick_fontsize=8,
        xtick_params=[True, False, False],
        ):
    r"""Create generic plots

    Parameters
    ----------
    idata : dict
        {key: plot_type, [*args], {**kwargs}}
        Plot data. args and kwargs are directly fed into the plot function
        depending on plot_type.
    xlim, ylim : list of float or datetime
        Y-axis limit.
    projection : {None, str}, optional
        Projection of the axes object.
    label : str, optional
        Name of the Axes object.
    texts : list of dict
        Each dict is kwargs for fig.text object, i.e. fig.text(**kwargs).
    yticks : list of floats
        Y-axis tick locations. If empty, matplotlib automation is used.
    loc_plot : list of floats
        Location of ploat area.
    tick_fontsize : float
        Tick fontsize.
    xtick_params : list
        First element determines whether data are time series
        [True, formatter, locator, lims]
        [False, ticks, labels]
        Subsequent elements set as False will trigger default settings.

    Returns
    -------
    fig, ax : object
        Figure and axis objects.

    """
    ax = fig.add_axes(loc_plot, projection=projection, label=label)
    if np.array(yticks).size > 0:
        ax.set_yticks(yticks)
    ax.set_xlim(*xlim)
    ax.set_ylim(*ylim)
    ax.tick_params(
            axis='both', which='major', direction='in',
            labelsize=tick_fontsize,
            labelleft=True, labelbottom=True, labelright=False, labeltop=False
            )
    for k, v in idata.items():
        if v[0] == 'line':
            ax.plot(*v[1], **v[2])
        elif v[0] == 'err':
            ax.errorbar(*v[1], **v[2])
        elif v[0] == 'scatter':
            ax.scatter(*v[1], **v[2])
        elif v[0] == 'stack':
            ax.stackplot(*v[1], **v[2])
        elif v[0] == 'bar':
            ax.bar(*v[1], **v[2])
        elif v[0] == 'fill':
            ax.fill_between(*v[1], **v[2])
        elif v[0] == 'hline':
            ax.axhline(*v[1], **v[2])
        elif v[0] == 'vline':
            ax.axvline(*v[1], **v[2])
    if xtick_params[0]:
        ax.xaxis_date()
        if xtick_params[1] is not False:
            ax.xaxis.set_major_formatter(xtick_params[1])
        if xtick_params[2] is not False:
            ax.xaxis.set_major_locator(xtick_params[2])
    else:
        if xtick_params[1] is not False:
            ax.set_xticks(xtick_params[1])
        if xtick_params[2] is not False:
            ax.set_xticklabels(xtick_params[2])
    for text in texts:
        if text:
            fig.text(**text)
    return ax


def generic2(
        fig,
        idata,
        texts=[],
        legend_params=[[], [], {}],
        ):
    r"""Create generic plots

    Parameters
    ----------
    idata : dict
        {
            key: [
                plot_type, [*args], {**kwargs},
                {
                    loc_plot, ylim, yticks, tick_params, label, projection,
                    xlim, xtick_params
                    },
                {bound methods}
                ]
            }
        Plot data. args and kwargs are directly fed into the plot function
        depending on plot_type.
    texts : list of dict
        Each dict is kwargs for fig.text object, i.e. fig.text(**kwargs).
    legend_params : list
        [list of str, list of str, kwargs]
        list of keys.
        kwargs for ax.legend.

    Returns
    -------
    fig, ax : object
        Figure and axis objects.

    """
    axs = {}
    legObj = {}
    for k, v in idata.items():
        ax = fig.add_axes(
            v[3]['loc_plot'],
            projection=v[3]['projection'],
            label=v[3]['label']
            )
        if 'yticks' in v[3] and np.array(v[3]['yticks']).size > 0:
            ax.set_yticks(v[3]['yticks'])
        if 'xlim' in v[3]:
            ax.set_xlim(v[3]['xlim'])
        if 'ylim' in v[3]:
            ax.set_ylim(v[3]['ylim'])
        if 'tick_params' in v[3]:
            ax.tick_params(**v[3]['tick_params'])
        if v[0] == 'line':
            p = ax.plot(*v[1], **v[2])
        elif v[0] == 'err':
            p = ax.errorbar(*v[1], **v[2])
        elif v[0] == 'scatter':
            p = ax.scatter(*v[1], **v[2])
        elif v[0] == 'stack':
            p = ax.stackplot(*v[1], **v[2])
        elif v[0] == 'bar':
            p = ax.bar(*v[1], **v[2])
        elif v[0] == 'fill':
            p = ax.fill_between(*v[1], **v[2])
        elif v[0] == 'hline':
            p = ax.axhline(*v[1], **v[2])
        elif v[0] == 'vline':
            p = ax.axvline(*v[1], **v[2])
        elif v[0] == 'pcolormesh':
            p = ax.pcolormesh(*v[1], **v[2])
        if 'xtick_params' in v[3]:
            if v[3]['xtick_params'][0]:
                ax.xaxis_date()
                if v[3]['xtick_params'][1] is not False:
                    ax.xaxis.set_major_formatter(v[3]['xtick_params'][1])
                if v[3]['xtick_params'][2] is not False:
                    ax.xaxis.set_major_locator(v[3]['xtick_params'][2])
            else:
                if v[3]['xtick_params'][1] is not False:
                    ax.set_xticks(v[3]['xtick_params'][1])
                if v[3]['xtick_params'][2] is not False:
                    ax.set_xticklabels(v[3]['xtick_params'][2])
        axs[k] = ax
        if k in legend_params[0]:
            legObj[k] = p[0]
        if 'patch_alpha' in v[3]:
            ax.patch.set_alpha(v[3]['patch_alpha'])
        if len(v) == 5:
            for k1, (v1, v2) in v[4].items():
                getattr(ax, k1)(*v1, **v2)
    if legend_params[0]:
        leg = fig.legend(
            handles=[legObj[k] for k in legend_params[0]],
            labels=legend_params[1],
            **legend_params[2]
            )
    for text in texts:
        if text:
            fig.text(**text)
    return axs


def generic3(
        fig,
        axs,
        pobjs,
        new_axs,
        new_pobjs,
        texts=[],
        legend_params=[[], [], {}],
        ):
    r"""Create generic plots

    Parameters
    ----------
    fig : Figure
        Figure object to draw upon.
    axs : dict
        Dict containing axes objects
    pobjs : dict
        Dict containing plots/texts etc.
    new_axs : dict
        {
            ax_name: [{kwargs}, {bound methods}, {extra}]
            }
        kwargs include {rect, projection, sharex, sharey, label}
        bound methods include {
            tick_params, xaxis_date,
            set_xlim, set_ylim, set_xticks, set_yticks,
            ax.set_xticklabels, ax.set_yticklabels,
            xaxis.set_major_formatter, xaxis.set_major_locator,
            }
        extra include {
            patch_alpha
            }
        New axes added into axs.
    new_pobjs : dict
        {
            key: [
                axs_key, plot_type, [*args], {**kwargs},
                ]
            }
        New plots added into pojbs. args and kwargs are directly fed into the
        plot function depending on plot_type.
    texts : list of dict
        Each dict is kwargs for fig.text object, i.e. fig.text(**kwargs).
    legend_params : list
        [list of str, list of str, kwargs]
        list of keys.
        kwargs for ax.legend.

    """
    for k, v in new_axs.items():
        axs[k] = fig.add_axes(**v[0])
        for k1, (v1, v2) in v[1].items():
            rgetattr(axs[k], k1)(*v1, **v2)
        if 'patch_alpha' in v[2]:
            axs[k].patch.set_alpha(v[2]['patch_alpha'])
    legObj = {}
    for k, v in new_pobjs.items():
        pobjs[k] = rgetattr(axs[v[0]], v[1])(*v[2], **v[3])
        if k in legend_params[0]:
            legObj[k] = pobsj[k]
    if legend_params[0]:
        leg = fig.legend(
            handles=[legObj[k] for k in legend_params[0]],
            labels=legend_params[1],
            **legend_params[2]
            )
    for text in texts:
        if text:
            fig.text(**text)


def compare(
        fig,
        idata, lim,
        texts=[], ticks=[],
        err=0.,
        loc_plot=[0.2, 0.2, 0.75, 0.75],
        fontsize=8,
        ):
    r"""Comparison plot (e.g. observation-model mismatch)

    Parameters
    ----------
    idata : dict
        {key: plot_type, [*args], {**kwargs}}
        Plot data. args and kwargs are directly fed into the plot function
        depending on plot_type.
    lim : list of floats
        X- and Y-axis limit.
    err : float or ndarray
        Error range.
    texts : list of dict
        Each dict is kwargs for fig.text object, i.e. fig.text(**kwargs).
    ticks : list of floats
        Y-axis tick locations. If empty, matplotlib automation is used.
    loc_plot : list of floats
        Location of ploat area.
    fontsize : float
        Tick fontsize.

    Returns
    -------
    fig, ax : object
        Figure and axis objects.

    """
    ax = fig.add_axes(loc_plot)
    ax.set_xlim(*lim)
    ax.set_ylim(*lim)
    ax.tick_params(
            axis='both', which='major', direction='in', labelsize=fontsize,
            labelleft=True, labelbottom=True, labelright=False, labeltop=False
            )
    if np.array(ticks).size > 0:
        ax.set_xticks(ticks)
        ax.set_yticks(ticks)
    ax.plot(lim, lim, c='#FF0000', lw=1, ls='-', marker=None)
    if np.array(err).any():
        ax.fill_between(
                np.array(lim),
                np.array(lim)-err,
                np.array(lim)+err,
                color='#FF0000',
                alpha=0.5, lw=0, ls='-'
                )
    for k, i in idata.items():
        if not i:
            continue
        ax.scatter(*i[0], **i[1])
    for text in texts:
        if text:
            fig.text(**text)
    return ax

