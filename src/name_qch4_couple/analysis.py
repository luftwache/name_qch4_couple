import numpy as np
import pandas as pd


def det_seasons(index):
    r"""

    Returns
    -------
    season : pd.Int64index
        0 is winter.
    """
    season = index.month % 12 // 3
    return season


def group_seasons(idata, t_s, t_e, season):
    r"""

    Parameters
    ----------
    idata : pd.Series or pd.DataFrame
        Input data with time as index.
    t_s, t_e : str
        Time string
    season : int
        0 is winter.

    Returns
    -------
    odata : pd.Series or pd.DataFrame
        Filtered data.

    """
    idata_t = idata.between_time(t_s, t_e)
    seasons = det_seasons(idata_t.index)
    odata = idata.loc[seasons == season]
    return odata

