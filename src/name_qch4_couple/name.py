import numpy as np


def txt2array(idata, dim, hours, fill=np.nan):
    r"""

    Convert [lon, lat, value] data to array for given hours

    Parameters
    ----------
    idata : ndarray
        Data from fields file.
    dim : list-like
        [nlat, nlon]
    hours : list-like
        Hours of the daily data to read.
        First hour is the 0th hour.
    fill : float
        Value for empty data points. Defaults to np.nan.

    Returns
    -------
    odata : ndarray
        Counts of number of particles leaving each domain borders

    """
    odata = np.full((len(hours), dim[0], dim[1]), fill)
    n = int(24 / (idata.shape[1] - 4))
    for i, hour in enumerate(hours):
        valid = idata[idata[:, 4+(hour//n)] != 0]
        x = (valid[:, 0] - 1.5).astype(int)
        y = (valid[:, 1] - 1.5).astype(int)
        odata[i, y, x] = valid[:, 4+(hour//n)]
    return odata


def validate(initial, name, prior, area, m_total):
    r"""
    """
    obs = initial + np.nansum(prior * area * name / m_total)
    return obs


def calc_cell_area(lat1, lat2, hres):
    r"""

    A = r^2 * lon * d(sin(lat))

    Parameters
    ----------
    lat1, lat2 : ndarray
        Latitudes where lat1/lat2 is the lower/upper bound.
    hres : float
        Horizontal resolution in decimal degrees.

    Returns
    -------
    odata : ndarray
        Cell area.

    """
    odata = (6371.0e3**2.0 * np.deg2rad(hres) *
             np.abs(np.sin(np.deg2rad(lat2)) - np.sin(np.deg2rad(lat1)))
             )
    return odata


def cat_end_loc(idata, grid, hours, h1=3.e3, h2=8.e3):
    r"""

    Categorise particle end locations into 17 domain borders and the domain
    origin is at the centre of the domain
    17 domain borders are 8 octants starting from cardinal north (e.g. 0 =
    [0, 45] degrees) in two vertical levels (<6, 6-9 km) and the top (< 9km).

    Parameters
    ----------
    idata : ndarray
        Data from particles file.
        Header should be [lon, lat, ht, lifetime, hr].
    grid : list
        [lons, lats] where lons, lats are list-likes.
    hours : list-like of int
        Times of the day to read in
    h1, h2 : float
        Heights dividing the vertical layers. Unit is m.

    Returns
    -------
    odata : ndarray
        Counts of number of particles leaving each domain borders
    end_loc : ndarray
        1d array with the length of idata.
        Domain borders particles pass through. 0 is assigned for those left in
        the domain, and -1 for those left the domain through the tropopause.

    """
    lons, lats = grid
    lon0, lon2 = lons[0], lons[-1]
    lon1 = np.mean([lon0, lon2])
    lat0, lat2 = lats[0], lats[-1]
    lat1 = np.mean([lat0, lat2])
    # Find octant
    x = idata[:, 0]-lon1
    y = idata[:, 1]-lat1
    if lon2-lon0 > lat2-lat0:
        y *= (lon2-lon0)/(lat2-lat0)
    elif lat2-lat0 > lon2-lon0:
        x *= (lat2-lat0)/(lon2-lon0)
    octant = np.arctan2(x, y) // (np.pi / 4)
    octant[octant < 0] += 8
    # Check if out of domain
    escape_h = np.zeros(octant.shape)
    escape_h[
            (idata[:, 0] < lon0) | (lon2 < idata[:, 0]) |
            (idata[:, 1] < lat0) | (lat2 < idata[:, 1])
            ] = 1.
    escape_v = np.zeros(octant.shape)
    escape_v[idata[:, 2] < h1] = 0.  # surface
    escape_v[(h1 <= idata[:, 2]) & (idata[:, 2] < h2)] = 1.  # u. tropo
    escape_v[h2 < idata[:, 2]] = 2.  # strato
    # Final location
    end_loc = np.full(octant.shape, 0.)
    esc_h0 = np.where((escape_h == 1.) & (escape_v == 0.))
    esc_h1 = np.where((escape_h == 1.) & (escape_v == 1.))
    end_loc[esc_h0] = octant[esc_h0] + 1.
    end_loc[esc_h1] = octant[esc_h1] + 9.
    end_loc[(escape_v == 2.)] = 17.
    end_loc[(escape_h == 0.) & (escape_v != 2)] == 0.
    # Counts
    n = int(24 / np.amax(idata[:, 4]))
    odata = np.array([
        [
            end_loc[(idata[:, 4] == (t//n)+1) & (end_loc == i)].size
            for i in range(18)
            ]
        for t in hours
        ])
    return odata, end_loc

