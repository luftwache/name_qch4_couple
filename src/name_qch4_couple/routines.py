import numpy as np


def forward_weight(
        M, R_std, p, T,
        p_chi0, p_d0i1, p_d0i2, p_QA, p_dQi1, p_dQi2, p_dosage,
        chi_scale=1., select=np.s_[:], dosage_type=0
        ):
    r"""

    dosage_type : int
        0: g m-3 s
        1: ppms

    """
    i_R0i1 = (p_d0i1 * 1.e-3 + 1) * R_std[1]
    i_R0i2 = (p_d0i2 * 1.e-3 + 1) * R_std[2]
    i_R0sum = 1 + i_R0i1 + i_R0i2
    i_A0i0 = 1 / i_R0sum
    i_A0i1 = i_R0i1 / i_R0sum
    i_A0i2 = i_R0i2 / i_R0sum
    i_RQi1 = (p_dQi1 * 1.e-3 + 1) * R_std[1]
    i_RQi2 = (p_dQi2 * 1.e-3 + 1) * R_std[2]
    i_RQsum_M = 1*M[0] + i_RQi1*M[1] + i_RQi2*M[2]
    i_AQi0_M = 1 / i_RQsum_M  # mol(i) kg(CH4)-1
    i_AQi1_M = i_RQi1 / i_RQsum_M
    i_AQi2_M = i_RQi2 / i_RQsum_M

    factor = 8.3145 * T / p if dosage_type == 0 else 1.

    dimt = p_dosage.shape[0]
    dims = p_QA.shape[0]
    diluted_rho = (p_QA / 3600 * p_dosage)  # (t, s, y, x)
    diluted_chii0 = (
            diluted_rho * i_AQi0_M * factor * chi_scale
            )[..., select].reshape(dimt, -1).sum((-1))
    diluted_chii1 = (
            diluted_rho * i_AQi1_M * factor * chi_scale
            )[..., select].reshape(dimt, -1).sum((-1))
    diluted_chii2 = (
            diluted_rho * i_AQi2_M * factor * chi_scale
            )[..., select].reshape(dimt, -1).sum((-1))
 
    y_chii0 = p_chi0 * i_A0i0 + diluted_chii0
    y_chii1 = p_chi0 * i_A0i1 + diluted_chii1
    y_chii2 = p_chi0 * i_A0i2 + diluted_chii2
    return y_chii0, y_chii1, y_chii2

