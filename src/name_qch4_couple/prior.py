#import gdal
import h5py
#import iris
import linecache
import netCDF4
import numpy as np
import pandas as pd
import scipy.io
import os
import re
import sys


import name_qch4_couple.io


def create_nc(dim, dim_order, var, ofile):
    r"""Create netCDF4 file

    Parameters
    ----------
    dim : dict
        {key: [values, unit, long_name]} where keys include (time, lat, lon).
    dim_order : list of str
        Order of dimensions (keys for dim)
    var : dict
        Values, with same structure as dim.
    ofile : path_like
        Output file.

    """
    with netCDF4.Dataset(ofile, 'w', format='NETCDF4') as ncfile:
        for k, v in dim.items():
            if k in ['time']:
                fmt = np.float64
            else:
                fmt = np.float32
            idata_dim = ncfile.createDimension(k, len(v[0]))
            idata = ncfile.createVariable(k, fmt, (k,))
            idata[:] = v[0]
            idata.units = v[1]
            idata.long_name = v[2]
            if k == 'time':
                idata.calendar = "gregorian"
        for k, v in var.items():
            idata = ncfile.createVariable(k, np.float64, dim_order)
            idata[:] = v[0]
            idata.units = v[1]
            idata.long_name = v[2]


def calc_cell_area(lat1, lat2, hres):
    r"""

    A = r^2 * lon * d(sin(lat))

    Parameters
    ----------
    lat1, lat2 : ndarray
        Latitudes where lat1/lat2 is the lower/upper bound.
    hres : float
        Horizontal resolution in decimal degrees.

    Returns
    -------
    odata : ndarray
        Cell area

    """
    odata = (6371.0e3**2.0 * np.deg2rad(hres) *
             np.abs(np.sin(np.deg2rad(lat2)) - np.sin(np.deg2rad(lat1)))
             )
    return odata


def vector2array(idata, lat, lon, gridspec, sd=1):
    r"""

    gridspec : list
        [y0, yn, ysize, x0, xn, xsize]
    """
    y0, yn, ysize, x0, xn, xsize = gridspec
    ygrid = np.linspace(y0, yn, ysize).round(sd)
    xgrid = np.linspace(x0, xn, xsize).round(sd)
    odata = np.zeros((ygrid.size, xgrid.size))
    valid = np.isin(lat, ygrid) & np.isin(lon, xgrid)
    ydiff = np.diff(ygrid[[0,1]])
    xdiff = np.diff(xgrid[[0,1]])
    yidx = ((lat[valid] - y0) / ydiff).round(0).astype(int)
    xidx = ((lon[valid] - x0) / xdiff).round(0).astype(int)
    odata[yidx, xidx] = idata[valid]
    return odata, ygrid, xgrid


def edgar_csv2nc(ifile, time, ofile):
    r"""Wrapper to convert an EDGAR csv file to netCDF.
    """
    idata = pd.read_csv(ifile, sep=';', header=2)
    old, old_ygrid, old_xgrid = (
        vector2array(
            idata.iloc[:, 2],
            idata.iloc[:, 0],
            idata.iloc[:, 1],
            [-89.9, 89.9, 1799, -180.0, 179.9, 3600]
            )
        )
    dim = {'time': [time, 'days since 1970-01-01 00:00:00', 'time'],
           'lat': [old_ygrid, 'degrees_north', 'latitude'],
           'lon': [old_xgrid, 'degrees_east', 'longitude']
           }
    var = {'CH4_emissions': [old[np.newaxis], 'kg m-2 s-1', 'CH4_emissions']}
    dim_order = ['time', 'lat', 'lon']
    create_nc(dim, dim_order, var, ofile)


def edgar_multi_csv2nc(dpath, regex, ofile):
    r"""Wrapper to aggregate EDGAR csv files to netCDF.
    """
    #regex = r'.*{}_([0-9]{{4}})_IPCC_({}).*txt'.format(cat1, cat2)
    files = [f for f in os.listdir(dpath) if re.fullmatch(regex, f)]
    files.sort()
    time_regex = r'.*([0-9]{{4}})_IPCC_.*'
    time = np.unique([int(re.match(time_regex, f)[1]) for f in files]) - 1970
    for n, f in enumerate(files):
        idata = pd.read_csv(os.path.join(dpath, f), sep=';', header=2)
        old, old_ygrid, old_xgrid = (
            vector2array(
                idata.iloc[:, 2],
                idata.iloc[:, 0],
                idata.iloc[:, 1],
                [-89.9, 89.9, 1799, -180.0, 179.9, 3600]
                )
            )
        if n == 0:
            odata = np.zeros(time.size, *old.shape)
        t = int(re.match(time_regex, f)[1]) - 1970
        odata[t] += old
    dim = {'time': [time, 'days since 1970-01-01 00:00:00', 'time'],
           'lat': [old_ygrid, 'degrees_north', 'latitude'],
           'lon': [old_xgrid, 'degrees_east', 'longitude']
           }
    var = {'CH4_emissions': [odata, 'kg m-2 s-1', 'CH4_emissions']}
    if ofile:
        dim_order = ['time', 'lat', 'lon']
        create_nc(dim, dim_order, var, ofile)
    return dim, var


def edgar_aggregate_nc(dates, ifiles, ofile=False):
    r"""Produce single EDGAR nc file from yearly/monthly files.

    Parameters 
    ----------
    dates : list-like
        Date strings or datetime objects.
    ifiles : list-like of path-likes
        Input files for each of the date in dates.
    ofile : False or path-like, optional
        Output file.

    Returns
    -------
    dim : dict
        Output dimensions.
    var : dict
        Output data.

    """
    if len(dates) != len(ifiles):
        raise Exception(
            f"Numbers of dates {len(dates)}"
            f"and ifiles ({len(ifiles)}) do not match"
            )
    flag = 0  # for first file
    t_ref = pd.to_datetime('1970-01-01 00:00:00')
    t = (pd.to_datetime(dates) - t_ref).astype('timedelta64[D]').to_numpy()
    for i, ifile in enumerate(ifiles):
        with netCDF4.Dataset(ifile, 'r') as idata:
            temp = idata['emi_ch4'][:]
            if flag == 0:
                ygrid = idata['lat'][:]
                xgrid = idata['lon'][:]
        if flag == 0:
            odata = np.zeros((len(dates), *temp.shape))
        odata[i] = temp
        flag = 1.
    dim = {
        'time': [
            t,
            {'size': None},
            {'datatype': np.float64, 'zlib': True},
            {
                'units': 'days since 1970-01-01 00:00:00',
                'long_name': 'time',
                'calendar': 'gregorian',
                }
            ],
        'lat': [
            ygrid,
            {'size': ygrid.size},
            {'datatype': np.float32, 'zlib': True},
            {'units': 'degrees_north', 'long_name': 'latitude'}
            ],
        'lon': [
            xgrid,
            {'size': xgrid.size},
            {'datatype': np.float32, 'zlib': True},
            {'units': 'degrees_east', 'long_name': 'longitude'}
            ]
        }
    var = {
        'CH4_emissions': [
            odata,
            {
                'dimensions': ('time', 'lat', 'lon'), 'datatype': np.float64,
                'zlib': True
                },
            {'units': 'kg m-2 s-1', 'long_name': 'CH4_emissions'}
            ]
        }
    if ofile:
        name_qch4_couple.io.w_nc(
            dim=dim,
            var=var,
            ofile=ofile
            )
    return dim, var


def wetcharts_nc2nc(ifile, ofile):
    r"""Convert wetcharts to .nc format recognised by CDO

    units = mg m^{-2} day^{-1} -> kg m^{-2} s^{-1}

    Parameters
    ----------
    ifile : path-like
        Input file.
    ofile : path-like
        Output file.

    Returns
    -------
    dim : dict
        Output dimensions fed into `create_nc`.
    var : dict
        Output data that gets fed into `create_nc`.

    """
    with netCDF4.Dataset(ifile, 'r') as idata:
        y = idata['lat']
        x = idata['lon']
        v = idata['wetland_CH4_emissions'][:].mean(0) * 1.e-6 / 86400
        # Convert months since ref to days since ref
        t = idata['time']
        unit, t0_str = re.match('([a-z]+) since ([0-9-: ]+)', t.units).groups()
        t0 = np.datetime64(t0_str).astype('M8[M]')
        t_ref = '1970-01-01 00:00:00'
        dt = (t[:] // 1).astype('m8[M]')
        time = (
                (t0 + dt).astype('M8[D]')
                - np.array([t_ref], dtype='M8[D]')
                ).astype('m8[D]')
        dim = {
            'time': [time.astype(int), f'days since {t_ref}', 'time'],
            'lat': [y[:], y.units, y.long_name], 
            'lon': [x[:], x.units, x.long_name]
            }
        var = {
            'CH4_emissions': [v, 'kg m-2 s-1', 'CH4_emissions']}
    if ofile:
        dim_order = ['time', 'lat', 'lon']
        create_nc(dim, dim_order, var, ofile)
    return dim, var


def gfed_hdf2nc(dpath, ofile):
    r"""
    units = kg m^{-2} month^{-1} -> kg m^{-2} s^{-1}

    Parameters
    ----------
    dpath : path-like
        Directory where data files are located.
    ofile : path-like
        Output file.

    Returns
    -------
    dim : dict
        Output dimensions fed into `create_nc`.
    var : dict
        Output data that gets fed into `create_nc`.

    """
    regex = r'GFED4\.1s_([0-9]{4}).*\.hdf5'
    ratio = {'SAVA': 1.94e-3,
             'BORF': 5.96e-3,
             'TEMF': 3.36e-3,
             'DEFO': 5.07e-3,
             'PEAT': 20.8e-3,
             'AGRI': 5.82e-3}
    files = [f for f in os.listdir(dpath) if re.fullmatch(regex, f)]
    files.sort()
    # shape
    years = np.array([int(re.fullmatch(regex, f)[1]) for f in files])
    t0 = np.array([f'{years[0]}'], dtype='M8[M]')
    t_ref = '1970-01-01 00:00:00'
    dt = np.arange(0, (years[-1]-years[0]+1)*12, dtype='m8[M]')
    time = (
            (t0 + dt).astype('M8[D]')
            - np.array([t_ref], dtype='M8[D]')
            ).astype('m8[D]')

    with h5py.File(os.path.join(dpath, files[0]), 'r') as idata:
        gridsize = idata['lat'].shape
        y = idata['lat'][:, 0]
        x = idata['lon'][0]
    odata = np.zeros((time.size, *gridsize))
    i = 0
    for f in files:
        print(f)
        year = re.fullmatch(regex, f)[1]
        with h5py.File(os.path.join(dpath, f), 'r') as idata:
            for month in range(1, 13):
                dinm = pd.to_datetime(f'{year}-{month}').daysinmonth
                monthly = idata['emissions/{:02d}'.format(month)]
                total = monthly['DM'][:]
                for k, v in ratio.items():
                    partition = 'partitioning/DM_{}'.format(k)
                    src_ratio = monthly[partition][:]
                    odata[i] += total * src_ratio * v / dinm / 86400
                i += 1
    dim = {'time': [time.astype(int), f'days since {t_ref}', 'time'],
           'lat': [y, 'degrees_north', 'latitude'],
           'lon': [x, 'degrees_east', 'longitude']
           }
    var = {'CH4_emissions': [odata, 'kg m-2 s-1', 'CH4_emissions']}
    if ofile:
        dim_order = ['time', 'lat', 'lon']
        create_nc(dim, dim_order, var, ofile)
    return dim, var


def naei_aggregate_nc(dpath, fpattern, years, ofile):
    r"""Wrapper to aggregate NAEI files in netCDF.

    Parameters
    ----------
    dpath : path-like
        Directory where data files are located.
    fpattern : str
        Regex of the file names. It must include string "year" that will be
        replaced with the year within years parameter.
    years : list-like
        2 item list-like indicating temporal range
    ofile : path-like
        Output file

    Returns
    -------
    dim : dict
        Output dimensions fed into `create_nc`.
    var : dict
        Output data that gets fed into `create_nc`.

    """
    n_years = years[1] - years[0] + 1
    flag = 0
    t = np.zeros(n_years)
    t_ref = '1970-01-01 00:00:00' 
    for i, year in enumerate(np.linspace(*years, n_years, dtype=int)):
        ifile = os.path.join(
                dpath,
                re.sub('[^\w.]', '', re.sub('year', f'{year}', fpattern))
                )
        if not os.path.exists(ifile):
            continue
        if flag == 0:
            with netCDF4.Dataset(ifile, 'r') as idata:
                temp = idata['Band1'][:]
                ygrid = idata['lat'][:]
                xgrid = idata['lon'][:]
                odata = np.zeros((n_years, *temp.shape))
        flag = 1
    if flag == 0:
        raise Exception("No files detected")
    for i, year in enumerate(np.linspace(*years, n_years, dtype=int)):
        t[i] = (
                np.array(f'{year}', dtype='M8[D]')
                - np.array(t_ref, dtype='M8[D]')
                ).astype(int)
        ifile = os.path.join(
                dpath,
                re.sub('[^\w.]', '', re.sub('year', f'{year}', fpattern))
                )
        if not os.path.exists(ifile):
            continue
        with netCDF4.Dataset(ifile, 'r') as idata:
            temp = idata['Band1'][:]
        odata[i] = temp * 1.e-3 / 86400 / 365.25
        flag = 1.
    dim = {'time': [t, 'days since 1970-01-01 00:00:00', 'time'],
           'lat': [ygrid, 'degrees_north', 'latitude'],
           'lon': [xgrid, 'degrees_east', 'longitude']
           }
    var = {'CH4_emissions': [odata, 'kg m-2 s-1', 'CH4_emissions']}
    if ofile:
        dim_order = ['time', 'lat', 'lon']
        create_nc(dim, dim_order, var, ofile)
    return dim, var


def read_prior(ifile, key, time):
    r"""
    """
    with netCDF4.Dataset(ifile, 'r') as idata:
        odata = idata[key][time]
    return odata


def calc_area_weight_latlon(src_y, src_x, src_res, dst_y, dst_x, dst_res):
    r"""exp

    src_y, src_x : 
        Should be sorted
    src_res, dst_res :
        Resolution
        [y, x]

    """
    odata = ()
    dst_y_upper = dst_y + dst_res[0]
    dst_y_lower = dst_y - dst_res[0]
    src_y_upper = src_y + src_res[0]
    src_y_lower = src_y - src_res[0]
    for nj, j in enumerate(dst_y):
        idx_y = np.where((src_y_upper > dst_y_lower[nj]) &
                         (src_y_lower < dst_y_upper[nj]))

