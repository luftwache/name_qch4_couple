import cartopy.crs as ccrs
import cartopy.feature as cfeature
import cartopy.io as cio
import gzip
import h5py
import matplotlib.colors
import matplotlib.pyplot as plt
import netCDF4
import numpy as np
import os
import pandas as pd
import re
import sys
from matplotlib.ticker import FormatStrFormatter



def io_r_prior(ifile, key, time):
    r"""
    """
    with netCDF4.Dataset(ifile, 'r') as idata:
        odata = idata[key][time]
    return odata


def io_r_name(fpath):
    '''Read csv file within .gz

    Parameters
    ----------
    fpath : file-like object, string, or pathlib.Path
        Path of the data file.

    Returns
    -------
    f : ndarray or DataFrame
        Output data.

    '''
    with gzip.open(fpath, 'r') as f_ext:
        f = pd.read_csv(f_ext, sep=',', header=23, skipinitialspace=True)
    return np.array(f.iloc[:, :-1])


def io_r_name_particles(fpath):
    '''Read csv file within .gz

    Parameters
    ----------
    fpath : file-like object, string, or pathlib.Path
        Path of the data file.

    Returns
    -------
    f : ndarray or DataFrame
        Output data.

    '''
    with gzip.open(fpath, 'r') as f_ext:
        f = pd.read_csv(f_ext, sep=',', header=1, skipinitialspace=True)
    return np.array(f.iloc[:, :-1])


def name_vector2array(idata, dim, hours):
    r"""
    """
    odata = np.full((24, dim[0], dim[1]), np.nan)
    for hour in hours:
        valid = idata[idata[:, 4+hour] != 0]
        x = (valid[:, 0] - 1.5).astype(int)
        y = (valid[:, 1] - 1.5).astype(int)
        odata[hour, y, x] = valid[:, 4+hour]
    return odata


def name_validate(initial, name, prior, area, m_total):
    r"""
    """
    obs = initial + np.nansum(prior * area * name / m_total)
    return obs


def calc_cell_area(lat1, lat2, hres):
    r"""

    A = r^2 * lon * d(sin(lat))

    Parameters
    ----------
    lat1, lat2 : ndarray
        Latitudes where lat1/lat2 is the lower/upper bound.
    hres : float
        Horizontal resolution in decimal degrees.

    Returns
    -------
    odata : ndarray
        Cell area.

    """
    odata = (6371.0e3**2.0 * np.deg2rad(hres) *
             np.abs(np.sin(np.deg2rad(lat2)) - np.sin(np.deg2rad(lat1)))
             )
    return odata


def io_r_instaar(fpath, dtype):
    """
    """
    with open(fpath) as fline:
        line = next(fline)
        header_no = int(line.split(':')[1].strip())
        for i, line in enumerate(fline):
            if i == header_no - 2:
                col_name = line
                break
    col_name = re.sub('# data_fields: ', '', col_name)
    col_name = [name.strip() for name in col_name.split(' ')]
    idata = pd.read_csv(
                fpath,
                sep=r'\s+',
                skiprows=header_no,
                header=None,
                names=col_name)
    if dtype == 'month':
        idata['date'] = pd.to_datetime((idata['year']*100 +
                                        idata['month']).astype('i8'),
                                       format='%Y%m')
        idata.set_index('date', inplace=True)
        date = pd.date_range(idata.index[0], idata.index[-1], freq='MS')
        odata = idata['value'].reindex(date, fill_value=np.nan).interpolate()
    else:
        idata = idata[idata['analysis_flag'].str.match('\.(.){2}')]
        idata['date'] = pd.to_datetime(
                (idata['sample_year']*1e10
                    + idata['sample_month'].astype('i8')*1e8
                    + idata['sample_day'].astype('i8')*1e6
                    + idata['sample_hour'].astype('i8')*1e4
                    + idata['sample_minute'].astype('i8')*1e2
                    + idata['sample_seconds'].astype('i8')
                    ).astype('i8').astype(str),
                format='%Y%m%d%H%M%S'
                )
        idata.set_index('date', inplace=True)
        odata = idata['analysis_value'].groupby('date').mean()
    return odata


def io_r_agage(f):
    r"""
    """
    odata = pd.read_csv(
                f,
                usecols=lambda x: x.lower() in ['time', 'ch4_c'],
                index_col=['time'],
                skipinitialspace=True,
                parse_dates=['time']
                ).dropna()
    return odata


def io_r_unfccc(fpath):
    r"""
    """
    # Get header and nrows
    with open(fpath) as f:
        head = False
        foot = False
        header_no = 0
        nrows = 0
        for i, line in enumerate(f):
            if line[:6] == ' ,Year':
                years = [i.strip() for i in line.split(',')[2:]]
                years[-1] = re.sub(r'.*\(([0-9]+)\).*', r'\g<1>', years[-1])
            elif line.split(',')[0].strip() == 'Party':
                header_no = i+1
                head = True
                col_name = line.split('\\')[0].strip().split(',') + years
            elif re.match(r'^\W+$', line) and (head) and (not foot):
                nrows = i - header_no
                foot = True
    # Read
    odata = pd.read_csv(
                fpath,
                skiprows=header_no,
                header=None,
                names=col_name,
                nrows=nrows
                )
    return odata


def plot_name_contour(idata, grid, extent, ofile):
    r"""Contour plot over map

    Parameters
    ----------
    idata : ndarray
        2d lat x lon, float
        Geographical data from gridded NAME output.
        Can accept ungridded NAME data.
    grid : list
        List of longitudes and latitudes.
        [lon(1d array), lat (1d array)]
    extent : str {uk, eu, None}
        Constrains plot boundary.
    hour : int
        Hour of day.
    """
    # Set LonLat
    lons, lats = grid
    lon, lat = np.meshgrid(lons, lats)
    nlon, nlat = lon.size, lat.size
    if extent == 'uk':
        size = (4, 4)
        bbox = [-1.8, 2.2, 49.0, 53.0]
        shapefile = 'shape_files/CNTR_BN_10M_2016_4326.shp'
    elif extent == 'eu':
        size = (4, 4)
        bbox = [-13.0, 40.0, 33.0, 72.0]
        shapefile = 'shape_files/CNTR_BN_10M_2016_4326.shp'
    else:
        size = (6, 3)
        bbox = [-100.0, 40.0, 10.0, 80.0]
        shapefile = 'shape_files/CNTR_BN_60M_2016_4326.shp'
    fig = plt.figure(figsize=(size), dpi=300)
    ax = fig.add_axes([0.05, 0.05, 0.8, 0.9], projection=ccrs.PlateCarree())
    ax.set_extent(bbox)
    # colors = ("#F9FFAF", "#CFEFA8", "#A6DEA2", "#7FCD9F", "#58BA9D",
    #           "#33A69A", "#109195", "#057C8E", "#1C6685", "#2E4F79")
    levels = np.power(10, np.linspace(-9.5, -3.5, 13))
    contour = ax.contourf(lon, lat, idata,
                          # colors=colors,
                          cmap='viridis_r',
                          levels=levels,
                          vmin=1e-9, vmax=1e-4,
                          norm=matplotlib.colors.LogNorm())
    ax.add_feature(
            cfeature.ShapelyFeature(
                    cio.shapereader.Reader(
                            shapefile).geometries(),
                    ccrs.PlateCarree(),
                    edgecolor='black',
                    linewidth=0.5,
                    facecolor='none'))
    if extent == 'uk':
        shapefile = 'shape_files/london.shp'
        ax.add_feature(
            cfeature.ShapelyFeature(
                    cio.shapereader.Reader(
                            shapefile).geometries(),
                    ccrs.PlateCarree(),
                    edgecolor='black',
                    linewidth=0.5,
                    facecolor='none'))
    ax.gridlines()
    cax = fig.add_axes([0.86, 0.05, 0.02, 0.9])

    colorbar = fig.colorbar(contour, cax, orientation='vertical',
                            fraction=0.1, pad=0,
                            format='%6.1e',
                            ticks=levels[1:-1:2])
    colorbar.ax.tick_params(labelsize=6, pad=0.05)
    plt.savefig(ofile, dpi=300)


def plot_t_series(idata, ylim, ylabel, ofile):
    r"""
    """
    fig = plt.figure(figsize=(4, 3), dpi=300)
    ax = fig.add_axes([0.15, 0.1, 0.84, 0.88])
    ax.set_ylim(*ylim)
    ax.set_xlabel('Date', fontsize=6)
    ax.set_ylabel(ylabel, fontsize=6)
    ax.tick_params(axis='both', which='major', direction='in',
                   labelsize=6,
                   labelbottom=True, labeltop=False,
                   labelleft=True, labelright=False)
    for k, v in idata.items():
        ax.plot(v[0], v[1], c=v[2], ls='-', lw=.5)
        '''
        if k == 'obs':
            ax.scatter(v[0], v[1], c=v[2], s=.5)
        else:
            ax.plot(v[0], v[1], c=v[2], ls='-', lw=.5)
        '''
    #ax.scatter(date2, idata2, c='#777C21', s=.5)
    #ax.scatter(date1, idata1, c='#3C3077', s=.5)
    ax.xaxis_date()
    plt.savefig(ofile, dpi=300)
    plt.close()


def plot_t_series_Q(idata, seq, lbl, ylim, ylabel, ofile):
    r"""
    """
    fig = plt.figure(figsize=(4, 3), dpi=300)
    ax = fig.add_axes([0.15, 0.1, 0.84, 0.88])
    ax.set_ylim(*ylim)
    ax.set_xlabel('Date', fontsize=6)
    ax.set_ylabel(ylabel, fontsize=6)
    ax.tick_params(axis='both', which='major', direction='in',
                   labelsize=6,
                   labelbottom=True, labeltop=False,
                   labelleft=True, labelright=False)
    ax.stackplot(
            idata[0],
            *[idata[1][n] for n in seq],
            labels=[lbl[n] for n in seq],
            colors=[idata[2][n] for n in seq]
            )
    ax.xaxis_date()
    plt.savefig(ofile, dpi=300)
    plt.close()


def plot_compare(idata, lim, ofile):
    r"""
    """
    fig = plt.figure(figsize=(4, 4), dpi=300)
    ax = fig.add_axes([0.10, 0.10, 0.85, 0.85])
    ax.set_xlim(*lim)
    ax.set_ylim(*lim)
    ax.set_xlabel(idata[0][0], fontsize=6)
    ax.set_ylabel(idata[1][0], fontsize=6)
    ax.tick_params(axis='both', which='major', direction='in',
                   labelsize=6,
                   labelbottom=True, labeltop=False,
                   labelleft=True, labelright=False)
    ax.plot(lim, lim, c='#FF0000', lw=1, ls='-', marker=None)
    ax.scatter(idata[0][1], idata[1][1], s=1, c='#000000')
    plt.savefig(ofile, dpi=300)
    plt.close()

