import h5py
import numpy as np
import pandas as pd
import re
import xarray as xr


def hdf_key_check(fpath, key):
    r"""
    """
    with pd.HDFStore(fpath, mode='r') as ifile_hdf:
        if ifile_hdf.__contains__(key):
            return True
        else:
            return False


def date_calc_days(a):
    r"""
    """
    return None


def date_extended_range(start, end, freq, extend):
    r"""
    start, end : str or datetime-like
        Left and right bound date
    freq : str or DateOffset
        Frequency
    extend : tuple
        (multiple, unit)
    """
    dates = pd.date_range(
            start,
            np.datetime64(end) + np.timedelta(*extend),
            freq=freq
            )
    return dates


def date_freq_str2tuple(res):
    r"""
    """
    match = re.match('([0-9]+)([A-Z])', res)
    n = int(match[1])
    unit = match[2]
    return n, unit


def date_floor(idate, res):
    r"""
    """
    match = re.match('([0-9]+)([A-Z])', res)
    n = int(match[1])
    unit = match[2]
    if unit == 'Y':
        t = pd.DatetimeIndex(pd.to_datetime({
            'year': (idate.year - 1) // n * n + 1,
            'month': 1,
            'day': 1
            }))
    elif unit == 'M':
        t = pd.DatetimeIndex(pd.to_datetime({
            'year': idate.year,
            'month': (idate.month - 1) // n * n + 1,
            'day': 1
            }))
    elif unit == 'D':
        t = pd.to_datetime({
            'year': idate.year,
            'month': idate.month,
            'day': (idate.day - 1) // n * n + 1
            })
    else:
        t = idate.floor(res)
    return t


def date_find_closest(date0, date1, t_res):
    r"""

    Find similar date of date0 from date1

    date0 : pd.DatetimeIndex or dict of pd.DatetimeIndex
        Refence
    date1 : pd.DatetimeIndex or dict of pd.DatetimeIndex
        b 
    t_res : str or dict of str
        Temporal resolution of date1

    t1 : pd.DatetimeIndex or dict of pd.DatetimeIndex
        length
    """
    isdict0 = isinstance(date0, dict)
    isdict1 = isinstance(date1, dict)
    if isdict1 and not isdict0:
        d0 = {k: date0 for k in date1}
        d1 = date1
        res = t_res
    elif not isdict0 and not isdict1:
        d0 = {0: date0}
        d1 = {0: date1}
        res = {0: t_res}
    elif isdict0 and not isdict1:
        d0 = date0
        d1 = {k: date1 for k in date0}
        res = {k: t_res for k in date0}
    else:
        d0 = date0
        d1 = date1
        res = t_res
    t0 = {
            k:
            date_floor(v, res[k])
            for k, v in d0.items()
            }
    t1 = {}
    for k, v in t0.items():
        if date_freq_str2tuple(res[k])[1] == 'Y':
            t1[k] = pd.to_datetime([
                date
                if date in d1[k] else
                d1[k][0]
                if date < d1[k][0] else
                d1[k][-1]
                for i, date in enumerate(v)
                ])
        elif date_freq_str2tuple(res[k])[1] == 'M':
            t1[k] = pd.to_datetime([
                date
                if date in d1[k] else
                d1[k][d1[k].month == date.month][0]
                if date < d1[k][0] else
                d1[k][d1[k].month == date.month][-1]
                for i, date in enumerate(v)
                ])
    if isdict0 or isdict1:
        return t1
    else:
        return t1[0]


def if_int(i):
    r"""
    """
    try:
        int(i)
        return True
    except:
        return False


def resample(idata, from_res, to_res):
    r"""
    """
    rank = {'Y': 0, 'M': 1, 'D': 2}
    kw = {'Y': 'years', 'M': 'months', 'D': 'days'}
    if from_res == to_res:
        odata = idata
    else:
        from_match = re.match('([0-9]+)([A-Z])', to_res)
        if not from_match:
            raise Exception('Wrong format for res, provide f"{n}{unit}"')
        from_n = int(from_match[1])
        from_unit = from_match[2]
        to_match = re.match('([0-9]+)([A-Z])', to_res)
        if not to_match:
            raise Exception('Wrong format for res, provide f"{n}{unit}"')
        to_n = int(to_match[1])
        to_unit = to_match[2]
        if isinstance(idata, xr.DataArray):
            idx = idata.time.to_index()
        else:
            idx = idata.index
        if rank[from_unit] < rank[to_unit]:
            upscale = True
        elif rank[from_unit] == rank[to_unit] and from_n > to_n:
            upscale = True
        else:
            upscale = False
        if upscale:
            if isinstance(idata, xr.DataArray):
                idx_new = idx.union(
                        idx + pd.offsets.DateOffset(**{kw[to_unit]:from_n})
                        )
                odata = (
                    idata.reindex(time=idx_new).resample(time=to_res).isel(time=np.s_[:-1]).ffill('time')
                    )
            else:
                idx_new = idx.union(
                        idx + pd.offsets.DateOffset(**{kw[to_unit]:to_n})
                        )
                odata = (
                    idata.reindex(idx_new).resample(to_res).iloc[:-1]
                    )
        elif to_unit == 'Y':
            group = pd.DatetimeIndex(pd.to_datetime({
                'year': (idx.year - 1) // to_n * to_n + 1,
                'month': idx.month,
                'day': idx.day
                }))
            odata = idata.groupby(group).mean()
        elif to_unit == 'M':
            group = pd.DatetimeIndex(pd.to_datetime({
                'year': idx.year,
                'month': (idx.month - 1) // to_n * to_n + 1,
                'day': idx.day
                }))
            if updown:  # finer
                pass
            else:
                len_mon = idx.month.days_in_month.to_series(index=idx)
                len_group = len_mon.groupby(group).sum()
                weight = len_mon/len_group.loc[group].values
                odata = (idata*weight).groupby(group).sum()
        elif to_unit == 'D':
            group = pd.to_datetime({
                'year': idx.year,
                'month': idx.month,
                'day': (idx.day - 1) // n * n + 1
                })
            odata = idata.groupby(group).mean()
        else:
            odata.resample(to_res).mean()
    return odata


def viewer(idata, y0, x0, yr, xr):
    r"""
    """
    if idata.ndim != 2:
        raise Exception("Dimension error: only 2d is supported")
    ymax, xmax = idata.shape
    return (
        np.flip(
            idata[
                max([0, y0-yr]):min([ymax, y0+1+yr]),
                max([0, x0-xr]):min([xmax, x0+1+xr])
                ],
            axis=0
            )
        )


def locator(dimy, dimx, y, x):
    r"""
    """
    odata = np.zeros((dimy, dimx), dtype=np.int8)
    odata[y, x] = 1
    return odata

