# Regional Modelling of Atmospheric Species

## Preparing Priors
Peform these in sequence.
The preparation of some of the priors requires cdo, which you can get from conda-forge.

### GFED
python function
```python
prior.gfed_hdf2nc()
```

then

cdo
```
cdo remapcon,<grid_file> <ifile> <$ofile>
```

### WetCHARTs
python function
```python
prior.wetcharts_nc2nc()
```

then

cdo
```
cdo remapcon,<grid_file> <ifile> <$ofile>
```

### EDGAR v5.0
bash
```
scripts/prior_edgar_v5_0/regrid.sh prefix0 prefix1
```
where prefix is destination directory + start name of the file.

tyhen

python function
```python
prior.edgar_aggregate()
```

### EDGAR v5.0
bash
```
scripts/prior_edgar_v6_0/wrap.sh <sector> <idir> <gfile> <setaxisfile>
```

`sector` is the 3-digit alphabet code sectors defined by EDGAR, e.g. AGS.

`idir` is the directory containing the data files.

`gfile` is the path to the grid file (`scripts/name_grid`).

`settaxisfile` is the path to `scripts/prior_edgar_v6_0/settaxis.sh`.

then
```
scripts/prior_edgar_v6_0/ipcc2snap.sh
```

### NAEI
bash
```
prior_naei.sh idir odir
```

