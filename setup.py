# -*- coding: utf-8 -*-

r"""
Rn_Processor
============
Suite of functions to process ANSTO Radon measurements.
"""

# Standard library imports
import io
import os
import sys
import shutil

# Third party imports
from setuptools import setup, find_packages


# =============================================================================
# Check for Python 3
# =============================================================================
PY3 = sys.version_info[0] == 3
v = sys.version_info
if v[0] >= 3 and v[:2] < (3, 6):
    error = "ERROR: name_qch4_couple requires Python version 3.6 or above."
    print(error, file=sys.stderr)
    sys.exit(1)

# =============================================================================
# Constants
# =============================================================================
NAME = 'name_qch4_couple'
LIBNAME = 'name_qch4_couple'
VERSION = '0.0.1'

# =============================================================================
# Use Readme for long description
# =============================================================================
with io.open('README.md', encoding='utf-8') as f:
    LONG_DESCRIPTION = f.read()

#==============================================================================
# Setup arguments
#==============================================================================
setup_args = dict(
    name=NAME,
    version=VERSION,
    description='NAME - Emission Model',
    long_description=LONG_DESCRIPTION,
    long_description_content_type='text/markdown',
    #author="",
    #author_email=",
    license='MIT',
    keywords='Rn',
    platforms=["Windows", "Linux", "Mac OS-X"],
    packages=find_packages('src'),
    package_dir={'': 'src'},
    classifiers=[
        #'License :: OSI Approved :: MIT License',
        'Operating System :: MacOS',
        'Operating System :: Microsoft :: Windows',
        'Operating System :: POSIX :: Linux',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
    ],
)

install_requires = [
    'matplotlib',
    'numpy',
    'pandas',
    'h5py',
    'xarray'
]

setup_args['install_requires'] = install_requires


# =============================================================================
# Main setup
# =============================================================================
setup(**setup_args)

