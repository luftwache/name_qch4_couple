r"""

Common routines

"""
# Standard Library imports
import datetime
import numpy as np
import os
import pandas as pd
import re

# Third party imports
from collections import OrderedDict

# Semi-local imports
import name_qch4_couple.name
import name_qch4_couple.region_EU


def read_obs(sites, obs_files, date_range, dates_tHour):
    r"""
    """
    obs = OrderedDict()
    obs_err = OrderedDict()
    for site, y in sites.items():
        obs[site] = {}
        obs_err[site] = {}
        if site == 'cbw':
            if 'chi' in y and int(y['chi']) in [0, 1]:
                obs[site]['chi'] = (
                    pd.read_csv(
                        obs_files[site]['chi'],
                        parse_dates=['time'], index_col='time')
                    ).loc[date_range[0]:date_range[1], 'ch4']
                obs_err[site]['chi'] = (
                    pd.read_csv(
                        obs_files[site]['chi'],
                        parse_dates=['time'], index_col='time')
                    ).loc[date_range[0]:date_range[1], 'ch4_repeatability']
        elif site == 'cbw020':
            date_parser = (
                #lambda s: pd.to_datetime(s, '%d.%m.%Y %H:%M')
                lambda s: datetime.datetime.strptime(s, '%d.%m.%Y %H:%M')
                )
            idata1 = pd.read_csv(
                obs_files[site]['d_13C'],
                sep='\t', skipinitialspace=True,
                parse_dates=['CBW_d13C_Time'], date_parser=date_parser,
                index_col=['CBW_d13C_Time']
                )[date_range[0]:date_range[1]]
            idata2 = pd.read_csv(
                obs_files[site]['d_2H'],
                sep='\t', skipinitialspace=True,
                parse_dates=['CBW_dD_Time'], date_parser=date_parser,
                index_col=['CBW_dD_Time']
                )[date_range[0]:date_range[1]]
            if 'chi' in y and int(y['chi']) in [0, 1]:
                obs[site]['chi'] = pd.concat(
                    [idata1['CBW_d13C_Conc'], idata2['CBW_dD_Conc']],
                    sort=True
                    ).groupby(level=0).first().to_frame('chi')
                obs_err[site]['chi'] = pd.concat(
                    [idata1['CBW_d13C_ConcErr'], idata2['CBW_dD_ConcErr']],
                    sort=True
                    ).groupby(level=0).first()
            if 'd_13C' in y and int(y['d_13C']) in [0, 1]:
                obs[site]['d_13C'] = (
                    idata1['CBW_d13C_Delta'].groupby(level=0).first()
                    )
                obs_err[site]['d_13C'] = (
                    idata1['CBW_d13C_DeltaErr'].groupby(level=0).first()
                    )
            if 'd_2H' in y and int(y['d_2H']) in [0, 1]:
                obs[site]['d_2H'] = (
                    idata2['CBW_dD_Delta'].groupby(level=0).first()
                    )
                obs_err[site]['d_2H'] = (
                    idata2['CBW_dD_DeltaErr'].groupby(level=0).first()
                    )
        else:
            if 'chi' in y and int(y['chi']) in [0, 1]:
                obs[site]['chi'] = (
                    name_qch4_couple.io.r_decc(
                        obs_files[site]['chi']
                        ).iloc[:, 0][date_range[0]:date_range[1]]
                    ).squeeze()
                obs_err[site]['chi'] = (
                    pd.Series(0.2, index=obs[site]['chi'].index)
                    if site == 'mhd' else
                    name_qch4_couple.io.r_decc(
                        obs_files[site]['chi']
                        ).iloc[:, 1][date_range[0]:date_range[1]]
                    ).squeeze()
            if 'd_13C' in y and int(y['d_13C']) in [0, 1]:
                obs[site]['d_13C'] = name_qch4_couple.io.r_instaar(
                    obs_files[site]['d_13C'], 'event'
                    )[date_range[0]:date_range[1]].squeeze()
                obs_err[site]['d_13C'] = pd.Series(
                    0.1, index=obs[site]['d_13C'].index
                    )
            if 'd_2H' in y and int(y['d_2H']) in [0, 1]:
                obs[site]['d_2H'] = name_qch4_couple.io.r_instaar(
                    obs_files[site]['d_2H'], 'event'
                    )[date_range[0]:date_range[1]].squeeze()
                obs_err[site]['d_2H'] = pd.Series(
                    2.0, index=obs[site]['d_2H'].index
                    )
    for site, y in sites.items():
        for k in ['chi', 'd_13C', 'd_2H']:
            if k in y:
                if int(y[k]) == 2:
                    obs[site][k] = pd.Series(
                        0.,
                        index=pd.DatetimeIndex(np.unique(np.concatenate([
                            v.index for v in obs[site].values()
                            ])))
                        )
                elif int(y['chi']) == 3:
                    obs[site][k] = pd.Series(0., index=dates_tHour)
                obs_err[site][k] = pd.Series(
                    0.2 if k == 'chi' else 0.1 if k == 'd_13C' else 2.0,
                    index=obs[site][k].index
                    )
    return obs, obs_err


def define_grid(divide_gb_eng=False):
    r"""
    """
    # Grid
    lon0 = -98.07600
    lat0 = 10.61200
    dlon = 0.3520000
    dlat = 0.2340000
    nlon = 391
    nlat = 293
    grid_centre = [
        np.linspace(lon0 + dlon/2, lon0 + dlon*(nlon - 0.5), nlon),
        np.linspace(lat0 + dlat/2, lat0 + dlat*(nlat - 0.5), nlat)
        ]
    grid_vertex = [
        np.linspace(lon0, lon0 + dlon*nlon, nlon+1),
        np.linspace(lat0, lat0 + dlat*nlat, nlat+1)
        ]
    area = name_qch4_couple.name.calc_cell_area(
        grid_vertex[1][:-1],
        grid_vertex[1][1:],
        dlon
        )[:, np.newaxis]

    ## Inversion grid
    inv_lon0 = 238
    inv_lat0 = 110
    inv_nlon = 128
    inv_nlat = 128
    inv_lon1 = inv_lon0 + inv_nlon
    inv_lat1 = inv_lat0 + inv_nlat
    lon_idx, lat_idx = np.meshgrid(np.arange(nlon), np.arange(nlat))
    inv_mask = (
        (inv_lon0 > lon_idx) | (lon_idx >= inv_lon1) |
        (inv_lat0 > lat_idx) | (lat_idx >= inv_lat1)
        )
    inv_lon_s_ = np.s_[inv_lon0:inv_lon1]
    inv_lat_s_ = np.s_[inv_lat0:inv_lat1]
    inv_area = area[inv_lat_s_]

    ## Inversion regions
    reg_dict = {
        'gb_eng': ['gb_eng'],  #
        'gb_sct': ['gb_sct'],
        'gb_wls': ['gb_wls'],
        'gb_nir': ['gb_nir'],
        'ie' : ['ie'],
        'fr0': ['fr0'],  #
        'fr1': ['fr1'],
        'benllu': ['benllu'],
        'de': ['de'],
        'dk': ['dk'],
        'espt': ['espt'],  #
        'no': ['no'],
        'NEEU': ['NEEU'],
        'it0': ['it0'],
        'it1': ['it1'],
        'chat': ['chat'],  #
        'SEEU': ['SEEU'],
        'fo': ['fo'],
        'is': ['is'],
        'seaAtNWo': ['seaAtNWo', 'seaAtMH', 'seaAtMM'],  #
        'seaAtNNo': ['seaAtNNo', 'seaAtNE'],
        'seaAtS': ['seaAtS', 'seaAtML', 'seaBaBi', 'seaIrSe'],
        'seaNoSe': ['seaNoSe', 'seaSk', 'seaEnCh'],
        'seaMdE': ['seaMdE', 'seaMdNW', 'seaIo', 'seaAd'],
        'seaMdS': ['seaMdS']  #
        }
    reg_names = {
        'gb': ['United Kingdom of Great Britain and Northern Ireland'],
        'ie': ['Ireland'],
        'fr': ['France'],
        'benllu': ['Belgium', 'Netherlands', 'Luxembourg'],
        'de': ['Germany'],
        'dk': ['Denmark'],
        'espt': ['Spain', 'Portugal'],
        'no': ['Norway'],
        'NEEU': ['NE Europe'],
        'SEEU': ['SE Europe'],
        'it': ['Italy'],
        'chat': ['Switzerland', 'Austria']
        }
    if divide_gb_eng:
        del reg_dict['gb_eng']
        reg_custom = {
            'gb_eng0': { # London
                'cat': 171,
                'lon': [
                    39, 40, 41
                    ],
                'lat': [
                    [63, 65], [63, 65], [63, 65],
                    ]
                },
            'gb_eng1': { # East Anglia
                'cat': 172,
                'lon': [
                    43, 44, 45, 46
                    ],
                'lat': [
                    [66, 71], [66, 71], [66, 71], [66, 71],
                    ]
                },
            'gb_eng2': { # SE
                'cat': 173,
                'lon': [
                    41, 42, 43, 44
                    ],
                'lat': [
                    [61, 62], [61, 65], [62, 65], [62, 65],
                    ]
                },
            'gb_eng3': { # SM
                'cat': 174,
                'lon': [
                    35, 36, 37, 38, 39, 40
                    ],
                'lat': [
                    [60, 62], [60, 62], [60, 62], [61, 62], [61, 62], [61, 62],
                    ]
                },
            'gb_eng4': { # SW
                'cat': 175,
                'lon': [
                    22, 23, 24, 25, 26,
                    27, 28, 29, 30,
                    31, 32, 33, 34
                    ],
                'lat': [
                    [57, 58], [57, 58], [57, 60], [57, 61], [57, 63],
                    [58, 63], [58, 63], [58, 63], [58, 63],
                    [60, 63], [60, 63], [60, 63], [60, 63],
                    ]
                },
            'gb_eng5': { # ME
                'cat': 176,
                'lon': [
                    39, 40, 41, 42
                    ],
                'lat': [
                    [66, 71], [66, 71], [66, 71], [66, 71],
                    ]
                },
            'gb_eng6': { # MW
                'cat': 177,
                'lon': [
                    32, 33, 34,
                    35, 36, 37, 38
                    ],
                'lat': [
                    [68, 69], [64, 69], [64, 69],
                    [63, 69], [63, 69], [63, 69], [63, 69],
                    ]
                },
            'gb_eng7': { # NE
                'cat': 178,
                'lon': [
                    35, 36, 37, 38,
                    39, 40, 41
                    ],
                'lat': [
                    [70, 83], [70, 82], [70, 79], [70, 78],
                    [72, 77], [72, 76], [72, 74],
                    ]
                },
            'gb_eng8': { # NW
                'cat': 179,
                'lon': [
                    29, 30, 31,
                    32, 33, 34
                    ],
                'lat': [
                    [75, 76], [74, 78], [74, 79],
                    [70, 80], [70, 81], [70, 82],
                    ]
                },
            }

    reg_map_default = -1.
    reg_map = np.full((nlat, nlon), reg_map_default)
    inv_reg_map0 = np.full((inv_nlat, inv_nlon), 0, dtype=np.int16)
    reg_codes_dict = {}
    reg_dict_T = {i: k for k, v in reg_dict.items() for i in v}
    for k, v in name_qch4_couple.region_EU.intem_regions(reg_dict_T).items():
        if v:
            name_qch4_couple.region_EU.allocate_lonlat(
                inv_reg_map0,
                np.array(v['lon']),
                np.array(v['lat']),
                v['cat']
                )
            reg_codes_dict[k] = v['cat']
    if divide_gb_eng:
        for k, v in reg_custom.items():
            if v and v['lon'] and v['lat']:
                name_qch4_couple.region_EU.allocate_lonlat(
                    inv_reg_map0,
                    np.array(v['lon']),
                    np.array(v['lat']),
                    v['cat']
                    )
                reg_codes_dict[k] = v['cat']
    reg_codes_dict['SEEU'] = 0
    reg_codes_dict_T = {v: k for k, v in reg_codes_dict.items()}

    reg_map[inv_lat_s_, inv_lon_s_] = inv_reg_map0

    inv_reg_map = np.full((inv_nlat, inv_nlon), 0, dtype=np.int16)
    for k, v in reg_dict_T.items():
        inv_reg_map[inv_reg_map0 == reg_codes_dict[k]] = reg_codes_dict[v]
    if divide_gb_eng:
        for k in reg_custom:
            if k in reg_codes:
               inv_reg_map[inv_reg_map0 == reg_codes[k]] = reg_codes[k]

    inv_reg_uniq = np.unique(inv_reg_map)
    inv_reg_codes = np.array([reg_codes_dict_T[i] for i in inv_reg_uniq])

    inv_reg_areas = np.array([
        (
            np.ones((inv_nlat, inv_nlon))*inv_area
            )[..., inv_reg_map == inv_reg].sum(-1)
        for inv_reg in inv_reg_uniq
        ]).T

    grid_info = {
        'lat0': lat0, 'nlat': nlat, 'dlat': dlat,
        'lon0': lon0, 'nlon': nlon, 'dlon': dlon,
        'inv_lat0': inv_lat0, 'inv_lat1': inv_lat1, 'inv_nlat': inv_nlat,
        'inv_lon0': inv_lon0, 'inv_lon1': inv_lon1, 'inv_nlon': inv_nlon,
        'grid_centre': grid_centre, 'grid_vertex': grid_vertex,
        'area': area, 'reg_names': reg_names,
        'inv_reg_map': inv_reg_map,
        'inv_reg_uniq': inv_reg_uniq,
        'inv_reg_codes': inv_reg_codes,
        }

    return grid_info


def div_regions(
        Q, Q_min, D, regions_def, thr_freq, thr_sens, s_sel,
        strip_ratio=5, verbose=False
        ):
    r"""Divide regions
    """
    nlat, nlon = Q.shape[-2:]
    regions_temp = np.array([regions_def.copy()]*Q.shape[0])
    regions_new = np.full(Q.shape, -1)
    lats = np.tile(np.arange(nlat)[:, np.newaxis], (1, nlat))
    lons = np.tile(np.arange(nlon)[np.newaxis, :], (nlon, 1))
    reg_px = np.full((nlat, nlon), False)
    n = 0
    for s, Qs in enumerate(Q):
        pool = list(np.unique(regions_def))
        region_temp = regions_temp[s]
        rtemp = -10
        if s not in s_sel:
            for r in pool:
                if verbose:
                    print(f'{r:8n}, {n:8n}, not in s_sel')
                regions_new[s, region_temp == r] = n
                n += 1
            continue
        sens = np.concatenate([Qs * v for v in D.values()])
        sens_min = np.concatenate([Q_min * v for v in D.values()])
        if verbose:
            print(
                np.percentile(
                    np.concatenate([
                        sens[..., regions_def == r].sum(-1)
                        for r in np.unique(regions_def)
                        ]),
                    [16, 50, 84]
                    )
                )
        while pool:
            r = pool.pop(0)
            #if verbose:
            #    print(r, n)
            reg_px = (region_temp == r)
            # Check if whole region is not too sensitive
            if (sens[..., reg_px].sum(-1) >= thr_sens).sum() <= thr_freq:
                if verbose:
                    print(f'{r:8n}, {n:8n}, one region')
                regions_new[s, reg_px] = n
                n += 1
                continue
            lmode = 0
            repeat = 0
            while reg_px.any():
                """
                """
                if reg_px.sum() == 1:
                    if verbose:
                        print(f'{r:8n}, {n:8n}, 1 px left')
                    regions_new[s, reg_px] = n
                    n += 1
                    break
                reg_lats, reg_lons = [
                    np.unique(i) for i in np.where(reg_px)
                    ]
                """
                lat>lon lmode   direction
                1       0       lat
                1       1       lon
                0       0       lon
                0       1       lat
                """
                if (reg_lats.size > reg_lons.size) != lmode:
                    l0 = lats
                    l1 = reg_lats
                    l2 = reg_lons
                else:
                    l0 = lons
                    l1 = reg_lons
                    l2 = reg_lats
                divs = np.array([  # criteria
                    (
                        np.maximum(*[
                            (
                                sen[:, reg_px & (l0 >= l)].sum(-1) >= thr_sens
                                ).sum()
                            for sen in [sens, sens_min]
                            ]),
                        np.maximum(*[
                            (
                                sen[:, reg_px & (l0 < l)].sum(-1) >= thr_sens
                                ).sum()
                            for sen in [sens, sens_min]
                            ])
                        )
                    for l in l1[1:]
                    ])
                divs_diff = np.abs(np.diff(divs, 1)).flatten()
                divs_cc = np.array([  # cell count
                    [
                        (reg_px & (l0 >= l)).sum(),
                        (reg_px & (l0 < l)).sum(),
                        ]
                    for l in l1[1:]
                    ])
                divs_cc_diff = np.abs(np.diff(divs_cc, 1)).flatten()
                divs_dim1 = np.array([  # dim1 size
                    [
                        np.subtract(*np.where(
                            np.any(reg_px & (l0 >= l), axis=0))[0][[-1, 0]]
                            ) + 1,
                        np.subtract(*np.where(
                            np.any(reg_px & (l0 < l), axis=0))[0][[-1, 0]]
                            ) + 1,
                        ]
                    for l in l1[1:]
                    ])
                divs_dim2 = np.array([  # dim2 size
                    [
                        np.subtract(*np.where(
                            np.any(reg_px & (l0 >= l), axis=1))[0][[-1, 0]]
                            ) + 1,
                        np.subtract(*np.where(
                            np.any(reg_px & (l0 < l), axis=1))[0][[-1, 0]]
                            ) + 1,
                        ]
                    for l in l1[1:]
                    ])
                divs_dimratio = divs_dim1/divs_dim2
                divs_pass = (
                    (divs <= thr_freq)
                    & (divs_dimratio >= 1/strip_ratio)
                    & (divs_dimratio <= strip_ratio)
                    )
                if repeat:
                    l = np.argmin(divs_cc_diff) + 1
                    region_temp[reg_px & (l0 >= l1[l])] = rtemp
                    region_temp[reg_px & (l0 < l1[l])] = rtemp - 1
                    repeat = 0
                    pool.insert(0, rtemp)
                    pool.insert(1, rtemp - 1)
                    rtemp -= 2
                    break
                elif divs_pass.all(1).any():
                    if verbose:
                        print(f'{r:8n}, {n:8n}, divide, selected both')
                    sel0 = np.where(divs_pass.all(1))
                    sel1 = np.where(divs_diff[sel0] >= divs_diff[sel0].min())
                    sel2 = np.argmin(divs_cc_diff[sel0][sel1])
                    l = sel0[0][sel1][sel2] + 1
                    regions_new[s, reg_px & (l0 >= l1[l])] = n
                    regions_new[s, reg_px & (l0 < l1[l])] = n + 1
                    n += 2
                    break
                elif divs_pass.any(1).any():
                    if verbose:
                        print(f'{r:8n}, {n:8n}, divide, selected one')
                    sel0 = np.where(divs_pass)
                    sel1 = np.where(
                        divs[sel0[0]].min(1) == divs[sel0[0]].min(1).max()
                        )
                    sel2 = np.argmin(divs_cc_diff[sel0[0]][sel1])
                    l = np.array(sel0).T[sel1][sel2] + np.array([1, 0])
                    if l[1] == 0:
                        regions_new[s, reg_px & (l0 >= l1[l[0]])] = n
                        reg_px[reg_px & (l0 >= l1[l[0]])] *= False
                    else:
                        regions_new[s, reg_px & (l0 < l1[l[0]])] = n
                        reg_px[reg_px & (l0 < l1[l[0]])] *= False
                    n += 1
                elif l2.size == 1:
                    for l in l1:
                        if verbose:
                            print(f'{r:8n}, {n:8n}, divide, strip left')
                        regions_new[s, reg_px & (l0 == l)] = n
                        n += 1
                    break
                elif not lmode:
                    lmode = 1
                    continue
                elif lmode:
                    lmode = 0
                    repeat = 1
                    continue
                else:
                    raise Exception('think more')
                lmode = 0
    return regions_new


def populate_array(original, sel, default):
    r"""
    """
    output = np.full(original.shape, default)
    output[sel] = original[sel]
    return output


def read_results(
        sce, idir, rdates, sites, post,
        dates_tHour, dates_tSite, dates_tObs, grid_info, obs
        ):
    r"""
    """
    print('  Observation Vector')
    # Regularise observation time axis
    obs_y_df = {
        site: {
            k:
            pd.Series(
                np.array(
                    v.resample('H').mean().reindex(dates_tObs[site][k])
                    ).flatten(),
                index=dates_tObs[site][k]
                )
            for k, v in y.items()
            }
        for site, y in obs.items()
        }
    obs_y_df_err = {
        site: {
            k:
            pd.Series(
                np.array(
                    v.resample('H').mean().reindex(dates_tObs[site][k])
                    ).flatten(),
                index=dates_tObs[site][k]
                )
            for k, v in y.items()
            }
        for site, y in obs.items()
        }

    prior_obs = {
        site:
        pd.DataFrame(np.nan, index=dates_tHour, columns=['chi', 'd_13C', 'd_2H'])
        for site in sites
        }
    post_obs = {
        site:
        pd.DataFrame(
            np.nan, index=dates_tHour,
            columns=[
                f'{k}{i}'
                for k in ['chi', 'd_13C', 'd_2H']
                for i in ['_l', '', '_u']
                ]
            )
        for site in sites
        }
    for rdate in rdates:
        rdir = os.path.join(idir, f'{rdate}')
        f_obs_obs = os.path.join(rdir, 'obs_y.npz')
        f_prior_obs = os.path.join(rdir, 'pri_y.npz')
        if not os.path.exists(rdir):
            continue
        if not os.path.exists(f_obs_obs):
            continue
        if not os.path.exists(f_prior_obs):
            continue
        selH = pd.date_range(
            f"{rdate}-01",
            pd.to_datetime(f"{rdate}-01") + pd.DateOffset(months=1),
            freq='1H',
            closed='left'
            ).intersection(dates_tHour)
        selS = {
            site: y.to_series()[selH[0]:selH[-1]].index
            for site, y in dates_tSite.items()
            }
        for site in sites:
            obs_y_keys = [
                k for k in np.load(f_obs_obs, 'r').keys()
                ]
            prior_obs_keys = [
                k for k in np.load(f_prior_obs, 'r').keys()
                ]
            for k in obs_y_df[site]:
                if f'{site}_{k}' in obs_y_keys:  # obs
                    selO = selH.intersection(dates_tObs[site][k])
                    obs_y_df[site][k].update(pd.Series(
                        np.load(f_obs_obs, 'r')[f'{site}_{k}'],
                        index=selO
                        ))
                    obs_y_df_err[site][k].update(pd.Series(
                        np.load(f_obs_obs, 'r')[f'var_{site}_{k}'],
                        index=selO
                        ))
                if f'{site}_{k}' in prior_obs_keys:  # site
                    prior_obs[site][k].update(pd.Series(
                        np.load(f_prior_obs, 'r')[f'{site}_{k}'],
                        index=selH
                        ))
        if not post:
            continue
        f_post_obs = os.path.join(rdir, 'pos_y.npz')
        if not os.path.exists(f_post_obs):
            continue
        for site in sites:
            post_obs_keys = [
                k for k in np.load(f_post_obs, 'r').keys()
                ]
            for k in ['chi', 'd_13C', 'd_2H']:
                if f'{site}_{k}' in post_obs_keys:  # H
                    post_obs[site][f'{k}_l'].loc[selH] = np.load(
                        f_post_obs, 'r'
                        )[f'{site}_{k}'][0]
                    post_obs[site][k].loc[selH] = np.load(
                        f_post_obs, 'r'
                        )[f'{site}_{k}'][1]
                    post_obs[site][f'{k}_u'].loc[selH] = np.load(
                        f_post_obs, 'r'
                        )[f'{site}_{k}'][2]
    obs_y = {
        site: {
            k:
            np.array(v).flatten()
            for k, v in y.items()
            }
        for site, y in obs_y_df.items()
        }
    obs_y_err = {
        site: {
            k:
            np.array(v).flatten()
            for k, v in y.items()
            }
        for site, y in obs_y_df_err.items()
        }

    print('  Grid')
    # Grid
    inv_reg_map0 = grid_info['inv_reg_map']
    nlat = grid_info['nlat']
    nlon = grid_info['nlon']
    inv_lat0 = grid_info['inv_lat0']
    inv_lat1 = grid_info['inv_lat1']
    inv_lon0 = grid_info['inv_lon0']
    inv_lon1 = grid_info['inv_lon1']
    area = grid_info['area']
    grid_vertex = grid_info['grid_vertex']
    inv_reg_uniq = grid_info['inv_reg_uniq']
    inv_reg_codes = grid_info['inv_reg_codes']
    inv_mask = np.full((nlat, nlon), False)
    inv_mask[inv_lat0:inv_lat1, inv_lon0:inv_lon1] = True

    prior_state = {site: {} for site in sites}
    post_state = {site: {} for site in sites}
    def_state = {site: {} for site in sites}
    f_pri = [
        os.path.join(idir, f'{rdate}', 'pri_theta.npz')
        for rdate in rdates
        ]
    f_pos = [
        os.path.join(idir, f'{rdate}', 'pos_theta.npz')
        for rdate in rdates
        ]

    print('  State Vector')
    ## Inversion regions
    dim_sAll = 5
    inv_reg_maps = [
        np.load(f, 'r')[f'inv_reg_maps']
        if os.path.exists(f) else
        np.array([])
        for f in f_pri
        ]
    inv_reg_area = [
        np.load(f, 'r')[f'inv_reg_area']
        if os.path.exists(f) else
        np.array([])
        for f in f_pri
        ]
    reg_map_sel = [
        np.load(f, 'r')['reg_map_sel']
        if os.path.exists(f) else
        np.array([])
        for f in f_pri
        ]
    inv_Q_dist = [
        np.load(f, 'r')['inv_Q_dist']
        if os.path.exists(f) else
        np.array([])
        for f in f_pri
        ]
    inv_reg_codes = [
        np.load(f, 'r')['inv_reg_codes']
        if os.path.exists(f) else
        np.array([])
        for f in f_pri
        ]
    inv_Q_scale = [
        np.load(f, 'r')['inv_Q_scale']
        if os.path.exists(f) else
        np.array([])
        for f in f_pri
        ]
    nx_rAll_rSel = [
        np.load(f, 'r')['nx_rAll_rSel']
        if os.path.exists(f) else
        np.array([])
        for f in f_pri
        ]
    inv_s_sel = [
        [n for n, m in enumerate(i) if m.any()]
        for i in reg_map_sel
        ]
    nx_sAll_rSel = [
        np.concatenate([
            np.full(np.unique(i[reg_map_sel[d][n]]).size, n)
            for n, i in enumerate(m)
            ])
        if m.size else
        np.array([])
        for d, m in enumerate(inv_reg_maps)
        ]
    nx_sSel_rSel = [
        np.concatenate([
            np.full(np.unique(i[reg_map_sel[d][inv_s_sel[d][n]]]).size, n)
            for n, i in enumerate(m[inv_s_sel[d]])
            ])
        if m.size else
        np.array([])
        for d, m in enumerate(inv_reg_maps)
        ]

    # QA
    Qmode = 'Q'
    def_state['v_QA'] = [
        (
            np.load(f, 'r')[f'def_v_{Qmode}']
            * (1. if Qmode == 'QA' else area[inv_lat0:inv_lat1])
            ) * 86400 * 365.25 * 1.e-9
        if os.path.exists(f) else
        np.array([])
        for n, f in enumerate(f_pri)
        ]
    prior_state['v_Q'] = [
        (
            np.load(f, 'r')[f'v_{Qmode}']
            / (1. if Qmode == 'Q' else inv_reg_area[n][nx_rAll_rSel[n]])
            ) / inv_Q_scale[n] * 86400 * 365.25 * 1.e-9
        if os.path.exists(f) else
        np.array([])
        for n, f in enumerate(f_pri)
        ]
    prior_state['v_QA'] = [
        (
            np.load(f, 'r')[f'v_{Qmode}']
            * (1. if Qmode == 'QA' else inv_reg_area[n][nx_rAll_rSel[n]])
            ) / inv_Q_scale[n] * 86400 * 365.25 * 1.e-9
        if os.path.exists(f) else
        np.array([])
        for n, f in enumerate(f_pri)
        ]
    prior_state['u_QA'] = [
        (
            np.load(f, 'r')[f'u_{Qmode}']
            * (1. if Qmode == 'QA' else inv_reg_area[n][nx_rAll_rSel[n]])
            ) / inv_Q_scale[n] * 86400 * 365.25 * 1.e-9
        if os.path.exists(f) else
        np.array([])
        for n, f in enumerate(f_pri)
        ]
    if sce:
        prior_state['sce_Q'] = [
            (
                np.load(f, 'r')[f'sce_v_{Qmode}']
                / (1. if Qmode == 'Q' else inv_reg_area[n][nx_rAll_rSel[n]])
                ) / inv_Q_scale[n] * 86400 * 365.25 * 1.e-9
            if os.path.exists(f) else
            np.array([])
            for n, f in enumerate(f_pri)
            ]
        prior_state['sce_QA'] = [
            (
                np.load(f, 'r')[f'sce_v_{Qmode}']
                * (1. if Qmode == 'QA' else inv_reg_area[n][nx_rAll_rSel[n]])
                ) / inv_Q_scale[n] * 86400 * 365.25 * 1.e-9
            if os.path.exists(f) else
            np.array([])
            for n, f in enumerate(f_pri)
            ]
    if post:
        post_state['v_Q'] = [
            (
                np.load(f, 'r')[f'v_{Qmode}']
                / (1. if Qmode == 'Q' else inv_reg_area[n][nx_rAll_rSel[n]])
                ) / inv_Q_scale[n] * 86400 * 365.25 * 1.e-9
            if os.path.exists(f) else
            np.array([])
            for n, f in enumerate(f_pos)
            ]
        post_state['v_QA'] = [
            (
                np.load(f, 'r')[f'v_{Qmode}']
                * (1. if Qmode == 'QA' else inv_reg_area[n][nx_rAll_rSel[n]])
                ) / inv_Q_scale[n] * 86400 * 365.25 * 1.e-9
            if os.path.exists(f) else
            np.array([])
            for n, f in enumerate(f_pos)
            ]
    focus_rgroups = [
        [
            re.sub('^id_', '', i)
            for i in np.load(f, 'r').keys() if i.startswith('id_')
            ]
        if os.path.exists(f) else
        []
        for f in f_pri
        ]
    focus_rgroup_uniq = list(set([j for i in focus_rgroups for j in i]))
    focus_codes = [
        {i: np.load(f, 'r')[f'id_{i}'] for i in focus_rgroups[n]}
        for n, f in enumerate(f_pri)
        ]
    for rgroup in focus_rgroup_uniq:
        prior_state[f'QA_{rgroup}'] = np.full((len(f_pri), 3, 5), np.nan)
        prior_state[f'QA_{rgroup}'][:, 1] = np.array([
            np.array([
                Qs[
                    np.isin(inv_reg_maps[n], focus_codes[n][rgroup]).any(0)
                    ].sum()
                for Qs in Q
                ])
            if Q.size else
            np.full((dim_sAll,), np.nan)
            for n, Q in enumerate(def_state['v_QA'])
            ])
        prior_state[f'QA_{rgroup}'][:, 0] = np.array([
            np.array([
                (
                    prior_state[f'QA_{rgroup}'][n, 1, s]
                    - (
                        prior_state['u_QA'][n][
                            ...,
                            np.isin(nx_rAll_rSel[n], focus_codes[n][rgroup])
                            & np.isin(nx_sAll_rSel[n], [s])
                            ]**2
                        ).sum()**0.5
                    )
                if s in inv_s_sel[n] else
                np.nan
                for s in range(dim_sAll)
                ])
            if Q.size else
            np.full((dim_sAll,), np.nan)
            for n, Q in enumerate(prior_state['v_QA'])
            ])
        prior_state[f'QA_{rgroup}'][:, 2] = np.array([
            np.array([
                (
                    prior_state[f'QA_{rgroup}'][n, 1, s]
                    + (
                        prior_state['u_QA'][n][
                            ...,
                            np.isin(nx_rAll_rSel[n], focus_codes[n][rgroup])
                            & np.isin(nx_sAll_rSel[n], [s])
                            ]**2
                        ).sum()**0.5
                    )
                if s in inv_s_sel[n] else
                np.nan
                for s in range(dim_sAll)
                ])
            if Q.size else
            np.full((dim_sAll,), np.nan)
            for n, Q in enumerate(prior_state['v_QA'])
            ])
        prior_state[f'QA_{rgroup}_total'] = np.full((len(f_pri), 3, 1), np.nan)
        prior_state[f'QA_{rgroup}_total'][:, 1] = np.array([
            [Q[..., np.isin(inv_reg_maps[n], focus_codes[n][rgroup])].sum()]
            if Q.size else
            [np.nan]
            for n, Q in enumerate(def_state['v_QA'])
            ])
        prior_state[f'QA_{rgroup}_total'][:, 0] = np.array([
            (
                prior_state[f'QA_{rgroup}_total'][n, 1]
                - (
                    prior_state['u_QA'][n][
                        ...,
                        np.isin(nx_rAll_rSel[n], focus_codes[n][rgroup])
                        ]**2
                    ).sum()[..., np.newaxis]**0.5
                )
            if Q.size else
            [np.nan]
            for n, Q in enumerate(prior_state['v_QA'])
            ])
        prior_state[f'QA_{rgroup}_total'][:, 2] = np.array([
            (
                prior_state[f'QA_{rgroup}_total'][n, 1]
                + (
                    prior_state['u_QA'][n][
                        ...,
                        np.isin(nx_rAll_rSel[n], focus_codes[n][rgroup])
                        ]**2
                    ).sum()[..., np.newaxis]**0.5
                )
            if Q.size else
            [np.nan]
            for n, Q in enumerate(prior_state['v_QA'])
            ])
        if sce:
            prior_state[f'sce_QA_{rgroup}'] = np.full((len(f_pri), 1, 5), np.nan)
            prior_state[f'sce_QA_{rgroup}'][:, 0] = np.array([
                np.array([
                    Q[
                        ...,
                        np.isin(nx_rAll_rSel[n], focus_codes[n][rgroup])
                        & np.isin(nx_sAll_rSel[n], [s])
                        ].sum()
                    if s in inv_s_sel[n] else
                    np.nan
                    for s in range(dim_sAll)
                    ])
                if Q.size else
                np.full((dim_sAll,), np.nan)
                for n, Q in enumerate(prior_state['sce_QA'])
                ])
            prior_state[f'sce_QA_{rgroup}_total'] = np.nansum(
                np.where(
                    np.isfinite(prior_state[f'sce_QA_{rgroup}']),
                    prior_state[f'sce_QA_{rgroup}'],
                    prior_state[f'QA_{rgroup}']
                    ),
                axis=-1, keepdims=True
                )
        if post:
            post_state[f'QA_{rgroup}'] = np.array([
                populate_array(
                    np.load(f, 'r')[f'QA_{rgroup}']
                    / inv_Q_scale[n] * 86400. * 365.25 * 1.e-9,
                    np.s_[..., inv_s_sel[n]],
                    np.nan
                    )
                if os.path.exists(f) and rgroup in focus_rgroups[n] else
                np.full((3, 5), np.nan)
                for n, f in enumerate(f_pos)
                ])
            post_state[f'QA_{rgroup}_total'] = np.array([
                np.load(f, 'r')[f'QA_{rgroup}_total']
                / inv_Q_scale[n] * 86400. * 365.25 * 1.e-9
                if os.path.exists(f) and rgroup in focus_rgroups[n] else
                np.full((3, 1), np.nan)
                for n, f in enumerate(f_pos)
                ])

    # dchi_s
    dm0_s = pd.DataFrame(
        np.nan, index=dates_tHour,
        columns=pd.MultiIndex.from_product([sites.keys(), range(dim_sAll)])
        )
    dm1_s = pd.DataFrame(
        np.nan, index=dates_tHour,
        columns=pd.MultiIndex.from_product([sites.keys(), range(dim_sAll)])
        )
    dm2_s_pri = pd.DataFrame(
        np.nan, index=dates_tHour,
        columns=pd.MultiIndex.from_product([sites.keys(), range(dim_sAll)])
        )
    dm2_s_sce = pd.DataFrame(
        np.nan, index=dates_tHour,
        columns=pd.MultiIndex.from_product([sites.keys(), range(dim_sAll)])
        ) if sce else pd.DataFrame()
    dm2_s_pos = pd.DataFrame(
        np.nan, index=dates_tHour,
        columns=pd.MultiIndex.from_product([sites.keys(), range(dim_sAll)])
        ) if post else pd.DataFrame()

    n = 0
    for rdate in rdates:
        if not os.path.exists(f_pri[n]):
            continue
        selH = pd.date_range(
            f"{rdate}-01",
            pd.to_datetime(f"{rdate}-01") + pd.DateOffset(months=1),
            freq='1H',
            closed='left'
            ).intersection(dates_tHour)
        selS = {
            site: y.to_series()[selH[0]:selH[-1]].index
            for site, y in dates_tSite.items()
            }
        for site in sites:
            sel0 = selH.intersection(dates_tSite[site])
            dm0_s.loc[sel0, site] = pd.DataFrame(
                np.load(f_pri[n], 'r')[f'dm0_s_{site}'],
                index=sel0,
                columns=pd.MultiIndex.from_product([[site], range(dim_sAll)])
                )
            dm1_s.loc[sel0, site] = pd.DataFrame(
                np.load(f_pri[n], 'r')[f'dm1_s_{site}'],
                index=sel0,
                columns=pd.MultiIndex.from_product([[site], range(dim_sAll)])
                )
            dm2_s_pri.loc[sel0, site] = pd.DataFrame(
                np.load(f_pri[n], 'r')[f'dm2_s_{site}'],
                index=sel0,
                columns=pd.MultiIndex.from_product([[site], range(dim_sAll)])
                )
            if sce:
                dm2_s_sce.loc[sel0, site] = pd.DataFrame(
                    np.load(f_pri[n], 'r')[f'dm2_s_sce_{site}'],
                    index=sel0,
                    columns=pd.MultiIndex.from_product([[site], range(dim_sAll)])
                    )
            if post and os.path.exists(f_pos[n]):
                dm2_s_pos.loc[sel0, site] = pd.DataFrame(
                    np.load(f_pos[n], 'r')[f'dm2_s_{site}'],
                    index=sel0,
                    columns=pd.MultiIndex.from_product([[site], range(dim_sAll)])
                    )
        n += 1

    # dQ
    def_state['v_dQ'] = {}
    for k in ['dQi1', 'dQi2']:
        k1 = f'v_{k}'
        k2 = f'h_{k}'
        def_state['v_dQ'][k] = np.array([
            np.load(f, 'r')[f'def_{k1}']
            if os.path.exists(f) else
            np.full((dim_sAll,), np.nan)
            for f in f_pri
            ])
        prior_state[k1] = np.full((len(f_pri), 3, dim_sAll), np.nan)
        prior_state[k1][:, 1] = def_state['v_dQ'][k].copy()
        for n, s_sel in enumerate(inv_s_sel):
            prior_state[k1][n, 0, s_sel] = (
                prior_state[k1][n, 1, s_sel]
                - (
                    np.load(f_pri[n], 'r')[k2].mean(0)
                    if os.path.exists(f_pri[n]) else
                    np.nan
                    )
                )
            prior_state[k1][n, 2, s_sel] = (
                prior_state[k1][n, 1, s_sel]
                + (
                    np.load(f_pri[n], 'r')[k2].mean(0)
                    if os.path.exists(f_pri[n]) else
                    np.nan
                    )
                )
        if sce:
            k3 = f'sce_v_{k}'
            prior_state[k3] = np.full((len(f_pri), 3, dim_sAll), np.nan)
            for n, s_sel in enumerate(inv_s_sel):
                prior_state[k3][n, 1, s_sel] = np.load(f_pri[n], 'r')[k3]
        if post:
            post_state[k1] = np.full((len(f_pri), 3, dim_sAll), np.nan)
            for n, s_sel in enumerate(inv_s_sel):
                post_state[k1][n][:, s_sel] = (
                    np.load(f_pos[n], 'r')[k1]
                    if os.path.exists(f_pos[n])
                    and k1 in np.load(f_pos[n], 'r').keys() else
                    np.nan
                    )

    return [
        obs_y, obs_y_err, prior_obs, post_obs,
        prior_state, post_state, def_state,
        dm0_s, dm1_s, dm2_s_pri, dm2_s_sce, dm2_s_pos,
        inv_reg_maps, reg_map_sel, inv_Q_dist,
        focus_rgroup_uniq, dim_sAll
        ]


def find_neighbours(array, region):
    y, x = np.where(array == region)
    ny, nx = (i-1 for i in array.shape)
    where = np.unique(np.concatenate([
        np.array([np.maximum(y-1, 0), x]),
        np.array([np.minimum(y+1, ny), x]),
        np.array([y, np.maximum(x-1, 0)]),
        np.array([y, np.minimum(x+1, nx)]),
        ], axis=1), axis=1)
    boundary = np.full(array.shape, False)
    boundary[tuple(where)] = True
    boundary[array == region] = False
    neighbours = np.unique(array[boundary])
    return neighbours, boundary

