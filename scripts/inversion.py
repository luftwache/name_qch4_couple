r"""

Inverse Model

"""
# Standard Library imports
import argparse
import datetime
import h5py
import json
import matplotlib.dates as mdates
import matplotlib.pyplot as plt
import netCDF4
import numpy as np
import os
import pandas as pd
import psutil
import pymc3 as pm
import re
import sys
import theano
import theano.tensor as tt
import xarray as xr

# Third party imports
from collections import OrderedDict

# Semi-local imports
import name_qch4_couple.io
import name_qch4_couple.name
import name_qch4_couple.plot
import name_qch4_couple.region_EU
import name_qch4_couple.routines
import name_qch4_couple.util


def sd2cov(sigma):
    r"""
    """
    cov = np.zeros((sigma.size, sigma.size))
    np.fill_diagonal(cov, sigma.flatten()**2)
    return cov


def check_memory(s):
    r"""
    """
    process = psutil.Process(os.getpid())
    rss = process.memory_info().rss / 1e6
    rssp = process.memory_percent()
    print(f'        {s:24s}\t{rss:14.3f} MB\t{rssp:14} %')


def calc_unc_mean(x):
    r"""Calculate uncertainties from a mean
    """
    if np.array(x).size == 0:
        return np.nan
    elif np.isfinite(x).all():
        return(np.nansum(x*x)**0.5/np.isfinite(x).sum())
    else:
        return np.nan


def regionQA2griddedQ(QA, area, region_grid):
    r"""
    """
    Q = np.zeros((QA.shape[0], *region_grid.shape))
    regions = np.unique(region_grid)
    for i, v in enumerate((QA/area).T):
        n = regions[i]
        Q[..., region_grid == n] = v.reshape(-1, 1)
    return Q


def calc_init_edge(chi0, d0i1, d0i2, chi0f, d0i1f, d0i2f, R_std):
    r"""
    d0i1[dates_nx_E4C]
    """
    # d0 (i, E)
    R0i1 = (d0i1 * d0i1f * 1.e-3 + 1) * R_std[1]
    R0i2 = (d0i2 * d0i2f * 1.e-3 + 1) * R_std[2]
    R0sum = 1 + R0i1 + R0i2
    # chi0 (i, E)
    chi0i0 = (chi0 * chi0f * (1. / R0sum))
    chi0i1 = (chi0 * chi0f * (R0i1 / R0sum))
    chi0i2 = (chi0 * chi0f * (R0i2 / R0sum))
    return chi0i0, chi0i1, chi0i2


def calc_init_site(chi0i0, chi0i1, chi0i2, E, idx_bin_site, R_std):
    r"""
    """
    i0 = np.array([np.bincount(idx_bin_site, i*E) for i in chi0i0])
    i1 = np.array([np.bincount(idx_bin_site, i*E) for i in chi0i1])
    i2 = np.array([np.bincount(idx_bin_site, i*E) for i in chi0i2])
    chi0 = i0 + i1 + i2
    d0i1 = (i1 / i0 / R_std[1] - 1) * 1.e3
    d0i2 = (i2 / i0 / R_std[2] - 1) * 1.e3
    return chi0, d0i1, d0i2 


def create_h(dimt, idx, hdef, hlower, hupper):
    r"""
    """
    unc = np.concatenate([np.full([1, dimt], i) for i in hdef], axis=0)
    unc[:, idx] = [[hlower], [hupper]]
    return unc


# =============================================================================
#   Settings
#       Qtype
#           0: EDGAR + Natural
#           1: EDGAR scaled to UNFCCC + Natural
#           2: EDGAR-NAEI + Natural
#           3: EDGAR-NAEI scaled to UNFCCC + Natural
#           4: (EDGAR-NAEI + Natural) scaled to InTEM posteriors
#           9: Posteriors as priors
# =============================================================================
print('Start Process')

# Argument Parser
parser = argparse.ArgumentParser()
parser.add_argument("-date", required=True)
parser.add_argument("-itype", required=True, type=int)
parser.add_argument("-mode", required=True, type=int)
parser.add_argument("-force", required=True, type=int)
parser.add_argument("-Qtype", required=True, type=int)
parser.add_argument("-idir", required=True)
parser.add_argument("-odir", required=True)
args = parser.parse_args()

date = args.date
itype = args.itype
mode = args.mode
if args.force:
    force_compute = True
else:
    force_compute = False
Qtype = args.Qtype
idir = args.idir
if idir is None and Qtype == 9:
    raise Exception('Input directory is required if Qtype is 9.')
odir = args.odir

# Run mode
inversion = False
prior_only = False
if mode == 0:
    prior_only = True
elif mode == 1:
    inversion = True

# Obsertion sites and variables
sites = {
    'mhd': ['chi', 'd_13C'],
    'cbw': ['chi', 'd_13C', 'd_2H'],
    'tac': ['chi'],
    }
y_vars = ['chi', 'd_13C', 'd_2H']
theta_vars = [
    'chi0',
    'd0i1', 'd0i2',
    'QA',
    'dQ',
    #'dQi1', 'dQi2'
    ]

# Baseline site
site_b = 'mhd'

# Inversion sample size
pm_tune = 5000
pm_draws = 10000
pm_cores = 2
pm_chains = 2
pm_seed = 1

# Constant to convert dosage*QA/m to the unit of mixing ratio
chi_scale = 1.e3

# Legacy
p = 101325  # Pa
T = 288.15  # K

# Read station info
field_dirs = OrderedDict()
particle_dirs = OrderedDict()
obs_files = OrderedDict()
long_names = OrderedDict()
locations = OrderedDict()
field_prefix = OrderedDict()
particle_prefix = OrderedDict()
for site in sites:
    with open(f'stations/{site}.json', 'r') as f:
        st_info = json.load(f)
        field_dirs[site] = OrderedDict(st_info['mix_dir'])
        particle_dirs[site] = OrderedDict(st_info['particle_dir'])
        obs_files[site] = OrderedDict(st_info['obs_files'])
        long_names[site] = st_info['long_name']
        locations[site] = st_info['location']
        field_prefix[site] = OrderedDict(st_info['mix_prefix'])
        particle_prefix[site] = OrderedDict(st_info['particle_prefix'])

baseline_files = {
    'chi': [f'baseline/{site_b}.hdf', 'chi'],
    'd_13C': [f'baseline/{site_b}.hdf', 'd_13C'],
    'd_2H': [f'baseline/{site_b}.hdf', 'd_2H']
    }
unfccc_file = r'/home/ec5/hpc-work/data_archive/unfccc/ch4.csv'

# Prior files
"""
0   combustion energy
1   combustion other
2   combustion industry
3   industry production
4   fossil fuel production
5   road transport
6   other transport
7   waste
8   agriculture
9   biomass
10  wetland
"""
Q_files = OrderedDict([
    (0, ['priors/prior_edgar_v5_0_SNAP01.nc', 'CH4_emissions', '1Y']),
    (1, ['priors/prior_edgar_v5_0_SNAP02.nc', 'CH4_emissions', '1Y']),
    (2, ['priors/prior_edgar_v5_0_SNAP03.nc', 'CH4_emissions', '1Y']),
    (3, ['priors/prior_edgar_v5_0_SNAP04.nc', 'CH4_emissions', '1Y']),
    (4, ['priors/prior_edgar_v5_0_SNAP05.nc', 'CH4_emissions', '1Y']),
    (5, ['priors/prior_edgar_v5_0_SNAP07.nc', 'CH4_emissions', '1Y']),
    (6, ['priors/prior_edgar_v5_0_SNAP08.nc', 'CH4_emissions', '1Y']),
    (7, ['priors/prior_edgar_v5_0_SNAP09.nc', 'CH4_emissions', '1Y']),
    (8, ['priors/prior_edgar_v5_0_SNAP10.nc', 'CH4_emissions', '1Y']),
    (9, ['priors/prior_gfed.nc', 'CH4_emissions', '1M']),
    (10, ['priors/prior_wetcharts.nc', 'CH4_emissions', '1M'])
    ])
# Prior signatures
prior_dQ = {
    0: [0.0, -41.9, -177.3],#, -209.4, -317.5],
    1: [0.0, -41.9, -177.3],#, -209.4, -317.5],
    2: [0.0, -41.9, -177.3],#, -209.4, -317.5],
    3: [0.0, -41.9, -177.3],#, -209.4, -317.5],
    4: [0.0, -41.9, -177.3],#, -209.4, -317.5],
    5: [0.0, -41.9, -177.3],#, -209.4, -317.5],
    6: [0.0, -41.9, -177.3],#, -209.4, -317.5],
    7: [0.0, -55.0, -310.0],#, -373.3, -551.8],
    8: [0.0, -61.9, -324.2],#, -360.6, -535.0],
    9: [0.0, -24.6, -225.0],#, -241.8, -394.8],
    10: [0.0, -58.1, -338.1],#, -373.3, -551.8],        }
    }
Q_cat = OrderedDict([
    (0, 0),
    (1, 0),
    (2, 0),
    (3, 0),
    (4, 0),
    (5, 0),
    (6, 0),
    (7, 1),
    (8, 2),
    (9, 3),
    (10, 4),
    ])

# =============================================================================
#   Preprocessing
# =============================================================================
print(f'Initialising')
print(f'  site  : {sites}')
print(f'  date  : {date}')
print(f'  itype : {itype}')
print(f'  Qtype : {Qtype}')

# Constants
R_std = np.array([
    1.0,
    1.12372000e-02,
    6.23040000e-04,
    7.00122509e-06,
    1.45567066e-07
    ])
M = np.array([
    16.031300,
    17.034655,
    17.037577,
    18.040932,
    18.043854
    ])

# Dates
if itype in [0, 1]:
    dates_fH = pd.date_range(
        f"{date}-01" if not itype else f"{date}-16",
        (
            pd.to_datetime(f"{date}-01") + pd.DateOffset(days=15)
            if not itype else
            pd.to_datetime(f"{date}-01") + pd.DateOffset(months=1)
            ),
        freq='1H',
        closed='left'
        )
elif itype in [2, 3]:
    dates_fH = pd.date_range(
        f"{date}-01",
        pd.to_datetime(f"{date}-01") + pd.DateOffset(months=1),
        freq='1H',
        closed='left'
        )
if itype not in [3]:
    dates_H = dates_fH
elif itype in [3]:
    dates_H = pd.date_range(
        f"{date}-01",
        pd.to_datetime(f"{date}-01") + pd.DateOffset(months=1),
        freq='1H',
        closed='left'
        ).to_series().between_time('0900', '1500').index
dates_d = dates_H.floor('D').unique()

check_memory('Memory 0')

# Observations
obs = OrderedDict()
obs_err = OrderedDict()
dr = [dates_d[i].strftime('%Y-%m-%d') for i in [0, -1]]
for site, y_vars in sites.items():
    obs[site] = {}
    obs_err[site] = {}
    if site == 'cbw':
        date_parser = lambda s: datetime.datetime.strptime(s,'%d.%m.%Y %H:%M')
        idata1 = pd.read_csv(
            obs_files[site]['d_13C'],
            sep='\t', skipinitialspace=True,
            parse_dates=['CBW_d13C_Time'], date_parser=date_parser,
            index_col=['CBW_d13C_Time']
            )[dr[0]:dr[1]]
        idata2 = pd.read_csv(
            obs_files[site]['d_2H'],
            sep='\t', skipinitialspace=True,
            parse_dates=['CBW_dD_Time'], date_parser=date_parser,
            index_col=['CBW_dD_Time']
            )[dr[0]:dr[1]]
        if 'chi' in y_vars:
            obs[site]['chi'] = pd.concat(
                [idata1['CBW_d13C_Conc'], idata2['CBW_dD_Conc']],
                sort=True
                ).groupby(level=0).first().to_frame('chi')
            obs_err[site]['chi'] = pd.concat(
                [idata1['CBW_d13C_ConcErr'], idata2['CBW_dD_ConcErr']],
                sort=True
                ).groupby(level=0).first()
        if 'd_13C' in y_vars:
            obs[site]['d_13C'] = (
                    idata1['CBW_d13C_Delta'].groupby(level=0).first()
                    )
            obs_err[site]['d_13C'] = (
                    idata1['CBW_d13C_DeltaErr'].groupby(level=0).first()
                    )
        if 'd_2H' in y_vars:
            obs[site]['d_2H'] = (
                    idata2['CBW_dD_Delta'].groupby(level=0).first()
                    )
            obs_err[site]['d_2H'] = (
                    idata2['CBW_dD_DeltaErr'].groupby(level=0).first()
                    )
    else:
        if 'chi' in y_vars:
            obs[site]['chi'] = (
                name_qch4_couple.io.r_agage(
                    obs_files[site]['chi']
                    )[dr[0]:dr[1]]
                * 1.0003
                )
            obs_err[site]['chi'] = pd.Series(
                    0.2, index=obs[site]['chi'].index
                    )
        if 'd_13C' in y_vars:
            obs[site]['d_13C'] = name_qch4_couple.io.r_instaar(
                    obs_files[site]['d_13C'], 'event'
                    )[dr[0]:dr[1]]
            obs_err[site]['d_13C'] = pd.Series(
                    .1, index=obs[site]['d_13C'].index
                    )
        if 'd_2H' in y_vars:
            obs[site]['d_2H'] = name_qch4_couple.io.r_instaar(
                    obs_files[site]['d_2H'], 'event'
                    )[dr[0]:dr[1]]
            obs_err[site]['d_2H'] = pd.Series(
                    2., index=obs[site]['d_2H'].index
                    )

# Footprint files
## Dosage
f_fields_paths = {
    site:
    pd.concat(
        [
            dates_d.strftime(
                os.path.join(v[k], field_prefix[site][k])
                ).to_series(index=dates_d).apply(np.vectorize(
                    lambda x: x if os.path.exists(x) else False
                    ))
            for k in sorted(v)
            ],
        axis=1
        ).replace(False, np.nan)
    for site, v in field_dirs.items()
    }
f_fields_path = {
    site:
    v.bfill(axis=1).iloc[:, 0].replace(np.nan, False)
    for site, v in f_fields_paths.items()
    }
## Particles
f_particles_paths = {
    site:
    pd.concat(
        [
            dates_d.strftime(
                os.path.join(v[k], particle_prefix[site][k])
                ).to_series(index=dates_d).apply(np.vectorize(
                    lambda x: x if os.path.exists(x) else False
                    ))
            for k in sorted(v)
            ],
        axis=1
        ).replace(False, np.nan)
    for site, v in particle_dirs.items()
    }
f_particles_path = {
    site:
    v.bfill(axis=1).iloc[:, 0].replace(np.nan, False)
    for site, v in f_particles_paths.items()
    }

# Date index
dates_foot = {
    site:
    pd.concat(
        [f_fields_path[site], f_particles_path[site]], axis=1
        ).replace(False, np.nan).dropna().index
    for site in sites
    }
dates_obs = { #dates_obs
    site: {
        k:
        v.loc[date:date].resample('H').mean().dropna().index.to_series().apply(
            np.vectorize(
                lambda x: x if x.date() in dates_foot[site].date else pd.NaT
                )
            ).reindex(index=dates_H).dropna().index
        for k, v in y.items()
        }
    for site, y in obs.items()
    }
'''
dates_obs = { #dates_obs
    site: {
        k: v.loc[date:date].resample('H').mean().dropna().index
        for k, v in y.items()
        }
    for site, y in obs.items()
    }
'''
dates_site = { #dates_site
    site: pd.DatetimeIndex(np.unique(np.concatenate([v for v in d.values()])))
    for site, d in dates_obs.items()
    }
dates_nx_obs4H = { #dates_nx_obs4H
    site: {
        k: dates_H.get_indexer(v)
        for k, v in d.items()
        }
    for site, d in dates_obs.items()
    }
dates_nx_site4H = { #dates_nx_site4H
    site: dates_H.get_indexer(d)
    for site, d in dates_site.items()
    }
dates_nx_site4fH = { #dates_nx_site4fH
    site: dates_fH.get_indexer(d)
    for site, d in dates_site.items()
    }
dates_C = pd.DatetimeIndex( # dates_C
    np.unique(np.concatenate([d for d in dates_site.values()]))
    )
dates_nx_obs4C = { #dates_nx_obs4C
    site: {
        k: dates_C.get_indexer(v)
        for k, v in d.items()
        }
    for site, d in dates_obs.items()
    }
dates_nx_site4C = { #dates_nx_site4C
    site: dates_C.get_indexer(d)
    for site, d in dates_site.items()
    }
dates_nx_C4H = dates_H.get_indexer(dates_C)
dates_nx_obs4site = {
    site: {
        k: dates_site[site].get_indexer(v)
        for k, v in d.items()
        }
    for site, d in dates_obs.items()
    }
dimt = dates_C.size

# Regularise time axis of observations
p_obs = {
    site: {
        k:
        np.array(
            v.loc[date:date].resample('H').mean().reindex(dates_obs[site][k])
            ).flatten()
        for k, v in y.items()
        }
    for site, y in obs.items()
    }
p_obs_err = {
    site: {
        k:
        np.array(
            v.loc[date:date].resample('H').apply(calc_unc_mean).reindex(
                dates_obs[site][k])
            ).flatten()
        if v is not None else
        None
        for k, v in y.items()
        }
    for site, y in obs_err.items()
    }

check_memory('After obs')

# Grid
lon0 = -98.07600
lat0 = 10.61200
dlon = 0.3520000
dlat = 0.2340000
nlon = 391
nlat = 293
grid = [
    np.linspace(lon0 + dlon/2, lon0 + dlon*(nlon - 0.5), nlon),
    np.linspace(lat0 + dlat/2, lat0 + dlat*(nlat - 0.5), nlat)
    ]
grid2 = [
    np.linspace(lon0, lon0 + dlon*nlon, nlon+1),
    np.linspace(lat0, lat0 + dlat*nlat, nlat+1)
    ]
area = name_qch4_couple.name.calc_cell_area(
    grid2[1][:-1],
    grid2[1][1:],
    dlon
    )[:, np.newaxis]

## Inversion grid
inv_lon0 = 238
inv_lat0 = 110
inv_nlon = 128
inv_nlat = 128
inv_lon1 = inv_lon0 + inv_nlon
inv_lat1 = inv_lat0 + inv_nlat
lon_idx, lat_idx = np.meshgrid(np.arange(nlon), np.arange(nlat))
inv_mask = (
    (inv_lon0 > lon_idx) | (lon_idx >= inv_lon1) |
    (inv_lat0 > lat_idx) | (lat_idx >= inv_lat1)
    )
inv_lon_s_ = np.s_[inv_lon0:inv_lon1]
inv_lat_s_ = np.s_[inv_lat0:inv_lat1]
inv_area = area[inv_lat_s_]

## Inversion regions
c_land = [
    'gb_wls', 'gb_sct', 'gb_nir', 'gb_eng', 'ie',
    'fr0', 'fr1', 'benllu',
    'de', 'dk', 'espt', 'no', 'NEEU',
    'it0', 'it1', 'chat', 'SEEU', 'fo', 'is'
    ]
c_sea = [
    'seaEnCh', 'seaIrSe',
    'seaAtS', 'seaBaBi', 'seaAtML', 'seaAtMM', 'seaAtMH',
    'seaAtNWo', 'seaAtNNo', 'seaAtNE', 'seaSk', 'seaNoSe',
    'seaMdNW', 'seaMdS', 'seaMdE', 'seaIo', 'seaAd'
    ]
c_regs = {
    'gb_eng': ['gb_eng'],  #
    'gb_sct': ['gb_sct'],
    'gb_wls': ['gb_wls'],
    'gb_nir': ['gb_nir'],
    'ie' : ['ie'],
    'fr0': ['fr0'],  #
    'fr1': ['fr1'],
    'benllu': ['benllu'],
    'de': ['de'],
    'dk': ['dk'],
    'espt': ['espt'],  #
    'no': ['no'],
    'NEEU': ['NEEU'],
    'it0': ['it0'],
    'it1': ['it1'],
    'chat': ['chat'],  #
    'SEEU': ['SEEU'],
    'fo': ['fo'],
    'is': ['is'],
    'seaAtNWo': ['seaAtNWo', 'seaAtMH', 'seaAtMM'],  #
    'seaAtNNo': ['seaAtNNo', 'seaAtNE'],
    'seaAtS': ['seaAtS', 'seaAtML', 'seaBaBi', 'seaIrSe'],
    'seaNoSe': ['seaNoSe', 'seaSk', 'seaEnCh'],
    'seaMdE': ['seaMdE', 'seaMdNW', 'seaIo', 'seaAd'],
    'seaMdS': ['seaMdS']  #
    }
c_regs_T = {i: k for k, v in c_regs.items() for i in v}
c_names = {
    'gb': ['United Kingdom of Great Britain and Northern Ireland'],
    'ie': ['Ireland'],
    'fr': ['France'],
    'benllu': ['Belgium', 'Netherlands', 'Luxembourg'],
    'de': ['Germany'],
    'dk': ['Denmark'],
    'espt': ['Spain', 'Portugal'],
    'no': ['Norway'],
    'NEEU': ['NE Europe'],
    'SEEU': ['SE Europe'],
    'it': ['Italy'],
    'chat': ['Switzerland', 'Austria']
    }
c_fill = -1.
c_grid1 = np.full((nlat, nlon), c_fill)
c_grid0 = np.full((inv_nlat, inv_nlon), 0, dtype=np.int16)
c_cats = {}
for k, v in name_qch4_couple.region_EU.intem_regions(c_regs_T).items():
    if v:
        name_qch4_couple.region_EU.allocate_lonlat(
            c_grid0,
            np.array(v['lon']),
            np.array(v['lat']),
            v['cat']
            )
        c_cats[k] = v['cat']
c_cats['SEEU'] = 0
c_cats_T = {v: k for k, v in c_cats.items()}
c_grid1[inv_lat_s_, inv_lon_s_] = c_grid0

inv_regs = np.full((inv_nlat, inv_nlon), 0, dtype=np.int16)
for k, v in c_regs_T.items():
    inv_regs[c_grid0 == c_cats[k]] = c_cats[v]

inv_regs_c = np.unique([v for k, v in c_cats.items() if k in c_regs])
inv_regs_c_cats = np.array([c_cats_T[i] for i in inv_regs_c])
inv_regs_c_land = np.array([
    (not c_cats_T[i].startswith('sea')) for i in inv_regs_c
    ])
inv_regs_c_sea = np.array([
    (c_cats_T[i].startswith('sea')) for i in inv_regs_c
    ])

inv_regs_A = np.array([
    (
        np.ones((inv_nlat, inv_nlon))*inv_area
        )[..., inv_regs == inv_reg].sum(-1)
    for inv_reg in np.unique(inv_regs)
    ]).T

check_memory('After grid')

# Dimensions
dims = max(Q_cat.values())+1
dimy = nlat
dimx = nlon
dimr = np.unique(inv_regs).size

# Particles
diml = 12
p_end = {
    site:
    np.concatenate(
        [
            name_qch4_couple.name.cat_end_loc(
                name_qch4_couple.io.r_name_particles(f),
                grid2, np.arange(24), 3.e3, 8.e3
                )[0]
            if f else
            np.zeros((24, 18))
            for f in fs
            ],
        axis=0
        )[dates_nx_site4fH[site]]
    for site, fs in f_particles_path.items()
    }
end_loc = {site: np.zeros((v.shape[0], diml)) for site, v in p_end.items()}
for site, v in p_end.items():
    end_loc[site][:, :9] = v[:, :9]
    end_loc[site][:, 9] = v[:, [9, 10, 15, 16]].sum(1)
    end_loc[site][:, 10] = v[:, [11, 12, 13, 14]].sum(1)
    end_loc[site][:, 11] = v[:, 17]
check_memory('After particles')

## Overlap between particle end locations and baseline
idx0_site = {site: np.full((dates_C.size, diml), False) for site in sites}
for site, v in end_loc.items():
    idx0_site[site][dates_nx_site4C[site]] = (v != 0)
idx0 = np.array([v for v in idx0_site.values()]).any(0)
dimE = idx0.sum()

dates_nx_E4C = np.tile(np.arange(dates_C.size).reshape(-1, 1), diml)[idx0]
dates_nx_site4E = {  # index of site in E
    site: np.arange(dimE)[v[idx0]]
    for site, v in idx0_site.items()
    }
dates_nx_E4site = {  # time binned for bincount
    site: np.tile(np.arange(v.shape[0]).reshape(-1, 1), (1, diml))[v != 0]
    for site, v in end_loc.items()
    }
E = {site: (v/v.sum(1, keepdims=True))[v != 0] for site, v in end_loc.items()}

# Prior Baseline
## Values
variables = ['chi', 'd_13C', 'd_2H']
baseline = pd.DataFrame(np.nan, index=dates_H, columns=variables)
for k, v in baseline_files.items():
    baseline[k].update(pd.read_hdf(v[0], v[1]))
    if baseline[k].isnull().all():
        if dates_H[0] > pd.read_hdf(v[0], v[1]).index[-1]:
            idx = -1
        elif dates_H[-1] < pd.read_hdf(v[0], v[1]).index[0]:
            idx = 0
        baseline[k] = pd.read_hdf(v[0], k).iloc[idx]
baseline.interpolate('linear', limit_direction='both', inplace=True)
chi0 = np.array(baseline['chi']).flatten()
d0i1 = np.array(baseline['d_13C']).flatten()
d0i2 = np.array(baseline['d_2H']).flatten()
d0i2 = np.full(np.array(baseline['d_2H']).flatten().shape, -86)
if Qtype == 9:
    postf = os.path.join(idir, 'post_theta.npz')
    keys = [k for k in np.load(postf, 'r').keys()]
    if 'p_chi0' in keys:
        chi0[dates_nx_C4H] = np.load(postf, 'r')['p_chi0'][1]
    if 'p_d0i1' in keys:
        d0i1[dates_nx_C4H] = np.load(postf, 'r')['p_d0i1'][1]
    if 'p_d0i2' in keys:
        d0i2[dates_nx_C4H] = np.load(postf, 'r')['p_d0i2'][1]
## Hyperparameters
inv_chi0_h = create_h(
    dimt=dates_H.size,
    idx=np.unique(np.concatenate([
        dates_nx_obs4H[site]['chi'] for site in sites
        if 'chi' in dates_nx_obs4C[site]
        ])),
    **{'hdef': [1.5, 4.5], 'hlower': 1.5, 'hupper': 4.5}
    )
inv_d0i1_h = create_h(
    dimt=dates_H.size,
    idx=np.unique(np.concatenate([
        dates_nx_obs4H[site]['d_13C'] for site in sites
        if 'd_13C' in dates_nx_obs4C[site]
        ])),
    **{'hdef': [1.e-1, 3.e-1], 'hlower': 0.1, 'hupper': 0.3},
    )
inv_d0i2_h = create_h(
    dimt=dates_H.size,
    idx=np.unique(np.concatenate([
        dates_nx_obs4H[site]['d_2H'] for site in sites
        if 'd_2H' in dates_nx_obs4C[site]
        ])),
    **{'hdef': [4.0, 8.0], 'hlower': 4.0, 'hupper': 8.0},
    )
## Scale for each particle end locations
inv_chi0f_u = create_h(
    dimt=dimt,
    idx=np.unique(np.concatenate([
        dates_nx_obs4C[site]['chi'] for site in sites
        if 'chi' in dates_nx_obs4C[site]
        ])),
    **{'hdef': [1-1.e-2, 1+1.e-2], 'hlower': 0.8, 'hupper': 1.2}
    )[:, dates_nx_E4C]
inv_d0i1f_u = create_h(
    dimt=dimt,
    idx=np.unique(np.concatenate([
        dates_nx_obs4C[site]['d_13C'] for site in sites
        if 'd_13C' in dates_nx_obs4C[site]
        ])),
    **{'hdef': [1-1.e-2, 1+1.e-2], 'hlower': 0.8, 'hupper': 1.2}
    )[:, dates_nx_E4C]
inv_d0i2f_u = create_h(
    dimt=dimt,
    idx=np.unique(np.concatenate([
        dates_nx_obs4C[site]['d_2H'] for site in sites
        if 'd_2H' in dates_nx_obs4C[site]
        ])),
    **{'hdef': [1-1.e-2, 1+1.e-2], 'hlower': 0.8, 'hupper': 1.2}
    )[:, dates_nx_E4C]
if Qtype == 9:
    inv_chi0f_u += (
        -1. + np.load(os.path.join(idir, 'post_theta.npz'), 'r')['p_chi0f'][1]
        )
    inv_d0i1f_u += (
        -1. + np.load(os.path.join(idir, 'post_theta.npz'), 'r')['p_d0i1f'][1]
        )
    inv_d0i2f_u += (
        -1. + np.load(os.path.join(idir, 'post_theta.npz'), 'r')['p_d0i2f'][1]
        )
## Calculate prior baseline for each site
prior_chi0i0, prior_chi0i1, prior_chi0i2 = calc_init_edge(
    chi0=chi0[dates_nx_C4H][np.newaxis, dates_nx_E4C],
    d0i1=d0i1[dates_nx_C4H][np.newaxis, dates_nx_E4C],
    d0i2=d0i2[dates_nx_C4H][np.newaxis, dates_nx_E4C],
    chi0f=inv_chi0f_u.mean(0),
    d0i1f=inv_d0i1f_u.mean(0),
    d0i2f=inv_d0i2f_u.mean(0),
    R_std=R_std,
    )
prior_baseline = {}
for site in sites:
    (
        prior_baseline[f'{site}_p_chi0'],
        prior_baseline[f'{site}_p_d0i1'],
        prior_baseline[f'{site}_p_d0i2']
        ) = calc_init_site(
            chi0i0=prior_chi0i0[:, dates_nx_site4E[site]],
            chi0i1=prior_chi0i1[:, dates_nx_site4E[site]],
            chi0i2=prior_chi0i2[:, dates_nx_site4E[site]],
            E=E[site],
            idx_bin_site=dates_nx_E4site[site],
            R_std=R_std
            )
check_memory('After baseline')

# Prior Emissions
Q = np.zeros((max(Q_cat.values())+1, nlat, nlon))
for k, v in Q_files.items():
    with xr.open_dataset(v[0]) as ds_read:
        Q_in = ds_read.load()
        t_Q = Q_in['time']
        if v[2] == '1Y':
            t = np.datetime64(dates_d[0].floor('d') - pd.offsets.YearBegin(1))
            t_in = min(t_Q, key=lambda x: abs(x - t))
        else:
            t = np.datetime64(dates_d[0].floor('d') - pd.offsets.MonthBegin(1))
            t_in = min(
                    t_Q[t_Q.dt.month==dates_d[0].month],
                    key=lambda x: abs(x - t)
                    )
        Q[Q_cat[k]] += Q_in[v[1]].sel(time=t_in).values * 1.e3  # kg -> g

## Substitutions and scaling
if Qtype > 0 and Qtype < 9:
    if Qtype in [2, 3]:
        # Replace NAEI pixels
        naei_files = OrderedDict([
            (1, ['priors/prior_edgar_v5_0_SNAP01.nc', 'CH4_emissions', '1Y']),
            (2, ['priors/prior_edgar_v5_0_SNAP02.nc', 'CH4_emissions', '1Y']),
            (3, ['priors/prior_edgar_v5_0_SNAP03.nc', 'CH4_emissions', '1Y']),
            (4, ['priors/prior_edgar_v5_0_SNAP04.nc', 'CH4_emissions', '1Y']),
            (5, ['priors/prior_edgar_v5_0_SNAP05.nc', 'CH4_emissions', '1Y']),
            (6, ['priors/prior_edgar_v5_0_SNAP07.nc', 'CH4_emissions', '1Y']),
            (7, ['priors/prior_edgar_v5_0_SNAP08.nc', 'CH4_emissions', '1Y']),
            (8, ['priors/prior_edgar_v5_0_SNAP09.nc', 'CH4_emissions', '1Y']),
            ])
        c_grid_gb = np.isin(
            c_grid1, [v for k, v in c_cats.items() if k.startswith('gb')]
            )
        for k, v in naei_files.items():
            with xr.open_dataset(v[0]) as ds_read:
                Q_in = ds_read.load()
                t_Q = Q_in['time']
                if v[2] == '1Y':
                    t = np.datetime64(
                        dates_d[0].floor('d') - pd.offsets.YearBegin(1)
                        )
                    t_in = min(t_Q, key=lambda x: abs(x - t))
                else:
                    t = np.datetime64(
                        dates_d[0].floor('d') - pd.offsets.MonthBegin(1)
                        )
                    t_in = min(
                        t_Q[t_Q.dt.month==dates_d[0].month],
                        key=lambda x: abs(x - t)
                        )
                Q[k, c_grid_gb] = Q_in[v[1]].sel(time=t_in)[..., c_grid_gb] * 1.e3
    if Qtype in [1, 3]:
        # Scale to UNFCCC totals
        c_codes = [
            'gb', 'ie', 'fr', 'benllu',
            'de', 'dk', 'espt', 'no', 'it', 'chat'
            ]
        c_grid2 = {
            c_code:
            np.isin(
                c_grid1,
                [v for k, v in c_cats.items() if k.startswith(c_code)]
                )
            for c_code in c_codes
            }
        unfccc_cat = 'Total GHG emissions without LULUCF'
        unfccc = name_qch4_couple.io.r_unfccc(unfccc_file)
        unfccc_time = [
            i for i in unfccc.columns if name_qch4_couple.util.if_int(i)
            ]
        t = dates_d[0].year
        t_in = min(unfccc_time, key=lambda x: abs(int(x) - int(t)))
        QA_unfccc = pd.Series(0., index=c_codes)
        for k in c_codes:
            QA_unfccc[k] = unfccc.loc[
                unfccc['Party'].isin(c_names[k]) &
                (unfccc['Category'] == unfccc_cat),
                t_in
                ].sum(0) * 1.e6  # t y-1 -> g y-1
        for k, v in c_grid2.items():
            v1 = np.array([*[v]*(dims-2), *np.full((2, *v.shape), False)])
            scale_nom = QA_unfccc.loc[k]
            scale_denom = (Q*area*86400*365.25)[v1].sum()
            if not np.isfinite(scale_nom) or not np.isfinite(scale_denom):
                continue
            if scale_denom != 0:
                Q[v1] *= scale_nom/scale_denom
            else:
                Q[v1] += scale_nom/(v*area).sum()
    else:  # InTEM
        pass
if Qtype != 9:
    ## Inversion QA
    inv_QA_scale = 1.e-3  # Scaling to avoid very small/large number
    inv_QA_gridded = Q[..., inv_lat_s_, inv_lon_s_] * inv_area
    inv_QA = np.array([
            inv_QA_gridded[..., inv_regs == inv_reg].sum(-1)
            for inv_reg in np.unique(inv_regs)
            ]).T * inv_QA_scale
    inv_QA_unc_min_gridded = np.tile(
            1.e-3 / 365.25 / 86400. * inv_area,  # 1 mg m-2 yr-1
            (1, inv_nlon)
            ) * inv_QA_scale
    inv_QA_unc = np.maximum(
            inv_QA_gridded * 0.1,
            inv_QA_unc_min_gridded
            )
    inv_QA_unc0 = np.array([
            inv_QA_unc[..., inv_regs == inv_reg].sum(-1)
            #(inv_QA_unc[..., inv_regs == inv_reg]**2).sum(-1)**0.5
            for inv_reg in np.unique(inv_regs)
            ]).T * inv_QA_scale
    inv_QA_unc1 = np.array([
            (inv_QA_unc*3.0)[..., inv_regs == inv_reg].sum(-1)
            #((inv_QA_unc*4.0)[..., inv_regs == inv_reg]**2).sum(-1)**0.5
            for inv_reg in np.unique(inv_regs)
            ]).T * inv_QA_scale
    inv_QA_h = np.array([inv_QA_unc0, inv_QA_unc1])
else:
    try:
        inv_QA_scale = np.load(
            os.path.join(odir, 'constants.npz'), 'r'
            )['inv_QA_scale']
    except:
        inv_QA_scale = 50.
    Q[..., inv_lat_s_, inv_lon_s_] = regionQA2griddedQ(
        np.load(os.path.join(idir, 'post_theta.npz'), 'r')['p_QA'][1],
        inv_regs_A, inv_regs
        ) / inv_QA_scale
    inv_QA_unc0, inv_QA, inv_QA_unc1 = (
        np.load(os.path.join(idir, 'post_theta.npz'), 'r')['p_QA']
        / inv_QA_scale
        )
    inv_QA_h = np.array([inv_QA_unc0, inv_QA_unc1])
dimr = np.unique(inv_regs).size

# Prior Emission Signatures
if Qtype != 9:
    dQ = np.array([prior_dQ[s] for s in range(len(prior_dQ)) if s > 5])
else:
    dQ = np.full((dims, 3), 0.)
    dQ[:, [1, 2]] = np.load(
        os.path.join(idir, 'post_theta.npz'), 'r'
        )['p_dQ'][1].reshape(-1, dims).T
dQi1 = dQ[:, 1].reshape(dims, 1, 1)
dQi2 = dQ[:, 2].reshape(dims, 1, 1)
inv_dQi1 = dQi1.reshape(dims, 1)
inv_dQi2 = dQi2.reshape(dims, 1)
inv_dQi1_h = np.abs(np.array([inv_dQi1 * 0.1, inv_dQi1 * 0.3]))
inv_dQi2_h = np.abs(np.array([inv_dQi2 * 0.1, inv_dQi2 * 0.3]))
inv_dQ = np.concatenate([inv_dQi1.flatten(), inv_dQi2.flatten()])
inv_dQ_h = np.concatenate([inv_dQi1_h, inv_dQi2_h], axis=1).reshape(2, -1)

check_memory('After Q')

# Dosage
inv_dilution_factor = {}
inv_diluted0_i0 = {}
inv_diluted0_i1 = {}
inv_diluted0_i2 = {}
y0_hres_i0 = {}
y0_hres_i1 = {}
y0_hres_i2 = {}
for site, fs in f_fields_path.items():
    dosage = np.concatenate(
        [
            name_qch4_couple.name.txt2array(
                name_qch4_couple.io.r_name(f),
                [nlat, nlon], np.arange(24), 0.
                )[:, np.newaxis]
            if f else
            np.zeros((24, 1, nlat, nlon))
            for f in fs
            ],
        axis=0
        )[dates_nx_site4fH[site]]
    """
    if site == 'cbw':
        dos_radius = 4
        dos_scale_max = 5
        st_idx = [
            np.abs(grid[i] - l).argmin()
            for i, l in enumerate(locations[site])
            ]
        # 0 = local, 1 = ext. local, 2 = rest, a = all
        dos_sel0 = np.full((nlat, nlon), False)
        dos_sel1 = np.full((nlat, nlon), False)
        dos_sel2 = np.full((nlat, nlon), True)
        dos_sel0[
            st_idx[1]-dos_radius:st_idx[1]+dos_radius+1,
            st_idx[0]-dos_radius:st_idx[0]+dos_radius+1,
            ] = True
        dos_sel1[
            st_idx[1]-(3*dos_radius+1):st_idx[1]+(3*dos_radius+1)+1,
            st_idx[0]-(3*dos_radius+1):st_idx[0]+(3*dos_radius+1)+1,
            ] = True
        dos_sel1[dos_sel0] = False
        dos_sel2[dos_sel0] = False
        dos_sel2[dos_sel1] = False
        dos_a = dosage.sum((1, 2, 3))
        dos_0 = dosage[..., dos_sel0].sum((1, 2)) 
        dos_1 = dosage[..., dos_sel1].sum((1, 2)) 
        dos_2 = dosage[..., dos_sel2].sum((1, 2)) 
        dos_scale_0 = np.minimum(
            dos_scale_max,
            dos_a/(dos_0 + dos_1/2)
            )
        dos_scale_1 = dos_scale_0 / 2
        dos_scale_2 = (
            (dos_a - dos_0*dos_scale_0 - dos_1*dos_scale_1)
            / (dos_2)
            )
        print(dosage.sum((1,2,3)))
        dosage[..., dos_sel0] *= dos_scale_0.reshape(-1, 1, 1)
        dosage[..., dos_sel1] *= dos_scale_1.reshape(-1, 1, 1)
        dosage[..., dos_sel2] *= dos_scale_2.reshape(-1, 1, 1)
        print(dosage.sum((1,2,3)))
    """
    inv_dilution_matrix = (
        dosage[:, 0, inv_lat_s_, inv_lon_s_] * inv_area / 3600.
        )
    inv_dilution_factor[site] = (
        chi_scale  # ratio scale e.g. ppm conversion
        * np.array([
            inv_dilution_matrix[..., inv_regs == inv_reg].sum(-1)
            for inv_reg in np.unique(inv_regs)
            ]).T  # summed dilution matrix
        / inv_regs_A  # area average of dilution matrix
        )[:, np.newaxis]
    (
        y0_hres_i0[site],
        y0_hres_i1[site],
        y0_hres_i2[site]
        ) = (
            name_qch4_couple.routines.forward_weight(
                M=M, R_std=R_std, p=p, T=T,
                p_chi0=chi0[dates_nx_site4H[site]],
                p_d0i1=d0i1[dates_nx_site4H[site]],
                p_d0i2=d0i2[dates_nx_site4H[site]],
                p_QA=Q*area, p_dQi1=dQi1, p_dQi2=dQi2,
                p_dosage=dosage, chi_scale=chi_scale,
                select=np.s_[:], dosage_type=1
                )
            )
    (
        inv_diluted0_i0[site],
        inv_diluted0_i1[site],
        inv_diluted0_i2[site]
        ) = (
            name_qch4_couple.routines.forward_weight(
                M=M, R_std=R_std, p=p, T=T,
                p_chi0=np.zeros(dates_site[site].size),
                p_d0i1=np.full(dates_site[site].size, -1.e3),
                p_d0i2=np.full(dates_site[site].size, -1.e3),
                p_QA=Q*area, p_dQi1=dQi1, p_dQi2=dQi2,
                p_dosage=dosage, chi_scale=chi_scale,
                select=inv_mask, dosage_type=1
                )
            )
del dosage
check_memory('After dosage')

# =============================================================================
#   Main Processing
# =============================================================================
print('Main Processing')

# Decide whether to compute or read
compute = 1 if force_compute else 0
if not os.path.exists(odir):
    os.makedirs(odir)
    compute = 1
if not compute and os.path.exists(os.path.join(odir, 'constants.npz')):
    try:
        chi_scale = np.load(
            os.path.join(odir, 'constants.npz'), 'r'
            )['chi_scale']
    except:
        pass
    try:
        inv_QA_scale = np.load(
            os.path.join(odir, 'constants.npz'), 'r'
            )['inv_QA_scale']
    except:
        pass

print('  Low-res Forward')
y0_lres_i0 = {}
y0_lres_i1 = {}
y0_lres_i2 = {}
# d0 (t,)
y0_i_R0i1 = (d0i1[dates_nx_C4H] * 1.e-3 + 1) * R_std[1]
y0_i_R0i2 = (d0i2[dates_nx_C4H] * 1.e-3 + 1) * R_std[2]
y0_i_R0sum = 1 + y0_i_R0i1 + y0_i_R0i2
# chi0 (t,)
y0_i_chi0i0 = (chi0[dates_nx_C4H] * (1. / y0_i_R0sum))
y0_i_chi0i1 = (chi0[dates_nx_C4H] * (y0_i_R0i1 / y0_i_R0sum))
y0_i_chi0i2 = (chi0[dates_nx_C4H] * (y0_i_R0i2 / y0_i_R0sum))
# dQ (s,)
y0_i_RQi1 = ((inv_dQi1 * 1.e-3 + 1) * R_std[1]).reshape((dims, -1))
y0_i_RQi2 = ((inv_dQi2 * 1.e-3 + 1) * R_std[2]).reshape((dims, -1))
y0_i_RQsum_M = 1*M[0] + y0_i_RQi1*M[1] + y0_i_RQi2*M[2]
for site in sites:
    y0_lres_i0[site] = ( 
        y0_i_chi0i0[dates_nx_site4C[site]]
        + inv_diluted0_i0[site]
        + (
            (inv_QA * 1 / y0_i_RQsum_M).sum(0) / inv_QA_scale
            * inv_dilution_factor[site]
            ).sum((1, 2))
        )
    y0_lres_i1[site] = ( 
        y0_i_chi0i1[dates_nx_site4C[site]]
        + inv_diluted0_i1[site]
        + (
            (inv_QA * y0_i_RQi1 / y0_i_RQsum_M).sum(0) / inv_QA_scale
            * inv_dilution_factor[site]
            ).sum((1, 2))
        )
    y0_lres_i2[site] = ( 
        y0_i_chi0i2[dates_nx_site4C[site]]
        + inv_diluted0_i2[site]
        + (
            (inv_QA * y0_i_RQi2 / y0_i_RQsum_M).sum(0) / inv_QA_scale
            * inv_dilution_factor[site]
            ).sum((1, 2))
        )
if prior_only:
    sys.exit()

print('  Inversion')
if not inversion:
    pass
else:
    model = pm.Model()
    # =========================================================================
    # State
    # =========================================================================
    print('    Defining State')
    with model:
        # chi0 - baseline chi (t or E)
        if 'chi0' in theta_vars:
            h_chi0 = pm.Uniform(
                    'h_chi0',
                    lower=inv_chi0_h[0, dates_nx_C4H],
                    upper=inv_chi0_h[1, dates_nx_C4H],
                    shape=(dimt,)
                    )
            p_chi0 = pm.Normal(
                    'p_chi0',
                    mu=chi0[dates_nx_C4H], sigma=h_chi0, shape=(dimt,)
                    )
            p_chi0f = pm.Uniform(
                    'p_chi0f',
                    lower=inv_chi0f_u[0],
                    upper=inv_chi0f_u[1],
                    shape=(dimE,)
                    )
        else:
            p_chi0 = chi0[dates_nx_C4H]
            p_chi0f = inv_chi0f_u.mean(0)
        # d0i1 - baseline d_13CH4 (t or E)
        if 'd0i1' in theta_vars:
            h_d0i1 = pm.Uniform(
                    'h_d0i1',
                    lower=inv_d0i1_h[0, dates_nx_C4H],
                    upper=inv_d0i1_h[1, dates_nx_C4H],
                    shape=(dimt,)
                    )
            p_d0i1 = pm.Normal(
                    'p_d0i1',
                    mu=d0i1[dates_nx_C4H], sigma=h_d0i1, shape=(dimt,)
                    )
            p_d0i1f = pm.Uniform(
                    'p_d0i1f',
                    lower=inv_d0i1f_u[0],
                    upper=inv_d0i1f_u[1],
                    shape=(dimE,)
                    )
        else:
            p_d0i1 = d0i1[dates_nx_C4H]
            p_d0i1f = inv_d0i1f_u.mean(0)
        # d0i2 - baseline d_12CH3D (t or E)
        if 'd0i2' in theta_vars:
            h_d0i2 = pm.Uniform(
                    'h_d0i2',
                    lower=inv_d0i2_h[0, dates_nx_C4H],
                    upper=inv_d0i2_h[1, dates_nx_C4H],
                    shape=(dimt,)
                    )
            p_d0i2 = pm.Normal(
                    'p_d0i2',
                    mu=d0i2[dates_nx_C4H], sigma=h_d0i2, shape=(dimt,)
                    )
            p_d0i2f = pm.Uniform(
                    'p_d0i2f',
                    lower=inv_d0i2f_u[0],
                    upper=inv_d0i2f_u[1],
                    shape=(dimE,)
                    )
        else:
            p_d0i2 = d0i2[dates_nx_C4H]
            p_d0i2f = inv_d0i2f_u.mean(0)

        # QA (s, r)  ## scaled due to pymc3 refusing very small number
        if 'QA' in theta_vars:
            h_QA = pm.Uniform(
                    'h_QA',
                    lower=inv_QA_unc0,
                    upper=inv_QA_unc1,
                    shape=inv_QA.shape
                    )
            p_QA = pm.TruncatedNormal(
                    'p_QA',
                    mu=inv_QA,
                    sigma=h_QA,
                    lower=0., shape=inv_QA.shape
                    )
        else:
            p_QA = inv_QA
        # dQ - d_13CH4, d_12CH3D (2s)
        if 'dQ' in theta_vars:
            h_dQ = pm.Uniform(
                   'h_dQi1',
                   lower=inv_dQ_h[0],
                   upper=inv_dQ_h[1],
                   shape=(2*dims,)
                   )
            h_dQcor = sd2cov(np.ones(2*dims))  # correlation matrix
            if dims > 1:
                np.fill_diagonal(h_dQcor[dims:], 0.20)
                np.fill_diagonal(h_dQcor[:, dims:], 0.20)
            p_dQ = pm.MvNormal(
                    'p_dQ',
                    mu=inv_dQ,
                    cov=h_dQcor*theano.tensor.outer(h_dQ, h_dQ),
                    shape=(2*dims,)
                    )
            p_dQi1 = p_dQ[(slice(0, dims))].reshape((dims, 1))
            p_dQi2 = p_dQ[(slice(dims, 2*dims))].reshape((dims, 1))
        else:
            # dQi1 - d_13CH4 (s, 1)
            if 'dQi1' in theta_vars:
                h_dQi1 = pm.Uniform(
                        'h_dQi1',
                        lower=np.abs(inv_dQi1*0.1), upper=np.abs(inv_dQi1*0.3),
                        shape=inv_dQi1.shape
                        )
                p_dQi1 = pm.Normal(
                        'p_dQi1', mu=inv_dQi1, sigma=h_dQi1, shape=inv_dQi1.shape
                        )
            else:
                p_dQi1 = inv_dQi1
            # dQi2 - d_13CH4 (s, 1)
            if 'dQi2' in theta_vars:
                h_dQi2 = pm.Uniform(
                        'h_dQi2',
                        lower=np.abs(inv_dQi2*0.1), upper=np.abs(inv_dQi2*0.3),
                        shape=inv_dQi2.shape
                        )
                p_dQi2 = pm.Normal(
                        'p_dQi2', mu=inv_dQi2, sigma=h_dQi2, shape=inv_dQi2.shape
                        )
            else:
                p_dQi2 = inv_dQi2

        # Dosage (t, r)
        #p_dosage = inv_dosage
        p_dilution_factor = inv_dilution_factor
        check_memory('After state')

    # =========================================================================
    # Calculations
    #   Calculating isotopologues separately
    # =========================================================================
    print('    Setting Calculation Up')
    with model:
        print('      Initial conditions')
        # d0 (E,)
        i_R0i1 = (p_d0i1[dates_nx_E4C] * p_d0i1f * 1.e-3 + 1) * R_std[1]
        i_R0i2 = (p_d0i2[dates_nx_E4C] * p_d0i2f * 1.e-3 + 1) * R_std[2]
        i_R0sum = 1 + i_R0i1 + i_R0i2
        # chi0 (E,)
        i_chi0i0 = (p_chi0[dates_nx_E4C] * p_chi0f * (1. / i_R0sum))
        i_chi0i1 = (p_chi0[dates_nx_E4C] * p_chi0f * (i_R0i1 / i_R0sum))
        i_chi0i2 = (p_chi0[dates_nx_E4C] * p_chi0f * (i_R0i2 / i_R0sum))
        print('      Emissions')
        # dQ (s,)
        i_RQi1 = ((p_dQi1 * 1.e-3 + 1) * R_std[1]).reshape((dims, -1))
        i_RQi2 = ((p_dQi2 * 1.e-3 + 1) * R_std[2]).reshape((dims, -1))
        i_RQsum_M = 1*M[0] + i_RQi1*M[1] + i_RQi2*M[2]
        check_memory('After Isotopes')

    # =========================================================================
    # Mixing ratios of isotopologues
    # =========================================================================
    with model:
        print('      Mixing Ratios')
        y_chii0 = {}
        y_chii1 = {}
        y_chii2 = {}
        for site in sites:
            y_chii0[site] = pm.Deterministic(
                f'y_chii0_{site}',
                theano.tensor.bincount(
                    dates_nx_E4site[site],
                    i_chi0i0[dates_nx_site4E[site]] * E[site]
                    )
                + inv_diluted0_i0[site]  # dilution outside domain
                + pm.math.sum(
                    (
                        pm.math.sum(
                            p_QA
                            * theano.tensor.tile(1. / i_RQsum_M, (1, dimr)),
                            0
                            )  # Q
                        * p_dilution_factor[site]
                        ),
                    (1, 2)
                    ) / inv_QA_scale #* chi_scale
                )
            y_chii1[site] = pm.Deterministic(
                f'y_chii1_{site}',
                theano.tensor.bincount(
                    dates_nx_E4site[site],
                    i_chi0i1[dates_nx_site4E[site]] * E[site]
                    )
                + inv_diluted0_i1[site]
                + (
                    (
                        p_QA
                        * theano.tensor.tile(i_RQi1 / i_RQsum_M, (1, dimr))
                        ).sum(0)
                    * p_dilution_factor[site]
                    ).sum((1, 2)) / inv_QA_scale #* chi_scale
                )
            y_chii2[site] = pm.Deterministic(
                f'y_chii2_{site}',
                theano.tensor.bincount(
                    dates_nx_E4site[site],
                    i_chi0i2[dates_nx_site4E[site]] * E[site]
                    )
                + inv_diluted0_i2[site]
                + (
                    (
                        p_QA
                        * theano.tensor.tile(i_RQi2 / i_RQsum_M, (1, dimr))
                        ).sum(0)
                    * p_dilution_factor[site]
                    ).sum((1, 2)) / inv_QA_scale #* chi_scale
                )
            check_memory(f'After intermediate ({site})')

    # =========================================================================
    # Modelled observations
    # =========================================================================
    print('    Defining Observations')
    with model:
        unc_mod = {site: {} for site in sites}
        obs_mod = {site: {} for site in sites}
        for site, vs in sites.items():
            k = 'chi'
            if k in y_vars and k in vs and dates_obs[site][k].size > 0:
                unc_mod[site]['chi'] = pm.Uniform(
                        f'unc_{site}_chi', lower=1.0, upper=3.0,
                        shape=p_obs[site]['chi'].shape
                        )
                obs_mod[site]['chi'] = pm.Normal(
                        f'obs_{site}_chi',
                        mu=(
                            y_chii0[site] + y_chii1[site] + y_chii2[site]
                            )[dates_nx_obs4site[site]['chi']],
                        sigma=unc_mod[site]['chi'],
                        observed=p_obs[site]['chi']
                        )
            k = 'd_13C'
            if k in y_vars and k in vs and dates_obs[site][k].size > 0:
                unc_mod[site]['d_13C'] = pm.Uniform(
                        f'unc_{site}_d_13C', lower=0.01, upper=0.03,
                        shape=p_obs[site]['d_13C'].shape
                        )
                obs_mod[site]['d_13C'] = pm.Normal(
                        f'obs_{site}_d_13C',
                        mu=(
                            (y_chii1[site]/y_chii0[site]/R_std[1] - 1) * 1.e3
                            )[dates_nx_obs4site[site]['d_13C']],
                        sigma=unc_mod[site]['d_13C'],
                        observed=p_obs[site]['d_13C']
                        )
            k = 'd_2H'
            if k in y_vars and k in vs and dates_obs[site][k].size > 0:
                unc_mod[site]['d_2H'] = pm.Uniform(
                        f'unc_{site}_d_2H', lower=0.5, upper=1.5,
                        shape=p_obs[site]['d_2H'].shape
                        )                        
                obs_mod[site]['d_2H'] = pm.Normal(
                        f'obs_{site}_d_2H',
                        mu=(
                            (y_chii2[site]/y_chii0[site]/R_std[2] - 1) * 1.e3
                            )[dates_nx_obs4site[site]['d_2H']],
                        sigma=unc_mod[site]['d_2H'],
                        observed=p_obs[site]['d_2H']
                        )
            check_memory(f'After config ({site})')

    # =========================================================================
    # Sample
    # =========================================================================
    with model:
        if compute:
            print('    Sampling Started')
            trace = pm.sample(
                    pm_draws, cores=pm_cores, chains=pm_chains, tune=pm_tune,
                    target_accept=.8, random_seed=pm_seed
                    )
            print('    Saving Results')
            result = pm.save_trace(trace, directory=odir, overwrite=True)
        else:
            print('    Reading Output')
            trace = pm.load_trace(odir)
        check_memory('After sampling')

# =============================================================================
#   Post-processing
# =============================================================================
print('Post-Processing')
# Save constants
if inversion:
    np.savez(
            os.path.join(odir, 'constants.npz'),
            **{
                'chi_scale': chi_scale,
                'inv_QA_scale': inv_QA_scale,
                }
            )
else:
    np.savez(
            os.path.join(odir, 'constants.npz'),
            **{'chi_scale': chi_scale}
            )
np.savez(
    os.path.join(odir, 'dates.npz'),
    **{
        **{f'dates_site_{site}': v for site, v in dates_site.items()},
        **{
            f'dates_obs_{site}_{k}': v
            for site, y in dates_obs.items()
            for k, v in y.items()
            },
        }
    )

# Plot parameters
plot_params = {
    'chi': {
        'ytick': [[1700., 3300.], 200.],
        'ytick_diff': [[-20.0, 20.0], 5.0],
        'lbl': u'$\chi$ (CH$_4$) (ppb)',
        },
    'd_13C': {
        'ytick': [[-55., -45.], 2.],
        'ytick_diff': [[-0.5, 0.5], 0.2],
        'lbl': u'$\delta$ $^{13}$CH$_4$ (\u2030)',
        },
    'd_2H': {
        'ytick': [[-180., -60.], 20.],
        'ytick_diff': [[-10., 10], 4.],
        'lbl': u'$\delta$ $^{12}$CH$_3$D (\u2030)',
        },
    'p_chi0': {
        'ytick': [[1700., 2100.], 100.],
        'ytick_diff': [[-15., 15.], 3.],
        'lbl': u'Baseline $\chi$ (CH$_4$) (ppb)',
        },
    'p_d0i1': {
        'ytick': [[-51., -45.], 2.],
        'ytick_diff': [[-0.5, 0.5], 0.2],
        'lbl': u'Baseline $\delta$ $^{13}$CH$_4$ (\u2030)',
        },
    'p_d0i2': {
        'ytick': [[-140., -60.], 20.],
        'ytick_diff': [[-10., 10.], 4.],
        'lbl': u'Baseline $\delta$ $^{12}$CH$_3$D (\u2030)',
        },
    'p_QA': {  # Actual Q will be plotted instead
        'ytick':  [
            np.power(10., np.linspace(-12, -5, 15)),
            np.power(10., np.linspace(-12, -5, 8))
            ],
        'ytick_diff': [
            np.array([
                *-np.linspace(1.2, 0., 7)[:-1],
                *np.linspace(0., 1.2, 7)[1:]
                ]),
            np.array([
                *-np.linspace(1.2, 0., 4)[:-1],
                0.,
                *np.linspace(0., 1.2, 4)[1:]
                ])
            ],
        'lbl': u'Emission\n(g m$^{-2}$ s$^{-1}$)',
        },
    'p_QA_unc': {
        'ytick': [
            np.power(10., np.linspace(-12, -5, 15)),
            np.power(10., np.linspace(-12, -5, 8))
            ],
        'ytick_diff': [
            np.array([
                *-np.linspace(1.2, 0., 7),
                *np.linspace(0., 1.2, 7)
                ]),
            np.array([
                *-np.linspace(1.2, 0., 4),
                *np.linspace(0., 1.2, 4)
                ])
            ],
        'lbl': u'Emission\nUncertainty\n(g m$^{-2}$ s$^{-1}$)',
        },
    'p_dQi1': {
        'ytick': [[-80., -10.], 10.],
        'ytick_diff': [[-1.0, 1.0], 0.4],
        'lbl': u'Emission $\delta$ $^{13}$CH$_4$ (\u2030)',
        },
    'p_dQi2': {
        'ytick': [[-450., -130.], 50.],
        'ytick_diff': [[-1.0, 1.0], 0.4],
        'lbl': u'Emission $\delta$ $^{12}$CH$_3$D (\u2030)',
        },
    }
colours = {
        0: '#FC8E50',  # observation
        1: '#000000',  # prior lres
        2: '#008DE1',  # posterior
        3: '#464E00',  # prior hres
        }
# Figure dict
fig_y = {}
fig_theta = {}

# Observations
## Observation from posteriors
if inversion:
    post_obs = {
            f'{site}_{y}': np.full((3, dates_H.size), np.nan)
            for site, ys in sites.items()
            for y in ys
            }
    for site in sites:
        trace_i0 = trace.get_values(f'y_chii0_{site}')
        trace_i1 = trace.get_values(f'y_chii1_{site}')
        trace_i2 = trace.get_values(f'y_chii2_{site}')
        if 'chi' in sites[site]:
            post_obs[f'{site}_chi'][:, dates_nx_site4H[site]] = (
                np.percentile(
                    trace_i0 + trace_i1 + trace_i2,
                    [16, 50, 84], axis=0
                    )
                )
        if 'd_13C' in sites[site]:
            post_obs[f'{site}_d_13C'][:, dates_nx_site4H[site]] = (
                np.percentile(
                    (trace_i1 / trace_i0 / R_std[1] - 1.) * 1.e3,
                    [16, 50, 84], axis=0
                    )
                )
        if 'd_2H' in sites[site]:
            post_obs[f'{site}_d_2H'][:, dates_nx_site4H[site]] = (
                np.percentile(
                    (trace_i2 / trace_i0 / R_std[2] - 1.) * 1.e3,
                    [16, 50, 84], axis=0
                    )
                )
    np.savez(os.path.join(odir, 'post_y.npz'), **post_obs)

## Observation from priors
y0_lres = {
    site: {
        'chi': y0_lres_i0[site] + y0_lres_i1[site] + y0_lres_i2[site],
        'd_13C': (y0_lres_i1[site] / y0_lres_i0[site] / R_std[1] - 1.) * 1.e3,
        'd_2H': (y0_lres_i2[site] / y0_lres_i0[site] / R_std[2] - 1.) * 1.e3,
        }
    for site in sites
    }
np.savez(
    os.path.join(odir, 'y0_lres.npz'),
    **{f'{site}_{k}': v for site, y in y0_lres.items() for k, v in y.items()}
    )
y0_hres = {
    site: {
        'chi': y0_hres_i0[site] + y0_hres_i1[site] + y0_hres_i2[site],
        'd_13C': (y0_hres_i1[site] / y0_hres_i0[site] / R_std[1] - 1.) * 1.e3,
        'd_2H': (y0_hres_i2[site] / y0_hres_i0[site] / R_std[2] - 1.) * 1.e3,
        }
    for site in sites
    }
np.savez(
    os.path.join(odir, 'y0_hres.npz'),
    **{f'{site}_{k}': v for site, y in y0_hres.items() for k, v in y.items()}
    )
## Plot
loc_y = {
    f'{site}_{k}': (y, x)
    for site, x in {site: x for x, site in enumerate(sites)}.items()
    for k, y in {k: y for y, k in enumerate(
        ['chi', 'd_13C', 'd_2H'] #set([k for v in sites.values() for k in v])
        )}.items()
    }
wdh = 2.0
hgt1 = 1.5
n_site = len(sites)
n_ky = len(set([k for v in sites.values() for k in v]))
fig_y['normal'] = plt.figure(figsize=(wdh*n_site, hgt1*n_ky), dpi=300)
fig_y['compare'] = plt.figure(figsize=(wdh*n_site, hgt1*n_ky), dpi=300)
fig_y['mismatch'] = plt.figure(figsize=(wdh*n_site, hgt1*n_ky), dpi=300)
for k, fig in fig_y.items():
    fig.clf()
for site in sites:
    for k in set([k for v in sites.values() for k in v]):
        key = f'{site}_{k}'
        # Normal
        name_qch4_couple.plot.generic(
            fig=fig_y['normal'],
            idata={
                'obs': [
                    'err',
                    [],
                    {
                        'x': dates_obs[site][k], 'y': p_obs[site][k],
                        'yerr': p_obs_err[site][k] if k != 'chi' else None,
                        'c': colours[0], 'fmt': '.',
                        'markersize': 2.0, 'elinewidth': 0.5,
                        'capsize': 1.0, 'capthick': 0.5,
                        'zorder': 1
                        }
                    ] if k in p_obs[site] else ['', [], {}],
                'priorl': [
                    'line',
                    [dates_site[site], y0_lres[site][k]],
                    {
                        'c': colours[1], 'ls': '', 'marker': 'o',
                        'ms': 2.0, 'mew': 0.,
                        'lw': 0.5, 'zorder': 2
                        }
                    ],
                'priorh': [
                    'line',
                    [dates_site[site], y0_hres[site][k]],
                    {
                        'c': colours[3], 'ls': '', 'marker': 'o',
                        'ms': 2.0, 'mew': 0.,
                        'lw': 0.5, 'zorder': 2
                        }
                    ] if not inversion else ['', [], {}],
                'posterior': [
                    'err',
                    [],
                    {
                        'x': dates_obs[site][k],
                        'y': post_obs[f'{site}_{k}'][1, dates_nx_obs4H[site][k]],
                        'yerr': np.diff(
                            post_obs[f'{site}_{k}'][:, dates_nx_obs4H[site][k]],
                            axis=0
                            ),
                        'color': colours[2], 'fmt': '.',
                        'markersize': 2.0, 'elinewidth': 0.5,
                        'capsize': 1.0, 'capthick': 0.5,
                        'zorder': 3
                        }
                    ] if inversion and k in sites[site] else ['', [], {}],
                },
            xlim=[
                np.datetime64(dates_H[0]) - np.timedelta64(1, 'D'),
                np.datetime64(dates_H[-1]) + np.timedelta64(1, 'D'),
                ],
            ylim=plot_params[k]['ytick'][0],
            texts=[],
            yticks=np.arange(
                plot_params[k]['ytick'][0][0],
                plot_params[k]['ytick'][0][1] + plot_params[k]['ytick'][1],
                plot_params[k]['ytick'][1]
                ),
            loc_plot=[
                (loc_y[f'{site}_{k}'][1]*wdh + 0.5)/wdh/n_site,
                ((n_ky-1-loc_y[f'{site}_{k}'][0])*hgt1 + 0.2)/hgt1/n_ky,
                (wdh-0.55)/wdh/n_site,
                (hgt1-0.25)/hgt1/n_ky
                ],
            tick_fontsize=6,
            xtick_params=[
                True,
                mdates.DateFormatter('%Y-%m-%d'),
                mdates.WeekdayLocator(byweekday=6, interval=1),
                ],
            )
        if k not in sites[site] or p_obs[site][k].size == 0:
            continue
        # Comparison (obs vs model)
        name_qch4_couple.plot.compare(
            fig=fig_y['compare'],
            idata={
                'prior': [
                    [
                        p_obs[site][k],
                        y0_lres[site][k][dates_nx_obs4site[site][k]]
                        ],
                    {'s':1., 'c':colours[1]}
                    ],
                'posterior': [
                    [
                        p_obs[site][k],
                        post_obs[f'{site}_{k}'][1, dates_nx_obs4H[site][k]]
                        ],
                    {'s':1., 'c':colours[2]}
                    ] if inversion else False,
                },
            lim=plot_params[k]['ytick'][0],
            err=p_obs_err[site][k].max(),
            texts=[],
            ticks=np.arange(
                plot_params[k]['ytick'][0][0],
                plot_params[k]['ytick'][0][1]
                + plot_params[k]['ytick'][1],
                plot_params[k]['ytick'][1]
                )[::2],
            loc_plot=[
                (loc_y[f'{site}_{k}'][1]*wdh + 0.5)/wdh/n_site,
                ((n_ky-1-loc_y[f'{site}_{k}'][0])*hgt1 + 0.2)/hgt1/n_ky,
                (hgt1-0.25)/wdh/n_site,
                (hgt1-0.25)/hgt1/n_ky
                ],
            fontsize=6,
            )
        # Mismatch (observation - model)
        name_qch4_couple.plot.generic(
            fig=fig_y['mismatch'],
            idata={
                'obs': [
                    'err',
                    [],
                    {
                        'x': dates_obs[site][k],
                        'y': np.zeros(dates_obs[site][k].size),
                        'yerr': p_obs_err[site][k] if k != 'chi' else None,
                        'c': colours[0], 'fmt': '.',
                        'markersize': 2.0, 'elinewidth': 0.5,
                        'capsize': 1.0, 'capthick': 0.5,
                        'zorder': 1
                        }
                    ] if k in p_obs[site] else ['', [], {}],
                'priorl': [
                    'line',
                    [
                        dates_obs[site][k],
                        p_obs[site][k]
                        - y0_lres[site][k][dates_nx_obs4site[site][k]]
                        ],
                    {
                        'c': colours[1], 'ls': '', 'marker': 'o',
                        'ms': 2.0, 'mew': 0.,
                        'lw': 0.5, 'zorder': 2
                        }
                    ],
                'priorh': [
                    'line',
                    [
                        dates_obs[site][k],
                        p_obs[site][k]
                        - y0_hres[site][k][dates_nx_obs4site[site][k]]
                        ],
                    {
                        'c': colours[3], 'ls': '', 'marker': 'o',
                        'ms': 2.0, 'mew': 0.,
                        'lw': 0.5, 'zorder': 2
                        }
                    ] if not inversion else ['', [], {}],
                'posterior': [
                    'err',
                    [],
                    {
                        'x': dates_obs[site][k],
                        'y': (
                            p_obs[site][k]
                            - post_obs[key][1, dates_nx_obs4H[site][k]]
                            ),
                        'yerr': (
                            np.diff(
                                post_obs[key][:, dates_nx_obs4H[site][k]],
                                axis=0
                                )
                            ),
                        'color': colours[2], 'fmt': '.',
                        'markersize': 2.0, 'elinewidth': 0.5,
                        'capsize': 1.0, 'capthick': 0.5,
                        'zorder': 3
                        }
                    ] if inversion and k in sites[site] else ['', [], {}],
                },
            xlim=[
                np.datetime64(dates_H[0]) - np.timedelta64(1, 'D'),
                np.datetime64(dates_H[-1]) + np.timedelta64(1, 'D'),
                ],
            ylim=plot_params[k]['ytick_diff'][0],
            texts=[],
            yticks=np.arange(
                plot_params[k]['ytick_diff'][0][0],
                plot_params[k]['ytick_diff'][0][1]
                + plot_params[k]['ytick_diff'][1],
                plot_params[k]['ytick_diff'][1]
                ),
            loc_plot=[
                (loc_y[f'{site}_{k}'][1]*wdh + 0.5)/wdh/n_site,
                ((n_ky-1-loc_y[f'{site}_{k}'][0])*hgt1 + 0.2)/hgt1/n_ky,
                (wdh-0.55)/wdh/n_site,
                (hgt1-0.25)/hgt1/n_ky
                ],
            tick_fontsize=6,
            xtick_params=[
                True,
                mdates.DateFormatter('%Y-%m-%d'),
                mdates.WeekdayLocator(byweekday=6, interval=1),
                ],
            )
for k, fig in fig_y.items():
    for n, var in enumerate(['chi', 'd_13C', 'd_2H']):
        fig.text(**{
            'x': 0.05/wdh/n_site,
            'y': (n_ky - n - 0.45)/n_ky,
            's': plot_params[var]['lbl'],
            'fontsize': 6,
            'ha': 'left', 'va': 'center', 'rotation': 90
            })
    for site in sites:
        fig.text(**{
            'x': (
                (wdh*(loc_y[f'{site}_{sites[site][0]}'][1]+1) - 0.1)
                /wdh/n_site
                if k not in ['compare'] else
                (loc_y[f'{site}_{sites[site][0]}'][1] + 0.6)/n_site
                ),
            'y': (n_ky*hgt1 - 0.1)/hgt1/n_ky,
            's': site,
            'fontsize': 6,
            'ha': 'right', 'va': 'top', 'rotation': 0
            })
for k, fig in fig_y.items():
    fig.savefig(os.path.join(odir, f'inv_y_{k}.jpeg'), dpi=300)

# State
sector_names = {
    0: ['FF', 'Fossil Fuel'],
    1: ['WM', 'Waste Management'],
    2: ['MA', 'Anthropogenic Microbial'],
    3: ['BB', 'Biomass Burning'],
    4: ['MN', 'Natural Microbial'],
    }
## Priors
prior_state = {
    'p_chi0': chi0,
    'p_d0i1': d0i1,
    'p_d0i2': d0i2,
    'p_chi0f': inv_chi0f_u, 
    'p_d0i1f': inv_d0i1f_u, 
    'p_d0i2f': inv_d0i2f_u, 
    'p_QA': inv_QA,
    'p_dQi1': inv_dQi1,
    'p_dQi2': inv_dQi2,
    'h_chi0': inv_chi0_h,
    'h_d0i1': inv_d0i1_h,
    'h_d0i2': inv_d0i2_h,
    'h_QA': inv_QA_h,
    'h_dQi1': inv_dQi1_h,
    'h_dQi2': inv_dQi2_h,
    **prior_baseline
    }
np.savez(os.path.join(odir, 'prior_theta.npz'), **prior_state)
## Posteriors
if inversion:  # Process prior and posterior state
    post_state = {
        f'p_{k}': np.percentile(
            trace.get_values(f'p_{k}'), [16, 50, 84],
            axis=0
            )
        for k in [
            'QA', 'dQ', 'dQi1', 'dQi2'
            ]
        if k in theta_vars
        }
    if 'QA' in theta_vars:
        id_uk = np.nonzero(np.isin(
            np.unique([v for k, v in c_cats.items() if k in c_regs]),
            [
                v for k, v in c_cats.items()
                if k.startswith('gb') and k in c_regs
                ]
            ))[0]
        id_euw = np.nonzero(np.isin(
            np.unique([v for k, v in c_cats.items() if k in c_regs]),
            [
                v for k, v in c_cats.items()
                if (
                    k.startswith(('benllu', 'de', 'espt', 'fr', 'gb', 'ie'))
                    and k in c_regs
                    )
                ]
            ))[0]
        post_state['QA_uk'] = np.percentile(
            trace.get_values('p_QA')[..., id_uk].sum(-1), [16, 50, 84],
            axis=0
            )
        post_state['QA_euw'] = np.percentile(
            trace.get_values('p_QA')[..., id_euw].sum(-1), [16, 50, 84],
            axis=0
            )
    if 'dQ' in theta_vars:
        post_state['p_dQi1'] =  post_state['p_dQ'][:, :dims]
        post_state['p_dQi2'] =  post_state['p_dQ'][:, dims:2*dims]
    flag = False
    if 'chi0' in theta_vars:
        post_state['p_chi0'] = np.percentile(
            trace.get_values(f'p_chi0'), [16, 50, 84],
            axis=0
            )
        post_state['p_chi0f'] = np.percentile(
            trace.get_values(f'p_chi0f'), [16, 50, 84],
            axis=0
            )
        flag = True
    if 'd0i1' in theta_vars:
        post_state['p_d0i1'] = np.percentile(
            trace.get_values(f'p_d0i1'), [16, 50, 84],
            axis=0
            )
        post_state['p_d0i1f'] = np.percentile(
            trace.get_values(f'p_d0i1f'), [16, 50, 84],
            axis=0
            )
        flag = True
    if 'd0i2' in theta_vars:
        post_state['p_d0i2'] = np.percentile(
            trace.get_values(f'p_d0i2'), [16, 50, 84],
            axis=0
            )
        post_state['p_d0i2f'] = np.percentile(
            trace.get_values(f'p_d0i2f'), [16, 50, 84],
            axis=0
            )
        flag = True
    if flag:
        post_chi0i0, post_chi0i1, post_chi0i2 = calc_init_edge(
            chi0=(
                trace.get_values('p_chi0')
                if 'chi0' in theta_vars else
                chi0[np.newaxis, dates_nx_C4H]
                )[:, dates_nx_E4C],
            d0i1=(
                trace.get_values('p_d0i1')
                if 'd0i1' in theta_vars else
                d0i1[np.newaxis, dates_nx_C4H]
                )[:, dates_nx_E4C],
            d0i2=(
                trace.get_values('p_d0i2')
                if 'd0i2' in theta_vars else
                d0i2[np.newaxis, dates_nx_C4H]
                )[:, dates_nx_E4C],
            chi0f=(
                trace.get_values('p_chi0f')
                if 'chi0' in theta_vars else
                prior_state['p_chi0f'].mean(0, keepdims=True)
                ),
            d0i1f=(
                trace.get_values('p_d0i1f')
                if 'd0i1' in theta_vars else
                prior_state['p_d0i1f'].mean(0, keepdims=True)
                ),
            d0i2f=(
                trace.get_values('p_d0i2f')
                if 'd0i2' in theta_vars else
                prior_state['p_d0i2f'].mean(0, keepdims=True)
                ),
            R_std=R_std,
            )
        for site in sites:
            (
                post_state[f'{site}_p_chi0'],
                post_state[f'{site}_p_d0i1'],
                post_state[f'{site}_p_d0i2']
                ) = (
                    np.percentile(v, [16, 50, 84], axis=0)
                    for v in calc_init_site(
                        chi0i0=post_chi0i0[:, dates_nx_site4E[site]],
                        chi0i1=post_chi0i1[:, dates_nx_site4E[site]],
                        chi0i2=post_chi0i2[:, dates_nx_site4E[site]],
                        E=E[site],
                        idx_bin_site=dates_nx_E4site[site],
                        R_std=R_std
                        )
                    )
    np.savez(os.path.join(odir, 'post_theta.npz'), **post_state)
## Plot
wdh_Q = wdh * (3 if inversion else 1) + (0 if inversion else 0.5)
hgt2 = 1.2
fig_theta['Q_normal'] = plt.figure(figsize=(wdh_Q, hgt2*dims), dpi=300)
fig_theta['Q_unc'] = plt.figure(figsize=(wdh_Q, hgt2*dims), dpi=300)
fig_theta['dQ'] = plt.figure(figsize=(wdh*1, hgt1*2), dpi=300)
fig_theta['init'] = plt.figure(figsize=(wdh*n_site, hgt1*n_ky), dpi=300)
for k, fig in fig_theta.items():
    fig.clf()
for k, v in prior_state.items():
    if k.endswith('_h'):  # Hyperparameters - skip
        continue
    elif k in ['p_chi0f', 'p_d0i1f', 'p_d0i2f']:  # Baseline factor - skip
        continue
    elif k in ['p_chi0', 'p_d0i1', 'p_d0i2']:  # Baseline
        if k == 'p_chi0':
            obs_key = 'chi'
        elif k == 'p_d0i1':
            obs_key = 'd_13C'
        else:
            obs_key = 'd_2H'
        for site in sites:
            name_qch4_couple.plot.generic(
                fig=fig_theta['init'],
                idata={
                    'obs': [
                        'err',
                        [],
                        {
                            'x': dates_obs[site][obs_key],
                            'y': p_obs[site][obs_key],
                            'yerr': (
                                p_obs_err[site][obs_key]
                                if k != 'chi' else
                                None
                                ),
                            'c': colours[0], 'fmt': '.',
                            'markersize': 2.0, 'elinewidth': 0.5,
                            'capsize': 1.0, 'capthick': 0.5,
                            'zorder': 1
                            }
                        ] if obs_key in p_obs[site] else ['', [], {}],
                    'prior': [
                        'err',
                        [],
                        {
                            'x': dates_obs[site][obs_key],
                            'y': (
                                prior_state[f'{site}_{k}']
                                )[0, dates_nx_obs4site[site][obs_key]],
                            'yerr': None,
                            'c': colours[1], 'ls': '', 'marker': 'o',
                            'ms': 2.0, 'mew': 0.,
                            'lw': 0.5, 'zorder': 2
                            }
                        ] if obs_key in p_obs[site] else ['', [], {}],
                    'posterior': [
                        'err',
                        [],
                        {
                            'x': dates_site[site],
                            'y': post_state[f'{site}_{k}'][1],
                            'yerr': np.diff(
                                post_state[f'{site}_{k}'], axis=0
                                ),
                            'color': colours[2], 'fmt': '.',
                            'markersize': 2.0, 'elinewidth': 0.5,
                            'capsize': 1.0, 'capthick': 0.5,
                            'zorder': 3
                            }
                        ] if inversion and k in post_state else ['', [], {}],
                    },
                xlim=[
                    np.datetime64(dates_H[0]) - np.timedelta64(1, 'D'),
                    np.datetime64(dates_H[-1]) + np.timedelta64(1, 'D'),
                    ],
                ylim=plot_params[k]['ytick'][0],
                texts=[],
                yticks=np.arange(
                    plot_params[k]['ytick'][0][0],
                    plot_params[k]['ytick'][0][1] + plot_params[k]['ytick'][1],
                    plot_params[k]['ytick'][1]
                    ),
                loc_plot=[
                    (loc_y[f'{site}_{obs_key}'][1]*wdh + 0.5)/wdh/n_site,
                    ((n_ky-1-loc_y[f'{site}_{obs_key}'][0])*hgt1 + 0.2)/hgt1/n_ky,
                    (wdh-0.55)/wdh/n_site,
                    (hgt1-0.25)/hgt1/n_ky
                    ],
                tick_fontsize=6,
                xtick_params=[
                    True,
                    mdates.DateFormatter('%Y-%m-%d'),
                    mdates.WeekdayLocator(byweekday=6, interval=1),
                    ],
                )
    elif k in ['p_QA']:  # Emissions
        for k1, v1 in {
                'prior': v / inv_QA_scale,
                'posterior':
                    post_state[k][1] / inv_QA_scale if inversion else None,
                'diff': (
                    (v - post_state[k][1]) / v
                    ) if inversion else None,
                'unc_prior': inv_QA_h.mean(0) / inv_QA_scale,
                'unc_posterior':
                    post_state[k][[0,2]].mean(0) / inv_QA_scale
                    if inversion else None,
                'unc_diff': (
                    (
                        inv_QA_h.mean(0)
                        - post_state[k][[0,2]].mean(0)
                        ) / inv_QA_h.mean(0)
                    ) if inversion else None,
                }.items():
            if v1 is None:
                continue
            if k1.startswith('unc'):
                plot_param = plot_params['p_QA_unc']
            else:
                plot_param = plot_params['p_QA']
            if k1.endswith('diff'):
                colour = 'RdBu_r'
                ctick = plot_param['ytick_diff']
            else:
                colour = 'viridis_r'
                ctick = plot_param['ytick']
            fx = (
                    0 if k1.endswith('prior') else
                    1 if k1.endswith('posterior') else
                    2 if k1.endswith('diff') else
                    3
                    )
            for s in range(dims):
                name_qch4_couple.plot.geographical(
                    fig=fig_theta[
                        'Q_unc' if k1.startswith('unc') else 'Q_normal'
                        ],
                    idata=regionQA2griddedQ(
                        v1[[s]],
                        inv_regs_A if not k1.endswith('diff') else 1.,
                        inv_regs
                        )[0],
                    grid=[
                        grid2[0][inv_lon0:inv_lon1+1],
                        grid2[1][inv_lat0:inv_lat1+1]
                        ],
                    texts=[],
                    loc_plot=(
                        [
                            (0.14)/wdh_Q,
                            ((dims-s-1)*hgt2+0.05)/hgt2/dims,
                            1.6/wdh_Q,
                            (hgt2-0.1)/hgt2/dims
                            ]
                        if fx == 0 else
                        [
                            (0.14+fx*1.6+(fx-1)*0.53)/wdh_Q,
                            ((dims-s-1)*hgt2+0.05)/hgt2/dims,
                            1.6/wdh_Q,
                            (hgt2-0.1)/hgt2/dims
                            ]
                        ),
                    loc_bar=[
                        (0.14+(fx+1)*1.6+(fx-1)*0.53+0.02)/wdh_Q,
                        ((dims-s-1)*hgt2+0.1)/hgt2/dims,
                        (.1)/wdh_Q,
                        (hgt2-0.5)/hgt2/dims
                        ] if fx != 0 else False,
                    bbox=[
                        grid2[0][inv_lon0], grid2[0][inv_lon1],
                        grid2[1][inv_lat0], grid2[1][inv_lat1]
                        ],
                    tick_params = {
                        'labelsize': 6 if fx == 0 else 4,
                        'pad': 0.05
                        },
                    shapefiles=[
                        '/home/ec5/name_qch4_couple/shape_files/'
                        'CNTR_BN_10M_2016_4326.shp'
                        ],
                    colour=colour,
                    vlim=[ctick[0][0], ctick[0][-1], ctick[0], ctick[1]],
                    extend='both' if k1.endswith('diff') else 'max',
                    )
    elif k in ['p_dQi1', 'p_dQi2']:
        n = (1 if k == 'p_dQi1' else 0 if k == 'p_dQi2' else 2)
        hyper = (
            'h_dQi1' if k == 'p_dQi1' else
            'h_dQi2' if k == 'p_dQi2' else
            'h_dQi3'
            )
        name_qch4_couple.plot.generic(
            fig=fig_theta['dQ'],
            idata={
                'prior': [
                    'err',
                    [],
                    {
                        'x': np.arange(dims),
                        'y': v.flatten(),
                        'yerr': prior_state[hyper].mean(0).flatten(),
                        'c': colours[0], 'fmt': 'o',
                        'markersize': 2.0, 'elinewidth': 0.5,
                        'capsize': 2.0, 'capthick': 0.5,
                        'zorder': 1
                        }
                    ],
                'posterior': [
                    'err' if inversion else '',
                    [],
                    {
                        'x': np.arange(Q.shape[0]),
                        'y': post_state[k][1].flatten(),
                        'yerr': np.diff(post_state[k], axis=0).reshape(2, -1),
                        'c': colours[2], 'fmt': 'o',
                        'markersize': 2.0, 'elinewidth': 0.5,
                        'capsize': 2.0, 'capthick': 0.5,
                        'zorder': 2
                        } if inversion else {}
                    ],
                },
            xlim=[-0.5, dims-0.5],
            ylim=plot_params[k]['ytick'][0],
            texts=[
                {
                    'x': 0.05/wdh/1 ,
                    'y': (n + 0.55) / 2,
                    's': plot_params[k]['lbl'],
                    'fontsize': 6,
                    'ha': 'left', 'va': 'center', 'rotation': 90
                    }, # y label
                    ],
            yticks=np.arange(
                plot_params[k]['ytick'][0][0],
                plot_params[k]['ytick'][0][1] + plot_params[k]['ytick'][1],
                plot_params[k]['ytick'][1]
                ),
            loc_plot=[
                0.5/wdh/1,
                (n*hgt1 + 0.2)/hgt1/2,
                (wdh-0.55)/wdh/1,
                (hgt1-0.25)/hgt1/2
                ],
            tick_fontsize=6,
            xtick_params=[
                False,
                np.arange(5),
                [sector_names[i][0] for i in range(5)]
                ],
            )
for k, v in {'p_QA': 'Q_normal', 'p_QA_unc': 'Q_unc'}.items():
    for s in range(dims):
        for text in [
                {  # sector name
                    'x': 0.05/wdh_Q,
                    'y': (dims-s-0.5)/dims,
                    's': (
                        f'{sector_names[s][1]} ({sector_names[s][0]})'
                        ),
                    'ha': 'left', 'va': 'center', 'size': 6, 'rotation': 90
                    },
                {  # colorbar label 1
                    'x': (
                        (0.14+2*1.6+0*0.53+0.02)/wdh_Q
                        if inversion else
                        (wdh+0.05)/wdh_Q
                        ),
                    'y': ((dims-s)*hgt2-0.1)/hgt2/dims,
                    's': plot_params[k]['lbl'],
                    'ha': 'left', 'va': 'top', 'size': 6
                    },
                {  # colorbar label 2
                    'x': (
                        (0.14+3*1.6+1*0.53+0.02)/wdh_Q
                        if inversion else
                        (wdh+0.05)/wdh_Q
                        ),
                    'y': ((dims-s)*hgt2-0.1)/hgt2/dims,
                    's': plot_params[k]['lbl'],
                    'ha': 'left', 'va': 'top', 'size': 6
                    } if inversion else {}
                ]:
            if text:
                fig_theta[v].text(**text)
if 'init' in fig_theta:
    fig = fig_theta['init']
    for n, var in enumerate(['chi', 'd_13C', 'd_2H']):
        fig.text(**{
            'x': 0.05/wdh/n_site,
            'y': (n_ky - n - 0.45)/n_ky,
            's': plot_params[var]['lbl'],
            'fontsize': 6,
            'ha': 'left', 'va': 'center', 'rotation': 90
            })
    for site in sites:
        fig.text(**{
            'x': (
                (wdh*(loc_y[f'{site}_{sites[site][0]}'][1]+1) - 0.1)
                /wdh/n_site
                if k not in ['compare'] else
                (loc_y[f'{site}_{sites[site][0]}'][1] + 0.6)/n_site
                ),
            'y': (n_ky*hgt1 - 0.1)/hgt1/n_ky,
            's': site,
            'fontsize': 6,
            'ha': 'right', 'va': 'top', 'rotation': 0
            })
for k, fig in fig_theta.items():
    fig.savefig(os.path.join(odir, f'inv_theta_{k}.jpeg'))

