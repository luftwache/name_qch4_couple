r"""

Radon/CH4 Forward Model

"""
# Standard Library imports
import argparse
import gzip
import matplotlib as mpl
import matplotlib.dates as mdates
import matplotlib.pyplot as plt
import netCDF4
import numpy as np
import os
import pandas as pd
import scipy.linalg
import scipy.stats
import sys
import xarray as xr

# Third party imports
from collections import OrderedDict
from datetime import datetime
from sklearn.decomposition import PCA
from sklearn import linear_model

# Semi-local imports
import name_qch4_couple.io
import name_qch4_couple.name
import name_qch4_couple.plot

# Local imports
import routines


def partial_corr(C):
    r"""
    Partial Correlation in Python (clone of Matlab's partialcorr)
    This uses the linear regression approach to compute the partial 
    correlation (might be slow for a huge number of variables). The 
    algorithm is detailed here:
        http://en.wikipedia.org/wiki/Partial_correlation#Using_linear_regression
    Taking X and Y two variables of interest and Z the matrix with all the variable minus {X, Y},
    the algorithm can be summarized as
        1) perform a normal linear least-squares regression with X as the target and Z as the predictor
        2) calculate the residuals in Step #1
        3) perform a normal linear least-squares regression with Y as the target and Z as the predictor
        4) calculate the residuals in Step #3
        5) calculate the correlation coefficient between the residuals from Steps #2 and #4; 
        The result is the partial correlation between X and Y while controlling for the effect of Z
    Date: Nov 2014
    Author: Fabian Pedregosa-Izquierdo, f@bianp.net
    Testing: Valentina Borghesani, valentinaborghesani@gmail.com

    Returns the sample linear partial correlation coefficients between pairs of variables in C, controlling 
    for the remaining variables in C.
    Parameters
    ----------
    C : array-like, shape (n, p)
        Array with the different variables. Each column of C is taken as a variable
    Returns
    -------
    P : array-like, shape (p, p)
        P[i, j] contains the partial correlation of C[:, i] and C[:, j] controlling
        for the remaining variables in C.
    """
    C = np.asarray(C)
    p = C.shape[1]
    P_corr = np.zeros((p, p), dtype=np.float)
    for i in range(p):
        P_corr[i, i] = 1
        for j in range(i+1, p):
            idx = np.ones(p, dtype=np.bool)
            idx[i] = False
            idx[j] = False
            beta_i = scipy.linalg.lstsq(C[:, idx], C[:, j])[0]
            beta_j = scipy.linalg.lstsq(C[:, idx], C[:, i])[0]

            res_j = C[:, j] - C[:, idx].dot( beta_i)
            res_i = C[:, i] - C[:, idx].dot(beta_j)
            
            corr = scipy.stats.pearsonr(res_i, res_j)[0]
            P_corr[i, j] = corr
            P_corr[j, i] = corr
        
    return P_corr


def KMO(C):
    r"""Perform KMO test
    """
    corr = C.corr()
    p_corr = partial_corr(C)
    x_c = np.array(C.corr())**2
    x_p = p_corr**2
    np.fill_diagonal(x_c, 0)
    np.fill_diagonal(x_p, 0)
    kmo_per_item = x_c.sum(0) / (x_c.sum(0) + x_p.sum(0))
    kmo_total = x_c.sum() / (x_c.sum() + x_p.sum())
    return kmo_per_item, kmo_total


# Argument Parser
parser = argparse.ArgumentParser()
parser.add_argument("-date0", required=True)
parser.add_argument("-date1", required=True)
parser.add_argument("-Qtype", required=True, type=int)
parser.add_argument("-idir", required=True)
parser.add_argument("-odir", required=True)
args = parser.parse_args()

date0 = args.date0
date1 = args.date1
Qtype = args.Qtype
idir = args.idir
odir = args.odir


# =============================================================================
# Preprocessing
# =============================================================================
print('Preprocessing')

# Dates
dates_tMonth = pd.date_range(
    pd.to_datetime(date0),
    pd.to_datetime(date1),
    closed=None,
    freq='1MS'
    )
dates_tHour = pd.date_range(
    pd.to_datetime(date0),
    pd.to_datetime(date1) + pd.DateOffset(months=1),
    closed='left',
    freq='1H'
    )

# Grid
grid_info = routines.define_grid()
inv_reg_map0 = grid_info['inv_reg_map']
nlat = grid_info['nlat']

area = grid_info['area']
grid_centre = grid_info['grid_centre']
grid_vertex = grid_info['grid_vertex']
inv_reg_uniq = grid_info['inv_reg_uniq']

# Standard atmospheric conditions
p_std = 1013.25
T_std = 15

alphabet = 'abcdefghijklmnopqrstuvwxyz'
name_222Rn = '$C\mathregular{_{^{222}Rn}}$ (mBq m$^{-3}$)'
name_CH4 = '$\chi\mathregular{_{CH_{4}}}$ (nmol mol$^{-1}$)'

# Read data
mod_data = pd.concat([
    pd.read_csv(
        os.path.join(idir, f"{d.strftime('%Y-%m')}_mod0.csv"),
        skipinitialspace=True, parse_dates=[0], index_col=[0]
        )
    for d in dates_tMonth
    ], axis=0)
obs_data = pd.concat([
    pd.read_csv(
        os.path.join(idir, f"{d.strftime('%Y-%m')}_obs.csv"),
        skipinitialspace=True, parse_dates=[0], index_col=[0]
        )
    for d in dates_tMonth
    ], axis=0)

io_date_range = [
    pd.to_datetime(date0) - pd.to_timedelta('30min'),
    pd.to_datetime(date1) + pd.DateOffset(months=1),
    #pd.to_datetime(date) + pd.tseries.offsets.MonthEnd(0) + pd.to_timedelta('1d')
    ]
obs_Rn_file = '/home/ec5/hpc-work/data_archive/decc/obs/hfd_radon.dat'
obs_Rn_rraw = (
    pd.read_csv(
        obs_Rn_file,
        sep='\t', skipinitialspace=True,
        parse_dates={'datetime': ['date', 'time']},
        date_parser=lambda x: datetime.strptime(x, '%y%m%d %H%M'),
        index_col='datetime'
        )
    if os.path.exists(obs_Rn_file) else
    pd.Series(np.nan, index=dates_tHour)
    ).loc[io_date_range[0]:io_date_range[1]]
obs_Rn_raw = (
    obs_Rn_rraw['radon'].loc[date0:date1]
    * (273.15 + obs_Rn_rraw['detector_temp'].shift(-1, '30min').loc[date0:date1])#.rename()
    / obs_Rn_rraw['detector_pressure'].shift(-1, '30min').loc[date0:date1]#.rename()
    * p_std
    / (273.15 + T_std)
    ).resample('1H').mean().reindex(dates_tHour)

Rn_cols = ['Rn_stp']

dom_gb_eng = mod_data['chi_CH4_Rn_stp'] > 0.

met = {
    'wd': [
        'Wind Direction',
        mod_data['wind_direction'],
        np.arange(0., 361., 45.),
        [f'{i:03.0f}' for i in np.arange(0., 361., 45.)]
        ],
    'ws': [
        'Wind Speed (m s$^{-1}$)',
        mod_data['wind_speed'],
        np.arange(0., 16., 5.),
        np.arange(0., 16., 5.)
        #np.arange(0., 21., 5.),
        #np.arange(0., 21., 5.)
        ],
    'T': [
        u'Temperature (\u00B0C)',
        mod_data['T'],
        np.arange(-5., 26., 5.),
        np.arange(-5., 26., 5.)
        #np.arange(-20., 41., 10.),
        #np.arange(-20., 41., 10.)
        ],
    'p': [
        u'Pressure (hPa)',
        mod_data['p'],
        np.arange(940., 1061., 10.),
        np.arange(940., 1061., 10.)
        ],
    'PBLH': [
        u'PBLH (m)',
        mod_data['pblh'],
        np.arange(0., 2001., 250.),
        np.arange(0., 2001., 250.),
        #np.arange(0., 5001., 1000.)
        #np.arange(0., 5001., 1000.)
        ],
    }


# Calculate uncertainties
obs_Rn_deconv = pd.read_csv(
    '/hpc-work/ec5/data_archive/decc/obs/'
    #'hfd_decRn_STP_202007-202104.csv',
    'Heathfield_RT_corrected_radon_30min_preliminary.csv',
    sep=',', skipinitialspace=True,
    #date_parser=lambda x: pd.to_datetime(x, format='%d/%m/%Y %H:%M'),
    parse_dates={'Datetime': ['time']},
    index_col='Datetime'
    ).loc[io_date_range[0]:io_date_range[1]]
obs_Rn_deconv_flag = pd.read_csv(
    '/hpc-work/ec5/data_archive/decc/obs/'
    'hfd_decRn_STP_202007-202104.csv',
    #'Heathfield_RT_corrected_radon_30min_preliminary.csv',
    sep=',', skipinitialspace=True,
    date_parser=lambda x: pd.to_datetime(x, format='%d/%m/%Y %H:%M'),
    parse_dates={'Datetime': ['date']},
    index_col='Datetime'
    ).loc[io_date_range[0]:io_date_range[1]].isnull().any(1)
obs_Rn_deconv.loc[obs_Rn_deconv_flag] = np.nan
obs_Rn_dall = obs_Rn_deconv[[
    'radon_deconv_emcee_16pc', 'radon_deconv_emcee', 'radon_deconv_emcee_84pc'
    ]].diff(axis=1).iloc[:, 1:].mean(1)
obs_Rn_dstp = (
    obs_Rn_dall.loc[dates_tHour].rename()
    * (273.15 + obs_Rn_deconv['temp'].shift(-1, '30min').loc[dates_tHour]).rename()
    / obs_Rn_deconv['press'].shift(-1, '30min').loc[dates_tHour].rename()
    * p_std
    / (273.15 + T_std) 
    ).resample('1H').mean() * 1.e3
obs_Rn = (
    obs_Rn_deconv['radon_deconv_emcee'].loc[dates_tHour].rename()
    * (273.15 + obs_Rn_deconv['temp'].shift(-1, '30min').loc[dates_tHour]).rename()
    / obs_Rn_deconv['press'].shift(-1, '30min').loc[dates_tHour].rename()
    * p_std
    / (273.15 + T_std) 
    ).resample('1H').mean() * 1.e3
obs_Rn_p16 = (
    obs_Rn_deconv['radon_deconv_emcee_16pc'].loc[dates_tHour].rename()
    * (273.15 + obs_Rn_deconv['temp'].shift(-1, '30min').loc[dates_tHour]).rename()
    / obs_Rn_deconv['press'].shift(-1, '30min').loc[dates_tHour].rename()
    * p_std
    / (273.15 + T_std) 
    ).resample('1H').mean() * 1.e3
obs_Rn_p84 = (
    obs_Rn_deconv['radon_deconv_emcee_84pc'].loc[dates_tHour].rename()
    * (273.15 + obs_Rn_deconv['temp'].shift(-1, '30min').loc[dates_tHour]).rename()
    / obs_Rn_deconv['press'].shift(-1, '30min').loc[dates_tHour].rename()
    * p_std
    / (273.15 + T_std) 
    ).resample('1H').mean() * 1.e3

# Plots
print('Processing')
fig = {}
ax = {}
pobjs = {}
colours = {
    'obs': '#000000',
    'mo1': '#FF8800',
    'bas': '#886600',
    'mo2': '#8888FF',
    }
fig_param = {
    'w': 6, 'h': 3,
    'px0': 0.80, 'py0': 0.50,
    'pw': 5.15, 'ph': 2.45,
    'ylblx': 0.05, 'ylbly': 1.5,  # left, centre aligned
    'fontsize': 8,
    'polar_px0': 0.30, 'polar_py0': 0.50,
    'polar_pw': 2.00, 'polar_ph': 2.00,
    'mw': 6, 'mh': 5.0,
    'mpw': 5.5, 'mph': 1.5,
    'mgap': 0.05,
    'mlmargin': 0.5, 'mbmargin': 0.5,
    }
fig_param_def = fig_param.copy()

cmap_ticks = [*np.arange(-50, 0, 10), -2.5, 2.5, *np.arange(10, 51, 10)]
cmap_colour = plt.get_cmap('RdBu_r')(
    np.linspace(0., 1., len(cmap_ticks)+1)
    )
cmap = mpl.colors.ListedColormap(cmap_colour[1:-1])
cmap.set_over(cmap_colour[-1])
cmap.set_under(cmap_colour[0])
norm = mpl.colors.BoundaryNorm(
    boundaries=cmap_ticks, ncolors=cmap.N, clip=False
    )

fac_ticks = np.arange(0., 3.01, 0.5)

fac_cmap_ticks = np.array([
    *np.linspace(0., 8., 5)/10, *np.linspace(12., 26., 8)/10
    #*(np.arange(-1.50, -0., 0.5)), -0.05, 0.05, *(np.arange(0.50, 1.51, 0.5))
    #*(np.arange(98.0, 99.6, 0.5)/100), *(np.arange(100.5, 102.1, 0.5)/100)
    ])
fac_cmap_side = max(sum(fac_cmap_ticks > 1), sum(fac_cmap_ticks < 1))
fac_cmap_colour = plt.get_cmap('RdBu_r')(
    [
        *np.linspace(0.0, 0.5, sum(fac_cmap_ticks < 1)+1),
        *np.linspace(0.5, 1.0, sum(fac_cmap_ticks > 1)+1)[1:]
        ]
    #np.linspace(
    #    0., 1., fac_cmap_side*2 + 1
    #    )[
    #        fac_cmap_side - sum(fac_cmap_ticks < 1)
    #        : fac_cmap_side + sum(fac_cmap_ticks > 1) + 1
    #        ]
    #np.linspace(0., 1., len(fac_cmap_ticks)+1)
    )
fac_cmap = mpl.colors.ListedColormap(fac_cmap_colour[1:-1])
fac_cmap.set_over(fac_cmap_colour[-1])
fac_cmap.set_under(fac_cmap_colour[0])
fac_norm = mpl.colors.BoundaryNorm(
    boundaries=fac_cmap_ticks, ncolors=fac_cmap.N, clip=False
    )


plt.close('all')

# Observaions
fig['obs'] = plt.figure(figsize=(fig_param['w'], fig_param['h']), dpi=300)
ax['obs'] = {}
obs_cols = {
    'obs_Rn_stp': ['$^{222}$Rn', 'obs'],
    'obs_CH4': ['CH$_{4}$', 'mo1']
    }
fig['obs'].clf()
for n, (k, v) in enumerate(obs_cols.items()):
    ax['obs'][n] =  name_qch4_couple.plot.generic(
        fig=fig['obs'],
        label=k,
        idata={
            k: [
                'line',
                [dates_tHour, obs_data[k], 'o'],
                {
                    'c': colours[v[1]],
                    'ms': 1.0, 'mew': 0.,
                    'label': v[0]
                    }
                ],
            **{f'dummy{i}': [
                'line',
                [dates_tHour[0], np.nan, 'o'],
                {
                    'c': colours[obs_cols[i][1]],
                    'ms': 1.0, 'mew': 0.,
                    'label': obs_cols[i][0]
                    }
                ]
                for i in obs_cols if n == 0 and i != k
                }
            },
        texts=[
            {
                'x': (
                    (fig_param['ylblx'] / fig_param['w'])
                    if n == 0 else
                    ((fig_param['w'] - fig_param['ylblx']) / fig_param['w'])
                    ),
                'y': fig_param['ylbly'] / fig_param['h'],
                's': ( 
                    name_222Rn
                    if n == 0 else
                    name_CH4
                    ),
                'ha': 'left' if n == 0 else 'right', 'va': 'center',
                'size': fig_param['fontsize'], 'rotation': 90, 'label': ''
                },
            ],
        xlim=[
            dates_tHour[0] - pd.to_timedelta('1D'),
            dates_tHour[-1] + pd.to_timedelta('1D')
            ],
        ylim=[0., 12000.] if n == 0 else [1900., 2400.],
        yticks=(
            np.arange(0., 12001., 2000.)
            if n == 0 else
            np.arange(1900., 2401., 100.)
            ),
        tick_fontsize=fig_param['fontsize'],
        loc_plot=[
            fig_param['px0'] / fig_param['w'],
            fig_param['py0'] / fig_param['h'],
            (fig_param['w'] - 2 * fig_param['px0']) / fig_param['w'],
            fig_param['ph'] / fig_param['h']
            ],
        xtick_params=[
            True,
            mdates.DateFormatter('%Y-%m-%d'),
            mdates.DayLocator(bymonthday=1),
            ]
        )
    if n == 0:
        for l in ax['obs'][n].get_xticklabels():
            l.set_ha("right")
            l.set_rotation(30)
        ax['obs'][n].legend(
            loc='upper right', ncol=4, fontsize=fig_param['fontsize'],
            markerscale=5./1.
            )
    else:
        ax['obs'][n].tick_params(
            left=False, bottom=False,
            right=True, top=False,
            labelleft=False, labelbottom=False,
            labelright=True, labeltop=False
            )
        ax['obs'][n].patch.set_alpha(0.)
fig['obs'].savefig(os.path.join(
    odir, f'a_obs.png')
    )


# Diurnal composites
deconv_cols = {
    'obs_Rn_stp': ['$^{222}$Rn', 'obs'],
    'obs_CH4': ['CH$_{4}$', 'mo1']
    }
obs_index = obs_data.dropna().index
deconv_data = obs_data.loc[obs_index].groupby(obs_index.time).mean()
fig['deconv'] = plt.figure(figsize=(fig_param['w'], fig_param['h']), dpi=300)
ax['deconv'] = {}
fig['deconv'].clf()
for n, (k, v) in enumerate(obs_cols.items()):
    ax['deconv'][n] =  name_qch4_couple.plot.generic(
        fig=fig['deconv'],
        label=k,
        idata={
            k: [
                'line',
                [
                    dates_tHour[:24],
                    deconv_data[k],
                    'o-'
                    ],
                {
                    'c': colours[v[1]],
                    'ms': 3.5, 'mew': 0.5, 'mec': '#000000',
                    'label': v[0]
                    }
                ],
            **({
                f'{k}_raw': [
                    'line',
                    [
                        dates_tHour[:24],
                        obs_Rn_raw.loc[obs_index].groupby(obs_index.hour).mean(),
                        'o-'
                        ],
                    {
                        'c': colours['mo2'],
                        'ms': 3.5, 'mew': 0.5, 'mec': '#000000',
                        'label': f'{v[0]}$_{{raw}}$'
                        },
                    ],
                } if n == 0 else {}),
            **{f'dummy{i}': [
                'line',
                [dates_tHour[0], np.nan, 'o-'],
                {
                    'c': colours[obs_cols[i][1]],
                    'ms': 3.5, 'mew': 0.5, 'mec': '#000000',
                    'label': obs_cols[i][0]
                    }
                ]
                for i in obs_cols if n == 0 and i != k
                }
            },
        texts=[
            {
                'x': (
                    (fig_param['ylblx'] / fig_param['w'])
                    if n == 0 else
                    ((fig_param['w'] - fig_param['ylblx']) / fig_param['w'])
                    ),
                'y': fig_param['ylbly'] / fig_param['h'],
                's': ( 
                    name_222Rn
                    if n == 0 else
                    name_CH4
                    ),
                'ha': 'left' if n == 0 else 'right', 'va': 'center',
                'size': fig_param['fontsize'], 'rotation': 90, 'label': ''
                },
            ],
        xlim=[
            dates_tHour[0] - pd.to_timedelta('30min'),
            dates_tHour[23] + pd.to_timedelta('30min')
            ],
        ylim=[1200., 1800.] if n == 0 else [2010., 2030.],
        yticks=(
            np.arange(1200., 1801., 200.)
            if n == 0 else
            np.arange(2010., 2031., 5.)
            ),
        tick_fontsize=fig_param['fontsize'],
        loc_plot=[
            fig_param['px0'] / fig_param['w'],
            fig_param['py0'] / fig_param['h'],
            (fig_param['w'] - 2 * fig_param['px0']) / fig_param['w'],
            fig_param['ph'] / fig_param['h']
            ],
        xtick_params=[
            True,
            mdates.DateFormatter('%H:%M'),
            mdates.HourLocator(byhour=(np.arange(12)*2)),
            ]
        )
    if n == 0:
        for l in ax['deconv'][n].get_xticklabels():
            l.set_ha("right")
            l.set_rotation(30)
        ax['deconv'][n].legend(
            loc='upper right', ncol=4, fontsize=fig_param['fontsize'],
            numpoints=2, markerscale=5.0/3.5
            )
    else:
        ax['deconv'][n].tick_params(
            left=False, bottom=False,
            right=True, top=False,
            labelleft=False, labelbottom=False,
            labelright=True, labeltop=False
            )
        ax['deconv'][n].patch.set_alpha(0.)
fig['deconv'].savefig(os.path.join(
    odir, f'a_deconv.png')
    )


# Seasonal diurnal composites
fk = 'seasonal'
fig_param['mpw'] = 2
fig_param['mw'] = 2*fig_param['mpw'] + 4*fig_param['mlmargin']
fig[fk] = plt.figure(figsize=(fig_param['mw'], fig_param['mh']), dpi=300)
ax[fk] = {}
pobjs[fk] = {}
panels = OrderedDict([
    ('autumn', [9, 10, 11]),
    ('winter', [12, 1, 2]),
    ('spring', [3, 4, 5]),
    ])
panel_data = OrderedDict([
    ('Rn', [obs_data.loc[obs_index]['obs_Rn_stp'], '$^{222}$Rn', 'obs']),
    ('CH4', [obs_data.loc[obs_index]['obs_CH4'], 'CH$_{4}$', 'mo1']),
    ('PBLH', [mod_data.loc[obs_index]['pblh'], 'PBLH', 'mo2']),
    ('T', [mod_data.loc[obs_index]['T'], 'T', 'bas']),
    ])

fig[fk].clf()
name_qch4_couple.plot.generic3(
    fig=fig[fk],
    axs=ax[fk],
    pobjs=pobjs[fk],
    new_axs={
        **{
            f'{k}_{n2}': [
                dict(
                    rect=[
                        ((1 + 2*(n2//2)) * fig_param['mlmargin']
                            + (n2//2) * fig_param['mpw']
                            + fig_param['mgap'])
                        / fig_param['mw'],
                        (fig_param['mh']
                            - (n1+1)*fig_param['mph']
                            + fig_param['mgap'])
                        / fig_param['mh'],
                        (fig_param['mpw']
                            - 2*fig_param['mgap'])
                        / fig_param['mw'],
                        (fig_param['mph']
                            - 2*fig_param['mgap'])
                        / fig_param['mh']
                        ],
                    label=f'{k}_{n2}',
                    projection=None 
                    ),
                dict(
                    set_xticks=[[np.arange(25)[::3]], {}],
                    set_yticks=[[
                        np.arange(800., 2400.1, 400.)
                        if not (n2 // 2) and not (n2 % 2) else
                        np.arange(1990., 2040.1, 10.)
                        if not (n2 // 2) and (n2 % 2) else
                        np.arange(0., 1200.1, 200.)
                        if (n2 // 2) and not (n2 % 2) else
                        np.arange(0., 20.1, 5.)
                        ], {}
                        ],
                    set_xlim=[[-0.5, 24.5], {}],
                    set_ylim=[[
                        [800., 2600.]
                        if not (n2 // 2) and not (n2 % 2) else
                        [1990., 2040.]
                        if not (n2 // 2) and (n2 % 2) else
                        [0., 1200.]
                        if (n2 // 2) and not (n2 % 2) else
                        [0., 20.]
                        ], {}
                        ],
                    tick_params=[[], dict(
                        axis='both', which='major', direction='in',
                        labelsize=fig_param['fontsize'],
                        left=True if not n2 % 2 else False,
                        bottom=True if not n2 % 2 else False,
                        right=False if not n2 % 2 else True,
                        top=True if not n2 % 2 else False,
                        labelleft=True if not n2 % 2 else False,
                        labelbottom=(
                            True
                            if not (n2 % 2) and (n1 == len(panels)-1) else
                            False
                            ),
                        labelright=False if not n2 % 2 else True,
                        labeltop=False
                        )]
                    ),
                dict(
                    patch_alpha=0.0
                    )
                ]
            for n1, k in enumerate(panels)
            for n2 in range(len(panel_data))
            },
        },
    new_pobjs={
        f'{k1}_{k2}': [
            f'{k1}_{n2}', 'plot',
            [
                np.arange(25),
                np.tile(
                    v2[0].loc[v2[0].index.month.isin(v1)].groupby(
                        v2[0].index[v2[0].index.month.isin(v1)].hour
                        ).mean(),
                    2
                    )[:25],
                'o-'
                ],
            dict(
                label=f'{k1}_{n2}',
                c=colours[v2[2]],
                ms=3.5, mew=0.5, mec='#000000',
                )
            ]
        for n1, (k1, v1) in enumerate(panels.items())
        for n2, (k2, v2) in enumerate(panel_data.items())
        },
    texts=[
        *[
            dict(  # yaxis
                x=(
                    (2 * ((n1 + 1) // (len(panel_data) / 2)) * fig_param['mlmargin']
                        + ((n1 + 1) // (len(panel_data) / 2)) * fig_param['mpw']
                        + (1.0 if not n1 % 2 else -1.0) * fig_param['mgap'])
                    / fig_param['mw']
                    ),
                y=(
                    (fig_param['mh']
                        - (len(panels)/2)*fig_param['mph'])
                    / fig_param['mh']
                    ),
                s={
                    'Rn': name_222Rn,
                    'CH4': name_CH4,
                    'PBLH': met['PBLH'][0],
                    'T': met['T'][0],
                    }[k],
                ha='left' if not n1 % 2 else 'right', va='center',
                size=fig_param['fontsize'], rotation=90
                )
            for n1, k in enumerate(panel_data)
            ],
        dict(  # xaxis
            x=(fig_param['mw']/2)/ fig_param['mw'],
            y=(fig_param['mgap'] / fig_param['mh']),
            s='Hour',
            ha='center', va='bottom',
            size=fig_param['fontsize']
            ),
        *[  # numberings
            dict(
                x=(
                    ((1 + 2 * (n1 // len(panels))) * fig_param['mlmargin']
                        + (n1 // len(panels)) * fig_param['mpw']
                        + 2 * fig_param['mgap'])
                    / fig_param['mw']
                    ),
                y=(
                    (
                        fig_param['mh']
                        - (n1 % len(panels)) * fig_param['mph']
                        - 2 * fig_param['mgap']
                        )
                    / fig_param['mh']
                    ),
                s=f'{alphabet[n1]})',
                ha='left', va='top',
                size=fig_param['fontsize'],
                fontweight='bold'
                )
            for n1 in range(len(panels)*2)
            ]
        ]
    )
pobjs[fk]['leg0'] = fig[fk].legend(
    handles=[
        pobjs[fk][f'autumn_{k}'][0]
        for n, (k, v) in enumerate(panel_data.items()) if n < 2
        ],
    labels=[
        v[1] for n, (k, v) in enumerate(panel_data.items()) if n < 2
        ],
    loc='upper right',
    bbox_to_anchor=[
        (fig_param['mlmargin']
            + fig_param['mpw']
            - fig_param['mgap'])
        / fig_param['mw'],
        (fig_param['mh'] - fig_param['mgap']) / fig_param['mh']
        ],
    numpoints=2, fontsize=fig_param['fontsize'], ncol=2,
    markerscale=5.0/3.5, handletextpad=0.2, columnspacing=1.0,
    borderpad=0.2, borderaxespad=0.2
    )
pobjs[fk]['leg1'] = fig[fk].legend(
    handles=[
        pobjs[fk][f'autumn_{k}'][0]
        for n, (k, v) in enumerate(panel_data.items()) if n >= 2
        ],
    labels=[
        v[1] for n, (k, v) in enumerate(panel_data.items()) if n >= 2
        ],
    loc='upper right',
    bbox_to_anchor=[
        (3*fig_param['mlmargin']
            + 2*fig_param['mpw']
            - fig_param['mgap'])
        / fig_param['mw'],
        (fig_param['mh'] - fig_param['mgap']) / fig_param['mh']
        ],
    numpoints=2, fontsize=fig_param['fontsize'], ncol=2,
    markerscale=5.0/3.5, handletextpad=0.2, columnspacing=1.0,
    borderpad=0.2, borderaxespad=0.2
    )
fig[fk].savefig(os.path.join(odir, f'a_all_{fk}.png'))

fig_param['mpw'] = fig_param_def['mpw']
fig_param['mw'] = fig_param_def['mw']

# Model
# Corrections
fig['cor'] = plt.figure(figsize=(fig_param['w'], fig_param['h']), dpi=300)
ax['cor'] = {}
fig['cor'].clf()
ax['cor'][n] =  name_qch4_couple.plot.generic(
    fig=fig['cor'],
    label='cor',
    idata={
        'obs': [
            'line',
            [dates_tHour[dom_gb_eng], obs_data['obs_CH4'].loc[dom_gb_eng], 'o'],
            {
                'c': colours['mo2'],
                'ms': 2.0, 'mew': 0.,
                'label': 'Measured'
                }
            ],
        'initial': [
            'line',
            [dates_tHour[dom_gb_eng], mod_data['chi_CH4'].loc[dom_gb_eng], 'o'],
            {
                'c': colours['obs'],
                'ms': 2.0, 'mew': 0.,
                'label': 'Initial'
                }
            ],
        'corrected': [
            'line',
            [dates_tHour, mod_data['chi_CH4_Rn_stp'], 'o'],
            {
                'c': colours['mo1'],
                'ms': 2.0, 'mew': 0.,
                'label': 'Corrected'
                }
            ],
        },
    texts=[
        {
            'x': (fig_param['ylblx'] / fig_param['w']),
            'y': fig_param['ylbly'] / fig_param['h'],
            's': ( 
                name_CH4
                ),
            'ha': 'left', 'va': 'center',
            'size': fig_param['fontsize'], 'rotation': 90, 'label': ''
            },
        ],
    xlim=[
        dates_tHour[0] - pd.to_timedelta('1D'),
        dates_tHour[-1] + pd.to_timedelta('1D')
        ],
    ylim=[0., 12000.] if n == 0 else [1900., 2400.],
    yticks=(
        np.arange(0., 12001., 2000.)
        if n == 0 else
        np.arange(1900., 2401., 100.)
        ),
    tick_fontsize=fig_param['fontsize'],
    loc_plot=[
        fig_param['px0'] / fig_param['w'],
        fig_param['py0'] / fig_param['h'],
        (fig_param['w'] - 2 * fig_param['px0']) / fig_param['w'],
        fig_param['ph'] / fig_param['h']
        ],
    xtick_params=[
        True,
        mdates.DateFormatter('%Y-%m-%d'),
        mdates.DayLocator(bymonthday=1),
        ]
    )
for l in ax['cor'][n].get_xticklabels():
    l.set_ha("right")
    l.set_rotation(30)
ax['cor'][n].legend(
    loc='upper right', ncol=4, fontsize=fig_param['fontsize'],
    markerscale=5.0/2.0
    )
fig['cor'].savefig(os.path.join(
    odir, f'a_cor.png')
    )

tranges = OrderedDict([
    ['2020-s2', ['2020-09', '2020-11']],
    ['2020-s3', ['2020-12', '2021-02']],
    ['2021-s0', ['2021-03', '2021-05']],
    ])
pvars = OrderedDict([
    ['chi_CH4', 'obs'],
    *[[f'chi_CH4_{j}', f'mo{i+1}'] for i, j in enumerate(Rn_cols)]
    ])

pblh_filters = {i: (mod_data['pblh'] > i) for i in [0, 100]}
xyticks = np.arange(1900, 2301, 100)

factor = pd.Series(np.nan, index=dates_tHour, name='cf')
factor.update(
#    pd.Series(
#        np.log2(
            obs_data['obs_Rn_stp'].loc[dom_gb_eng]
            / mod_data['ac_222Rn'].loc[dom_gb_eng]
#            ),
#        index=dates_tHour[dom_gb_eng]
#        )
    )

fig_param['mph'] = 2.0
fig_param['mh'] = 1*fig_param['mlmargin'] + 3*fig_param['mph']
fig_param['mw'] = 4*fig_param['mlmargin'] + 2*fig_param['mph']
fig['met_CH4_wind'] = plt.figure(figsize=(fig_param['mw'], fig_param['mh']), dpi=300)
for pblh, p_fil in pblh_filters.items():
    fig['met_CH4_wind'].clf()
    ax['met_CH4_wind'] = name_qch4_couple.plot.generic2(
        fig=fig['met_CH4_wind'],
        idata={
            **{
                f'{k1}_{n1}': [
                    'scatter',
                    [],
                    {
                        'x': np.deg2rad(
                            met['wd'][1][
                                dom_gb_eng & p_fil].loc[trange[0]:trange[1]]
                            ).values,
                        'y': met['ws'][1][
                            dom_gb_eng & p_fil].loc[trange[0]:trange[1]
                                ].values,
                        'c': (
                            obs_data['obs_CH4'] - mod_data[k1]
                            )[dom_gb_eng & p_fil].loc[trange[0]:trange[1]],
                        'cmap': cmap, 'norm': norm,
                        'marker': 'o', 's': 25,
                        'edgecolors': '#000000', 'linewidths': 0.5,
                        },
                    {
                        'loc_plot': [
                            ((n2+0.5)*fig_param['mlmargin']
                                + n2*fig_param['mph']
                                + fig_param['mgap']*4)
                            / fig_param['mw'],
                            (fig_param['mh']
                                - 0.5*fig_param['mbmargin']
                                - (n1+1)*fig_param['mph']
                                + fig_param['mgap']*4)
                            / fig_param['mh'],
                            (fig_param['mph']
                                - 2*fig_param['mgap']*4)
                            / fig_param['mw'],
                            (fig_param['mph']
                                - 2*fig_param['mgap']*4)
                            / fig_param['mh']
                            ],
                        'xlim': [0., 2*np.pi],
                        'ylim': [*met['ws'][2][[0, -1]]],
                        'yticks': met['ws'][2],
                        'tick_params': {
                            'axis': 'both', 'which': 'major', 'direction': 'in',
                            'labelsize': fig_param['fontsize'],
                            'left': False, 'bottom': False,
                            'right': False, 'top': False,
                            'labelleft': True,
                            'labelbottom': True,
                            'labelright': False, 'labeltop': False
                            },
                        'xtick_params': [
                            False,
                            np.deg2rad(met['wd'][2][:-1]),
                            met['wd'][3][:-1]
                            ],
                        'label': f'{k1}_{n1}',
                        'projection': 'polar',
                        'patch_alpha': 0.0,
                        }
                    ]
                for n1, (season, trange) in enumerate(tranges.items())
                for n2, (k1, v1) in enumerate(pvars.items())
                },
            },
        texts=[
            dict(  # cbar label
                x=(
                    2*fig_param['mlmargin']
                    + len(pvars)*fig_param['mph']
                    + fig_param['mgap']
                    ) / fig_param['mw'],
                y=(
                    (
                        fig_param['mh']
                        - 0.5*fig_param['mbmargin']
                        - (len(tranges)/2)*fig_param['mph']
                        - fig_param['mgap']
                        )
                    / fig_param['mh']
                    ),
                s=f'Residual {name_CH4}',
                ha='left', va='center',
                size=fig_param['fontsize'], rotation=90
                ),
            dict(  # x label
                x=(
                    (
                        fig_param['mlmargin']
                        + (len(pvars)/2)*fig_param['mph']
                        )
                    / fig_param['mw']
                    ),
                y=(
                    (
                        #fig_param['mbmargin']
                        #- fig_param['mgap']
                        fig_param['mgap']
                        )
                    / fig_param['mh']
                    ),
                s=met['ws'][0],
                ha='center', va='bottom',
                size=fig_param['fontsize'],
                ),
            *[
                dict(
                    x=(
                        ((n2+0.5)*fig_param['mlmargin']
                            + (n2+0.5)*fig_param['mph'])
                        / fig_param['mw']
                        ),
                    y=(
                        (
                        fig_param['mh']
                        - fig_param['mgap']
                            )
                        / fig_param['mh']
                        ),
                    s='Initial' if not n2 else 'Corrected',
                    ha='center', va='top',
                    size=fig_param['fontsize'],
                    )
                for n2 in range(len(pvars))
                ],
            *[
                dict(
                    x=(
                        (2*fig_param['mgap'])
                        / fig_param['mw']
                        ),
                    y=(
                        (
                            fig_param['mh']
                            - 0.5*fig_param['mbmargin']
                            - n1*fig_param['mph']
                            - 2*fig_param['mgap']
                            )
                        / fig_param['mh']
                        ),
                    s=f'{alphabet[n1]})',
                    ha='left', va='top',
                    size=fig_param['fontsize'],
                    fontweight='bold'
                    )
                for n1 in range(len(tranges))
                ]
            ],
        legend_params=[[], [], {}],
        )
    ax['met_CH4_wind']['cax'] = fig['met_CH4_wind'].add_axes([
        (
            2*fig_param['mlmargin']
            + len(pvars)*fig_param['mph']
            + 5*fig_param['mgap']
            )
        / fig_param['mw'],
        (
            fig_param['mbmargin']
            - 0.5*fig_param['mbmargin']
            + (len(tranges)-2.5)/2*fig_param['mph']
            ) / fig_param['mh'],
        0.1 / fig_param['mw'],
        2.5*fig_param['mph'] / fig_param['mh'],
        ])
    ax['met_CH4_wind']['cbar'] = fig['met_CH4_wind'].colorbar(
        ax['met_CH4_wind'][f'{list(pvars)[0]}_0'].collections[-1],
        ax['met_CH4_wind']['cax'], orientation='vertical',
        fraction=0.1, pad=0,
        format='%6.1f', ticks=cmap_ticks, extend='both'
        )
    ax['met_CH4_wind']['cbar'].ax.tick_params(
        labelsize=fig_param['fontsize'], pad=0.05
        )
    for k1, v1 in ax['met_CH4_wind'].items():
        if not k1.startswith('chi'):
            continue
        v1.set_theta_zero_location("N")
        v1.set_theta_direction(-1)
        v1.set_rlabel_position(120)
    fig['met_CH4_wind'].savefig(os.path.join(
        odir, f'a_met_wind_CH4_{pblh:03.0f}_{Qtype}.png')
        )
fig_param['mph'] = fig_param_def['mph']
fig_param['mh'] = fig_param_def['mh']
fig_param['mw'] = fig_param_def['mw']


fig_param['mph'] = 2.0
fig_param['mw'] = 4*fig_param['mlmargin'] + 2*fig_param['mph']
fig_param['mh'] = 1*fig_param['mlmargin'] + 3*fig_param['mph']
fk = 'met_wind_fac'
fig[fk] = plt.figure(figsize=(fig_param['mw'], fig_param['mh']), dpi=300)
fig[fk].clf()
ax[fk] = name_qch4_couple.plot.generic2(
    fig=fig[fk],
    idata={
        **{
            f'sca_{n2}_{n1}': [
                'scatter',
                [],
                {
                    'x': np.deg2rad(
                        met['wd'][1][
                            dom_gb_eng & p_fil].loc[trange[0]:trange[1]]
                        ).values,
                    'y': met['ws'][1][
                        dom_gb_eng & p_fil].loc[trange[0]:trange[1]
                            ].values,
                    'c': factor[dom_gb_eng & p_fil].loc[trange[0]:trange[1]],
                    'cmap': fac_cmap, 'norm': fac_norm,
                    'marker': 'o', 's': 25,
                    'edgecolors': '#000000', 'linewidths': 0.5,
                    },
                {
                    'loc_plot': [
                        ((n2+0.5)*fig_param['mlmargin']
                            + n2*fig_param['mph']
                            + fig_param['mgap']*4)
                        / fig_param['mw'],
                        (fig_param['mh']
                            - 0.5*fig_param['mbmargin']
                            - (n1+1)*fig_param['mph']
                            + fig_param['mgap']*4)
                        / fig_param['mh'],
                        (fig_param['mph']
                            - 2*fig_param['mgap']*4)
                        / fig_param['mw'],
                        (fig_param['mph']
                            - 2*fig_param['mgap']*4)
                        / fig_param['mh']
                        ],
                    'xlim': [0., 2*np.pi],
                    'ylim': [*met['ws'][2][[0, -1]]],
                    'yticks': met['ws'][2],
                    'tick_params': {
                        'axis': 'both', 'which': 'major', 'direction': 'in',
                        'labelsize': fig_param['fontsize'],
                        'left': False, 'bottom': False,
                        'right': False, 'top': False,
                        'labelleft': True,
                        'labelbottom': True,
                        'labelright': False, 'labeltop': False
                        },
                    'xtick_params': [
                        False,
                        np.deg2rad(met['wd'][2][:-1]),
                        met['wd'][3][:-1]
                        ],
                    'label': f'sca_{n2}_{n1}',
                    'projection': 'polar',
                    'patch_alpha': 0.0,
                    }
                ]
            for n1, (season, trange) in enumerate(tranges.items())
            for n2, p_fil in enumerate([~pblh_filters[100], pblh_filters[100]])
            },
        },
    texts=[
        dict(  # cbar label
            x=(
                2*fig_param['mlmargin']
                + 2*fig_param['mph']
                + fig_param['mgap']
                ) / fig_param['mw'],
            y=(
                (
                    fig_param['mh']
                    - 0.5*fig_param['mbmargin']
                    - (len(tranges)/2)*fig_param['mph']
                    - fig_param['mgap']
                    )
                / fig_param['mh']
                ),
            s=f'Correction Factor',
            ha='left', va='center',
            size=fig_param['fontsize'], rotation=90
            ),
        dict(  # x label
            x=(
                (
                    fig_param['mlmargin']
                    + (2/2)*fig_param['mph']
                    )
                / fig_param['mw']
                ),
            y=(
                (
                    #fig_param['mbmargin']
                    #- fig_param['mgap']
                    fig_param['mgap']
                    )
                / fig_param['mh']
                ),
            s=met['ws'][0],
            ha='center', va='bottom',
            size=fig_param['fontsize'],
            ),
        *[
            dict(
                x=(
                    ((n2+0.5)*fig_param['mlmargin']
                        + (n2+0.5)*fig_param['mph'])
                    / fig_param['mw']
                    ),
                y=(
                    (
                    fig_param['mh']
                    - fig_param['mgap']
                        )
                    / fig_param['mh']
                    ),
                s='PBLH < 100 m' if not n2 else 'PBLH \u2265 100 m',
                ha='center', va='top',
                size=fig_param['fontsize'],
                )
            for n2 in range(len(pvars))
            ],
        *[
            dict(
                x=(
                    (2*fig_param['mgap'])
                    / fig_param['mw']
                    ),
                y=(
                    (
                        fig_param['mh']
                        - 0.5*fig_param['mbmargin']
                        - n1*fig_param['mph']
                        - 2*fig_param['mgap']
                        )
                    / fig_param['mh']
                    ),
                s=f'{alphabet[n1]})',
                ha='left', va='top',
                size=fig_param['fontsize'],
                fontweight='bold'
                )
            for n1 in range(len(tranges))
            ]
        ],
    legend_params=[[], [], {}],
    )
ax[fk]['cax'] = fig[fk].add_axes([
    (
        2*fig_param['mlmargin']
        + 2*fig_param['mph']
        + 5*fig_param['mgap']
        )
    / fig_param['mw'],
    (
        0.5*fig_param['mbmargin']
        + (len(tranges)-2.5)/2*fig_param['mph']
        ) / fig_param['mh'],
    0.1 / fig_param['mw'],
    2.5*fig_param['mph'] / fig_param['mh'],
    ])
ax[fk]['cbar'] = fig[fk].colorbar(
    ax[fk][f'sca_0_0'].collections[-1],
    ax[fk]['cax'], orientation='vertical',
    fraction=0.1, pad=0,
    format='%6.2f', ticks=fac_cmap_ticks, extend='max'
    )
ax[fk]['cbar'].ax.tick_params(
    labelsize=fig_param['fontsize'], pad=0.05
    )
for k1, v1 in ax[fk].items():
    if not k1.startswith('sca'):
        continue
    v1.set_theta_zero_location("N")
    v1.set_theta_direction(-1)
    v1.set_rlabel_position(120)
fig[fk].savefig(os.path.join(
    odir, f'a_{fk}_{Qtype}.png')
    )
fig_param['mph'] = fig_param_def['mph']
fig_param['mh'] = fig_param_def['mh']
fig_param['mw'] = fig_param_def['mw']


fig['met_fac'] = plt.figure(figsize=(fig_param['mw'], fig_param['mh']), dpi=300)

for k, v in met.items():
    fig['met_fac'].clf()
    ax['met_fac'] = name_qch4_couple.plot.generic2(
        fig=fig['met_fac'],
        idata={
            **{
                f'hline_{n1}': [
                    'hline', [1.0], {'c': '#000000'},
                    {
                        'loc_plot': [
                            (fig_param['mlmargin'] + fig_param['mgap']) / fig_param['mw'],
                            (fig_param['mh'] - (n1+1)*fig_param['mph'] + fig_param['mgap']) / fig_param['mh'],
                            (fig_param['mpw'] - 2*fig_param['mgap']) / fig_param['mw'],
                            (fig_param['mph'] - 2*fig_param['mgap']) / fig_param['mh']
                            ],
                        'ylim': fac_ticks[[0, -1]],
                        'yticks': fac_ticks,
                        'tick_params': {
                            'axis': 'both', 'which': 'major', 'direction': 'in',
                            'labelsize': fig_param['fontsize'],
                            'left': False, 'bottom': True,
                            'right': False, 'top': True,
                            'labelleft': False,
                            'labelbottom': True if n1 == (len(tranges)-1) else False,
                            'labelright': False, 'labeltop': False
                            },
                        'xlim': [
                            v[2][0] - np.diff(v[2][[0, -1]])[0]*0.01,
                            v[2][-1] + np.diff(v[2][[0, -1]])[0]*0.01
                            ],
                        'xtick_params': [False, v[2], v[3]],
                        'label': f'hline_{n1}',
                        'projection': None,
                        'patch_alpha': 0.0,
                        }
                    ]
                for n1 in range(len(tranges))
                },
            **{
                f'{pblh}_{n1}': [
                    'line',
                    [
                        v[1][dom_gb_eng & p_fil].loc[trange[0]:trange[1]].values,
                        factor.loc[dom_gb_eng & p_fil].loc[trange[0]:trange[1]].values.flatten(),
                        'o'
                        ],
                    {
                        'c': 'None',
                        'mec': colours['obs'] if pblh == 0 else colours['mo1'],
                        'ms': 3.5, 'mew': 1.0,
                        'alpha': 1.0
                        },
                    {
                        'loc_plot': [
                            (fig_param['mlmargin'] + fig_param['mgap']) / fig_param['mw'],
                            (fig_param['mh'] - (n1+1)*fig_param['mph'] + fig_param['mgap']) / fig_param['mh'],
                            (fig_param['mpw'] - 2*fig_param['mgap']) / fig_param['mw'],
                            (fig_param['mph'] - 2*fig_param['mgap']) / fig_param['mh']
                            ],
                        'ylim': fac_ticks[[0, -1]],
                        'yticks': fac_ticks,
                        'tick_params': {
                            'axis': 'both', 'which': 'major', 'direction': 'in',
                            'labelsize': fig_param['fontsize'],
                            'left': True, 'bottom': False,
                            'right': True, 'top': False,
                            'labelleft': True,
                            'labelbottom': False,
                            'labelright': False, 'labeltop': False
                            },
                        'xlim': [
                            v[2][0] - np.diff(v[2][[0, -1]])[0]*0.01,
                            v[2][-1] + np.diff(v[2][[0, -1]])[0]*0.01
                            ],
                        'xtick_params': [False, v[2], v[3]],
                        'label': f'{pblh}_{n1}',
                        'projection': None,
                        'patch_alpha': 0.0,
                        }
                    ]
                for n1, (season, trange) in enumerate(tranges.items())
                for n2, (pblh, p_fil) in enumerate(pblh_filters.items())
                },
            },
        texts=[
            dict(
                x=fig_param['mgap'] / fig_param['mw'],
                y=(
                    (
                        fig_param['mh']
                        - (len(tranges)/2)*fig_param['mph']
                        - fig_param['mgap']
                        )
                    / fig_param['mh']
                    ),
                s=f'Correction Factor',
                ha='left', va='center',
                size=fig_param['fontsize'], rotation=90
                ),
            dict(
                x=(fig_param['mlmargin'] + fig_param['mpw']/2)/ fig_param['mw'],
                y=(fig_param['mgap'] / fig_param['mh']),
                s=v[0],
                ha='center', va='bottom',
                size=fig_param['fontsize']
                ),
            *[
                dict(
                    x=(
                        (fig_param['mlmargin'] + 2*fig_param['mgap'])
                        / fig_param['mw']
                        ),
                    y=(
                        (
                            fig_param['mh']
                            - n1*fig_param['mph']
                            - 2*fig_param['mgap']
                            )
                        / fig_param['mh']
                        ),
                    s=f'{alphabet[n1]})',
                    ha='left', va='top',
                    size=fig_param['fontsize'],
                    fontweight='bold'
                    )
                for n1 in range(len(tranges))
                ]
            ],
        legend_params=[
            ['0_0', '100_0'],
            ['PBLH < 100 m', 'PBLH \u2265 100 m'],
            {
                'loc': 'upper right',
                'bbox_to_anchor': [
                    (fig_param['mw'] - fig_param['mgap']) / fig_param['mw'],
                    (fig_param['mh'] - fig_param['mgap']) / fig_param['mh']
                    ],
                'numpoints': 1, 'fontsize': fig_param['fontsize'], 'ncol': 5,
                'markerscale': 5.0/3.5,
                'handletextpad': 0.2, 'columnspacing': 1.0,
                'borderpad': 0.2, 'borderaxespad': 0.2
                }
            ],
        )
    for l in ax['met_fac'][f'hline_{len(tranges)-1}'].get_xticklabels():
        l.set_ha("right")
        l.set_rotation(30)
    fig['met_fac'].savefig(os.path.join(
        odir, f'a_met_fac_{k}_{Qtype}.png')
        )


fig_param['mph'] = 2.0
fig_param['mw'] = 4*fig_param['mlmargin'] + 2*fig_param['mph']
fig_param['mh'] = 1*fig_param['mlmargin'] + 3*fig_param['mph']
fk = 'met_var_fac'
fig[fk] = plt.figure(figsize=(fig_param['mw'], fig_param['mh']), dpi=300)
fig[fk].clf()
ax[fk] = name_qch4_couple.plot.generic2(
    fig=fig[fk],
    idata={
        **{
            f'sca_{n2}_{n1}': [
                'scatter',
                [],
                {
                    'x': met['T'][1][
                        dom_gb_eng & p_fil].loc[trange[0]:trange[1]
                            ].values,
                    'y': met['p'][1][
                        dom_gb_eng & p_fil].loc[trange[0]:trange[1]
                            ].values,
                    'c': factor[dom_gb_eng & p_fil].loc[trange[0]:trange[1]],
                    'cmap': fac_cmap, 'norm': fac_norm,
                    'marker': 'o', 's': 25,
                    'edgecolors': '#000000', 'linewidths': 0.5,
                    },
                {
                    'loc_plot': [
                        ((1)*fig_param['mlmargin']
                            + n2*fig_param['mph']
                            + fig_param['mgap'])
                        / fig_param['mw'],
                        (fig_param['mh']
                            - 0.5*fig_param['mbmargin']
                            - (n1+1)*fig_param['mph']
                            + fig_param['mgap'])
                        / fig_param['mh'],
                        (fig_param['mph']
                            - 2*fig_param['mgap'])
                        / fig_param['mw'],
                        (fig_param['mph']
                            - 2*fig_param['mgap'])
                        / fig_param['mh']
                        ],
                    'xlim': met['T'][2][[0, -1]],
                    'ylim': met['p'][2][[0, -1]],
                    'yticks': met['p'][2][::4],
                    'tick_params': {
                        'axis': 'both', 'which': 'major', 'direction': 'in',
                        'labelsize': fig_param['fontsize'],
                        'left': True, 'bottom': True,
                        'right': True, 'top': True,
                        'labelleft': True if not n2 else False,
                        'labelbottom': True if n1 == len(tranges)-1 else False,
                        'labelright': False, 'labeltop': False
                        },
                    'xtick_params': [
                        False,
                        met['T'][2][1::2],
                        met['T'][3][1::2]
                        ],
                    'label': f'sca_{n2}_{n1}',
                    'projection': None,
                    'patch_alpha': 0.0,
                    }
                ]
            for n1, (season, trange) in enumerate(tranges.items())
            for n2, p_fil in enumerate([~pblh_filters[100], pblh_filters[100]])
            },
        },
    texts=[
        dict(  # y label
            x=(
                fig_param['mgap']
                ) / fig_param['mw'],
            y=(
                (
                    fig_param['mh']
                    - 0.5*fig_param['mbmargin']
                    - (len(tranges)/2)*fig_param['mph']
                    - fig_param['mgap']
                    )
                / fig_param['mh']
                ),
            s=met['p'][0],
            ha='left', va='center',
            size=fig_param['fontsize'], rotation=90
            ),
        dict(  # cbar label
            x=(
                2*fig_param['mlmargin']
                + 2*fig_param['mph']
                + fig_param['mgap']
                ) / fig_param['mw'],
            y=(
                (
                    fig_param['mh']
                    - 0.5*fig_param['mbmargin']
                    - (len(tranges)/2)*fig_param['mph']
                    - fig_param['mgap']
                    )
                / fig_param['mh']
                ),
            s=f'Correction Factor',
            ha='left', va='center',
            size=fig_param['fontsize'], rotation=90
            ),
        dict(  # x label
            x=(
                (
                    fig_param['mlmargin']
                    + (2/2)*fig_param['mph']
                    )
                / fig_param['mw']
                ),
            y=(
                (
                    #fig_param['mbmargin']
                    #- fig_param['mgap']
                    fig_param['mgap']
                    )
                / fig_param['mh']
                ),
            s=met['T'][0],
            ha='center', va='bottom',
            size=fig_param['fontsize'],
            ),
        *[
            dict(
                x=(
                    ((1)*fig_param['mlmargin']
                        + (n2+0.5)*fig_param['mph'])
                    / fig_param['mw']
                    ),
                y=(
                    (
                    fig_param['mh']
                    - fig_param['mgap']
                        )
                    / fig_param['mh']
                    ),
                s='PBLH < 100 m' if not n2 else 'PBLH \u2265 100 m',
                ha='center', va='top',
                size=fig_param['fontsize'],
                )
            for n2 in range(len(pvars))
            ],
        *[
            dict(
                x=(
                    (
                        fig_param['mlmargin']
                        + 2*fig_param['mgap']
                        )
                    / fig_param['mw']
                    ),
                y=(
                    (
                        fig_param['mh']
                        - 0.5*fig_param['mbmargin']
                        - n1*fig_param['mph']
                        - 2*fig_param['mgap']
                        )
                    / fig_param['mh']
                    ),
                s=f'{alphabet[n1]})',
                ha='left', va='top',
                size=fig_param['fontsize'],
                fontweight='bold'
                )
            for n1 in range(len(tranges))
            ]
        ],
    legend_params=[[], [], {}],
    )
ax[fk]['cax'] = fig[fk].add_axes([
    (
        2*fig_param['mlmargin']
        + 2*fig_param['mph']
        + 5*fig_param['mgap']
        )
    / fig_param['mw'],
    (
        0.5*fig_param['mbmargin']
        + (len(tranges)-2.5)/2*fig_param['mph']
        ) / fig_param['mh'],
    0.1 / fig_param['mw'],
    2.5*fig_param['mph'] / fig_param['mh'],
    ])
ax[fk]['cbar'] = fig[fk].colorbar(
    ax[fk][f'sca_0_0'].collections[-1],
    ax[fk]['cax'], orientation='vertical',
    fraction=0.1, pad=0,
    format='%6.2f', ticks=fac_cmap_ticks, extend='max'
    )
ax[fk]['cbar'].ax.tick_params(
    labelsize=fig_param['fontsize'], pad=0.05
    )
fig[fk].savefig(os.path.join(
    odir, f'a_{fk}_{Qtype}.png')
    )
fig_param['mph'] = fig_param_def['mph']
fig_param['mh'] = fig_param_def['mh']
fig_param['mw'] = fig_param_def['mw']


fig_param['mpw'] = 5.5
fig_param['mw'] = 3*fig_param['mlmargin'] + 1*fig_param['mpw']
fk = 'met_pblh_fac_ws'
fig[fk] = plt.figure(figsize=(fig_param['mw'], fig_param['mh']), dpi=300)
fig[fk].clf()
pcmap_ticks = np.arange(16.)
pcmap_colour = plt.get_cmap('plasma_r')(
    np.linspace(0., 1., len(pcmap_ticks)+1)
    )
pcmap = mpl.colors.ListedColormap(pcmap_colour[1:-1])
pcmap.set_over(pcmap_colour[-1])
pcmap.set_under(pcmap_colour[0])
pnorm = mpl.colors.BoundaryNorm(
    boundaries=pcmap_ticks, ncolors=pcmap.N, clip=False
    )
ax[fk] = name_qch4_couple.plot.generic2(
    fig=fig[fk],
    idata={
        **{
            f'vline_{n1}': [
                'vline', [100.0], {'c': '#000000', 'ls': ':'},
                {
                    'loc_plot': [
                        (fig_param['mlmargin'] + fig_param['mgap']) / fig_param['mw'],
                        (fig_param['mh'] - (n1+1)*fig_param['mph'] + fig_param['mgap']) / fig_param['mh'],
                        (fig_param['mpw'] - 2*fig_param['mgap']) / fig_param['mw'],
                        (fig_param['mph'] - 2*fig_param['mgap']) / fig_param['mh']
                        ],
                    'ylim': fac_ticks[[0, -1]],
                    'yticks': fac_ticks,
                    'tick_params': {
                        'axis': 'both', 'which': 'major', 'direction': 'in',
                        'labelsize': fig_param['fontsize'],
                        'left': False, 'bottom': False,
                        'right': False, 'top': False,
                        'labelleft': False,
                        'labelbottom': False,
                        'labelright': False, 'labeltop': False
                        },
                    'xlim': [
                        met['PBLH'][2][0] - np.diff(met['PBLH'][2][[0, -1]])[0]*0.01,
                        met['PBLH'][2][-1] + np.diff(met['PBLH'][2][[0, -1]])[0]*0.01
                        ],
                    'xtick_params': [False, met['PBLH'][2], met['PBLH'][3]],
                    'label': f'vline_{n1}',
                    'projection': None,
                    'patch_alpha': 0.0,
                    }
                ]
            for n1 in range(len(tranges))
            },
        **{
            f'hline_{n1}': [
                'hline', [1.0], {'c': '#000000'},
                {
                    'loc_plot': [
                        (fig_param['mlmargin'] + fig_param['mgap']) / fig_param['mw'],
                        (fig_param['mh'] - (n1+1)*fig_param['mph'] + fig_param['mgap']) / fig_param['mh'],
                        (fig_param['mpw'] - 2*fig_param['mgap']) / fig_param['mw'],
                        (fig_param['mph'] - 2*fig_param['mgap']) / fig_param['mh']
                        ],
                    'ylim': fac_ticks[[0, -1]],
                    'yticks': fac_ticks,
                    'tick_params': {
                        'axis': 'both', 'which': 'major', 'direction': 'in',
                        'labelsize': fig_param['fontsize'],
                        'left': False, 'bottom': True,
                        'right': False, 'top': True,
                        'labelleft': False,
                        'labelbottom': True if n1 == (len(tranges)-1) else False,
                        'labelright': False, 'labeltop': False
                        },
                    'xlim': [
                        met['PBLH'][2][0] - np.diff(met['PBLH'][2][[0, -1]])[0]*0.01,
                        met['PBLH'][2][-1] + np.diff(met['PBLH'][2][[0, -1]])[0]*0.01
                        ],
                    'xtick_params': [False, met['PBLH'][2], met['PBLH'][3]],
                    'label': f'hline_{n1}',
                    'projection': None,
                    'patch_alpha': 0.0,
                    }
                ]
            for n1 in range(len(tranges))
            },
        **{
            f'pblh_{n1}': [
                'scatter',
                [],
                {
                    'x': met['PBLH'][1][dom_gb_eng].loc[trange[0]:trange[1]].values,
                    'y': factor.loc[dom_gb_eng].loc[trange[0]:trange[1]].values.flatten(),
                    'c': met['ws'][1][
                        dom_gb_eng].loc[trange[0]:trange[1]
                            ].values,
                    'cmap': pcmap, 'norm': pnorm,
                    'marker': 'o', 's': 25,
                    'edgecolors': '#000000', 'linewidths': 0.5,
                    'alpha': 1.0
                    },
                {
                    'loc_plot': [
                        (fig_param['mlmargin'] + fig_param['mgap']) / fig_param['mw'],
                        (fig_param['mh'] - (n1+1)*fig_param['mph'] + fig_param['mgap']) / fig_param['mh'],
                        (fig_param['mpw'] - 2*fig_param['mgap']) / fig_param['mw'],
                        (fig_param['mph'] - 2*fig_param['mgap']) / fig_param['mh']
                        ],
                    'ylim': fac_ticks[[0, -1]],
                    'yticks': fac_ticks,
                    'tick_params': {
                        'axis': 'both', 'which': 'major', 'direction': 'in',
                        'labelsize': fig_param['fontsize'],
                        'left': True, 'bottom': False,
                        'right': True, 'top': False,
                        'labelleft': True,
                        'labelbottom': False,
                        'labelright': False, 'labeltop': False
                        },
                    'xlim': [
                        met['PBLH'][2][0] - np.diff(met['PBLH'][2][[0, -1]])[0]*0.01,
                        met['PBLH'][2][-1] + np.diff(met['PBLH'][2][[0, -1]])[0]*0.01
                        ],
                    'xtick_params': [False, met['PBLH'][2], met['PBLH'][3]],
                    'label': f'{pblh}_{n1}',
                    'projection': None,
                    'patch_alpha': 0.0,
                    }
                ]
            for n1, (season, trange) in enumerate(tranges.items())
            },
        },
    texts=[
        dict(
            x=fig_param['mgap'] / fig_param['mw'],
            y=(
                (
                    fig_param['mh']
                    - (len(tranges)/2)*fig_param['mph']
                    - fig_param['mgap']
                    )
                / fig_param['mh']
                ),
            s=f'Correction Factor',
            ha='left', va='center',
            size=fig_param['fontsize'], rotation=90
            ),
        dict(
            x=(fig_param['mlmargin'] + fig_param['mpw']/2)/ fig_param['mw'],
            y=(fig_param['mgap'] / fig_param['mh']),
            s=met['PBLH'][0],
            ha='center', va='bottom',
            size=fig_param['fontsize']
            ),
        dict(  # cbar label
            x=(
                1*fig_param['mlmargin']
                + 1*fig_param['mpw']
                + fig_param['mgap']
                ) / fig_param['mw'],
            y=(
                (
                    fig_param['mh']
                    - (len(tranges)/2)*fig_param['mph']
                    - fig_param['mgap']
                    )
                / fig_param['mh']
                ),
            s=met['ws'][0],
            ha='left', va='center',
            size=fig_param['fontsize'], rotation=90
            ),
        *[  # numberings
            dict(
                x=(
                    (fig_param['mlmargin'] + 2*fig_param['mgap'])
                    / fig_param['mw']
                    ),
                y=(
                    (
                        fig_param['mh']
                        - n1*fig_param['mph']
                        - 2*fig_param['mgap']
                        )
                    / fig_param['mh']
                    ),
                s=f'{alphabet[n1]})',
                ha='left', va='top',
                size=fig_param['fontsize'],
                fontweight='bold'
                )
            for n1 in range(len(tranges))
            ]
        ],
    legend_params=[[], [], {}],
    )
for l in ax[fk][f'hline_{len(tranges)-1}'].get_xticklabels():
    l.set_ha("right")
    l.set_rotation(30)
ax[fk]['cax'] = fig[fk].add_axes([
    (
        (1+0.1)*fig_param['mlmargin']
        + 1*fig_param['mpw']
        + 5*fig_param['mgap']
        )
    / fig_param['mw'],
    (
        1*fig_param['mbmargin']
        + (len(tranges)-2.5)/2*fig_param['mph']
        ) / fig_param['mh'],
    0.1 / fig_param['mw'],
    2.5*fig_param['mph'] / fig_param['mh'],
    ])
ax[fk]['cbar'] = fig[fk].colorbar(
    ax[fk][f'pblh_0'].collections[-1],
    ax[fk]['cax'], orientation='vertical',
    fraction=0.1, pad=0,
    format='%5.1f', ticks=pcmap_ticks, extend='max'
    )
ax[fk]['cbar'].ax.tick_params(
    labelsize=fig_param['fontsize'], pad=0.05
    )
fig[fk].savefig(os.path.join(
    odir, f'a_{fk}_{Qtype}.png')
    )
fig_param['mpw'] = fig_param_def['mpw']
fig_param['mw'] = fig_param_def['mw']


fig_param['mph'] = 2.0
fig_param['mw'] = 4*fig_param['mlmargin'] + 2*fig_param['mph']
fig_param['mh'] = 1*fig_param['mlmargin'] + 3*fig_param['mph']
fk = 'met_var_pblh'
pcmap_ticks = met['PBLH'][2]
pcmap_colour = plt.get_cmap('plasma_r')(
    np.linspace(0., 1., len(pcmap_ticks)+1)
    )
pcmap = mpl.colors.ListedColormap(pcmap_colour[1:-1])
pcmap.set_over(pcmap_colour[-1])
pcmap.set_under(pcmap_colour[0])
pnorm = mpl.colors.BoundaryNorm(
    boundaries=pcmap_ticks, ncolors=pcmap.N, clip=False
    )
fig[fk] = plt.figure(figsize=(fig_param['mw'], fig_param['mh']), dpi=300)
fig[fk].clf()
ax[fk] = name_qch4_couple.plot.generic2(
    fig=fig[fk],
    idata={
        **{
            f'sca_{n2}_{n1}': [
                'scatter',
                [],
                {
                    'x': met['T'][1][
                        dom_gb_eng & p_fil].loc[trange[0]:trange[1]
                            ].values,
                    'y': met['p'][1][
                        dom_gb_eng & p_fil].loc[trange[0]:trange[1]
                            ].values,
                    'c': met['PBLH'][1][
                        dom_gb_eng & p_fil].loc[trange[0]:trange[1]
                            ].values,
                    'cmap': pcmap, 'norm': pnorm,
                    'marker': 'o', 's': 25,
                    'edgecolors': '#000000', 'linewidths': 0.5,
                    },
                {
                    'loc_plot': [
                        ((1)*fig_param['mlmargin']
                            + n2*fig_param['mph']
                            + fig_param['mgap'])
                        / fig_param['mw'],
                        (fig_param['mh']
                            - 0.5*fig_param['mbmargin']
                            - (n1+1)*fig_param['mph']
                            + fig_param['mgap'])
                        / fig_param['mh'],
                        (fig_param['mph']
                            - 2*fig_param['mgap'])
                        / fig_param['mw'],
                        (fig_param['mph']
                            - 2*fig_param['mgap'])
                        / fig_param['mh']
                        ],
                    'xlim': met['T'][2][[0, -1]],
                    'ylim': met['p'][2][[0, -1]],
                    'yticks': met['p'][2][::4],
                    'tick_params': {
                        'axis': 'both', 'which': 'major', 'direction': 'in',
                        'labelsize': fig_param['fontsize'],
                        'left': True, 'bottom': True,
                        'right': True, 'top': True,
                        'labelleft': True if not n2 else False,
                        'labelbottom': True if n1 == len(tranges)-1 else False,
                        'labelright': False, 'labeltop': False
                        },
                    'xtick_params': [
                        False,
                        met['T'][2][1::2],
                        met['T'][3][1::2]
                        ],
                    'label': f'sca_{n2}_{n1}',
                    'projection': None,
                    'patch_alpha': 0.0,
                    }
                ]
            for n1, (season, trange) in enumerate(tranges.items())
            for n2, p_fil in enumerate([~pblh_filters[100], pblh_filters[100]])
            },
        },
    texts=[
        dict(  # y label
            x=(
                fig_param['mgap']
                ) / fig_param['mw'],
            y=(
                (
                    fig_param['mh']
                    - 0.5*fig_param['mbmargin']
                    - (len(tranges)/2)*fig_param['mph']
                    - fig_param['mgap']
                    )
                / fig_param['mh']
                ),
            s=met['p'][0],
            ha='left', va='center',
            size=fig_param['fontsize'], rotation=90
            ),
        dict(  # cbar label
            x=(
                2*fig_param['mlmargin']
                + 2*fig_param['mph']
                + fig_param['mgap']
                ) / fig_param['mw'],
            y=(
                (
                    fig_param['mh']
                    - 0.5*fig_param['mbmargin']
                    - (len(tranges)/2)*fig_param['mph']
                    - fig_param['mgap']
                    )
                / fig_param['mh']
                ),
            s=f'Correction Factor',
            ha='left', va='center',
            size=fig_param['fontsize'], rotation=90
            ),
        dict(  # x label
            x=(
                (
                    fig_param['mlmargin']
                    + (2/2)*fig_param['mph']
                    )
                / fig_param['mw']
                ),
            y=(
                (
                    #fig_param['mbmargin']
                    #- fig_param['mgap']
                    fig_param['mgap']
                    )
                / fig_param['mh']
                ),
            s=met['T'][0],
            ha='center', va='bottom',
            size=fig_param['fontsize'],
            ),
        *[
            dict(
                x=(
                    ((1)*fig_param['mlmargin']
                        + (n2+0.5)*fig_param['mph'])
                    / fig_param['mw']
                    ),
                y=(
                    (
                    fig_param['mh']
                    - fig_param['mgap']
                        )
                    / fig_param['mh']
                    ),
                s='PBLH < 100 m' if not n2 else 'PBLH \u2265 100 m',
                ha='center', va='top',
                size=fig_param['fontsize'],
                )
            for n2 in range(len(pvars))
            ],
        *[
            dict(
                x=(
                    (
                        fig_param['mlmargin']
                        + 2*fig_param['mgap']
                        )
                    / fig_param['mw']
                    ),
                y=(
                    (
                        fig_param['mh']
                        - 0.5*fig_param['mbmargin']
                        - n1*fig_param['mph']
                        - 2*fig_param['mgap']
                        )
                    / fig_param['mh']
                    ),
                s=f'{alphabet[n1]})',
                ha='left', va='top',
                size=fig_param['fontsize'],
                fontweight='bold'
                )
            for n1 in range(len(tranges))
            ]
        ],
    legend_params=[[], [], {}],
    )
ax[fk]['cax'] = fig[fk].add_axes([
    (
        2*fig_param['mlmargin']
        + 2*fig_param['mph']
        + 5*fig_param['mgap']
        )
    / fig_param['mw'],
    (
        0.5*fig_param['mbmargin']
        + (len(tranges)-2.5)/2*fig_param['mph']
        ) / fig_param['mh'],
    0.1 / fig_param['mw'],
    2.5*fig_param['mph'] / fig_param['mh'],
    ])
ax[fk]['cbar'] = fig[fk].colorbar(
    ax[fk][f'sca_0_0'].collections[-1],
    ax[fk]['cax'], orientation='vertical',
    fraction=0.1, pad=0,
    format='%6.2f', ticks=pcmap_ticks, extend='max'
    )
ax[fk]['cbar'].ax.tick_params(
    labelsize=fig_param['fontsize'], pad=0.05
    )
fig[fk].savefig(os.path.join(
    odir, f'a_{fk}_{Qtype}.png')
    )
fig_param['mph'] = fig_param_def['mph']
fig_param['mh'] = fig_param_def['mh']
fig_param['mw'] = fig_param_def['mw']

# =============================================================================
# Multiple Linear Regresion Model
# =============================================================================
print('Multiple Linear Regression Model')
var_atm =  ['x_wind', 'y_wind', 'T', 'p', 'pblh']
v_ys = {
    pblh:
    pd.DataFrame(
        {
            'correction': (
                factor
                #mod_data['chi_CH4_Rn_stp'] - mod_data['chi_CH4']
                )[dom_gb_eng & p_fil],
            'correction+': (
                factor
                )[dom_gb_eng & p_fil & (factor > 1)],
            'correction-': (
                factor
                )[dom_gb_eng & p_fil & (factor < 1)],
            #'residual_ini': (
            #    obs_data['obs_CH4'] - mod_data['chi_CH4']
            #    )[dom_gb_eng & p_fil],
            #'residual_cor': (
            #    obs_data['obs_CH4'] - mod_data['chi_CH4_Rn_stp']
            #    )[dom_gb_eng & p_fil],
            },
        index=dates_tHour[dom_gb_eng & p_fil]
        )
    for pblh, p_fil in pblh_filters.items()
    }
v_xs = {
    pblh: mod_data.loc[dom_gb_eng & p_fil, var_atm].copy()
    for pblh, p_fil in pblh_filters.items()
    }
v_y_stds = {}
for pblh, v_y in v_ys.items():
    v_y_stds[pblh] = v_y.copy()
    v_y_stds[pblh][['correction']] = (
        (v_y[['correction']] - 1)
        / (np.max(np.abs(v_y[['correction']].values - 1)))
        )
    v_y_stds[pblh][['correction+']] = (
        (v_y[['correction+']] - 1)
        / (np.max(np.abs(v_y[['correction']].values - 1)))
        )
    v_y_stds[pblh][['correction-']] = (
        (v_y[['correction-']] - 1)
        / (np.max(np.abs(v_y[['correction']].values - 1)))
        )
    #v_y_stds[pblh][['residual_ini']] = (
    #    v_y[['residual_ini']]
    #    / np.max(np.abs(v_y[['residual_ini', 'residual_cor']].values))
    #    )
    #v_y_stds[pblh][['residual_cor']] = (
    #    v_y[['residual_cor']]
    #    / np.max(np.abs(v_y[['residual_ini', 'residual_cor']].values))
    #    )
v_x_stds = {}
for pblh, v_x in v_xs.items():
    v_x['T'] += 273.15
    v_x_stds[pblh] = (v_x - v_x.min()) / (v_x.max() - v_x.min())
    v_x_stds[pblh][['x_wind', 'y_wind']] = (
        v_x[['x_wind', 'y_wind']]
        / mod_data.loc[dom_gb_eng & p_fil, ['wind_speed']].abs().max().values[0]
        )

models = {}
models_stats = {
    pblh:
    pd.DataFrame(
        np.nan,
        index=pd.MultiIndex.from_tuples(
            [
                (i, j)
                for i in tranges
                for j in [*v_ys[list(pblh_filters)[0]]]
                ],
            ),
        columns=['R2', 'N']
        )
    for pblh in pblh_filters
    }
models_result = {
    pblh:
    pd.DataFrame(
        np.nan,
        index=pd.MultiIndex.from_tuples(
            [
                (i, j, k)
                for i in tranges
                for j in [*v_ys[list(pblh_filters)[0]]]
                for k in ['c', 'p', 'var', 'ts']
                ],
            ),
        columns=['intercept', *v_xs[list(pblh_filters)[0]]]
        )
    for pblh in pblh_filters
    }
for pblh, v_x_std in v_x_stds.items():
    models[pblh] = {}
    for y in v_y_stds[pblh]:
        models[pblh][y] = {}
        model = models[pblh][y]
        for tstr, trange in tranges.items():
            v_y_sel = v_y_stds[pblh][y].loc[trange[0] : trange[1]].dropna()
            if v_y_sel.size == 0:
                continue
            v_x_sel = v_x_std.loc[v_y_sel.index]#.loc[trange[0] : trange[1]]
            model[tstr] = linear_model.LinearRegression()
            model[tstr].fit(v_x_sel, v_y_sel)
            v_x_new = pd.DataFrame(
                {'intercept': np.ones(v_x_sel.shape[0])}, index=v_x_sel.index
                ).join(v_x_sel)
            predict = model[tstr].predict(v_x_sel)
            MSE = (
                sum((v_y_sel - predict)**2)
                / np.abs(np.diff(v_x_new.shape)))
            models_result[pblh].loc[(tstr, y, 'c')] = (
                [model[tstr].intercept_, *model[tstr].coef_]
                )
            models_result[pblh].loc[(tstr, y, 'var')] = (
                MSE * (np.linalg.inv(np.dot(v_x_new.T, v_x_new)).diagonal())
                )
            models_result[pblh].loc[(tstr, y, 'ts')] = (
                models_result[pblh].loc[(tstr, y, 'c')]
                / models_result[pblh].loc[(tstr, y, 'var')]**.5
                )
            models_result[pblh].loc[(tstr, y, 'p')] = np.array([
                2
                * (
                    1 - scipy.stats.t.cdf(
                        np.abs(i), np.abs(np.diff(v_x_new.shape))
                        )[0]
                    )
                for i in models_result[pblh].loc[(tstr, y, 'ts')]
                ])
            models_stats[pblh].loc[(tstr, y), 'R2'] = model[tstr].score(
                v_x_sel, v_y_sel
                )
            models_stats[pblh].loc[(tstr, y), 'N'] = v_y_sel.size

print(
    models_result[0].xs('c', level=2).mask(
        models_result[0].xs('p', level=2) >= 0.01
        ).apply(
            lambda x: x.apply(lambda y: f'{y:5.2f}' if np.isfinite(y) else '')
            )
    )

print(
    models_result[100].xs('c', level=2).mask(
        models_result[100].xs('p', level=2) >= 0.01
        ).apply(
            lambda x: x.apply(lambda y: f'{y:5.2f}' if np.isfinite(y) else '')
            )
    )

# =============================================================================
# PCA
# =============================================================================
"""
v_x = pd.concat([
    mod_data.loc[
        dom_gb_eng,
        ['x_wind', 'y_wind', 'T', 'p', 'pblh']
        ].copy(),
    pd.Series(
        (mod_data['chi_CH4_Rn_stp'] - obs_data['obs_CH4'])[dom_gb_eng],
        index=dates_tHour[dom_gb_eng], name='residual'
        )
    ], axis=1)
v_x.pblh = np.log(v_x.pblh)

for tstr, trange in {
        '2020-s2': ['2020-09', '2020-11'],
        '2020-s3': ['2020-12', '2021-02'],
        '2021-s0': ['2021-03', '2021-05'],
        }.items():
    v_x_sel = v_x.loc[trange[0] : trange[1]]
    print(f"N\t: {v_x_sel.shape[0]}")
    print()
    print("KMO test\n  T\t: {}\n  p\t: {}".format(*KMO(v_x_sel)))
    print()
    print("Bartlett test\n  T\t: {}\n  p\t: {}".format(
        *scipy.stats.bartlett(*[v_x_sel[i] for i in v_x]))
        )
    print()
    print(f"Skewness")
    print(*[f"  {i:10s}\t: {v_x_sel[i].skew():+8.4f}" for i in v_x_sel], sep="\n")
    v_x_std = (v_x_sel - v_x_sel.mean()) / v_x_sel.std()
    #cor_x = v_x.corr()
    #eig_values, eig_vectors = np.linalg.eig(cor_x)
    #comp_x = np.dot(cor_x, eig_vectors)
    pca = PCA(.95)
    pca.fit(v_x_std)
    scores = pca.transform(v_x_std).shape
"""


sys.exit()















fig_param['mph'] = 2.0
fig_param['mh'] = 1*fig_param['mlmargin'] + 3*fig_param['mph']
fig_param['mw'] = 3*fig_param['mlmargin'] + 2*fig_param['mph']
fig['met_CH4'] = plt.figure(figsize=(fig_param['mw'], fig_param['mh']), dpi=300)
for k, v in met.items():
    pcmap_ticks = v[2]
    pcmap_colour = plt.get_cmap('plasma_r' if k != 'wd' else 'twilight')(
        np.linspace(0., 1., len(pcmap_ticks)+1)
        )
    pcmap = mpl.colors.ListedColormap(pcmap_colour[1:-1])
    pcmap.set_over(pcmap_colour[-1])
    pcmap.set_under(pcmap_colour[0])
    pnorm = mpl.colors.BoundaryNorm(
        boundaries=pcmap_ticks, ncolors=pcmap.N, clip=False
        )
    for pblh, p_fil in pblh_filters.items():
        fig['met_CH4'].clf()
        ax['met_CH4'] = name_qch4_couple.plot.generic2(
            fig=fig['met_CH4'],
            idata={
                **{
                    f'dline_{n1}_{n2}': [
                        'line',
                        [[*xyticks[[0, -1]]], [*xyticks[[0, -1]]], '-'],
                        {'c': '#000000'},
                        {
                            'loc_plot': [
                                (fig_param['mlmargin']
                                    + n2*fig_param['mph']
                                    + fig_param['mgap'])
                                / fig_param['mw'],
                                (fig_param['mh']
                                    - (n1+1)*fig_param['mph']
                                    + fig_param['mgap'])
                                / fig_param['mh'],
                                (fig_param['mph']
                                    - 2*fig_param['mgap'])
                                / fig_param['mw'],
                                (fig_param['mph']
                                    - 2*fig_param['mgap'])
                                / fig_param['mh']
                                ],
                            'ylim': [*xyticks[[0, -1]]],
                            'yticks': xyticks,
                            'tick_params': {
                                'axis': 'both', 'which': 'major', 'direction': 'in',
                                'labelsize': fig_param['fontsize'],
                                'left': True, 'bottom': True,
                                'right': True, 'top': True,
                                'labelleft': True if n2 == 0 else False,
                                'labelbottom': True if n1 == len(tranges)-1 else False,
                                'labelright': False, 'labeltop': False
                                },
                            'xlim': [*xyticks[[0, -1]]],
                            'xtick_params': [
                                False,
                                xyticks,
                                xyticks
                                ],
                            'label': f'hline_{n1}',
                            'projection': None,
                            'patch_alpha': 0.0,
                            }
                        ]
                    for n1 in range(len(tranges))
                    for n2, (k1, v1) in enumerate(pvars.items())
                    },
                **{
                    f'{k1}_{n1}': [
                        'scatter',
                        [],
                        {
                            'x': obs_data['obs_CH4'][dom_gb_eng & p_fil].loc[
                                trange[0]:trange[1]
                                ].values,
                            'y': mod_data[k1][dom_gb_eng & p_fil].loc[
                                trange[0]:trange[1]
                                ].values,
                            'c': v[1][dom_gb_eng & p_fil].loc[
                                trange[0]:trange[1]
                                ].values,
                            'cmap': pcmap, 'norm': pnorm,
                            'marker': 'o', 's': 10,
                            'edgecolors': '#000000', 'linewidths': 0.5,
                            },
                        {
                            'loc_plot': [
                                (fig_param['mlmargin']
                                    + n2*fig_param['mph']
                                    + fig_param['mgap'])
                                / fig_param['mw'],
                                (fig_param['mh']
                                    - (n1+1)*fig_param['mph']
                                    + fig_param['mgap'])
                                / fig_param['mh'],
                                (fig_param['mph']
                                    - 2*fig_param['mgap'])
                                / fig_param['mw'],
                                (fig_param['mph']
                                    - 2*fig_param['mgap'])
                                / fig_param['mh']
                                ],
                            'ylim': [*xyticks[[0, -1]]],
                            'yticks': xyticks,
                            'tick_params': {
                                'axis': 'both', 'which': 'major', 'direction': 'in',
                                'labelsize': fig_param['fontsize'],
                                'left': False, 'bottom': False,
                                'right': False, 'top': False,
                                'labelleft': False,
                                'labelbottom': False,
                                'labelright': False, 'labeltop': False
                                },
                            'xlim': [*xyticks[[0, -1]]],
                            'xtick_params': [
                                False,
                                xyticks,
                                xyticks
                                ],
                            'label': f'{k1}_{n1}',
                            'projection': None,
                            'patch_alpha': 0.0,
                            }
                        ]
                    for n1, (season, trange) in enumerate(tranges.items())
                    for n2, (k1, v1) in enumerate(pvars.items())
                    },
                },
            texts=[
                dict(  # y label
                    x=fig_param['mgap'] / fig_param['mw'],
                    y=(
                        (
                            fig_param['mh']
                            - (len(tranges)/2)*fig_param['mph']
                            - fig_param['mgap']
                            )
                        / fig_param['mh']
                        ),
                    s=f'Modelled {name_CH4}',
                    ha='left', va='center',
                    size=fig_param['fontsize'], rotation=90
                    ),
                dict(  # cbar label
                    x=(
                        fig_param['mlmargin']
                        + len(pvars)*fig_param['mph']
                        + fig_param['mgap']
                        ) / fig_param['mw'],
                    y=(
                        (
                            fig_param['mh']
                            - (len(tranges)/2)*fig_param['mph']
                            - fig_param['mgap']
                            )
                        / fig_param['mh']
                        ),
                    s=v[0],
                    ha='left', va='center',
                    size=fig_param['fontsize'], rotation=90
                    ),
                dict(  # x label
                    x=(
                        (
                            fig_param['mlmargin']
                            + (len(pvars)/2)*fig_param['mph']
                            )
                        / fig_param['mw']
                        ),
                    y=(
                        (
                            #fig_param['mbmargin']
                            #- fig_param['mgap']
                            fig_param['mgap']
                            )
                        / fig_param['mh']
                        ),
                    s=f'Measured {name_CH4}',
                    ha='center', va='bottom',
                    size=fig_param['fontsize'],
                    ),
                *[
                    dict(
                        x=(
                            (fig_param['mlmargin'] + 2*fig_param['mgap'])
                            / fig_param['mw']
                            ),
                        y=(
                            (
                                fig_param['mh']
                                - n1*fig_param['mph']
                                - 2*fig_param['mgap']
                                )
                            / fig_param['mh']
                            ),
                        s=f'{alphabet[n1]})',
                        ha='left', va='top',
                        size=fig_param['fontsize'],
                        fontweight='bold'
                        )
                    for n1 in range(len(tranges))
                    ]
                ],
            legend_params=[[], [], {}],
            )
        for n2 in range(len(pvars)):
            ax['met_CH4'][f'dline_{len(tranges)-1}_{n2}'].set_xticklabels([
                '',
                *[
                    l.get_text()
                    for l in  ax['met_CH4'][
                        f'dline_{len(tranges)-1}_{n2}'
                        ].get_xticklabels()[1:-1]
                    ],
                ''
                ])
            for l in ax['met_CH4'][f'dline_{len(tranges)-1}_{n2}'].get_xticklabels():
                l.set_ha("right")
                l.set_rotation(30)
        ax['met_CH4']['cax'] = fig['met_CH4'].add_axes([
            (
                fig_param['mlmargin']
                + len(pvars)*fig_param['mph']
                + 5*fig_param['mgap']
                )
            / fig_param['mw'],
            (
                fig_param['mbmargin']
                + (len(tranges)-2.5)/2*fig_param['mph']
                ) / fig_param['mh'],
            0.1 / fig_param['mw'],
            2.5*fig_param['mph'] / fig_param['mh'],
            ])
        ax['met_CH4']['cbar'] = fig['met_CH4'].colorbar(
            ax['met_CH4'][f'{list(pvars)[0]}_0'].collections[-1],
            ax['met_CH4']['cax'], orientation='vertical',
            fraction=0.1, pad=0,
            format='%6.1f', ticks=pcmap_ticks, extend='both'
            )
        ax['met_CH4']['cbar'].ax.tick_params(
            labelsize=fig_param['fontsize'], pad=0.05
            )
        fig['met_CH4'].savefig(os.path.join(
            odir, f'a_met_{k}_{pblh:03.0f}_{Qtype}.png')
            )
fig_param['mph'] = fig_param_def['mph']
fig_param['mh'] = fig_param_def['mh']
fig_param['mw'] = fig_param_def['mw']






fig['met_res'] = plt.figure(figsize=(fig_param['mw'], fig_param['mh']), dpi=300)
for k, v in met.items():
    for pblh, p_fil in pblh_filters.items():
        fig['met_res'].clf()
        ax['met_res'] = name_qch4_couple.plot.generic2(
            fig=fig['met_res'],
            idata={
                **{
                    f'hline_{n1}': [
                        'hline', [0.0], {'c': '#000000'},
                        {
                            'loc_plot': [
                                (fig_param['mlmargin'] + fig_param['mgap']) / fig_param['mw'],
                                (fig_param['mh'] - (n1+1)*fig_param['mph'] + fig_param['mgap']) / fig_param['mh'],
                                (fig_param['mpw'] - 2*fig_param['mgap']) / fig_param['mw'],
                                (fig_param['mph'] - 2*fig_param['mgap']) / fig_param['mh']
                                ],
                            'ylim': [-100, 100],
                            'yticks': np.arange(-100, 101, 50),
                            'tick_params': {
                                'axis': 'both', 'which': 'major', 'direction': 'in',
                                'labelsize': fig_param['fontsize'],
                                'left': False, 'bottom': True,
                                'right': False, 'top': True,
                                'labelleft': False,
                                'labelbottom': True if n1 == (len(tranges)-1) else False,
                                'labelright': False, 'labeltop': False
                                },
                            'xlim': [
                                v[2][0] - np.diff(v[2][[0, -1]])[0]*0.01,
                                v[2][-1] + np.diff(v[2][[0, -1]])[0]*0.01
                                ],
                            'xtick_params': [False, v[2], v[3]],
                            'label': f'hline_{n1}',
                            'projection': None,
                            'patch_alpha': 0.0,
                            }
                        ]
                    for n1 in range(len(tranges))
                    },
                **{
                    f'{k1}_{n1}': [
                        'line',
                        [
                            v[1][dom_gb_eng & p_fil].loc[trange[0]:trange[1]].values,
                            (
                                obs_data['obs_CH4'] - mod_data[k1]
                                )[dom_gb_eng & p_fil].loc[trange[0]:trange[1]].values,
                            'o'
                            ],
                        {
                            'c': colours[v1], 'ms': 3.5, 'mew': 0.,
                            'alpha': 1.0 if not n2 else 0.7
                            },
                        {
                            'loc_plot': [
                                (fig_param['mlmargin'] + fig_param['mgap']) / fig_param['mw'],
                                (fig_param['mh'] - (n1+1)*fig_param['mph'] + fig_param['mgap']) / fig_param['mh'],
                                (fig_param['mpw'] - 2*fig_param['mgap']) / fig_param['mw'],
                                (fig_param['mph'] - 2*fig_param['mgap']) / fig_param['mh']
                                ],
                            'ylim': [-100, 100],
                            'yticks': np.arange(-100, 101, 50),
                            'tick_params': {
                                'axis': 'both', 'which': 'major', 'direction': 'in',
                                'labelsize': fig_param['fontsize'],
                                'left': True if n2 == 0 else False, 'bottom': False,
                                'right': True if n2 == 0 else False, 'top': False,
                                'labelleft': True if n2 == 0 else False,
                                'labelbottom': False,
                                'labelright': False, 'labeltop': False
                                },
                            'xlim': [
                                v[2][0] - np.diff(v[2][[0, -1]])[0]*0.01,
                                v[2][-1] + np.diff(v[2][[0, -1]])[0]*0.01
                                ],
                            'xtick_params': [False, v[2], v[3]],
                            'label': f'{k1}_{n1}',
                            'projection': None,
                            'patch_alpha': 0.0,
                            }
                        ]
                    for n1, (season, trange) in enumerate(tranges.items())
                    for n2, (k1, v1) in enumerate(pvars.items())
                    },
                },
            texts=[
                dict(
                    x=fig_param['mgap'] / fig_param['mw'],
                    y=(
                        (
                            fig_param['mh']
                            - (len(tranges)/2)*fig_param['mph']
                            - fig_param['mgap']
                            )
                        / fig_param['mh']
                        ),
                    s=f'Residual {name_CH4}',
                    ha='left', va='center',
                    size=fig_param['fontsize'], rotation=90
                    ),
                dict(
                    x=(fig_param['mlmargin'] + fig_param['mpw']/2)/ fig_param['mw'],
                    y=(fig_param['mgap'] / fig_param['mh']),
                    s=v[0],
                    ha='center', va='bottom',
                    size=fig_param['fontsize']
                    ),
                *[
                    dict(
                        x=(
                            (fig_param['mlmargin'] + 2*fig_param['mgap'])
                            / fig_param['mw']
                            ),
                        y=(
                            (
                                fig_param['mh']
                                - n1*fig_param['mph']
                                - 2*fig_param['mgap']
                                )
                            / fig_param['mh']
                            ),
                        s=f'{alphabet[n1]})',
                        ha='left', va='top',
                        size=fig_param['fontsize'],
                        fontweight='bold'
                        )
                    for n1 in range(len(tranges))
                    ]
                ],
            legend_params=[
                [f'{k1}_0' for k1 in pvars],
                ['Initial', 'Corrected'],
                {
                    'loc': 'upper right',
                    'bbox_to_anchor': [
                        (fig_param['mw'] - fig_param['mgap']) / fig_param['mw'],
                        (fig_param['mh'] - fig_param['mgap']) / fig_param['mh']
                        ],
                    'numpoints': 1, 'fontsize': fig_param['fontsize'], 'ncol': 5,
                    'handletextpad': 0.2, 'columnspacing': 1.0,
                    'borderpad': 0.2, 'borderaxespad': 0.2
                    }
                ],
            )
        for l in ax['met_res'][f'hline_{len(tranges)-1}'].get_xticklabels():
            l.set_ha("right")
            l.set_rotation(30)
        fig['met_res'].savefig(os.path.join(
            odir, f'a_met_{k}_{pblh:03.0f}_residual_{Qtype}.png')
            )
for tstr, trange in {
        '2020-s2': ['2020-09', '2020-11'],
        '2020-s3': ['2020-12', '2021-02'],
        '2021-s0': ['2021-03', '2021-05'],
        }.items():
    for pblh, p_fil in pblh_filters.items():
        for k, v in met.items():
            x = v[1][dom_gb_eng & p_fil].loc[trange[0]:trange[1]]
            fig['met_res'].clf()
            ax['met_res'] =  name_qch4_couple.plot.generic(
                fig=fig['met_res'],
                idata={
                    'hline': ['hline', [0.0], {'c': '#000000'}],
                    'Initial': [
                        'line',
                        [
                            x,
                            (
                                obs_data['obs_CH4'] - mod_data['chi_CH4']
                                )[dom_gb_eng & p_fil].loc[trange[0]:trange[1]],
                            'o'
                            ],
                        {'c': colours['obs'], 'ms': 3.5, 'mew': 0., 'label': 'Initial'}
                        ],
                    **{
                        i: [
                            'line',
                            [
                                x,
                                (
                                    obs_data['obs_CH4'] - mod_data[f'chi_CH4_{i}']
                                    )[dom_gb_eng & p_fil].loc[trange[0]:trange[1]],
                                'o'
                                ],
                            {
                                'c': colours[f'mo{n+1}'], 'ms': 3.5, 'mew': 0.,
                                'label': 'Corrected' if i == 'Rn_stp' else i
                                }
                            ]
                        for n, i in enumerate(Rn_cols)
                        },
                    },
                texts=[
                    {
                        'x': fig_param['ylblx'] / fig_param['w'],
                        'y': fig_param['ylbly'] / fig_param['h'],
                        's': u'$\Delta\chi$ CH$_{4}$ (nmol mol$^{-1}$)',
                        'ha': 'left', 'va': 'center',
                        'size': fig_param['fontsize'], 'rotation': 90, 'label': ''
                        },
                    {
                        'x': (fig_param['px0'] + 0.5*fig_param['pw']) / fig_param['w'],
                        'y': 0.05 / fig_param['h'],
                        's': v[0],
                        'ha': 'center', 'va': 'bottom',
                        'size': fig_param['fontsize'], 'label': ''
                        }
                    ],
                xlim=[
                    v[2][0] - np.diff(v[2][[0, -1]])[0]*0.01,
                    v[2][-1] + np.diff(v[2][[0, -1]])[0]*0.01
                    ],
                ylim=[-100., 100.],
                yticks=np.arange(-100., 150., 50.),
                tick_fontsize=fig_param['fontsize'],
                loc_plot=[
                    fig_param['px0'] / fig_param['w'],
                    fig_param['py0'] / fig_param['h'],
                    fig_param['pw'] / fig_param['w'],
                    fig_param['ph'] / fig_param['h']
                    ],
                xtick_params=[False, v[2], v[3]]
                )
            for l in ax['met_res'].get_xticklabels():
                l.set_ha("right")
                l.set_rotation(30)
            ax['met_res'].legend(
                loc='upper right', ncol=4, fontsize=fig_param['fontsize']
                )
            fig['met_res'].savefig(os.path.join(
                odir, f'a_{tstr}_met_{k}_{pblh:03.0f}_dCH4_Rn_{Qtype}.png')
                )
        fig['met_res'].clf()
        for n in range(2):
            k = 'ws'
            ax['met_res'] = name_qch4_couple.plot.generic(
                fig=fig['met_res'],
                projection='polar',
                idata={
                    'scatter': [
                        'scatter',
                        [],
                        {
                            'x': np.deg2rad(
                                met['wd'][1][dom_gb_eng & p_fil].loc[trange[0]:trange[1]]
                                ),
                            'y': met['ws'][1][dom_gb_eng & p_fil].loc[trange[0]:trange[1]],
                            'c': (
                                (
                                    mod_data['chi_CH4']
                                    if n == 0 else
                                    mod_data['chi_CH4_Rn_stp']
                                    )
                                - obs_data['obs_CH4']
                                )[dom_gb_eng & p_fil].loc[trange[0]:trange[1]],
                            'cmap': cmap, 'norm': norm,
                            #'vmin': cmap_ticks[0],
                            #'vmax': cmap_ticks[-1],
                            's': 25.0, 'edgecolors': '#000000', 'linewidths': 0.5,
                            'label': 'Initial'
                            }
                        ],
                    },
                texts=[
                    {
                        'x': fig_param['ylblx'] / fig_param['w'] - 5,
                        'y': fig_param['ylbly'] / fig_param['h'],
                        's': u'$\Delta\chi$ CH$_{4}$ (nmol mol$^{-1}$)',
                        'ha': 'left', 'va': 'center',
                        'size': fig_param['fontsize'], 'rotation': 90, 'label': ''
                        },
                    {
                        'x': -(fig_param['px0'] + 0.5*fig_param['pw']) / fig_param['w'],
                        'y': 0.05 / fig_param['h'],
                        's': v[0],
                        'ha': 'center', 'va': 'bottom',
                        'size': fig_param['fontsize'], 'label': ''
                        }
                    ],
                xlim=[0., 2*np.pi],
                ylim=[*met['ws'][2][[0, -1]]],
                yticks=met['ws'][2],
                tick_fontsize=fig_param['fontsize'],
                loc_plot=[
                    (fig_param['polar_px0']*(1 + n*2) + fig_param['polar_pw']*n)
                    / fig_param['w'],
                    fig_param['polar_py0'] / fig_param['h'],
                    fig_param['polar_pw'] / fig_param['w'],
                    fig_param['polar_ph'] / fig_param['h']
                    ],
                xtick_params=[
                    False, met['wd'][2][:-1]/180*np.pi, met['wd'][3][:-1]
                    ]
                )
            #for l in ax['met_res'].get_xticklabels():
            #    l.set_ha("right")
            #    l.set_rotation(30)
            ax['met_res'].set_theta_zero_location("N")
            ax['met_res'].set_theta_direction(-1)
            ax['met_res'].set_rlabel_position(120)
        n = 2
        cax = fig['met_res'].add_axes([
            (fig_param['polar_px0']*(n*2) + fig_param['polar_pw']*n)
            / fig_param['w'],
            fig_param['polar_py0'] / fig_param['h'],
            0.1 / fig_param['w'],
            fig_param['polar_ph'] / fig_param['h'],
            ])
        cbar = fig['met_res'].colorbar(
            ax['met_res'].collections[-1], cax, orientation='vertical',
            fraction=0.1, pad=0,
            format='%6.1f', ticks=cmap_ticks, extend='both'
            )
        fig['met_res'].savefig(os.path.join(
            odir, f'a_{tstr}_metwind_{k}_{pblh:03.0f}_dCH4_Rn_{Qtype}.png')
            )


