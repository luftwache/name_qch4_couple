r"""

Plot output summary - inversion

"""
# Standard Library imports
import argparse
import datetime
import h5py
import json
import matplotlib.cm
import matplotlib.dates as mdates
import matplotlib.pyplot as plt
import netCDF4
import numpy as np
import os
import pandas as pd
import psutil
import pymc3 as pm
import re
import sys
import theano
import theano.tensor as tt
import xarray as xr

# Third party imports
from collections import OrderedDict

# Semi-local imports
import name_qch4_couple.io
import name_qch4_couple.name
import name_qch4_couple.plot
import name_qch4_couple.region_EU
import name_qch4_couple.routines
import name_qch4_couple.util

# Local imports
import routines


def calc_unc_mean(x):
    r"""Calculate uncertainties from a mean
    """
    if np.array(x).size == 0:
        return np.nan
    elif np.isfinite(x).all():
        return(np.nansum(x*x)**0.5/np.isfinite(x).sum())
    else:
        return np.nan


def aceil(i, sf, dtype=0):
    r"""Ceiling to sf
    """
    if dtype == 0:
        log0 = np.log10(np.abs(i))
        n0 = i / 10**(np.floor(log0) - sf + 1)
        o = np.ceil(n0) * 10**(np.floor(log0) - sf + 1)
    else:
        o = np.ceil(i / 10**sf) * 10**sf
    return o


def afloor(i, sf, dtype=0):
    r"""Flooring to sf
    """
    if dtype == 0:
        log0 = np.log10(np.abs(i))
        n0 = i / 10**(np.floor(log0) - sf + 1)
        o = np.floor(n0) * 10**(np.floor(log0) - sf + 1)
    else:
        o = np.floor(i / 10**sf) * 10**sf
    return o


def adiv(imin, imax, nmin, nmax):
    r"""
    """
    diff = imax - imin
    x = np.floor(np.log10(np.abs(diff)))
    steps = np.array([m*10**i for i in np.arange(x-1, x+2) for m in [1, 2, 5]])
    nsteps = np.array([np.arange(imin, imax, i).size for i in steps])
    step = steps[np.argmin(
        np.mean([np.abs(nsteps - nmin), np.abs(nsteps - nmax)], axis=0)
        )]
    return step


def aticks(imin, imax, nmin, nmax, sf, dtype=0):
    r"""
    """
    i0 = afloor(imin, sf, dtype)
    i1 = aceil(imax, sf, dtype)
    if i1 - i0 == 0.:
        i0 -= 10.**sf * 0.4
        i1 += 10.**sf * 0.4
    step = adiv(i0, i1, nmin, nmax)
    return [[i0, i1], step]


def calc_final_d(m, d1, d2, R_std, M, rdates, dim_sAll):
    r"""
    """
    ns = np.arange(len(m))
    R1 = [(d1[n] * 1.e-3 + 1.) * R_std[1] for n in ns]
    R2 = [(d2[n] * 1.e-3 + 1.) * R_std[2] for n in ns]
    RsumM = [1.*M[0] + R1[n]*M[1] + R2[n]*M[2] for n in ns]
    chi0 = [
        m[n].set_index(
            m[n].index.to_period('M'), append=True).mul(
                pd.DataFrame(
                    1. / RsumM[n],
                    index=pd.to_datetime(rdates).to_period('1M'),
                    columns=range(dim_sAll)
                    ),
                level=1
                )
        for n in ns
        ]
    chi1 = [
        m[n].set_index(
            m[n].index.to_period('M'), append=True).mul(
                pd.DataFrame(
                    R1[n] / RsumM[n],
                    index=pd.to_datetime(rdates).to_period('1M'),
                    columns=range(dim_sAll)
                    ),
                level=1
                )
        for n in ns
        ]
    chi2 = [
        m[n].set_index(
            m[n].index.to_period('M'), append=True).mul(
                pd.DataFrame(
                    R2[n] / RsumM[n],
                    index=pd.to_datetime(rdates).to_period('1M'),
                    columns=range(dim_sAll)
                    ),
                level=1
                )
        for n in ns
        ]
    return chi0, chi1, chi2


print('Start Post-processing')
# Argument Parser
parser = argparse.ArgumentParser()
parser.add_argument("-sfile", required=True)
parser.add_argument("-rmode", required=True, type=int)
parser.add_argument("-tmode", required=True, type=int)
parser.add_argument("-sdate", required=True)
parser.add_argument("-edate", required=True,)
parser.add_argument("-cfile", required=True)
parser.add_argument("-odir", required=True)
args = parser.parse_args()

sfile = args.sfile
rmode = args.rmode
tmode = args.tmode
sdate = args.sdate
edate = args.edate
cfile = args.cfile
odir = args.odir
if not os.path.exists(odir):
    os.mkdir(odir)

with open(sfile) as f:
    iset = json.load(f)
    sites = iset['sites']
with open(cfile) as f:
    iset = json.load(f)
    codes = [i[0] for i in iset["sce_list"]]
    idirs = [i[1] for i in iset["sce_list"]]

sce = 1.

# Read station info
field_dirs = OrderedDict()
mix_dirs = OrderedDict()
particle_dirs = OrderedDict()
obs_files = OrderedDict()
long_names = OrderedDict()
locations = OrderedDict()
field_prefix = OrderedDict()
mix_prefix = OrderedDict()
particle_prefix = OrderedDict()
met_file_old = OrderedDict()
met_dirs = OrderedDict()
met_prefix = OrderedDict()
for site in sites:
    with open(f'/home/ec5/name_qch4_couple/stations/{site}.json', 'r') as f:
        st_info = json.load(f)
        field_dirs[site] = OrderedDict(st_info['field_dir'])
        mix_dirs[site] = OrderedDict(st_info['mix_dir'])
        particle_dirs[site] = OrderedDict(st_info['particle_dir'])
        obs_files[site] = OrderedDict(st_info['obs_files'])
        long_names[site] = st_info['long_name']
        locations[site] = st_info['location']
        field_prefix[site] = OrderedDict(st_info['field_prefix'])
        mix_prefix[site] = OrderedDict(st_info['mix_prefix'])
        particle_prefix[site] = OrderedDict(st_info['particle_prefix'])
        if 'met_file_old' in st_info:
            met_file_old[site] = OrderedDict(st_info['met_file_old'])
        if 'met_dir' in st_info:
            met_dirs[site] = OrderedDict(st_info['met_dir'])
        if 'met_prefix' in st_info:
            met_prefix[site] = OrderedDict(st_info['met_prefix'])

print('  NCG')
# Network compatibility goals for measurement-model mismatch
NCG = {'chi': 2., 'd_13C': 0.02, 'd_2H': 1}
#NCG = {'chi': 5., 'd_13C': 0.2, 'd_2H': 5}  # extended
NCG_mul = {'chi': 2., 'd_13C': 1., 'd_2H': 1.}

# Constants
R_std = np.array([
    1.0,
    1.12372000e-02,
    6.23040000e-04,
    7.00122509e-06,
    1.45567066e-07
    ])
M = np.array([
    16.031300,
    17.034655,
    17.037577,
    18.040932,
    18.043854
    ])

print('  Dates')
# Dates
rdates = pd.date_range(sdate, edate, freq='1MS').strftime('%Y-%m').tolist()

if tmode in [0, 1, 2]:
    dates_tHour = pd.date_range(
        pd.to_datetime(rdates[0]),
        pd.to_datetime(rdates[-1]) + pd.DateOffset(months=1),
        freq='1H',
        closed='left'
        )
else:
    dates_tHour = pd.date_range(
        pd.to_datetime(rdates[0]),
        pd.to_datetime(rdates[-1]) + pd.DateOffset(months=1),
        freq='1H',
        closed='left'
        ).to_series().between_time('0900', '1500').index
dates_tDay = dates_tHour.floor('D').unique()
dates_plot = [
    pd.to_datetime(rdates)
    + pd.to_timedelta(
        (
            pd.to_datetime(rdates).days_in_month
            * (i+1) / (len(idirs)+bool(sce)+1)
            ),
        'D'
        )
    for i in range(len(idirs) + bool(sce))
    ]

print('  Observations (Measurements)')
# Observations
obs, obs_err = routines.read_obs(
    sites=sites,
    obs_files=obs_files,
    date_range=[dates_tDay[i].strftime('%Y-%m-%d') for i in [0, -1]],
    dates_tHour=dates_tHour
    )

# Footprint files
## Dosage
f_mix_paths = {
    site:
    pd.concat(
        [
            dates_tDay.strftime(
                os.path.join(v[k], mix_prefix[site][k])
                ).to_series(index=dates_tDay).apply(np.vectorize(
                    lambda x: x if os.path.exists(x) else False
                    ))
            for k in sorted(v)
            ],
        axis=1
        ).replace(False, np.nan)
    for site, v in mix_dirs.items()
    }
f_mix_path = {
    site:
    v.bfill(axis=1).iloc[:, 0].replace(np.nan, False)
    for site, v in f_mix_paths.items()
    }
f_field_paths = {
    site:
    pd.concat(
        [
           dates_tDay.strftime(
                os.path.join(v[k], field_prefix[site][k])
                ).to_series(index=dates_tDay).apply(np.vectorize(
                    lambda x: x if os.path.exists(x) else False
                    ))
            for k in sorted(v)
            ],
        axis=1
        ).replace(False, np.nan)
    for site, v in field_dirs.items()
    }
f_field_path = {
    site:
    v.bfill(axis=1).iloc[:, 0].replace(np.nan, False)
    for site, v in f_field_paths.items()
    }
f_foot_path = {
    site:
    pd.concat(
        [
            f_mix_path[site].replace(False, np.nan),
            f_field_path[site].replace(False, np.nan)
            ],
        axis=1
        ).bfill(axis=1).iloc[:, 0].replace(np.nan, False)
    for site in sites
    }
## Particles
f_particles_paths = {
    site:
    pd.concat(
        [
            dates_tDay.strftime(
                os.path.join(v[k], particle_prefix[site][k])
                ).to_series(index=dates_tDay).apply(np.vectorize(
                    lambda x: x if os.path.exists(x) else False
                    ))
            for k in sorted(v)
            ],
        axis=1
        ).replace(False, np.nan)
    for site, v in particle_dirs.items()
    }
f_particles_path = {
    site:
    v.bfill(axis=1).iloc[:, 0].replace(np.nan, False)
    for site, v in f_particles_paths.items()
    }

# Date index
dates_foot = {
    site:
    pd.concat(
        [f_foot_path[site], f_particles_path[site]], axis=1
        ).replace(False, np.nan).dropna().index
    for site in sites
    }
dates_tObs = { #dates_tObs
    site: {
        k:
        v.resample('H').mean().dropna().index.to_series().apply(
            np.vectorize(
                lambda x: x if x.date() in dates_foot[site].date else pd.NaT
                )
            ).reindex(index=dates_tHour).dropna().index
        for k, v in y.items()
        }
    for site, y in obs.items()
    }
dates_tSite = { #dates_tSite
    site: pd.DatetimeIndex(np.unique(np.concatenate([v for v in d.values()])))
    for site, d in dates_tObs.items()
    }
dates_tComp = pd.DatetimeIndex( # dates_tComp
    np.unique(np.concatenate([d for d in dates_tSite.values()]))
    )
nx_tH_tObs = { #nx_tH_tObs
    site: {
        k: dates_tHour.get_indexer(v)
        for k, v in d.items()
        }
    for site, d in dates_tObs.items()
    }

print('  Grid')
# Grid
grid_info = routines.define_grid()
inv_reg_map0 = grid_info['inv_reg_map']
nlat = grid_info['nlat']
nlon = grid_info['nlon']
inv_lat0 = grid_info['inv_lat0']
inv_lat1 = grid_info['inv_lat1']
inv_lon0 = grid_info['inv_lon0']
inv_lon1 = grid_info['inv_lon1']
area = grid_info['area']
grid_vertex = grid_info['grid_vertex']
inv_reg_uniq = grid_info['inv_reg_uniq']
inv_reg_codes = grid_info['inv_reg_codes']
inv_mask = np.full((nlat, nlon), False)
inv_mask[inv_lat0:inv_lat1, inv_lon0:inv_lon1] = True

# Observation and state vector
obs_y = {}
obs_y_err = {}
prior_obs = {}
post_obs = {}
prior_state = {}
post_state = {}
def_state = {}
dm0_s = {}
dm1_s = {}
dm2_s_pri = {}
dm2_s_sce = {}
dm2_s_pos = {}
inv_reg_maps = {}
reg_map_sel = {}
inv_Q_dist = {}
focus_rgroup_uniq = {}

for n, idir in enumerate(idirs):
    print(f'Read {idir}')
    c = codes[n]
    (
        obs_y[c], obs_y_err[c], prior_obs[c], post_obs[c],
        prior_state[c], post_state[c], def_state[c],
        dm0_s[c], dm1_s[c], dm2_s_pri[c], dm2_s_sce[c], dm2_s_pos[c],
        inv_reg_maps[c], reg_map_sel[c], inv_Q_dist[c],
        focus_rgroup_uniq[c], dim_sAll
        ) = routines.read_results(
            0 if c.startswith('s0') else sce, idir, rdates, sites,
            0 if c.startswith('s0') else 1,
            dates_tHour, dates_tSite, dates_tObs, grid_info, obs
            )

if rmode == 0:
    sys.exit()

# =============================================================================
# Plots
# =============================================================================
print('Plot Generation')
print('  Plot Preparation')
plt.close('all')
# Plot parameters
plot_params = {
    'dQi1': {
        'ytick':
            [[-80., -10.], 10.]
        },
    'dQi2': {
        'ytick':
            [[-450., -130.], 50.]
        },
    }
y_name = {
    'chi': u'$\chi$ (nmol mol$^{-1}$)',
    'd_13C': u'$\delta$$^{13}$CH$_4$ (\u2030)',
    'd_2H': u'$\delta$$^{12}$CH$_3$D (\u2030)',
    }
colours = {
    #0: '#005555',  # observation
    #1: '#8888FF',  # prior
    #2: '#FF8800',  # posterior0
    #3: '#886600',  # posterior1
    0: '#005555',  # prior
    1: '#aa4499',  # observation
    2: '#ee7733',  # posterior0
    3: '#88ccee',  # posterior1
    #0: '#fc8e50',  # observation
    #1: '#000000',  # prior
    #2: '#6699cc',  # posterior0
    #3: '#994455',  # posterior1
    #0: '#FC8E50',  # observation
    #1: '#000000',  # prior lres
    #2: '#008DE1',  # posterior
    #3: '#464E00',  # prior hres
    }
sector_names = {
    0: ['FF', 'Fossil Fuel'],
    1: ['WM', 'Waste Management'],
    2: ['MA', 'Anthropogenic Microbial'],
    3: ['BB', 'Biomass Burning'],
    4: ['MN', 'Natural Microbial'],
    5: ['Total', 'Total']
    }
subplot_hgt_obs0 = 1
subplot_wdt_obs0 = 5
subplot_hgt_obs1 = 1.5
subplot_wdt_obs1 = 1.0
subplot_hgt_QA = 1
subplot_wdt_QA = 5
subplot_hgt_Qmap = 1.5
subplot_wdt_Qmap = 2.1
subplot_wdt_Qgbie = 1
subplot_gap = 0.05
subplot_xmargin = 0.6
fontsize = 8
alphabet = 'abcdefghijklmnopqrstuvwxyz'
s_sel = [0, 1, 2, 4, 5]

fig_param = {
    'w': 6, 'h': 3,
    'px0': 0.80, 'py0': 0.50,
    'pw': 5.15, 'ph': 2.45,
    'ylblx': 0.05, 'ylbly': 1.5,  # left, centre aligned
    'fontsize': 8,
    'polar_px0': 0.30, 'polar_py0': 0.50,
    'polar_pw': 2.00, 'polar_ph': 2.00,
    'mw': 6, 'mh': 5.0,
    'mpw': 5.5, 'mph': 1.5,
    'mgap': 0.05,
    'mlmargin': 0.5, 'mbmargin': 0.5,
    }
fig_param_def = fig_param.copy()

PlateCarree = name_qch4_couple.plot.ccrs.PlateCarree()
shapefile = ( 
    '/home/ec5/name_qch4_couple/shape_files/'
    'CNTR_BN_10M_2016_4326.shp'
    )

plotable_abs = np.full(
    (len(rdates), dim_sAll, grid_info['inv_nlat'], grid_info['inv_nlon']),
    np.nan
    )
ytick_abs = [
    [  # 0
        np.array([
            *-np.power(10., np.linspace(-8., -13., 11)),
            *np.power(10., np.linspace(-13., -8., 11))
            ]),
        np.array([
            *-np.power(10., np.linspace(-8., -12., 3)),
            *np.power(10., np.linspace(-12., -8., 3))
            ])
        ],
    [  # 1
        np.array([
            *-np.power(10., np.linspace(-8., -13., 11)),
            *np.power(10., np.linspace(-13., -8., 11))
            ]),
        np.array([
            *-np.power(10., np.linspace(-8., -12., 3)),
            *np.power(10., np.linspace(-12., -8., 3))
            ])
        ],
    [  # 2
        np.array([
            *-np.power(10., np.linspace(-8., -13., 11)),
            *np.power(10., np.linspace(-13., -8., 11))
            ]),
        np.array([
            *-np.power(10., np.linspace(-8., -12., 3)),
            *np.power(10., np.linspace(-12., -8., 3))
            ])
        ],
    [  # 4
        np.array([
            *-np.power(10., np.linspace(-8., -13., 11)),
            *np.power(10., np.linspace(-13., -8., 11))
            ]),
        np.array([
            *-np.power(10., np.linspace(-8., -12., 3)),
            *np.power(10., np.linspace(-12., -8., 3))
            ])
        ],
    [  # 5
        np.array([
            *-np.power(10., np.linspace(-6., -12., 7)),
            *np.power(10., np.linspace(-12., -6., 7))
            ]),
        np.array([
            *-np.power(10., np.linspace(-6., -12., 4)),
            *np.power(10., np.linspace(-12., -6., 4))
            ])
        ],
    ]
plotable_rel = np.full(
    (len(rdates), dim_sAll, grid_info['inv_nlat'], grid_info['inv_nlon']),
    np.nan
    )
ytick_rel = [
    np.array([
        *-np.linspace(1.2, 0., 13)[:-1],
        *np.linspace(0., 1.2, 13)[1:]
        ]),
    np.array([
        *-np.linspace(1.2, 0., 4)[:-1],
        0.,
        *np.linspace(0., 1.2, 4)[1:]
        ])
    ]
grid_sel = [
    grid_vertex[0][inv_lon0:inv_lon1+1],
    grid_vertex[1][inv_lat0:inv_lat1+1],
    ]

# Define plots
figs = {}
axs = {}
pobjs = {}

# =============================================================================
# site_loc0: site locations
# =============================================================================
sites1 = ['mhd', 'hfd', 'tac', 'cbw020']
fk = 'site_loc0'

fig_param['w'] = 4
fig_param['h'] = 3

figs[fk] = plt.figure(figsize=(fig_param['w'], fig_param['h']), dpi=300)
axs[fk] = {}

figs[fk].clf()
axs[fk][0] = name_qch4_couple.plot.generic2(
    fig=figs[fk],
    idata={
        f'stations': [
            'scatter',
            [],
            {
                'x': [v[0] for v in locations.values()],
                'y': [v[1] for v in locations.values()],
                'c': '#0000ff',
                's': 20.0, 'linewidths': 1.0, 'edgecolors': '#000000'
                },
            {
                'loc_plot': [
                    fig_param['mgap'] / fig_param['w'],
                    fig_param['mgap'] / fig_param['h'],
                    (fig_param['w'] - 2*fig_param['mgap']) / fig_param['w'],
                    (fig_param['h'] - 2*fig_param['mgap']) / fig_param['h'],
                    ],
                'label': f'stations',
                'projection': PlateCarree,
                'patch_alpha': 0.0,
                },
            {   
                'set_extent': [[[-12, 6, 48, 62], PlateCarree], {}],
                'add_feature': [
                    [
                        name_qch4_couple.plot.cfeature.ShapelyFeature(
                            name_qch4_couple.plot.cio.shapereader.Reader(
                                shapefile
                                ).geometries(),
                            PlateCarree,
                            edgecolor='black',
                            linewidth=0.5,
                            facecolor='none'
                            )
                        ],
                    {}
                    ],
                },
            ]
        },
    texts=[],
    legend_params=[[], [], {}]
    )
[
    #axs[fk][0]['stations'].text(
    axs[fk][0]['stations'].annotate(
        xy=(v1, v2),
        xytext=(-10, 2),
        textcoords='offset points',
        #x=v1, y=v2,
        text=re.match('[a-z]+', k1)[0].upper(),
        #s=re.match('[a-z]+', k1)[0].upper()+' ',
        ha='right', va='center', size=fontsize,
        bbox={
            'boxstyle': 'round', 'pad': 0.1,
            'facecolor': '#ffffff', 'edgecolor': '#ffffff',
            'alpha': 0.5
            }
        )
    for k1, (v1, v2) in locations.items()
    ]
figs[fk].savefig(os.path.join(odir, f'{fk}.png'))

fig_param['w'] = fig_param_def['w']
fig_param['h'] = fig_param_def['h']

# =============================================================================
# for0: forward models
# =============================================================================
obs['cbw'] = {}
obs['cbw']['chi'] = (
    pd.read_csv(
        '/home/ec5/hpc-work/data_archive/cesar/cbw_200m_ch4.csv',
        parse_dates=['time'], index_col='time')
    ).loc[sdate:edate, 'ch4']
dates_tObs['cbw'] = {}
dates_tObs['cbw']['chi'] = (
    obs['cbw']['chi'].resample('1H').mean().loc[dates_tComp].dropna()
    ).index
nx_tH_tObs['cbw'] = {}
nx_tH_tObs['cbw']['chi'] = dates_tHour.get_indexer(dates_tObs['cbw']['chi'])
prior_obs['s0obq0']['cbw'] = {}
prior_obs['s0obq0']['cbw']['chi'] = np.concatenate([
    np.load(os.path.join(idirs[2], rdate, 'pri_y.npz'))['cbw_chi']
    for rdate in rdates
    ])
prior_obs['s0obq1']['cbw'] = {}
prior_obs['s0obq1']['cbw']['chi'] = np.concatenate([
    np.load(os.path.join(idirs[3], rdate, 'pri_y.npz'))['cbw_chi']
    for rdate in rdates
    ])
obs_y_err['s0obq0']['cbw'] = {}
obs_y_err['s0obq0']['cbw']['chi'] = np.concatenate([
    np.load(os.path.join(idirs[2], rdate, 'obs_y.npz'))['var_cbw_chi']
    for rdate in rdates
    ])

scodes1 = ['s0']
scodes2 = ['obq0', 'obq1']
sites1 = ['mhd', 'hfd', 'tac', 'cbw', 'cbw020']
y_vars = ['chi', 'd_13C', 'd_2H']

# Obs
yticks = {}
yrange = {}
sites_y = []
for scode in scodes1:
    codes_sen = [f'{scode}{i}' for i in scodes2]
    for site in sites1:
        for k in y_vars:
            if (k not in obs[site]) or (not np.count_nonzero(obs[site][k].values)):
                continue
            sites_y.append((site, k))
            sk = f'{scode}_{site}_{k}'
            ymin = np.nanmin(np.concatenate([
                *[
                    obs[site][k].resample('h').mean().loc[dates_tObs[site][k]].squeeze()
                    for c in codes_sen
                    ],
                *[prior_obs[c][site][k][nx_tH_tObs[site][k]] for c in codes_sen],
                ]))
            ymax = np.nanmax(np.concatenate([
                *[
                    obs[site][k].resample('h').mean().loc[dates_tObs[site][k]].squeeze()
                    for c in codes_sen
                    ],
                *[prior_obs[c][site][k][nx_tH_tObs[site][k]] for c in codes_sen],
                ]))
            ysf = np.floor(np.log10(np.abs([
                i if i != 0 else 1 for i in [ymin, ymax]
                ]))).max() - 1
            ysf = 1 if not ysf and k not in ['d_13C'] else ysf
            yticks[sk] = aticks(ymin, ymax, 3, 5, ysf, 1)
            yrange[sk] = yticks[sk][0][1] - yticks[sk][0][0]
            sk = f'{scode}_{site}_{k}_res'
            ymin = np.nanmin(np.concatenate([
                *[
                    obs[site][k].resample('h').mean().loc[dates_tObs[site][k]].squeeze()
                    - prior_obs[c][site][k][nx_tH_tObs[site][k]]
                    for c in codes_sen
                    ],
                ]))
            ymax = np.nanmax(np.concatenate([
                *[
                    obs[site][k].resample('h').mean().loc[dates_tObs[site][k]].squeeze()
                    - prior_obs[c][site][k][nx_tH_tObs[site][k]]
                    for c in codes_sen
                    ],
                ]))
            ysf = np.floor(np.log10(np.abs([
                i if i != 0 else 1 for i in [ymin, ymax]
                ]))).max() - 1
            ysf = 1 if not ysf and k not in ['d_13C'] else ysf
            yticks[sk] = aticks(ymin, ymax, 3, 5, ysf, 1)
            yrange[sk] = yticks[sk][0][1] - yticks[sk][0][0]
subplot_hgt_obs0 = 1
fs = (
        subplot_wdt_obs0 + subplot_xmargin,
        8*subplot_hgt_obs0 + 0.5
        )

fk = 'for0_obs'

figs[fk] = plt.figure(figsize=fs, dpi=300)
axs[fk] = {}

figs[fk].clf()
axs[fk][0] = name_qch4_couple.plot.generic2(
    fig=figs[fk],
    idata={
        **{
            f'obs_{c}_{site}_{k}': [
                'line',
                [
                    dates_tObs[site][k],
                    obs[site][k].resample('H').mean().loc[dates_tObs[site][k]],
                    'o'
                    ],
                {
                    'c': colours[1], 'ls': '',
                    'ms': 5.0, 'mew': 0.0,#0.5, 'mec': '#000000',
                    'zorder': 2
                    },
                {
                    'loc_plot': [
                        (subplot_xmargin + subplot_gap) / fs[0],
                        (fs[1] - (n+p+1)*subplot_hgt_obs0 + subplot_gap) / fs[1],
                        (subplot_wdt_obs0 - 2*subplot_gap) / fs[0],
                        (subplot_hgt_obs0 - 2*subplot_gap) / fs[1]
                        ],
                    'ylim': yticks[f'{scode}_{site}_{k}'][0],
                    'yticks': np.arange(
                        yticks[f'{scode}_{site}_{k}'][0][0],
                        yticks[f'{scode}_{site}_{k}'][0][1]
                        + yticks[f'{scode}_{site}_{k}'][1],
                        yticks[f'{scode}_{site}_{k}'][1]
                        ),
                    'tick_params': {
                        'axis': 'both', 'which': 'major', 'direction': 'in',
                        'labelsize': fontsize,
                        'left': True, 'bottom': False,
                        'right': False, 'top': False,
                        'labelleft': True, 'labelbottom': False,
                        'labelright': False, 'labeltop': False
                        },
                    'xlim': [
                        np.datetime64(dates_tHour[0]) - np.timedelta64(1, 'D'),
                        np.datetime64(dates_tHour[-1]) + np.timedelta64(1, 'D'),
                        ],
                    'xtick_params': [
                        True,
                        mdates.DateFormatter('%Y-%m-%d'),
                        mdates.DayLocator(bymonthday=(1, 16)),
                        ],
                    'label': f'obs_{scode}_{site}_{k}',
                    'projection': None,
                    'patch_alpha': 0.0,
                    },
                ]
            for n, scode in enumerate(scodes1)
            for o, c in enumerate([f'{scode}{i}' for i in [scodes2[0]]])
            for p, (site, k) in enumerate(sites_y)
            },
        **{
            f'pri_{c}_{site}_{k}': [
                'line',
                [
                    dates_tObs[site][k],
                    prior_obs[c][site][k][nx_tH_tObs[site][k]],
                    'o'
                    ],
                {
                    'c': 'None',
                    'mec': colours[o+2],
                    'ls': '',
                    'ms': 5.0, 'mew': 1.0,
                    'zorder': 1
                    },
                {
                    'loc_plot': [
                        (subplot_xmargin + subplot_gap) / fs[0],
                        (fs[1] - (n+p+1)*subplot_hgt_obs0 + subplot_gap) / fs[1],
                        (subplot_wdt_obs0 - 2*subplot_gap) / fs[0],
                        (subplot_hgt_obs0 - 2*subplot_gap) / fs[1]
                        ],
                    'ylim': yticks[f'{scode}_{site}_{k}'][0],
                    'yticks': np.arange(
                        yticks[f'{scode}_{site}_{k}'][0][0],
                        yticks[f'{scode}_{site}_{k}'][0][1]
                        + yticks[f'{scode}_{site}_{k}'][1],
                        yticks[f'{scode}_{site}_{k}'][1]
                        ),
                    'tick_params': {
                        'axis': 'both', 'which': 'major', 'direction': 'in',
                        'labelsize': fontsize,
                        'left': False, 'bottom': False,
                        'right': False, 'top': False,
                        'labelleft': False, 'labelbottom': False,
                        'labelright': False, 'labeltop': False
                        },
                    'xlim': [
                        np.datetime64(dates_tHour[0]) - np.timedelta64(1, 'D'),
                        np.datetime64(dates_tHour[-1]) + np.timedelta64(1, 'D'),
                        ],
                    'xtick_params': [
                        True,
                        mdates.DateFormatter('%Y-%m-%d'),
                        mdates.DayLocator(bymonthday=(1, 16)),
                        ],
                    'label': f'pri_{c}_{site}_{k}',
                    'projection': None,
                    'patch_alpha': 0.0,
                    },
                ]
            for n, scode in enumerate(scodes1)
            for o, c in enumerate([f'{scode}{i}' for i in scodes2])
            if not o
            for p, (site, k) in enumerate(sites_y)
            },
        **{
            f'{scode}_{site}_vline_{i}_{k}': [
                'vline', [t], {'ls': ':', 'linewidth': 0.5, 'c': '#000000'},
                {
                    'loc_plot': [
                        (subplot_xmargin + subplot_gap) / fs[0],
                        (fs[1] - (p+1)*subplot_hgt_obs0 + subplot_gap) / fs[1],
                        (subplot_wdt_obs0 - 2*subplot_gap) / fs[0],
                        (subplot_hgt_obs0 - 2*subplot_gap) / fs[1]
                        ],
                    'ylim': [None, None],
                    'yticks': [],
                    'tick_params': {
                        'axis': 'both', 'which': 'major', 'direction': 'in',
                        'labelsize': fontsize,
                        'left': False, 'bottom': True,
                        'right': False, 'top': True,
                        'labelleft': False,
                        'labelbottom': (
                            True if p == len(sites_y)-1 and not i else False
                            ),
                        'labelright': False, 'labeltop': False
                        },
                    'xlim': [
                        np.datetime64(dates_tHour[0]) - np.timedelta64(1, 'D'),
                        np.datetime64(dates_tHour[-1]) + np.timedelta64(1, 'D'),
                        ],
                    'xtick_params': [
                        True,
                        mdates.DateFormatter('%Y-%m-%d'),
                        mdates.DayLocator(bymonthday=(1, 16)),
                        ],
                    'label': f'{scode}_{site}_vline_{i}_{k}',
                    'projection': None,
                    'patch_alpha': 0.0,
                    }
                ]
            for n, scode in enumerate(scodes1)
            for o, c in enumerate([f'{scode}{i}' for i in [scodes2[0]]])
            for p, (site, k) in enumerate(sites_y)
            for i, t in enumerate(pd.to_datetime(rdates)[1:])
            },
        },
    texts=[
        *[
            {
                'x': subplot_gap / fs[0],
                'y': (
                    fs[1]
                    - (n+p+0.5)*subplot_hgt_obs0
                    - subplot_gap
                    - (subplot_gap if k == 'chi' else 0)
                    ) / fs[1],
                's': y_name[k],
                'ha': 'left',
                'va': 'center', 'size': fontsize, 'rotation': 90
                }
            for n, scode in enumerate(scodes1)
            for o, c in enumerate([f'{scode}{i}' for i in scodes2])
            for p, (site, k) in enumerate(sites_y)
            ],
        *[
            {
                'x': (subplot_xmargin+2*subplot_gap)/fs[0],
                'y': (fs[1] - i*subplot_hgt_obs0 - 2*subplot_gap) / fs[1],
                's': (
                    f'{alphabet[i]}) '+
                    ('MHD' if site == 'mhd' else
                    'HFD' if site == 'hfd' else
                    'TAC' if site == 'tac' else
                    'CBW 20 m' if site == 'cbw020' else
                    'CBW 200 m')
                    ),
                'ha': 'left', 'va': 'top', 'size': fontsize,
                'fontweight': 'bold'
                }
            for i, (site, y) in enumerate(sites_y)
            ]
        ],
    legend_params=[
        [
            f'obs_s0obq0_hfd_chi', f'pri_s0obq0_hfd_chi',
            #f'pri_s0obq1_hfd_chi'
            ],
        ['Measured', 'Q0'],# 'Q1'],
        {
            'loc': 'upper right',
            'bbox_to_anchor': [
                (fs[0] - subplot_gap) / fs[0],
                (fs[1] - subplot_gap) / fs[1]
                ],
            'numpoints': 1, 'fontsize': fontsize, 'ncol': 4,
            'handletextpad': 0.4
            }
        ]
    )
for label in axs[fk][0][f's0_cbw020_vline_0_d_2H'].get_xticklabels():
    label.set_ha("right")
    label.set_rotation(30)
figs[fk].savefig(os.path.join(odir, f'{fk}.png'))


fk = 'for0_obs_res'

figs[fk] = plt.figure(figsize=fs, dpi=300)
axs[fk] = {}

figs[fk].clf()
axs[fk][0] = name_qch4_couple.plot.generic2(
    fig=figs[fk],
    idata={
        **{
            f'pri_{c}_{site}_{k}': [
                'line',
                [
                    dates_tObs[site][k],
                    obs[site][k].resample('H').mean().loc[dates_tObs[site][k]].squeeze()
                    - prior_obs[c][site][k][nx_tH_tObs[site][k]],
                    'o'
                    ],
                {
                    'c': 'None',
                    'mec': colours[o+2],
                    'ls': '',
                    'ms': 5.0, 'mew': 1.0,
                    'zorder': 1
                    },
                {
                    'loc_plot': [
                        (subplot_xmargin + subplot_gap) / fs[0],
                        (fs[1] - (n+p+1)*subplot_hgt_obs0 + subplot_gap) / fs[1],
                        (subplot_wdt_obs0 - 2*subplot_gap) / fs[0],
                        (subplot_hgt_obs0 - 2*subplot_gap) / fs[1]
                        ],
                    'ylim': yticks[f'{scode}_{site}_{k}_res'][0],
                    'yticks': np.arange(
                        yticks[f'{scode}_{site}_{k}_res'][0][0],
                        yticks[f'{scode}_{site}_{k}_res'][0][1]
                        + yticks[f'{scode}_{site}_{k}_res'][1],
                        yticks[f'{scode}_{site}_{k}_res'][1]
                        ),
                    'tick_params': {
                        'axis': 'both', 'which': 'major', 'direction': 'in',
                        'labelsize': fontsize,
                        'left': False, 'bottom': False,
                        'right': False, 'top': False,
                        'labelleft': False, 'labelbottom': False,
                        'labelright': False, 'labeltop': False
                        },
                    'xlim': [
                        np.datetime64(dates_tHour[0]) - np.timedelta64(1, 'D'),
                        np.datetime64(dates_tHour[-1]) + np.timedelta64(1, 'D'),
                        ],
                    'xtick_params': [
                        True,
                        mdates.DateFormatter('%Y-%m-%d'),
                        mdates.DayLocator(bymonthday=(1, 16)),
                        ],
                    'label': f'pri_{c}_{site}_{k}',
                    'projection': None,
                    'patch_alpha': 0.0,
                    },
                ]
            for n, scode in enumerate(scodes1)
            for o, c in enumerate([f'{scode}{i}' for i in scodes2])
            for p, (site, k) in enumerate(sites_y)
            },
        **{
            f'{scode}_{site}_hline_{k}': [
                'hline', [0.], {'ls': '-', 'linewidth': 0.5, 'c': '#000000'},
                {
                    'loc_plot': [
                        (subplot_xmargin + subplot_gap) / fs[0],
                        (fs[1] - (p+1)*subplot_hgt_obs0 + subplot_gap) / fs[1],
                        (subplot_wdt_obs0 - 2*subplot_gap) / fs[0],
                        (subplot_hgt_obs0 - 2*subplot_gap) / fs[1]
                        ],
                    'ylim': yticks[f'{scode}_{site}_{k}_res'][0],
                    'yticks': np.arange(
                        yticks[f'{scode}_{site}_{k}_res'][0][0],
                        yticks[f'{scode}_{site}_{k}_res'][0][1]
                        + yticks[f'{scode}_{site}_{k}_res'][1],
                        yticks[f'{scode}_{site}_{k}_res'][1]
                        ),
                    'tick_params': {
                        'axis': 'both', 'which': 'major', 'direction': 'in',
                        'labelsize': fontsize,
                        'left': True, 'bottom': False,
                        'right': True, 'top': False,
                        'labelleft': True,
                        'labelbottom': False,
                        'labelright': False, 'labeltop': False
                        },
                    'xlim': [
                        np.datetime64(dates_tHour[0]) - np.timedelta64(1, 'D'),
                        np.datetime64(dates_tHour[-1]) + np.timedelta64(1, 'D'),
                        ],
                    'xtick_params': [
                        True,
                        mdates.DateFormatter('%Y-%m-%d'),
                        mdates.DayLocator(bymonthday=(1, 16)),
                        ],
                    'label': f'{scode}_{site}_hline_{k}',
                    'projection': None,
                    'patch_alpha': 0.0,
                    }
                ]
            for n, scode in enumerate(scodes1)
            for o, c in enumerate([f'{scode}{i}' for i in [scodes2[0]]])
            for p, (site, k) in enumerate(sites_y)
            },
        **{
            f'{scode}_{site}_vline_{i}_{k}': [
                'vline', [t], {'ls': ':', 'linewidth': 0.5, 'c': '#000000'},
                {
                    'loc_plot': [
                        (subplot_xmargin + subplot_gap) / fs[0],
                        (fs[1] - (p+1)*subplot_hgt_obs0 + subplot_gap) / fs[1],
                        (subplot_wdt_obs0 - 2*subplot_gap) / fs[0],
                        (subplot_hgt_obs0 - 2*subplot_gap) / fs[1]
                        ],
                    'ylim': [None, None],
                    'yticks': [],
                    'tick_params': {
                        'axis': 'both', 'which': 'major', 'direction': 'in',
                        'labelsize': fontsize,
                        'left': False, 'bottom': True,
                        'right': False, 'top': True,
                        'labelleft': False,
                        'labelbottom': (
                            True if p == len(sites_y)-1 and not i else False
                            ),
                        'labelright': False, 'labeltop': False
                        },
                    'xlim': [
                        np.datetime64(dates_tHour[0]) - np.timedelta64(1, 'D'),
                        np.datetime64(dates_tHour[-1]) + np.timedelta64(1, 'D'),
                        ],
                    'xtick_params': [
                        True,
                        mdates.DateFormatter('%Y-%m-%d'),
                        mdates.DayLocator(bymonthday=(1, 16)),
                        ],
                    'label': f'{scode}_{site}_vline_{i}_{k}',
                    'projection': None,
                    'patch_alpha': 0.0,
                    }
                ]
            for n, scode in enumerate(scodes1)
            for o, c in enumerate([f'{scode}{i}' for i in [scodes2[0]]])
            for p, (site, k) in enumerate(sites_y)
            for i, t in enumerate(pd.to_datetime(rdates)[1:])
            },
        },
    texts=[
        *[
            {
                'x': subplot_gap / fs[0],
                'y': (
                    fs[1]
                    - (n+p+0.5)*subplot_hgt_obs0
                    - subplot_gap
                    - (subplot_gap if k == 'chi' else 0)
                    ) / fs[1],
                's': y_name[k],
                'ha': 'left',
                'va': 'center', 'size': fontsize, 'rotation': 90
                }
            for n, scode in enumerate(scodes1)
            for o, c in enumerate([f'{scode}{i}' for i in scodes2])
            for p, (site, k) in enumerate(sites_y)
            ],
        *[
            {
                'x': (subplot_xmargin+2*subplot_gap)/fs[0],
                'y': (fs[1] - i*subplot_hgt_obs0 - 2*subplot_gap) / fs[1],
                's': (
                    f'{alphabet[i]}) '+
                    ('MHD' if site == 'mhd' else
                    'HFD' if site == 'hfd' else
                    'TAC' if site == 'tac' else
                    'CBW 20 m' if site == 'cbw020' else
                    'CBW 200 m')
                    ),
                'ha': 'left', 'va': 'top', 'size': fontsize,
                'fontweight': 'bold'
                }
            for i, (site, y) in enumerate(sites_y)
            ]
        ],
    legend_params=[
        [
            f'pri_s0obq0_hfd_chi',
            f'pri_s0obq1_hfd_chi'
            ],
        ['Q0', 'Q1'],
        {
            'loc': 'upper right',
            'bbox_to_anchor': [
                (fs[0] - subplot_gap) / fs[0],
                (fs[1] - subplot_gap) / fs[1]
                ],
            'numpoints': 1, 'fontsize': fontsize, 'ncol': 4,
            'handletextpad': 0.4
            }
        ]
    )
for label in axs[fk][0][f's0_cbw020_vline_0_d_2H'].get_xticklabels():
    label.set_ha("right")
    label.set_rotation(30)
figs[fk].savefig(os.path.join(odir, f'{fk}.png'))


fk = 'for0_obs_dobs'

meas_lim = {'chi': 7.0 ,'d_13C': 0.07, 'd_2H': 0.9}
meas_ticks = {
    'chi': np.arange(-0.2, 0.21, 0.1),
    'd_13C': np.arange(-0.2, 0.21, 0.1),
    'd_2H': np.arange(-4.0, 4.01, 2.0)
    }
figs[fk] = plt.figure(figsize=fs, dpi=300)
axs[fk] = {}

figs[fk].clf()
axs[fk][0] = name_qch4_couple.plot.generic2(
    fig=figs[fk],
    idata={
        **{
            f'pri_{c}_{site}_{k}': [
                'line',
                [
                    dates_tObs[site][k],
                    prior_obs['s0obq0'][site][k][nx_tH_tObs[site][k]]
                    - prior_obs['s0obq1'][site][k][nx_tH_tObs[site][k]],
                    'o'
                    ],
                {
                    'c': 'None',
                    'mec': colours[o+2],
                    'ls': '',
                    'ms': 5.0, 'mew': 1.0,
                    'zorder': 1
                    },
                {
                    'loc_plot': [
                        (subplot_xmargin + subplot_gap) / fs[0],
                        (fs[1] - (n+p+1)*subplot_hgt_obs0 + subplot_gap) / fs[1],
                        (subplot_wdt_obs0 - 2*subplot_gap) / fs[0],
                        (subplot_hgt_obs0 - 2*subplot_gap) / fs[1]
                        ],
                    'ylim': yticks[f'{scode}_{site}_{k}_res'][0],
                    'yticks': np.arange(
                        yticks[f'{scode}_{site}_{k}_res'][0][0],
                        yticks[f'{scode}_{site}_{k}_res'][0][1]
                        + yticks[f'{scode}_{site}_{k}_res'][1],
                        yticks[f'{scode}_{site}_{k}_res'][1]
                        ),
                    'tick_params': {
                        'axis': 'both', 'which': 'major', 'direction': 'in',
                        'labelsize': fontsize,
                        'left': False, 'bottom': False,
                        'right': False, 'top': False,
                        'labelleft': False, 'labelbottom': False,
                        'labelright': False, 'labeltop': False
                        },
                    'xlim': [
                        np.datetime64(dates_tHour[0]) - np.timedelta64(1, 'D'),
                        np.datetime64(dates_tHour[-1]) + np.timedelta64(1, 'D'),
                        ],
                    'xtick_params': [
                        True,
                        mdates.DateFormatter('%Y-%m-%d'),
                        mdates.DayLocator(bymonthday=(1, 16)),
                        ],
                    'label': f'pri_{c}_{site}_{k}',
                    'projection': None,
                    'patch_alpha': 0.0,
                    },
                ]
            for n, scode in enumerate(scodes1)
            for o, c in enumerate([f'{scode}{i}' for i in scodes2])
            for p, (site, k) in enumerate(sites_y)
            },
        **{
            f'{scode}_{site}_hline_{k}': [
                'hline', [0.], {'ls': '-', 'linewidth': 0.5, 'c': '#000000'},
                {
                    'loc_plot': [
                        (subplot_xmargin + subplot_gap) / fs[0],
                        (fs[1] - (p+1)*subplot_hgt_obs0 + subplot_gap) / fs[1],
                        (subplot_wdt_obs0 - 2*subplot_gap) / fs[0],
                        (subplot_hgt_obs0 - 2*subplot_gap) / fs[1]
                        ],
                    'ylim': yticks[f'{scode}_{site}_{k}_res'][0],
                    'yticks': np.arange(
                        yticks[f'{scode}_{site}_{k}_res'][0][0],
                        yticks[f'{scode}_{site}_{k}_res'][0][1]
                        + yticks[f'{scode}_{site}_{k}_res'][1],
                        yticks[f'{scode}_{site}_{k}_res'][1]
                        ),
                    'tick_params': {
                        'axis': 'both', 'which': 'major', 'direction': 'in',
                        'labelsize': fontsize,
                        'left': True, 'bottom': False,
                        'right': True, 'top': False,
                        'labelleft': True,
                        'labelbottom': False,
                        'labelright': False, 'labeltop': False
                        },
                    'xlim': [
                        np.datetime64(dates_tHour[0]) - np.timedelta64(1, 'D'),
                        np.datetime64(dates_tHour[-1]) + np.timedelta64(1, 'D'),
                        ],
                    'xtick_params': [
                        True,
                        mdates.DateFormatter('%Y-%m-%d'),
                        mdates.DayLocator(bymonthday=(1, 16)),
                        ],
                    'label': f'{scode}_{site}_hline_{k}',
                    'projection': None,
                    'patch_alpha': 0.0,
                    }
                ]
            for n, scode in enumerate(scodes1)
            for o, c in enumerate([f'{scode}{i}' for i in [scodes2[0]]])
            for p, (site, k) in enumerate(sites_y)
            },
        **{
            f'{scode}_{site}_vline_{i}_{k}': [
                'vline', [t], {'ls': ':', 'linewidth': 0.5, 'c': '#000000'},
                {
                    'loc_plot': [
                        (subplot_xmargin + subplot_gap) / fs[0],
                        (fs[1] - (p+1)*subplot_hgt_obs0 + subplot_gap) / fs[1],
                        (subplot_wdt_obs0 - 2*subplot_gap) / fs[0],
                        (subplot_hgt_obs0 - 2*subplot_gap) / fs[1]
                        ],
                    'ylim': [None, None],
                    'yticks': [],
                    'tick_params': {
                        'axis': 'both', 'which': 'major', 'direction': 'in',
                        'labelsize': fontsize,
                        'left': False, 'bottom': True,
                        'right': False, 'top': True,
                        'labelleft': False,
                        'labelbottom': (
                            True if p == len(sites_y)-1 and not i else False
                            ),
                        'labelright': False, 'labeltop': False
                        },
                    'xlim': [
                        np.datetime64(dates_tHour[0]) - np.timedelta64(1, 'D'),
                        np.datetime64(dates_tHour[-1]) + np.timedelta64(1, 'D'),
                        ],
                    'xtick_params': [
                        True,
                        mdates.DateFormatter('%Y-%m-%d'),
                        mdates.DayLocator(bymonthday=(1, 16)),
                        ],
                    'label': f'{scode}_{site}_vline_{i}_{k}',
                    'projection': None,
                    'patch_alpha': 0.0,
                    }
                ]
            for n, scode in enumerate(scodes1)
            for o, c in enumerate([f'{scode}{i}' for i in [scodes2[0]]])
            for p, (site, k) in enumerate(sites_y)
            for i, t in enumerate(pd.to_datetime(rdates)[1:])
            },
        },
    texts=[
        *[
            {
                'x': subplot_gap / fs[0],
                'y': (
                    fs[1]
                    - (n+p+0.5)*subplot_hgt_obs0
                    - subplot_gap
                    - (subplot_gap if k == 'chi' else 0)
                    ) / fs[1],
                's': y_name[k],
                'ha': 'left',
                'va': 'center', 'size': fontsize, 'rotation': 90
                }
            for n, scode in enumerate(scodes1)
            for o, c in enumerate([f'{scode}{i}' for i in scodes2])
            for p, (site, k) in enumerate(sites_y)
            ],
        *[
            {
                'x': (subplot_xmargin+2*subplot_gap)/fs[0],
                'y': (fs[1] - i*subplot_hgt_obs0 - 2*subplot_gap) / fs[1],
                's': (
                    f'{alphabet[i]}) '+
                    ('MHD' if site == 'mhd' else
                    'HFD' if site == 'hfd' else
                    'TAC' if site == 'tac' else
                    'CBW 20 m' if site == 'cbw020' else
                    'CBW 200 m')
                    ),
                'ha': 'left', 'va': 'top', 'size': fontsize,
                'fontweight': 'bold'
                }
            for i, (site, y) in enumerate(sites_y)
            ]
        ],
    legend_params=[
        [
            f'pri_s0obq0_hfd_chi',
            f'pri_s0obq1_hfd_chi'
            ],
        ['Q0', 'Q1'],
        {
            'loc': 'upper right',
            'bbox_to_anchor': [
                (fs[0] - subplot_gap) / fs[0],
                (fs[1] - subplot_gap) / fs[1]
                ],
            'numpoints': 1, 'fontsize': fontsize, 'ncol': 4,
            'handletextpad': 0.4
            }
        ]
    )
for label in axs[fk][0][f's0_cbw020_vline_0_d_2H'].get_xticklabels():
    label.set_ha("right")
    label.set_rotation(30)
figs[fk].savefig(os.path.join(odir, f'{fk}.png'))


# q sensitivity
yticks = {}
yrange = {}
for scode in scodes1:
    codes_sen = [f'{scode}{i}' for i in scodes2]
    for s in range(dim_sAll+1):
        '''QA'''
        if s < dim_sAll:
            k1 = 'QA_gbie'
            s1 = s
        else:
            k1 = 'QA_gbie_total'
            s1 = 0
        sk = f'{scode}_{s}_QA'
        ymin = np.nanmin(np.concatenate([
            *[prior_state[c][k1][..., s1].flatten() for c in codes_sen],
            ]))
        ymax = np.nanmax(np.concatenate([
            *[prior_state[c][k1][..., s1].flatten() for c in codes_sen],
            ]))
        ysf = np.floor(np.log10(np.abs([
            i if i != 0 else 1 for i in [ymin, ymax]
            ]))).max() - 1
        ysf = 1 if not ysf and k not in ['d_13C'] else ysf
        yticks[sk] = aticks(ymin, ymax, 3, 5, ysf, 1)
        yrange[sk] = yticks[sk][0][1] - yticks[sk][0][0]
subplot_hgt_obs0 = 1
dates_plot = [
    pd.to_datetime(rdates)
    + pd.to_timedelta(
        (
            pd.to_datetime(rdates).days_in_month * (i+1) / (len(scodes2)+1)
            ),
        'D'
        )
    for i in range(len(scodes2)+1)
    ]
fk = 'for0_state_Qgbie'
fs = (
        1*subplot_wdt_obs0 + 1*subplot_xmargin,
        6*subplot_hgt_obs0 + 0.5
        )
figs[fk] = plt.figure(figsize=fs, dpi=300)
axs[fk] = {}
figs[fk].clf()
axs[fk][0] = name_qch4_couple.plot.generic2(
    fig=figs[fk],
    idata={
        **{
            f'{c}_{th}_{s}': [
                'err',
                [],
                {
                    'x': dates_plot[o],
                    'y': prior_state[c][
                        'QA_gbie' if s < dim_sAll else 'QA_gbie_total'
                        ][..., 1, s if s < dim_sAll else 0],
                    'yerr': (
                        lambda x: x if not np.isnan(x).any() else None
                        )(np.diff(
                            prior_state[c][
                                'QA_gbie' if s < dim_sAll else 'QA_gbie_total'
                                ],
                            axis=1
                            )[..., s if s < dim_sAll else 0].T
                            ),
                    'c': colours[2+o],
                    'ls': '', 'marker': 'o',
                    'markersize': 5.0, 'elinewidth': 1.5,
                    'capsize': 4.0, 'capthick': 1.5,
                    },
                {
                    'loc_plot': [
                        (subplot_xmargin + subplot_gap) / fs[0],
                        (fs[1] - (p+1)*subplot_hgt_obs0 + subplot_gap) / fs[1],
                        (subplot_wdt_obs0 - 2*subplot_gap) / fs[0],
                        (subplot_hgt_obs0 - 2*subplot_gap) / fs[1]
                        ],
                    'ylim': [
                        yticks[f'{scode}_{s}_QA'][0][0]
                        if yticks[f'{scode}_{s}_QA'][0][0] != 0 else
                        -yrange[f'{scode}_{s}_QA']/5,
                        yticks[f'{scode}_{s}_QA'][0][1]
                        if p != 0 else
                        yticks[f'{scode}_{s}_QA'][0][1] + yrange[f'{scode}_{s}_QA']/3
                        ],
                    'yticks': np.arange(
                        yticks[f'{scode}_{s}_QA'][0][0],
                        yticks[f'{scode}_{s}_QA'][0][1]
                        + yticks[f'{scode}_{s}_QA'][1],
                        yticks[f'{scode}_{s}_QA'][1]
                        ),
                    'tick_params': {
                        'axis': 'both', 'which': 'major', 'direction': 'in',
                        'labelsize': fontsize,
                        'left': True if o == 0 else False,
                        'bottom': False,
                        'right': True if o == 0 else False,
                        'top': False,
                        'labelleft': True if o == 0 else False,
                        'labelbottom': False,
                        'labelright': False, 'labeltop': False
                        },
                    'xlim': [
                        np.datetime64(dates_tHour[0]) - np.timedelta64(1, 'D'),
                        np.datetime64(dates_tHour[-1]) + np.timedelta64(1, 'D'),
                        ],
                    'xtick_params': [
                        True,
                        mdates.DateFormatter('%Y-%m-%d'),
                        mdates.DayLocator(bymonthday=(1)),
                        ],
                    'label': f'{c}_{th}_{s}',
                    'projection': None,
                    'patch_alpha': 0.0,
                    }
                ]
            for n, scode in enumerate(scodes1)
            for o, c in enumerate([f'{scode}{i}' for i in scodes2])
            for p, s in enumerate(range(dim_sAll+1))
            for th in ['pri']
            },
        **{
            f'{scode}_{p}_vline_{i}': [
                'vline', [t], {'ls': ':', 'linewidth': 0.5, 'c': '#000000'},
                {
                    'loc_plot': [
                        (subplot_xmargin + subplot_gap) / fs[0],
                        (fs[1] - (p+1)*subplot_hgt_obs0 + subplot_gap) / fs[1],
                        (subplot_wdt_obs0 - 2*subplot_gap) / fs[0],
                        (subplot_hgt_obs0 - 2*subplot_gap) / fs[1]
                        ],
                    'ylim': [None, None],
                    'yticks': [],
                    'tick_params': {
                        'axis': 'both', 'which': 'major', 'direction': 'in',
                        'labelsize': fontsize,
                        'left': False, 'bottom': True,
                        'right': False, 'top': True,
                        'labelleft': False,
                        'labelbottom': True if p == 5 and not i else False,
                        'labelright': False, 'labeltop': False
                        },
                    'xlim': [
                        np.datetime64(dates_tHour[0]) - np.timedelta64(1, 'D'),
                        np.datetime64(dates_tHour[-1]) + np.timedelta64(1, 'D'),
                        ],
                    'xtick_params': [
                        True,
                        mdates.DateFormatter('%Y-%m-%d'),
                        mdates.DayLocator(bymonthday=(1)),
                        ],
                    'label': f'{scode}_vline_{i}',
                    'projection': None,
                    'patch_alpha': 0.0,
                    }
                ]
            for n, scode in enumerate(scodes1)
            for p, s in enumerate(range(6))
            for i, t in enumerate(pd.to_datetime(rdates)[1:])
            },
        },
    texts=[
        *[
            {
                'x': subplot_gap / fs[0],
                'y': (fs[1] - (p+0.5)*subplot_hgt_obs0 + subplot_gap) / fs[1],
                's': f'{sector_names[s][0]} (Gg yr$^{-1}$)',
                'ha': 'left', 'va': 'center', 'size': fontsize, 'rotation': 90
                }
            for p, s in enumerate(range(dim_sAll+1))
            ],
        ],
    legend_params=[
        [
            's0obq0_pri_0', 's0obq1_pri_0',
            ],
        ['Q0', 'Q1'],
        {
            'loc': 'upper right',
            'bbox_to_anchor': [
                (fs[0] - (subplot_gap)) / fs[0],
                (fs[1] - (subplot_gap)) / fs[1]
                ],
            'numpoints': 1, 'fontsize': fontsize, 'ncol': 5,
            'handletextpad': 0.2, 'columnspacing': 1.0,
            'borderpad': 0.2, 'borderaxespad': 0.2
            }
        ]
    )
for label in axs[fk][0]['s0_5_vline_0'].get_xticklabels():
    label.set_ha("right")
    label.set_rotation(30)
figs[fk].savefig(os.path.join(odir, f'{fk}.png'))


# Enhancement properties
site = 'hfd'
dchi_s_pri = {  # shape = (isotope, len([m0, m1, m2]), t, s)
    c:
    np.array(calc_final_d(
        [
            dm0_s[c][site],
            dm1_s[c][site],
            dm2_s_pri[c][site],
            ],
        [
            prior_state[c]['v_dQi1'][:, 1],
            prior_state[c]['v_dQi1'][:, 1],
            prior_state[c]['v_dQi1'][:, 1],
            ],
        [
            prior_state[c]['v_dQi2'][:, 1],
            prior_state[c]['v_dQi2'][:, 1],
            prior_state[c]['v_dQi2'][:, 1],
            ],
        R_std, M, rdates, dim_sAll
        ))
    for n1, scode in enumerate(scodes1)
    for n2, c in enumerate([f'{scode}{i}' for i in scodes2])
    }
dchi_dQi1_pri = {
    k: (v[1].sum((0, 2)) / v[0].sum((0, 2)) / R_std[1] - 1.) * 1.e3
    for k, v in dchi_s_pri.items()
    }
dchi_dQi2_pri = {
    k: (v[2].sum((0, 2)) / v[0].sum((0, 2)) / R_std[2] - 1.) * 1.e3
    for k, v in dchi_s_pri.items()
    }
dchi_sectors = {
    k:
    np.fmax(
        np.array(pd.DataFrame(
            v.sum((0, 1)), index=dates_tHour
            ).groupby(dates_tHour.date).mean()).T
        / np.array(pd.Series(
            v.sum((0, 1, 3)), index=dates_tHour
            ).groupby(dates_tHour.date).mean()).T,
        0.
        )
    for k, v in dchi_s_pri.items()
    }
pdata = {
    0: dchi_sectors['s0obq0'],
    1: dchi_sectors['s0obq1'],
    2: [
        np.array(dchi_s_pri['s0obq0']).sum((0, 1, 3)).T,
        np.array(dchi_s_pri['s0obq1']).sum((0, 1, 3)).T,
        ],
    3: [
        dchi_dQi1_pri['s0obq0'],
        dchi_dQi1_pri['s0obq1'],
        ],
    4: [
        dchi_dQi2_pri['s0obq0'],
        dchi_dQi2_pri['s0obq1'],
        ],
    5: [
        prior_obs['s0obq0'][site]['d_13C'][nx_tH_tObs[site]['d_13C']],
        prior_obs['s0obq1'][site]['d_13C'][nx_tH_tObs[site]['d_13C']],
        ],
    6: [
        prior_obs['s0obq0'][site]['d_2H'][nx_tH_tObs[site]['d_2H']],
        prior_obs['s0obq1'][site]['d_2H'][nx_tH_tObs[site]['d_2H']],
        ],
    }

yticks = {}
yrange = {}
for k, v in pdata.items():
    sk = k
    if k in [0, 1]:
        yticks[sk] = [[0., 1.], 0.2]
        yrange[sk] = 1.
        continue
    ymin = np.nanmin(np.concatenate(v))
    ymax = np.nanmax(np.concatenate(v))
    ysf = np.floor(np.log10(np.abs([
        i if i != 0 else 1 for i in [ymin, ymax]
        ]))).max() - 1
    ysf = 1 if not ysf and k not in [5] else ysf
    yticks[sk] = aticks(ymin, ymax, 3, 5, ysf, 1)
    yrange[sk] = yticks[sk][0][1] - yticks[sk][0][0]

fk = 'for0_hfd_enh'
fig_param['mph'] = 1.
fig_param['mlmargin'] = 0.75
fig_param['w'] = fig_param['mpw'] + 2*fig_param['mlmargin']
fig_param['h'] = len(pdata)*fig_param['mph'] + fig_param['mbmargin']

figs[fk] = plt.figure(figsize=(fig_param['w'], fig_param['h']), dpi=300)
axs[fk] = {}

figs[fk].clf()
axs[fk][0] = name_qch4_couple.plot.generic2(
    fig=figs[fk],
    idata={
        **{
            f'{n1}_plot_{n2}': [
                'bar' if n1 in [0, 1] else 'line',
                [] if n1 in [0, 1] else [
                    (
                        dates_tHour if n1 in [2, 3, 4] else
                        dates_tObs[site]['d_13C'] if n1 == 5 else
                        dates_tObs[site]['d_2H']
                        ),
                    v[n2],
                    'o'
                    ],
                (
                    {
                        'x': dates_tDay,
                        'height': v[n2],
                        'width': np.timedelta64(1, 'D'),
                        'bottom': v[:n2].sum(0),
                        'align': 'edge',
                        'color': matplotlib.cm.get_cmap(
                            'viridis_r'
                            )(0.1 + 0.2*n2),
                        }
                    if n1 in [0, 1] else
                    {
                        'ls': '',
                        'c': 'None', #colours[n2+2],
                        'mec': colours[n2+2],
                        'ms': 5.0, 'mew': 0.5,
                        }
                    ),
                {
                    'loc_plot': [
                        (fig_param['mlmargin'] + fig_param['mgap'])
                        / fig_param['w'],
                        (
                            fig_param['h']
                            - (n1+1)*fig_param['mph']
                            + fig_param['mgap']
                            )
                        / fig_param['h'],
                        (fig_param['mpw'] - 2*fig_param['mgap'])
                        / fig_param['w'],
                        (fig_param['mph'] - 2*fig_param['mgap'])
                        / fig_param['h'],
                        ],
                    'ylim': yticks[n1][0],
                    'yticks': np.arange(
                        yticks[n1][0][0],
                        yticks[n1][0][1] + yticks[n1][1],
                        yticks[n1][1]
                        ),
                    'tick_params': {
                        'axis': 'both', 'which': 'major', 'direction': 'in',
                        'labelsize': fontsize,
                        'left': True if not n2 else False,
                        'bottom': False,
                        'right': True if not n2 else False,
                        'top': False,
                        'labelleft': True if not n2 else False,
                        'labelbottom': False,
                        'labelright': False, 'labeltop': False
                        },
                    'xlim': [
                        np.datetime64(dates_tHour[0]) - np.timedelta64(1, 'D'),
                        np.datetime64(dates_tHour[-1]) + np.timedelta64(1, 'D'),
                        ],
                    'xtick_params': [
                        True,
                        mdates.DateFormatter('%Y-%m-%d'),
                        mdates.DayLocator(bymonthday=(1, 16)),
                        ],
                    'label': f'{n1}_plot_{n2}',
                    'projection': None,
                    'patch_alpha': 1.0 if not n2 else 0.0,
                    }
                ]
            for n1, v in pdata.items()
            for n2 in range(len(v))
            },
        **{
            f'{n1}_vline_{i}': [
                'vline', [t], {'ls': ':', 'linewidth': 0.5, 'c': '#000000'},
                {
                    'loc_plot': [
                        (fig_param['mlmargin'] + fig_param['mgap'])
                        / fig_param['w'],
                        (
                            fig_param['h']
                            - (n1+1)*fig_param['mph']
                            + fig_param['mgap']
                            )
                        / fig_param['h'],
                        (fig_param['mpw'] - 2*fig_param['mgap'])
                        / fig_param['w'],
                        (fig_param['mph'] - 2*fig_param['mgap'])
                        / fig_param['h'],
                        ],
                    'ylim': [None, None],
                    'yticks': [],
                    'tick_params': {
                        'axis': 'both', 'which': 'major', 'direction': 'in',
                        'labelsize': fontsize,
                        'left': False, 'bottom': True,
                        'right': False, 'top': True,
                        'labelleft': False,
                        'labelbottom': (
                            True if n1 == len(pdata)-1 and not i else False
                            ),
                        'labelright': False, 'labeltop': False
                        },
                    'xlim': [
                        np.datetime64(dates_tHour[0]) - np.timedelta64(1, 'D'),
                        np.datetime64(dates_tHour[-1]) + np.timedelta64(1, 'D'),
                        ],
                    'xtick_params': [
                        True,
                        mdates.DateFormatter('%Y-%m-%d'),
                        mdates.DayLocator(bymonthday=(1, 16)),
                        ],
                    'label': f'{n1}_vline_{i}',
                    'projection': None,
                    'patch_alpha': 0.0,
                    }
                ]
            for n1, v in pdata.items()
            for i, t in enumerate(pd.to_datetime(rdates)[1:])
            },
        **{
            f'{n1}_hline_{i}': [
                'hline', [i], {'ls': '-', 'linewidth': 0.5, 'c': '#000000'},
                {
                    'loc_plot': [
                        (fig_param['mlmargin'] + fig_param['mgap'])
                        / fig_param['w'],
                        (
                            fig_param['h']
                            - (n1+1)*fig_param['mph']
                            + fig_param['mgap']
                            )
                        / fig_param['h'],
                        (fig_param['mpw'] - 2*fig_param['mgap'])
                        / fig_param['w'],
                        (fig_param['mph'] - 2*fig_param['mgap'])
                        / fig_param['h'],
                        ],
                    'ylim': [None, None],
                    'yticks': [],
                    'tick_params': {
                        'axis': 'both', 'which': 'major', 'direction': 'in',
                        'labelsize': fontsize,
                        'left': False, 'bottom': False,
                        'right': False, 'top': False,
                        'labelleft': False, 'labelbottom': False,
                        'labelright': False, 'labeltop': False
                        },
                    'xlim': [
                        np.datetime64(dates_tHour[0]) - np.timedelta64(1, 'D'),
                        np.datetime64(dates_tHour[-1]) + np.timedelta64(1, 'D'),
                        ],
                    'xtick_params': [
                        True,
                        mdates.DateFormatter('%Y-%m-%d'),
                        mdates.DayLocator(bymonthday=(1, 16)),
                        ],
                    'label': f'{n1}_hline_{i}',
                    'projection': None,
                    'patch_alpha': 0.0,
                    }
                ]
            for n1, v in pdata.items()
            if n1 in [0, 1]
            for n2 in range(len(v))
            for i in np.arange(
                yticks[n1][0][0],
                yticks[n1][0][1] + yticks[n1][1],
                yticks[n1][1]
                )
            },
        },
    texts=[
        *[
            {
                'x': fig_param['mgap'] / fig_param['w'],
                'y': (
                    fig_param['h']
                    - (n1+0.5)*fig_param['mph']
                    ) / fig_param['h'],
                's': {
                    0: 'Relative\nContribution\n(Q0)',
                    1: 'Relative\nContribution\n(Q1)',
                    2: 'Enhancement\n(nmol mol$^{-1}$)',
                    3: '$\delta^{13}$C$_{enh}$ (\u2030)',
                    4: '$\delta^{2}$H$_{enh}$ (\u2030)',
                    5: '$\delta^{13}$C$_{obs}$ (\u2030)',
                    6: '$\delta^{2}$H$_{obs}$ (\u2030)',
                    }[n1],
                'ha': 'left',
                'va': 'center', 'size': fontsize, 'rotation': 90,
                'ma': 'center'
                }
            for n1 in pdata.keys()
            ],
        *[
            {
                'x': (
                    fig_param['mlmargin'] + 2*fig_param['mgap']
                    ) / fig_param['w'],
                'y': (
                    fig_param['h']
                    - (n1)*fig_param['mph']
                    - 2*fig_param['mgap']
                    ) / fig_param['h'],
                's': f'{alphabet[n1]})',
                'ha': 'left', 'va': 'top', 'size': fontsize,
                'fontweight': 'bold',
                'bbox': {
                    'boxstyle': 'round', 'pad': 0.1,
                    'facecolor': '#ffffff', 'edgecolor': '#ffffff',
                    }
                }
            for n1 in pdata.keys()
            ]
        ],
    legend_params=[
        [
            '0_plot_4', '0_plot_3', '0_plot_2', '0_plot_2', '0_plot_1',
            '2_plot_0', '2_plot_1'
            ],
        [
            'MN', 'BB', 'MA', 'WM', 'FF',
            'Q0', 'Q1'
            ],
        {
            'loc': 'upper left',
            'bbox_to_anchor': [
                (
                    fig_param['w']
                    - fig_param['mlmargin']
                    - fig_param['mgap']
                    ) / fig_param['w'],
                (
                    fig_param['h']
                    - fig_param['mgap']
                    ) / fig_param['h'],
                ],
            'numpoints': 1, 'fontsize': fontsize, 'ncol': 1,
            'handletextpad': 0.4
            }
        ]
    )
for label in axs[fk][0][f'{len(pdata)-1}_vline_0'].get_xticklabels():
    label.set_ha("right")
    label.set_rotation(30)
figs[fk].savefig(os.path.join(odir, f'{fk}.png'))

fig_param['mph'] = fig_param_def['mph']
fig_param['mlmargin'] = fig_param_def['mlmargin']
fig_param['w'] = fig_param_def['w']
fig_param['h'] = fig_param_def['h']


fk = 'for0_hfd_dobs'
site = 'hfd'
pdata = {
    0: [
        prior_obs['s0obq0'][site]['d_13C'][nx_tH_tObs[site]['d_13C']]
        - prior_obs['s0obq1'][site]['d_13C'][nx_tH_tObs[site]['d_13C']],
        ],
    1: [
        prior_obs['s0obq0'][site]['d_2H'][nx_tH_tObs[site]['d_2H']]
        - prior_obs['s0obq1'][site]['d_2H'][nx_tH_tObs[site]['d_2H']],
        ],
    }
boreas_lim = [0.07, 0.9]

fig_param['mph'] = 1.
fig_param['mlmargin'] = 0.75
fig_param['w'] = fig_param['mpw'] + 1*fig_param['mlmargin']
fig_param['h'] = len(pdata)*fig_param['mph'] + fig_param['mbmargin']

figs[fk] = plt.figure(figsize=(fig_param['w'], fig_param['h']), dpi=300)
axs[fk] = {}

figs[fk].clf()
axs[fk][0] = name_qch4_couple.plot.generic2(
    fig=figs[fk],
    idata={
        **{
            f'{n1}_plot_{n2}': [
                'line',
                [
                    (
                        dates_tObs[site]['d_13C'] if n1 == 0 else
                        dates_tObs[site]['d_2H']
                        ),
                    v[n2],
                    'o'
                    ],
                {
                    'ls': '',
                    'c': 'None', #colours[n2+2],
                    'mec': colours[n2+2],
                    'ms': 5.0, 'mew': 0.5,
                    },
                {
                    'loc_plot': [
                        (fig_param['mlmargin'] + fig_param['mgap'])
                        / fig_param['w'],
                        (
                            fig_param['h']
                            - (n1+1)*fig_param['mph']
                            + fig_param['mgap']
                            )
                        / fig_param['h'],
                        (fig_param['mpw'] - 2*fig_param['mgap'])
                        / fig_param['w'],
                        (fig_param['mph'] - 2*fig_param['mgap'])
                        / fig_param['h'],
                        ],
                    'ylim': [-0.2, 0.2] if n1 == 0 else [-4.0, 4.0],
                    'yticks': np.arange(
                        -0.2 if n1 == 0 else -4.0,
                        (0.2 if n1 == 0 else 4.0)+0.01,
                        0.1 if n1 == 0 else 2.0
                        ),
                    'tick_params': {
                        'axis': 'both', 'which': 'major', 'direction': 'in',
                        'labelsize': fontsize,
                        'left': True if not n2 else False,
                        'bottom': False,
                        'right': True if not n2 else False,
                        'top': False,
                        'labelleft': True if not n2 else False,
                        'labelbottom': False,
                        'labelright': False, 'labeltop': False
                        },
                    'xlim': [
                        np.datetime64(dates_tHour[0]) - np.timedelta64(1, 'D'),
                        np.datetime64(dates_tHour[-1]) + np.timedelta64(1, 'D'),
                        ],
                    'xtick_params': [
                        True,
                        mdates.DateFormatter('%Y-%m-%d'),
                        mdates.DayLocator(bymonthday=(1, 16)),
                        ],
                    'label': f'{n1}_plot_{n2}',
                    'projection': None,
                    'patch_alpha': 1.0 if not n2 else 0.0,
                    }
                ]
            for n1, v in pdata.items()
            for n2 in range(len(v))
            },
        **{
            f'{n1}_vline_{i}': [
                'vline', [t], {'ls': ':', 'linewidth': 0.5, 'c': '#000000'},
                {
                    'loc_plot': [
                        (fig_param['mlmargin'] + fig_param['mgap'])
                        / fig_param['w'],
                        (
                            fig_param['h']
                            - (n1+1)*fig_param['mph']
                            + fig_param['mgap']
                            )
                        / fig_param['h'],
                        (fig_param['mpw'] - 2*fig_param['mgap'])
                        / fig_param['w'],
                        (fig_param['mph'] - 2*fig_param['mgap'])
                        / fig_param['h'],
                        ],
                    'ylim': [None, None],
                    'yticks': [],
                    'tick_params': {
                        'axis': 'both', 'which': 'major', 'direction': 'in',
                        'labelsize': fontsize,
                        'left': False, 'bottom': True,
                        'right': False, 'top': True,
                        'labelleft': False,
                        'labelbottom': (
                            True if n1 == len(pdata)-1 and not i else False
                            ),
                        'labelright': False, 'labeltop': False
                        },
                    'xlim': [
                        np.datetime64(dates_tHour[0]) - np.timedelta64(1, 'D'),
                        np.datetime64(dates_tHour[-1]) + np.timedelta64(1, 'D'),
                        ],
                    'xtick_params': [
                        True,
                        mdates.DateFormatter('%Y-%m-%d'),
                        mdates.DayLocator(bymonthday=(1, 16)),
                        ],
                    'label': f'{n1}_vline_{i}',
                    'projection': None,
                    'patch_alpha': 0.0,
                    }
                ]
            for n1, v in pdata.items()
            for i, t in enumerate(pd.to_datetime(rdates)[1:])
            },
        **{
            f'{n1}_hline_{n2}': [
                'hline', [i], {
                    'ls': '-' if not n2 else '--',
                    'linewidth': 0.5, 'c': '#000000'
                    },
                {
                    'loc_plot': [
                        (fig_param['mlmargin'] + fig_param['mgap'])
                        / fig_param['w'],
                        (
                            fig_param['h']
                            - (n1+1)*fig_param['mph']
                            + fig_param['mgap']
                            )
                        / fig_param['h'],
                        (fig_param['mpw'] - 2*fig_param['mgap'])
                        / fig_param['w'],
                        (fig_param['mph'] - 2*fig_param['mgap'])
                        / fig_param['h'],
                        ],
                    'ylim': [-0.2, 0.2] if n1 == 0 else [-4.0, 4.0],
                    'yticks': np.arange(
                        -0.2 if n1 == 0 else -4.0,
                        (0.2 if n1 == 0 else 4.0)+0.01,
                        0.1 if n1 == 0 else 2.0
                        ),
                    'tick_params': {
                        'axis': 'both', 'which': 'major', 'direction': 'in',
                        'labelsize': fontsize,
                        'left': False, 'bottom': False,
                        'right': False, 'top': False,
                        'labelleft': False, 'labelbottom': False,
                        'labelright': False, 'labeltop': False
                        },
                    'xlim': [
                        np.datetime64(dates_tHour[0]) - np.timedelta64(1, 'D'),
                        np.datetime64(dates_tHour[-1]) + np.timedelta64(1, 'D'),
                        ],
                    'xtick_params': [
                        True,
                        mdates.DateFormatter('%Y-%m-%d'),
                        mdates.DayLocator(bymonthday=(1, 16)),
                        ],
                    'label': f'{n1}_hline_{i}',
                    'projection': None,
                    'patch_alpha': 0.0,
                    }
                ]
            for n1, v in pdata.items()
            for n2, i in enumerate([0.0, - boreas_lim[n1], boreas_lim[n1]])
            },
        },
    texts=[
        *[
            {
                'x': fig_param['mgap'] / fig_param['w'],
                'y': (
                    fig_param['h']
                    - (n1+0.5)*fig_param['mph']
                    ) / fig_param['h'],
                's': {
                    0: 'Q0 - Q1\n$\delta^{13}$C$_{obs}$ (\u2030)',
                    1: 'Q0 - Q1\n$\delta^{2}$H$_{obs}$ (\u2030)',
                    }[n1],
                'ha': 'left',
                'va': 'center', 'size': fontsize, 'rotation': 90,
                'ma': 'center'
                }
            for n1 in pdata.keys()
            ],
        *[
            {
                'x': (
                    fig_param['mlmargin'] + 2*fig_param['mgap']
                    ) / fig_param['w'],
                'y': (
                    fig_param['h']
                    - (n1)*fig_param['mph']
                    - 2*fig_param['mgap']
                    ) / fig_param['h'],
                's': f'{alphabet[n1]})',
                'ha': 'left', 'va': 'top', 'size': fontsize,
                'fontweight': 'bold',
                'bbox': {
                    'boxstyle': 'round', 'pad': 0.1,
                    'facecolor': '#ffffff', 'edgecolor': '#ffffff',
                    }
                }
            for n1 in pdata.keys()
            ]
        ],
    legend_params=[[], [], {}]
    )
for label in axs[fk][0][f'{len(pdata)-1}_vline_0'].get_xticklabels():
    label.set_ha("right")
    label.set_rotation(30)
figs[fk].savefig(os.path.join(odir, f'{fk}.png'))

fig_param['mph'] = fig_param_def['mph']
fig_param['mlmargin'] = fig_param_def['mlmargin']
fig_param['w'] = fig_param_def['w']
fig_param['h'] = fig_param_def['h']


fk = 'for0_cbw_dobs'
site = 'cbw020'
pdata = {
    0: [
        prior_obs['s0obq0'][site]['d_13C'][nx_tH_tObs[site]['d_13C']]
        - prior_obs['s0obq1'][site]['d_13C'][nx_tH_tObs[site]['d_13C']],
        ],
    1: [
        prior_obs['s0obq0'][site]['d_2H'][nx_tH_tObs[site]['d_2H']]
        - prior_obs['s0obq1'][site]['d_2H'][nx_tH_tObs[site]['d_2H']],
        ],
    }
boreas_lim = [0.07, 0.9]

fig_param['mph'] = 1.
fig_param['mlmargin'] = 0.75
fig_param['w'] = fig_param['mpw'] + 1*fig_param['mlmargin']
fig_param['h'] = len(pdata)*fig_param['mph'] + fig_param['mbmargin']

figs[fk] = plt.figure(figsize=(fig_param['w'], fig_param['h']), dpi=300)
axs[fk] = {}

figs[fk].clf()
axs[fk][0] = name_qch4_couple.plot.generic2(
    fig=figs[fk],
    idata={
        **{
            f'{n1}_plot_{n2}': [
                'line',
                [
                    (
                        dates_tObs[site]['d_13C'] if n1 == 0 else
                        dates_tObs[site]['d_2H']
                        ),
                    v[n2],
                    'o'
                    ],
                {
                    'ls': '',
                    'c': 'None', #colours[n2+2],
                    'mec': colours[n2+2],
                    'ms': 5.0, 'mew': 0.5,
                    },
                {
                    'loc_plot': [
                        (fig_param['mlmargin'] + fig_param['mgap'])
                        / fig_param['w'],
                        (
                            fig_param['h']
                            - (n1+1)*fig_param['mph']
                            + fig_param['mgap']
                            )
                        / fig_param['h'],
                        (fig_param['mpw'] - 2*fig_param['mgap'])
                        / fig_param['w'],
                        (fig_param['mph'] - 2*fig_param['mgap'])
                        / fig_param['h'],
                        ],
                    'ylim': [-0.2, 0.2] if n1 == 0 else [-4.0, 4.0],
                    'yticks': np.arange(
                        -0.2 if n1 == 0 else -4.0,
                        (0.2 if n1 == 0 else 4.0)+0.01,
                        0.1 if n1 == 0 else 2.0
                        ),
                    'tick_params': {
                        'axis': 'both', 'which': 'major', 'direction': 'in',
                        'labelsize': fontsize,
                        'left': True if not n2 else False,
                        'bottom': False,
                        'right': True if not n2 else False,
                        'top': False,
                        'labelleft': True if not n2 else False,
                        'labelbottom': False,
                        'labelright': False, 'labeltop': False
                        },
                    'xlim': [
                        np.datetime64(dates_tHour[0]) - np.timedelta64(1, 'D'),
                        np.datetime64(dates_tHour[-1]) + np.timedelta64(1, 'D'),
                        ],
                    'xtick_params': [
                        True,
                        mdates.DateFormatter('%Y-%m-%d'),
                        mdates.DayLocator(bymonthday=(1, 16)),
                        ],
                    'label': f'{n1}_plot_{n2}',
                    'projection': None,
                    'patch_alpha': 1.0 if not n2 else 0.0,
                    }
                ]
            for n1, v in pdata.items()
            for n2 in range(len(v))
            },
        **{
            f'{n1}_vline_{i}': [
                'vline', [t], {'ls': ':', 'linewidth': 0.5, 'c': '#000000'},
                {
                    'loc_plot': [
                        (fig_param['mlmargin'] + fig_param['mgap'])
                        / fig_param['w'],
                        (
                            fig_param['h']
                            - (n1+1)*fig_param['mph']
                            + fig_param['mgap']
                            )
                        / fig_param['h'],
                        (fig_param['mpw'] - 2*fig_param['mgap'])
                        / fig_param['w'],
                        (fig_param['mph'] - 2*fig_param['mgap'])
                        / fig_param['h'],
                        ],
                    'ylim': [None, None],
                    'yticks': [],
                    'tick_params': {
                        'axis': 'both', 'which': 'major', 'direction': 'in',
                        'labelsize': fontsize,
                        'left': False, 'bottom': True,
                        'right': False, 'top': True,
                        'labelleft': False,
                        'labelbottom': (
                            True if n1 == len(pdata)-1 and not i else False
                            ),
                        'labelright': False, 'labeltop': False
                        },
                    'xlim': [
                        np.datetime64(dates_tHour[0]) - np.timedelta64(1, 'D'),
                        np.datetime64(dates_tHour[-1]) + np.timedelta64(1, 'D'),
                        ],
                    'xtick_params': [
                        True,
                        mdates.DateFormatter('%Y-%m-%d'),
                        mdates.DayLocator(bymonthday=(1, 16)),
                        ],
                    'label': f'{n1}_vline_{i}',
                    'projection': None,
                    'patch_alpha': 0.0,
                    }
                ]
            for n1, v in pdata.items()
            for i, t in enumerate(pd.to_datetime(rdates)[1:])
            },
        **{
            f'{n1}_hline_{n2}': [
                'hline', [i], {
                    'ls': '-' if not n2 else '--',
                    'linewidth': 0.5, 'c': '#000000'
                    },
                {
                    'loc_plot': [
                        (fig_param['mlmargin'] + fig_param['mgap'])
                        / fig_param['w'],
                        (
                            fig_param['h']
                            - (n1+1)*fig_param['mph']
                            + fig_param['mgap']
                            )
                        / fig_param['h'],
                        (fig_param['mpw'] - 2*fig_param['mgap'])
                        / fig_param['w'],
                        (fig_param['mph'] - 2*fig_param['mgap'])
                        / fig_param['h'],
                        ],
                    'ylim': [-0.2, 0.2] if n1 == 0 else [-4.0, 4.0],
                    'yticks': np.arange(
                        -0.2 if n1 == 0 else -4.0,
                        (0.2 if n1 == 0 else 4.0)+0.01,
                        0.1 if n1 == 0 else 2.0
                        ),
                    'tick_params': {
                        'axis': 'both', 'which': 'major', 'direction': 'in',
                        'labelsize': fontsize,
                        'left': False, 'bottom': False,
                        'right': False, 'top': False,
                        'labelleft': False, 'labelbottom': False,
                        'labelright': False, 'labeltop': False
                        },
                    'xlim': [
                        np.datetime64(dates_tHour[0]) - np.timedelta64(1, 'D'),
                        np.datetime64(dates_tHour[-1]) + np.timedelta64(1, 'D'),
                        ],
                    'xtick_params': [
                        True,
                        mdates.DateFormatter('%Y-%m-%d'),
                        mdates.DayLocator(bymonthday=(1, 16)),
                        ],
                    'label': f'{n1}_hline_{i}',
                    'projection': None,
                    'patch_alpha': 0.0,
                    }
                ]
            for n1, v in pdata.items()
            for n2, i in enumerate([0.0, - boreas_lim[n1], boreas_lim[n1]])
            },
        },
    texts=[
        *[
            {
                'x': fig_param['mgap'] / fig_param['w'],
                'y': (
                    fig_param['h']
                    - (n1+0.5)*fig_param['mph']
                    ) / fig_param['h'],
                's': {
                    0: 'Q0 - Q1\n$\delta^{13}$C$_{obs}$ (\u2030)',
                    1: 'Q0 - Q1\n$\delta^{2}$H$_{obs}$ (\u2030)',
                    }[n1],
                'ha': 'left',
                'va': 'center', 'size': fontsize, 'rotation': 90,
                'ma': 'center'
                }
            for n1 in pdata.keys()
            ],
        *[
            {
                'x': (
                    fig_param['mlmargin'] + 2*fig_param['mgap']
                    ) / fig_param['w'],
                'y': (
                    fig_param['h']
                    - (n1)*fig_param['mph']
                    - 2*fig_param['mgap']
                    ) / fig_param['h'],
                's': f'{alphabet[n1]})',
                'ha': 'left', 'va': 'top', 'size': fontsize,
                'fontweight': 'bold',
                'bbox': {
                    'boxstyle': 'round', 'pad': 0.1,
                    'facecolor': '#ffffff', 'edgecolor': '#ffffff',
                    }
                }
            for n1 in pdata.keys()
            ]
        ],
    legend_params=[[], [], {}]
    )
for label in axs[fk][0][f'{len(pdata)-1}_vline_0'].get_xticklabels():
    label.set_ha("right")
    label.set_rotation(30)
figs[fk].savefig(os.path.join(odir, f'{fk}.png'))

fig_param['mph'] = fig_param_def['mph']
fig_param['mlmargin'] = fig_param_def['mlmargin']
fig_param['w'] = fig_param_def['w']
fig_param['h'] = fig_param_def['h']


site = 'cbw020'
fk = 'for0_cbw_unc'
pdata = {
    n: [
        obs_y['s0obq0'][site][k]
        - prior_obs['s0obq0'][site][k][nx_tH_tObs[site][k]],
        obs_y_err['s0obq0'][site][k]**0.5
        ]
    for n, k in enumerate(['chi', 'd_13C', 'd_2H'])
    }
pdata_ylim = {
    0: [
        [[-1200, 1200], np.arange(-1200, 1200.1, 400)],
        [[-600, 600], np.arange(-600, 600.1, 200)],
        ],
    1: [
        [[-5, 5], np.arange(-6, 6.1, 2)],
        [[-3, 3], np.arange(-3, 3.1, 1)],
        ],
    2: [
        [[-70, 70], np.arange(-80, 80.1, 40)],
        [[-30, 30], np.arange(-30, 30.1, 10)],
        ],
    }

fig_param['mph'] = 1.
fig_param['mlmargin'] = 0.75
fig_param['w'] = fig_param['mpw'] + 2*fig_param['mlmargin']
fig_param['h'] = len(pdata)*fig_param['mph'] + fig_param['mbmargin']

figs[fk] = plt.figure(figsize=(fig_param['w'], fig_param['h']), dpi=300)
axs[fk] = {}

figs[fk].clf()
axs[fk][0] = name_qch4_couple.plot.generic2(
    fig=figs[fk],
    idata={
        **{
            f'{n1}_plot_1': [
                'err',
                [
                    ],
                {
                    'x': (
                        dates_tObs[site]['chi'] if n1 == 0 else
                        dates_tObs[site]['d_13C'] if n1 == 1 else
                        dates_tObs[site]['d_2H']
                        ),
                    'y': np.zeros(v[1].size), 
                    'yerr': v[1],
                    'marker': 'None',
                    'ls': '',
                    'elinewidth': 0.2, 'ecolor': '#aaaaaa',
                    },
                {
                    'loc_plot': [
                        (fig_param['mlmargin'] + fig_param['mgap'])
                        / fig_param['w'],
                        (
                            fig_param['h']
                            - (n1+1)*fig_param['mph']
                            + fig_param['mgap']
                            )
                        / fig_param['h'],
                        (fig_param['mpw'] - 2*fig_param['mgap'])
                        / fig_param['w'],
                        (fig_param['mph'] - 2*fig_param['mgap'])
                        / fig_param['h'],
                        ],
                    'ylim': pdata_ylim[n1][1][0],
                    'yticks': pdata_ylim[n1][1][1],
                    'tick_params': {
                        'axis': 'both', 'which': 'major', 'direction': 'in',
                        'labelsize': fontsize,
                        'left': False,
                        'bottom': False,
                        'right': True,
                        'top': False,
                        'labelleft': False,
                        'labelbottom': False,
                        'labelright': True, 'labeltop': False
                        },
                    'xlim': [
                        np.datetime64(dates_tHour[0]) - np.timedelta64(1, 'D'),
                        np.datetime64(dates_tHour[-1]) + np.timedelta64(1, 'D'),
                        ],
                    'xtick_params': [
                        True,
                        mdates.DateFormatter('%Y-%m-%d'),
                        mdates.DayLocator(bymonthday=(1, 16)),
                        ],
                    'label': f'{n1}_plot_1',
                    'projection': None,
                    'patch_alpha': 0.0,
                    }
                ]
            for n1, v in pdata.items()
            },
        **{
            f'{n1}_plot_0': [
                'line',
                [
                    (
                        dates_tObs[site]['chi'] if n1 == 0 else
                        dates_tObs[site]['d_13C'] if n1 == 1 else
                        dates_tObs[site]['d_2H']
                        ),
                    v[0],
                    'o'
                    ],
                {
                    'ls': '',
                    'c': 'None', #colours[n2+2],
                    'mec': colours[2],
                    'ms': 2.0, 'mew': 0.5,
                    },
                {
                    'loc_plot': [
                        (fig_param['mlmargin'] + fig_param['mgap'])
                        / fig_param['w'],
                        (
                            fig_param['h']
                            - (n1+1)*fig_param['mph']
                            + fig_param['mgap']
                            )
                        / fig_param['h'],
                        (fig_param['mpw'] - 2*fig_param['mgap'])
                        / fig_param['w'],
                        (fig_param['mph'] - 2*fig_param['mgap'])
                        / fig_param['h'],
                        ],
                    'ylim': pdata_ylim[n1][0][0],
                    'yticks': pdata_ylim[n1][0][1],
                    'tick_params': {
                        'axis': 'both', 'which': 'major', 'direction': 'in',
                        'labelsize': fontsize,
                        'left': True,
                        'bottom': False,
                        'right': False,
                        'top': False,
                        'labelleft': True,
                        'labelbottom': False,
                        'labelright': False, 'labeltop': False
                        },
                    'xlim': [
                        np.datetime64(dates_tHour[0]) - np.timedelta64(1, 'D'),
                        np.datetime64(dates_tHour[-1]) + np.timedelta64(1, 'D'),
                        ],
                    'xtick_params': [
                        True,
                        mdates.DateFormatter('%Y-%m-%d'),
                        mdates.DayLocator(bymonthday=(1, 16)),
                        ],
                    'label': f'{n1}_plot_0',
                    'projection': None,
                    'patch_alpha': 0.0,
                    }
                ]
            for n1, v in pdata.items()
            },
        **{
            f'{n1}_hline': [
                'hline', [0.0], {'ls': '-', 'linewidth': 0.5, 'c': '#000000'},
                {
                    'loc_plot': [
                        (fig_param['mlmargin'] + fig_param['mgap'])
                        / fig_param['w'],
                        (
                            fig_param['h']
                            - (n1+1)*fig_param['mph']
                            + fig_param['mgap']
                            )
                        / fig_param['h'],
                        (fig_param['mpw'] - 2*fig_param['mgap'])
                        / fig_param['w'],
                        (fig_param['mph'] - 2*fig_param['mgap'])
                        / fig_param['h'],
                        ],
                    'ylim': pdata_ylim[n1][1][0],
                    'yticks': pdata_ylim[n1][1][1],
                    'tick_params': {
                        'axis': 'both', 'which': 'major', 'direction': 'in',
                        'labelsize': fontsize,
                        'left': False,
                        'bottom': False,
                        'right': False,
                        'top': False,
                        'labelleft': False,
                        'labelbottom': False,
                        'labelright': False, 'labeltop': False
                        },
                    'xlim': [
                        np.datetime64(dates_tHour[0]) - np.timedelta64(1, 'D'),
                        np.datetime64(dates_tHour[-1]) + np.timedelta64(1, 'D'),
                        ],
                    'xtick_params': [
                        True,
                        mdates.DateFormatter('%Y-%m-%d'),
                        mdates.DayLocator(bymonthday=(1, 16)),
                        ],
                    'label': f'{n1}_hline',
                    'projection': None,
                    'patch_alpha': 0.0,
                    }
                ]
            for n1, v in pdata.items()
            },
        **{
            f'{n1}_vline_{i}': [
                'vline', [t], {'ls': ':', 'linewidth': 0.5, 'c': '#000000'},
                {
                    'loc_plot': [
                        (fig_param['mlmargin'] + fig_param['mgap'])
                        / fig_param['w'],
                        (
                            fig_param['h']
                            - (n1+1)*fig_param['mph']
                            + fig_param['mgap']
                            )
                        / fig_param['h'],
                        (fig_param['mpw'] - 2*fig_param['mgap'])
                        / fig_param['w'],
                        (fig_param['mph'] - 2*fig_param['mgap'])
                        / fig_param['h'],
                        ],
                    'ylim': [None, None],
                    'yticks': [],
                    'tick_params': {
                        'axis': 'both', 'which': 'major', 'direction': 'in',
                        'labelsize': fontsize,
                        'left': False, 'bottom': True,
                        'right': False, 'top': True,
                        'labelleft': False,
                        'labelbottom': (
                            True if n1 == len(pdata)-1 and not i else False
                            ),
                        'labelright': False, 'labeltop': False
                        },
                    'xlim': [
                        np.datetime64(dates_tHour[0]) - np.timedelta64(1, 'D'),
                        np.datetime64(dates_tHour[-1]) + np.timedelta64(1, 'D'),
                        ],
                    'xtick_params': [
                        True,
                        mdates.DateFormatter('%Y-%m-%d'),
                        mdates.DayLocator(bymonthday=(1, 16)),
                        ],
                    'label': f'{n1}_vline_{i}',
                    'projection': None,
                    'patch_alpha': 0.0,
                    }
                ]
            for n1, v in pdata.items()
            for i, t in enumerate(pd.to_datetime(rdates)[1:])
            },
        },
    texts=[
        *[
            {
                'x': 1.0 - fig_param['mgap'] / fig_param['w'],
                'y': (
                    fig_param['h']
                    - (n1+0.5)*fig_param['mph']
                    ) / fig_param['h'],
                's': {
                    0: f'Uncertainty\n{y_name["chi"]}',
                    1: f'Uncertainty\n{y_name["d_13C"]}',
                    2: f'Uncertainty\n{y_name["d_2H"]}',
                    }[n1],
                'ha': 'right',
                'va': 'center', 'size': fontsize, 'rotation': 90,
                'ma': 'center'
                }
            for n1 in pdata.keys()
            ],
        *[
            {
                'x': fig_param['mgap'] / fig_param['w'],
                'y': (
                    fig_param['h']
                    - (n1+0.5)*fig_param['mph']
                    ) / fig_param['h'],
                's': {
                    0: f'Residual\n{y_name["chi"]}',
                    1: f'Residual\n{y_name["d_13C"]}',
                    2: f'Residual\n{y_name["d_2H"]}',
                    }[n1],
                'ha': 'left',
                'va': 'center', 'size': fontsize, 'rotation': 90,
                'ma': 'center'
                }
            for n1 in pdata.keys()
            ],
        *[
            {
                'x': (
                    fig_param['mlmargin'] + 2*fig_param['mgap']
                    ) / fig_param['w'],
                'y': (
                    fig_param['h']
                    - (n1)*fig_param['mph']
                    - 2*fig_param['mgap']
                    ) / fig_param['h'],
                's': f'{alphabet[n1]})',
                'ha': 'left', 'va': 'top', 'size': fontsize,
                'fontweight': 'bold',
                'bbox': {
                    'boxstyle': 'round', 'pad': 0.1,
                    'facecolor': '#ffffff', 'edgecolor': '#ffffff',
                    }
                }
            for n1 in pdata.keys()
            ]
        ],
    legend_params=[[], [], {}]
    )
for label in axs[fk][0][f'{len(pdata)-1}_vline_0'].get_xticklabels():
    label.set_ha("right")
    label.set_rotation(30)
figs[fk].savefig(os.path.join(odir, f'{fk}.png'))

fig_param['mph'] = fig_param_def['mph']
fig_param['mlmargin'] = fig_param_def['mlmargin']
fig_param['w'] = fig_param_def['w']
fig_param['h'] = fig_param_def['h']


# =============================================================================
# for1: Cabauw
# =============================================================================
prior_obs['s0cbw0']['cbw'] = {}
prior_obs['s0cbw0']['cbw']['chi'] = np.concatenate([
    np.load(os.path.join(idirs[2], rdate, 'pri_y.npz'))['cbw_chi']
    for rdate in rdates
    ])

scodes1 = ['s0']
scodes2 = ['cbw0']
sites1 = ['cbw020', 'cbw']
y_vars = ['chi']
# Obs
yticks = {}
yrange = {}
sites_y = []
for scode in scodes1:
    codes_sen = [f'{scode}{i}' for i in scodes2]
    for site in sites1:
        for k in y_vars:
            if (k not in obs[site]) or (not np.count_nonzero(obs[site][k].values)):
                continue
            sk = f'{scode}_{site}_{k}'
            sites_y.append((site, k))
            ymin = np.nanmin(np.concatenate([
                *[
                    obs[site][k].resample('H').mean().loc[dates_tObs[site][k]].squeeze()
                    for c in codes_sen
                    ],
                *[prior_obs[c][site][k][nx_tH_tObs[site][k]] for c in codes_sen],
                ]))
            ymax = np.nanmax(np.concatenate([
                *[
                    obs[site][k].resample('h').mean().loc[dates_tObs[site][k]].squeeze()
                    for c in codes_sen
                    ],
                *[prior_obs[c][site][k][nx_tH_tObs[site][k]] for c in codes_sen],
                ]))
            ysf = np.floor(np.log10(np.abs([
                i if i != 0 else 1 for i in [ymin, ymax]
                ]))).max() - 1
            ysf = 1 if not ysf and k not in ['d_13C'] else ysf
            yticks[sk] = aticks(ymin, ymax, 3, 5, ysf, 1)
            yrange[sk] = yticks[sk][0][1] - yticks[sk][0][0]
fk = 'for1_obs'
fs = (
        subplot_wdt_obs0 + subplot_xmargin,
        2*subplot_hgt_obs0 + 0.5
        )
figs[fk] = plt.figure(figsize=fs, dpi=300)
axs[fk] = {}
figs[fk].clf()
axs[fk][0] = name_qch4_couple.plot.generic2(
    fig=figs[fk],
    idata={
        **{
            f'obs_{c}_{site}_{k}': [
                'line',
                [
                    dates_tObs[site][k],
                    obs[site][k].resample('H').mean().loc[dates_tObs[site][k]],
                    'o'
                    ],
                {
                    'c': colours[1], 'ls': '',
                    'ms': 5.0, 'mew': 0.0,#0.5, 'mec': '#000000',
                    'zorder': 2
                    },
                {
                    'loc_plot': [
                        (subplot_xmargin + subplot_gap) / fs[0],
                        (fs[1] - (n+p+1)*subplot_hgt_obs0 + subplot_gap) / fs[1],
                        (subplot_wdt_obs0 - 2*subplot_gap) / fs[0],
                        (subplot_hgt_obs0 - 2*subplot_gap) / fs[1]
                        ],
                    'ylim': yticks[f'{scode}_{site}_{k}'][0],
                    'yticks': np.arange(
                        yticks[f'{scode}_{site}_{k}'][0][0],
                        yticks[f'{scode}_{site}_{k}'][0][1]
                        + yticks[f'{scode}_{site}_{k}'][1],
                        yticks[f'{scode}_{site}_{k}'][1]
                        ),
                    'tick_params': {
                        'axis': 'both', 'which': 'major', 'direction': 'in',
                        'labelsize': fontsize,
                        'left': True, 'bottom': False,
                        'right': False, 'top': False,
                        'labelleft': True, 'labelbottom': False,
                        'labelright': False, 'labeltop': False
                        },
                    'xlim': [
                        np.datetime64(dates_tHour[0]) - np.timedelta64(1, 'D'),
                        np.datetime64(dates_tHour[-1]) + np.timedelta64(1, 'D'),
                        ],
                    'xtick_params': [
                        True,
                        mdates.DateFormatter('%Y-%m-%d'),
                        mdates.DayLocator(bymonthday=(1, 16)),
                        ],
                    'label': f'obs_{c}_{site}_{k}',
                    'projection': None,
                    'patch_alpha': 0.0,
                    },
                ]
            for n, scode in enumerate(scodes1)
            for o, c in enumerate([f'{scode}{i}' for i in [scodes2[0]]])
            for p, (site, k) in enumerate(sites_y)
            },
        **{
            f'pri_{c}_{site}_{k}': [
                'line',
                [
                    dates_tObs[site][k],
                    prior_obs[c][site][k][nx_tH_tObs[site][k]],
                    'o'
                    ],
                {
                    'c': colours[o+2], 'ls': '',
                    'ms': 5.0, 'mew': 0.0,#0.5, 'mec': '#000000',
                    'zorder': 1
                    },
                {
                    'loc_plot': [
                        (subplot_xmargin + subplot_gap) / fs[0],
                        (fs[1] - (n+p+1)*subplot_hgt_obs0 + subplot_gap) / fs[1],
                        (subplot_wdt_obs0 - 2*subplot_gap) / fs[0],
                        (subplot_hgt_obs0 - 2*subplot_gap) / fs[1]
                        ],
                    'ylim': yticks[f'{scode}_{site}_{k}'][0],
                    'yticks': np.arange(
                        yticks[f'{scode}_{site}_{k}'][0][0],
                        yticks[f'{scode}_{site}_{k}'][0][1]
                        + yticks[f'{scode}_{site}_{k}'][1],
                        yticks[f'{scode}_{site}_{k}'][1]
                        ),
                    'tick_params': {
                        'axis': 'both', 'which': 'major', 'direction': 'in',
                        'labelsize': fontsize,
                        'left': False, 'bottom': False,
                        'right': False, 'top': False,
                        'labelleft': False, 'labelbottom': False,
                        'labelright': False, 'labeltop': False
                        },
                    'xlim': [
                        np.datetime64(dates_tHour[0]) - np.timedelta64(1, 'D'),
                        np.datetime64(dates_tHour[-1]) + np.timedelta64(1, 'D'),
                        ],
                    'xtick_params': [
                        True,
                        mdates.DateFormatter('%Y-%m-%d'),
                        mdates.DayLocator(bymonthday=(1, 16)),
                        ],
                    'label': f'pri_{c}_{site}_{k}',
                    'projection': None,
                    'patch_alpha': 0.0,
                    },
                ]
            for n, scode in enumerate(scodes1)
            for o, c in enumerate([f'{scode}{i}' for i in scodes2])
            for p, (site, k) in enumerate(sites_y)
            },
        **{
            f'{scode}_{site}_vline_{i}_{k}': [
                'vline', [i], {'ls': ':', 'linewidth': 0.5, 'c': '#000000'},
                {
                    'loc_plot': [
                        (subplot_xmargin + subplot_gap) / fs[0],
                        (fs[1] - (p+1)*subplot_hgt_obs0 + subplot_gap) / fs[1],
                        (subplot_wdt_obs0 - 2*subplot_gap) / fs[0],
                        (subplot_hgt_obs0 - 2*subplot_gap) / fs[1]
                        ],
                    'ylim': [None, None],
                    'yticks': [],
                    'tick_params': {
                        'axis': 'both', 'which': 'major', 'direction': 'in',
                        'labelsize': fontsize,
                        'left': False, 'bottom': True,
                        'right': False, 'top': True,
                        'labelleft': False,
                        'labelbottom': (
                            True if p == len(sites_y)-1 and not i else False
                            ),
                        'labelright': False, 'labeltop': False
                        },
                    'xlim': [
                        np.datetime64(dates_tHour[0]) - np.timedelta64(1, 'D'),
                        np.datetime64(dates_tHour[-1]) + np.timedelta64(1, 'D'),
                        ],
                    'xtick_params': [
                        True,
                        mdates.DateFormatter('%Y-%m-%d'),
                        mdates.DayLocator(bymonthday=(1, 16)),
                        ],
                    'label': f'{scode}_{site}_vline_{i}_{k}',
                    'projection': None,
                    'patch_alpha': 0.0,
                    }
                ]
            for n, scode in enumerate(scodes1)
            for o, c in enumerate([f'{scode}{i}' for i in [scodes2[0]]])
            for p, (site, k) in enumerate(sites_y)
            for i, t in enumerate(pd.to_datetime(rdates)[1:])
            },
        },
    texts=[
        *[
            {
                'x': subplot_gap / fs[0],
                'y': (
                    fs[1]
                    - (n+p+0.5)*subplot_hgt_obs0
                    - subplot_gap
                    - (subplot_gap if k == 'chi' else 0)
                    ) / fs[1],
                's': y_name[k],
                'ha': 'left',
                'va': 'center', 'size': fontsize, 'rotation': 90
                }
            for n, scode in enumerate(scodes1)
            for o, c in enumerate([f'{scode}{i}' for i in scodes2])
            for p, (site, k) in enumerate(sites_y)
            ],
        *[
            {
                'x': (subplot_xmargin+2*subplot_gap)/fs[0],
                'y': (fs[1] - i*subplot_hgt_obs0 - 2*subplot_gap) / fs[1],
                's': (
                    f'{alphabet[i]}) '+
                    ('CBW 20 m' if site == 'cbw020' else
                    'CBW 200 m')
                    ),
                'ha': 'left', 'va': 'top', 'size': fontsize,
                'fontweight': 'bold'
                }
            for i, (site, y) in enumerate(sites_y)
            ]
        ],
    legend_params=[
        [
            f'obs_s0cbw0_cbw_chi', f'pri_s0cbw0_cbw_chi',
            ],
        ['Measured', 'Q0'],
        {
            'loc': 'upper right',
            'bbox_to_anchor': [
                (fs[0] - subplot_gap) / fs[0],
                (fs[1] - subplot_gap) / fs[1]
                ],
            'numpoints': 1, 'fontsize': fontsize, 'ncol': 4,
            'handletextpad': 0.4
            }
        ]
    )
for label in axs[fk][0][f's0_cbw_vline_0_chi'].get_xticklabels():
    label.set_ha("right")
    label.set_rotation(30)
figs[fk].savefig(os.path.join(odir, f'{fk}.png'))


# =============================================================================
# set0: stress test
# =============================================================================
scodes1 = ['s1', 's2', 's3']
scodes2 = ['i2u0', 'i3u0']
sites1 = ['hfd']

# Obs sensitivity
yticks = {}
yrange = {}
for scode in scodes1:
    codes_sen = [f'{scode}{i}' for i in scodes2]
    for site in sites1:
        for k in ['chi', 'd_13C', 'd_2H']:
            sk = f'{scode}_{site}_{k}'
            ymin = np.nanmin(np.concatenate([
                *[obs_y[c][site][k] for c in codes_sen],
                *[prior_obs[c][site][k][nx_tH_tObs[site][k]] for c in codes_sen],
                *[post_obs[c][site][k][nx_tH_tObs[site][k]] for c in codes_sen],
                ]))
            ymax = np.nanmax(np.concatenate([
                *[obs_y[c][site][k] for c in codes_sen],
                *[prior_obs[c][site][k][nx_tH_tObs[site][k]] for c in codes_sen],
                *[post_obs[c][site][k][nx_tH_tObs[site][k]] for c in codes_sen],
                ]))
            ysf = np.floor(np.log10(np.abs([
                i if i != 0 else 1 for i in [ymin, ymax]
                ]))).max() - 1
            ysf = 1 if not ysf and k not in ['d_13C'] else ysf
            yticks[sk] = aticks(ymin, ymax, 3, 5, ysf, 1)
            yrange[sk] = yticks[sk][0][1] - yticks[sk][0][0]
            if k == 'chi':
                yticks[sk] = [[1800, 2200], 100]
                yrange[sk] = 400
            elif k == 'd_13C':
                yticks[sk] = [[-49, -47], 0.5]
                yrange[sk] = 2
            else:
                yticks[sk] = [[-110, -80], 10]
                yrange[sk] = 30
fs = (
        subplot_wdt_obs0 + 2*subplot_xmargin,
        9*subplot_hgt_obs0 + 0.5
        )
figs['set0_obs'] = plt.figure(figsize=fs, dpi=300)
axs['set0_obs'] = {}
figs['set0_obs'].clf()
axs['set0_obs'][0] = name_qch4_couple.plot.generic2(
    fig=figs['set0_obs'],
    idata={
        **{
            f'obs_{c}_{site}_{k}': [
                'line',
                [
                    dates_tObs[site][k],
                    obs_y[c][site][k],
                    'o'
                    ],
                {
                    'c': colours[1], 'ls': '',
                    'ms': 5.0, 'mew': 0.0,#0.5, 'mec': '#000000',
                    'zorder': 2
                    },
                {
                    'loc_plot': [
                        (subplot_xmargin + subplot_gap) / fs[0],
                        (fs[1] - (n+p+1)*3*subplot_hgt_obs0 + subplot_gap) / fs[1],
                        (subplot_wdt_obs0 - 2*subplot_gap) / fs[0],
                        (3*subplot_hgt_obs0 - 2*subplot_gap) / fs[1]
                        ],
                    'ylim': [
                        yticks[f'{scode}_{site}_{k}'][0][0]
                        - (2-q)*yrange[f'{scode}_{site}_{k}'],
                        yticks[f'{scode}_{site}_{k}'][0][1]
                        + (q)*yrange[f'{scode}_{site}_{k}'],
                        ],
                    'yticks': np.arange(
                        yticks[f'{scode}_{site}_{k}'][0][0],
                        yticks[f'{scode}_{site}_{k}'][0][1]
                        + yticks[f'{scode}_{site}_{k}'][1],
                        yticks[f'{scode}_{site}_{k}'][1]
                        ),
                    'tick_params': {
                        'axis': 'both', 'which': 'major', 'direction': 'in',
                        'labelsize': fontsize,
                        'left': True if not q%2 else False, 'bottom': False,
                        'right': False if not q%2 else True, 'top': False,
                        'labelleft': True if not q%2 else False, 'labelbottom': False,
                        'labelright': False if not q%2 else True, 'labeltop': False
                        },
                    'xlim': [
                        np.datetime64(dates_tHour[0]) - np.timedelta64(1, 'D'),
                        np.datetime64(dates_tHour[-1]) + np.timedelta64(1, 'D'),
                        ],
                    'xtick_params': [
                        True,
                        mdates.DateFormatter('%Y-%m-%d'),
                        mdates.DayLocator(bymonthday=(1, 16)),
                        ],
                    'label': f'obs_{scode}_{site}_{k}',
                    'projection': None,
                    'patch_alpha': 0.0,
                    },
                ]
            for n, scode in enumerate(scodes1)
            for o, c in enumerate([f'{scode}{i}' for i in [scodes2[0]]])
            for p, site in enumerate(['hfd'])
            for q, k in enumerate(['chi', 'd_13C', 'd_2H'])
            },
        **{
            f'pri_{c}_{site}_{k}': [
                'line',
                [
                    dates_tObs[site][k],
                    prior_obs[c][site][k][nx_tH_tObs[site][k]],
                    'o'
                    ],
                {
                    'c': colours[0], 'ls': '',
                    'ms': 5.0, 'mew': 0.0,#0.5, 'mec': '#000000',
                    'zorder': 1
                    },
                {
                    'loc_plot': [
                        (subplot_xmargin + subplot_gap) / fs[0],
                        (fs[1] - (n+p+1)*3*subplot_hgt_obs0 + subplot_gap) / fs[1],
                        (subplot_wdt_obs0 - 2*subplot_gap) / fs[0],
                        (3*subplot_hgt_obs0 - 2*subplot_gap) / fs[1]
                        ],
                    'ylim': [
                        yticks[f'{scode}_{site}_{k}'][0][0]
                        - (2-q)*yrange[f'{scode}_{site}_{k}'],
                        yticks[f'{scode}_{site}_{k}'][0][1]
                        + (q)*yrange[f'{scode}_{site}_{k}'],
                        ],
                    'yticks': np.arange(
                        yticks[f'{scode}_{site}_{k}'][0][0],
                        yticks[f'{scode}_{site}_{k}'][0][1]
                        + yticks[f'{scode}_{site}_{k}'][1],
                        yticks[f'{scode}_{site}_{k}'][1]
                        ),
                    'tick_params': {
                        'axis': 'both', 'which': 'major', 'direction': 'in',
                        'labelsize': fontsize,
                        'left': False, 'bottom': False,
                        'right': False, 'top': False,
                        'labelleft': False, 'labelbottom': False,
                        'labelright': False, 'labeltop': False
                        },
                    'xlim': [
                        np.datetime64(dates_tHour[0]) - np.timedelta64(1, 'D'),
                        np.datetime64(dates_tHour[-1]) + np.timedelta64(1, 'D'),
                        ],
                    'xtick_params': [
                        True,
                        mdates.DateFormatter('%Y-%m-%d'),
                        mdates.DayLocator(bymonthday=(1, 16)),
                        ],
                    'label': f'pri_{c}_{site}_{k}',
                    'projection': None,
                    'patch_alpha': 0.0,
                    },
                ]
            for n, scode in enumerate(scodes1)
            for o, c in enumerate([f'{scode}{i}' for i in [scodes2[0]]])
            for p, site in enumerate(['hfd'])
            for q, k in enumerate(['chi', 'd_13C', 'd_2H'])
            },
        **{
            f'pos_{c}_{site}_{k}': [
                'line',
                [
                    dates_tObs[site][k],
                    post_obs[c][site][k][nx_tH_tObs[site][k]],
                    '+'
                    ],
                {
                    'c': colours[2+o], 'ls': '',
                    'ms': 5.0, 'mew': 0.5, #'mec': '#000000',
                    'zorder': (3+o)
                    },
                {
                    'loc_plot': [
                        (subplot_xmargin + subplot_gap) / fs[0],
                        (fs[1] - (n+p+1)*3*subplot_hgt_obs0 + subplot_gap) / fs[1],
                        (subplot_wdt_obs0 - 2*subplot_gap) / fs[0],
                        (3*subplot_hgt_obs0 - 2*subplot_gap) / fs[1]
                        ],
                    'ylim': [
                        yticks[f'{scode}_{site}_{k}'][0][0]
                        - (2-q)*yrange[f'{scode}_{site}_{k}'],
                        yticks[f'{scode}_{site}_{k}'][0][1]
                        + (q)*yrange[f'{scode}_{site}_{k}'],
                        ],
                    'yticks': np.arange(
                        yticks[f'{scode}_{site}_{k}'][0][0],
                        yticks[f'{scode}_{site}_{k}'][0][1]
                        + yticks[f'{scode}_{site}_{k}'][1],
                        yticks[f'{scode}_{site}_{k}'][1]
                        ),
                    'tick_params': {
                        'axis': 'both', 'which': 'major', 'direction': 'in',
                        'labelsize': fontsize,
                        'left': False, 'bottom': False,
                        'right': False, 'top': False,
                        'labelleft': False, 'labelbottom': False,
                        'labelright': False, 'labeltop': False
                        },
                    'xlim': [
                        np.datetime64(dates_tHour[0]) - np.timedelta64(1, 'D'),
                        np.datetime64(dates_tHour[-1]) + np.timedelta64(1, 'D'),
                        ],
                    'xtick_params': [
                        True,
                        mdates.DateFormatter('%Y-%m-%d'),
                        mdates.DayLocator(bymonthday=(1, 16)),
                        ],
                    'label': f'pos_{c}_{site}_{k}',
                    'projection': None,
                    'patch_alpha': 0.0,
                    },
                ]
            for n, scode in enumerate(scodes1)
            for o, c in enumerate([f'{scode}{i}' for i in scodes2])
            for p, site in enumerate(['hfd'])
            for q, k in enumerate(['chi', 'd_13C', 'd_2H'])
            },
        **{
            f'{scode}_{site}_vline_{i}': [
                'vline', [i], {'ls': ':', 'linewidth': 0.5, 'c': '#000000'},
                {
                    'loc_plot': [
                        (subplot_xmargin + subplot_gap) / fs[0],
                        (fs[1] - (n+p+1)*3*subplot_hgt_obs0 + subplot_gap) / fs[1],
                        (subplot_wdt_obs0 - 2*subplot_gap) / fs[0],
                        (3*subplot_hgt_obs0 - 2*subplot_gap) / fs[1]
                        ],
                    'ylim': [None, None],
                    'yticks': [],
                    'tick_params': {
                        'axis': 'both', 'which': 'major', 'direction': 'in',
                        'labelsize': fontsize,
                        'left': False, 'bottom': True,
                        'right': False, 'top': True,
                        'labelleft': False,
                        'labelbottom': True if n == 2 and not i else False,
                        'labelright': False, 'labeltop': False
                        },
                    'xlim': [
                        np.datetime64(dates_tHour[0]) - np.timedelta64(1, 'D'),
                        np.datetime64(dates_tHour[-1]) + np.timedelta64(1, 'D'),
                        ],
                    'xtick_params': [
                        True,
                        mdates.DateFormatter('%Y-%m-%d'),
                        mdates.DayLocator(bymonthday=(1, 16)),
                        ],
                    'label': f'{scode}_{site}_vline_{i}',
                    'projection': None,
                    'patch_alpha': 0.0,
                    }
                ]
            for n, scode in enumerate(scodes1)
            for p, site in enumerate(['hfd'])
            for i, t in enumerate(pd.to_datetime(rdates)[1:])
            },
        },
    texts=[
        *[
            {
                'x': ((subplot_gap) if not q%2 else (fs[0]-subplot_gap))/fs[0],
                'y': (
                    fs[1]
                    - (n+p)*3*subplot_hgt_obs0
                    - (3*subplot_hgt_obs0 - 2*subplot_gap)/3*(q+0.5)
                    - subplot_gap
                    - (subplot_gap if k == 'chi' else 0)
                    ) / fs[1],
                's': y_name[k],
                'ha': 'left' if not q%2 else 'right',
                'va': 'center', 'size': fontsize, 'rotation': 90
                }
            for n, scode in enumerate(['s1', 's2', 's3'])
            for o, c in enumerate([f'{scode}{i}' for i in scodes2])
            for p, site in enumerate(['hfd'])
            for q, k in enumerate(['chi', 'd_13C', 'd_2H'])
            ],
        *[
            {
                'x': (subplot_xmargin+2*subplot_gap)/fs[0],
                'y': (fs[1] - 3*i*subplot_hgt_obs0 - 2*subplot_gap) / fs[1],
                's': f'{alphabet[i]})',
                'ha': 'left', 'va': 'top', 'size': fontsize,
                'fontweight': 'bold'
                }
            for i in range(len(scodes1))
            ]
        ],
    legend_params=[
        [
            f'obs_s1i2u0_hfd_chi', f'pri_s1i2u0_hfd_chi',
            f'pos_s1i2u0_hfd_chi', f'pos_s1i3u0_hfd_chi'
            ],
        ['Target', 'Prior', 'NHI', 'WHI'],
        {
            'loc': 'upper right',
            'bbox_to_anchor': [
                (fs[0] - subplot_xmargin - subplot_gap) / fs[0],
                (fs[1] - subplot_gap) / fs[1]
                ],
            'numpoints': 1, 'fontsize': fontsize, 'ncol': 4,
            'handletextpad': 0.4
            }
        ]
    )
for label in axs['set0_obs'][0]['s3_hfd_vline_0'].get_xticklabels():
    label.set_ha("right")
    label.set_rotation(30)
figs['set0_obs'].savefig(os.path.join(odir, 'set0_obs.png'))


# Q sensitivity
yticks = {}
yrange = {}
subplot_xmargin = 0.3
for scode in scodes1:
    codes_sen = [f'{scode}{i}' for i in scodes2]
    for s in s_sel:
        '''QA'''
        if s < dim_sAll:
            k1 = 'QA_sce'
            k2 = 'sce_QA_sce'
            s1 = s
        else:
            k1 = 'QA_sce_total'
            k2 = 'sce_QA_sce_total'
            s1 = 0
        sk = f'{scode}_{s}_QA'
        ymin = np.nanmin(np.concatenate([
            *[prior_state[c][k1][..., s1].flatten() for c in codes_sen],
            *[post_state[c][k1][..., s1].flatten() for c in codes_sen],
            *[prior_state[c][k2][..., s1].flatten() for c in codes_sen],
            ]))
        ymax = np.nanmax(np.concatenate([
            *[prior_state[c][k1][..., s1].flatten() for c in codes_sen],
            *[post_state[c][k1][..., s1].flatten() for c in codes_sen],
            *[prior_state[c][k2][..., s1].flatten() for c in codes_sen],
            ]))
        ysf = np.floor(np.log10(np.abs([
            i if i != 0 else 1 for i in [ymin, ymax]
            ]))).max() - 1
        ysf = 1 if not ysf and k not in ['d_13C'] else ysf
        yticks[sk] = aticks(ymin, ymax, 3, 5, ysf, 1)
        yrange[sk] = yticks[sk][0][1] - yticks[sk][0][0]
        if not s < dim_sAll:
            continue
        '''dQ'''
        for k in ['dQi1', 'dQi2']:
            k1 = f'v_{k}'
            sk = f'{scode}_{s}_{k}'
            ymin = np.nanmin(np.concatenate([
                *[prior_state[c][k1][..., s].flatten() for c in codes_sen],
                *[post_state[c][k1][..., s].flatten() for c in codes_sen],
                *[prior_state[c][k1][..., s].flatten() for c in codes_sen],
                ]))
            ymax = np.nanmax(np.concatenate([
                *[prior_state[c][k1][..., s].flatten() for c in codes_sen],
                *[post_state[c][k1][..., s].flatten() for c in codes_sen],
                *[prior_state[c][k1][..., s].flatten() for c in codes_sen],
                ]))
            ysf = np.floor(np.log10(np.abs([
                i if i != 0 else 1 for i in [ymin, ymax]
                ]))).max() - 1
            ysf = 1 if not ysf else ysf
            yticks[sk] = aticks(ymin, ymax, 3, 5, ysf, 1)
            yrange[sk] = yticks[sk][0][1] - yticks[sk][0][0]
fs = (
        3*subplot_wdt_obs1 + 4*subplot_xmargin,
        5*subplot_hgt_obs1 + 0.5
        )

figs['set0_state'] = plt.figure(figsize=fs, dpi=300)
axs['set0_state'] = {}
figs['set0_state'].clf()
axs['set0_state'][0] = name_qch4_couple.plot.generic2(
    fig=figs['set0_state'],
    idata={
        **{
            f'{c}_{th}_{s}_{k}': [
                'err',
                [],
                (
                    {
                        'x': np.arange(1, 1 + len(rdates)),
                        'y': (post_state if th == 'pos' else prior_state)[c][
                            ('QA_sce' if s < dim_sAll else 'QA_sce_total')
                            if th != 'sce' else
                            ('sce_QA_sce' if s < dim_sAll else 'sce_QA_sce_total')
                            ][
                                ...,
                                1 if ((s < dim_sAll) and (th != 'sce')) else
                                1 if s >= dim_sAll else
                                0,
                                s if s < dim_sAll else 0
                                ],
                        'yerr': np.diff(
                            (post_state if th == 'pos' else prior_state)[c][
                                ('QA_sce' if s < dim_sAll else 'QA_sce_total')
                                if th != 'sce' else
                                ('sce_QA_sce' if s < dim_sAll else 'sce_QA_sce_total')
                                ], axis=1
                            )[
                                ...,
                                s if s < dim_sAll else 0
                                ].T
                            if th != 'sce' else None,
                        'c': colours[
                            0 if th == 'pri' else
                            1 if th == 'sce' else
                            2+o
                            ],
                        'ls': '', 'marker': 'o',
                        'markersize': 5.0, 'elinewidth': 1.5,
                        'capsize': 4.0, 'capthick': 1.5,
                        'zorder': (
                            1 if th == 'pri' else
                            2 if th == 'sce' else
                            3+o
                            )
                        }
                    if k == 'QA' else
                    {
                        'x': np.arange(1, 1 + len(rdates)),
                        'y': (post_state if th == 'pos' else prior_state)[c][
                            f'v_{k}' if th != 'sce' else f'sce_v_{k}'
                            ][..., 1, s],
                        'yerr': np.diff(
                            (post_state if th == 'pos' else prior_state)[c][
                                f'v_{k}' if th != 'sce' else f'sce_v_{k}'
                                ][..., s],
                            axis=1
                            ).T
                            if th != 'sce' else None,
                        'c': colours[
                            0 if th == 'pri' else
                            1 if th == 'sce' else
                            2+o
                            ],
                        'ls': '', 'marker': 'o',
                        'markersize': 5.0, 'elinewidth': 1.5,
                        'capsize': 4.0, 'capthick': 1.5,
                        'zorder': (
                            1 if th == 'pri' else
                            2 if th == 'sce' else
                            3+o
                            )
                        }
                    ),
                {
                    'loc_plot': [
                        (
                            (4*q+n+2)*subplot_xmargin
                            + (3*q+n)*subplot_wdt_obs1
                            + subplot_gap
                            ) / fs[0],
                        (fs[1] - (p+1)*subplot_hgt_obs1 + subplot_gap) / fs[1],
                        (subplot_wdt_obs1 - 2*subplot_gap) / fs[0],
                        (subplot_hgt_obs1 - 2*subplot_gap) / fs[1]
                        ],
                    'ylim': [
                        yticks[f'{scode}_{s}_{k}'][0][0]
                        if yticks[f'{scode}_{s}_{k}'][0][0] != 0 else
                        -yrange[f'{scode}_{s}_{k}']/5,
                        yticks[f'{scode}_{s}_{k}'][0][1]
                        ],
                    'yticks': np.arange(
                        yticks[f'{scode}_{s}_{k}'][0][0],
                        yticks[f'{scode}_{s}_{k}'][0][1]
                        + yticks[f'{scode}_{s}_{k}'][1],
                        yticks[f'{scode}_{s}_{k}'][1]
                        ),
                    'tick_params': {
                        'axis': 'both', 'which': 'major', 'direction': 'in',
                        'labelsize': fontsize,
                        'left': True if th == 'pri' else False,
                        'bottom': True if th == 'pri' else False,
                        'right': True if th == 'pri' else False,
                        'top': False,
                        'labelleft': True if th == 'pri' else False,
                        'labelbottom': True if p == len(s_sel)-1 else False,
                        'labelright': False, 'labeltop': False
                        },
                    'xlim': [0, 1 + len(rdates)],
                    'xtick_params': [
                        False,
                        np.arange(2 + len(rdates)),
                        ['', 'O', 'N', 'D', 'J', 'F', 'M', '']
                        ],
                    'label': f'{c}_{th}_{s}_{k}',
                    'projection': None,
                    'patch_alpha': 0.0,
                    }
                ]
            for n, scode in enumerate(scodes1)
            for o, c in enumerate([f'{scode}{i}' for i in scodes2])
            for p, s in enumerate(s_sel)
            for th in ['pri', 'pos', 'sce']
            for q, k in enumerate(['QA'])
            if (
                    (c.endswith(scodes2[0]) or (th == 'pos'))
                    if k == 'QA' else
                    ((c.endswith(scodes2[0]) or (th == 'pos')) and s < dim_sAll)
                    )
            },
        },
    texts=[
        *[
            {
                'x': subplot_gap / fs[0],
                'y': (fs[1] - (p+0.5)*subplot_hgt_obs1 + subplot_gap) / fs[1],
                's': f'{sector_names[s][0]}',
                'ha': 'left', 'va': 'center', 'size': fontsize, 'rotation': 90
                }
            for n, scode in enumerate(scodes1[0])
            for p, s in enumerate(s_sel)
            ],
        *[
            {
                'x': (
                    (4*q)*subplot_xmargin
                    + (3*q)*subplot_wdt_obs1
                    + 3*subplot_gap
                    ) / fs[0],
                'y': (0.5 + (fs[1] - 0.5) / 2)/ fs[1],
                's': (
                    'Q (Gg yr$^{-1}$)' if k == 'QA' else
                    u'$\delta^{13}$C (\u2030)' if k == 'dQi1' else
                    u'$\delta^{2}$H (\u2030)'
                    ),
                'ha': 'left', 'va': 'center', 'size': fontsize, 'rotation': 90
                }
            for q, k in enumerate(['QA'])
            ],
        *[
            {
                'x': (
                    (4*q+n+2)*subplot_xmargin
                    + (3*q+n)*subplot_wdt_obs1
                    + 2*subplot_gap
                    ) / fs[0],
                'y': (fs[1] - 2*subplot_gap) / fs[1],
                's': f'{alphabet[n]})',
                'ha': 'left', 'va': 'top', 'size': fontsize,
                'fontweight': 'bold',
                'bbox': {
                    'boxstyle': 'round', 'pad': 0.1,
                    'facecolor': '#ffffff', 'edgecolor': '#ffffff'
                    }
                }
            for n in range(len(scodes1))
            for q, k in enumerate(['QA'])
            ],
        ],
    legend_params=[
        [
            f's1i2u0_sce_0_QA', f's1i2u0_pri_0_QA',
            f's1i2u0_pos_0_QA', f's1i3u0_pos_0_QA'
            ],
        ['Target', 'Prior', 'NHI', 'WHI'],
        {
            'loc': 'lower center',
            'bbox_to_anchor': [
                fs[0] / 2 / fs[0],
                (subplot_gap) / fs[1]
                ],
            'numpoints': 1, 'fontsize': fontsize, 'ncol': 4,
            'handletextpad': 0.2, 'columnspacing': 1.0
            }
        ]
    )
figs['set0_state'].savefig(os.path.join(odir, 'set0_state.png'))


# dQ sensitivity
fs = (
        6*subplot_wdt_obs1 + 8*subplot_xmargin,
        4*subplot_hgt_obs1 + 0.5
        )
figs['set0_state_dQ'] = plt.figure(figsize=fs, dpi=300)
axs['set0_state_dQ'] = {}
figs['set0_state_dQ'].clf()
axs['set0_state_dQ'][0] = name_qch4_couple.plot.generic2(
    fig=figs['set0_state_dQ'],
    idata={
        **{
            f'{c}_{th}_{s}_{k}': [
                'err',
                [],
                (
                    {
                        'x': np.arange(1, 1 + len(rdates)),
                        'y': (post_state if th == 'pos' else prior_state)[c][
                            ('QA_sce' if s < dim_sAll else 'QA_sce_total')
                            if th != 'sce' else
                            ('sce_QA_sce' if s < dim_sAll else 'sce_QA_sce_total')
                            ][
                                ...,
                                1 if ((s < dim_sAll) and (th != 'sce')) else
                                1 if s >= dim_sAll else
                                0,
                                s if s < dim_sAll else 0
                                ],
                        'yerr': np.diff(
                            (post_state if th == 'pos' else prior_state)[c][
                                ('QA_sce' if s < dim_sAll else 'QA_sce_total')
                                if th != 'sce' else
                                ('sce_QA_sce' if s < dim_sAll else 'sce_QA_sce_total')
                                ], axis=1
                            )[
                                ...,
                                s if s < dim_sAll else 0
                                ].T
                            if th != 'sce' else None,
                        'c': colours[
                            0 if th == 'pri' else
                            1 if th == 'sce' else
                            2+o
                            ],
                        'ls': '', 'marker': 'o',
                        'markersize': 5.0, 'elinewidth': 1.5,
                        'capsize': 4.0, 'capthick': 1.5,
                        'zorder': (
                            1 if th == 'pri' else
                            2 if th == 'sce' else
                            3+o
                            )
                        }
                    if k == 'QA' else
                    {
                        'x': np.arange(1, 1 + len(rdates)),
                        'y': (post_state if th == 'pos' else prior_state)[c][
                            f'v_{k}' if th != 'sce' else f'sce_v_{k}'
                            ][..., 1, s],
                        'yerr': np.diff(
                            (post_state if th == 'pos' else prior_state)[c][
                                f'v_{k}' if th != 'sce' else f'sce_v_{k}'
                                ][..., s],
                            axis=1
                            ).T
                            if th != 'sce' else None,
                        'c': colours[
                            0 if th == 'pri' else
                            1 if th == 'sce' else
                            2+o
                            ],
                        'ls': '', 'marker': 'o',
                        'markersize': 5.0, 'elinewidth': 1.5,
                        'capsize': 4.0, 'capthick': 1.5,
                        'zorder': (
                            1 if th == 'pri' else
                            2 if th == 'sce' else
                            3+o
                            )
                        }
                    ),
                {
                    'loc_plot': [
                        (
                            (4*q+n+2)*subplot_xmargin
                            + (3*q+n)*subplot_wdt_obs1
                            + subplot_gap
                            ) / fs[0],
                        (fs[1] - (p+1)*subplot_hgt_obs1 + subplot_gap) / fs[1],
                        (subplot_wdt_obs1 - 2*subplot_gap) / fs[0],
                        (subplot_hgt_obs1 - 2*subplot_gap) / fs[1]
                        ],
                    'ylim': [
                        yticks[f'{scode}_{s}_{k}'][0][0]
                        if yticks[f'{scode}_{s}_{k}'][0][0] != 0 else
                        -yrange[f'{scode}_{s}_{k}']/5,
                        yticks[f'{scode}_{s}_{k}'][0][1]
                        ],
                    'yticks': np.arange(
                        yticks[f'{scode}_{s}_{k}'][0][0],
                        yticks[f'{scode}_{s}_{k}'][0][1]
                        + yticks[f'{scode}_{s}_{k}'][1],
                        yticks[f'{scode}_{s}_{k}'][1]
                        ),
                    'tick_params': {
                        'axis': 'both', 'which': 'major', 'direction': 'in',
                        'labelsize': fontsize,
                        'left': True if th == 'pri' else False,
                        'bottom': True if th == 'pri' else False,
                        'right': True if th == 'pri' else False,
                        'top': False,
                        'labelleft': True if th == 'pri' else False,
                        'labelbottom': True if p == len(s_sel)-2 else False,
                        'labelright': False, 'labeltop': False
                        },
                    'xlim': [0, 1 + len(rdates)],
                    'xtick_params': [
                        False,
                        np.arange(2 + len(rdates)),
                        ['', 'O', 'N', 'D', 'J', 'F', 'M', '']
                        ],
                    'label': f'{c}_{th}_{s}_{k}',
                    'projection': None,
                    'patch_alpha': 0.0,
                    }
                ]
            for n, scode in enumerate(scodes1)
            for o, c in enumerate([f'{scode}{i}' for i in scodes2])
            for p, s in enumerate(s_sel)
            for th in ['pri', 'pos', 'sce']
            for q, k in enumerate(['dQi1', 'dQi2'])
            if (
                    (c.endswith(scodes2[0]) or (th == 'pos'))
                    if k == 'QA' else
                    ((c.endswith(scodes2[0]) or (th == 'pos')) and s < dim_sAll)
                    )
            },
        },
    texts=[
        *[
            {
                'x': subplot_gap / fs[0],
                'y': (fs[1] - (p+0.5)*subplot_hgt_obs1 + subplot_gap) / fs[1],
                's': f'{sector_names[s][0]}',
                'ha': 'left', 'va': 'center', 'size': fontsize, 'rotation': 90
                }
            for n, scode in enumerate(scodes1[0])
            for p, s in enumerate(s_sel)
            ],
        *[
            {
                'x': (
                    (4*q)*subplot_xmargin
                    + (3*q)*subplot_wdt_obs1
                    + 3*subplot_gap
                    ) / fs[0],
                'y': (0.5 + (fs[1] - 0.5) / 2)/ fs[1],
                's': (
                    'Q (Gg yr$^{-1}$)' if k == 'QA' else
                    u'$\delta^{13}$C (\u2030)' if k == 'dQi1' else
                    u'$\delta^{2}$H (\u2030)'
                    ),
                'ha': 'left', 'va': 'center', 'size': fontsize, 'rotation': 90
                }
            for q, k in enumerate(['dQi1', 'dQi2'])
            ],
        *[
            {
                'x': (
                    (4*q+n+2)*subplot_xmargin
                    + (3*q+n)*subplot_wdt_obs1
                    + 2*subplot_gap
                    ) / fs[0],
                'y': (fs[1] - 2*subplot_gap) / fs[1],
                's': f'{alphabet[q]}{n})',
                'ha': 'left', 'va': 'top', 'size': fontsize,
                'fontweight': 'bold',
                'bbox': {
                    'boxstyle': 'round', 'pad': 0.1,
                    'facecolor': '#ffffff', 'edgecolor': '#ffffff'
                    }
                }
            for n in range(len(scodes1))
            for q, k in enumerate(['dQi1', 'dQi2'])
            ],
        ],
    legend_params=[
        [
            f's1i2u0_sce_0_dQi1', f's1i2u0_pri_0_dQi1',
            f's1i2u0_pos_0_dQi1', f's1i3u0_pos_0_dQi1'
            ],
        ['Target', 'Prior', 'NHI', 'WHI'],
        {
            'loc': 'lower center',
            'bbox_to_anchor': [
                fs[0] / 2 / fs[0],
                (subplot_gap) / fs[1]
                ],
            'numpoints': 1, 'fontsize': fontsize, 'ncol': 4,
            'handletextpad': 0.2, 'columnspacing': 1.0
            }
        ]
    )
figs['set0_state_dQ'].savefig(os.path.join(odir, 'set0_state_dQ.png'))

subplot_xmargin = 0.6


# Q sensitivity map
fk = 'set0_Qmap_panel'
fig_param['mph'] = 1.5
fig_param['mpw'] = 1
fig_param['mw'] = 2*fig_param['mpw'] + 2*fig_param['mlmargin']
fig_param['mh'] = min(2, len(scodes2))*fig_param['mph'] + 1*fig_param['mbmargin']
for s in s_sel:
    fk_s = f'{fk}_{s}'
    figs[fk_s] = plt.figure(figsize=(fig_param['mw'], fig_param['mh']), dpi=300)
    axs[fk_s] = {}

pm_sym_ticks = lambda x: np.concatenate([-x[::-1], x])
for n0, scode in enumerate(scodes1):
    for s in s_sel:
        fk_s = f'{fk}_{s}'
        figs[fk_s].clf()
    for pn in range(2+len(scodes2)):
        if pn < 3:
            c = f'{scode}{scodes2[0]}'
        else:
            c = f'{scode}{scodes2[pn-2]}'
        plotable_abs[:] = np.nan
        for n2, rdate in enumerate(rdates):
            for n3, reg in enumerate(
                    np.unique(inv_reg_maps[c][n2][reg_map_sel[c][n2]])
                    ):
                plotable_abs[n2][inv_reg_maps[c][n2] == reg] = (
                    (
                        prior_state[c]['sce_Q'][n2][n3]
                        - post_state[c]['v_Q'][n2][1][n3]
                        )
                    if pn > 1 else
                    (
                        prior_state[c]['sce_Q'][n2][n3]
                        - prior_state[c]['v_Q'][n2][n3]
                        )
                    if pn == 1 else
                    prior_state[c]['v_Q'][n2][n3]
                    )
            #plotable_abs[n2][reg_map_sel[c][n0]] *= inv_Q_dist[c][n0]
        for n4, s in enumerate(s_sel):
            if pn == 0:
                plim = {
                    0: np.power(10., [-12, -8]),
                    1: np.power(10., [-10, -8]),
                    2: np.power(10., [-10, -7]),
                    4: np.power(10., [-10, -9]),
                    5: np.power(10., [-12, -7]),
                    }[s]
                pticks = {
                    0: np.power(10., np.linspace(*np.log10(plim), 9)),
                    1: np.power(10., np.linspace(*np.log10(plim), 5)),
                    2: np.power(10., np.linspace(*np.log10(plim), 5)),
                    4: np.power(10., np.linspace(*np.log10(plim), 5)),
                    5: np.power(10., np.linspace(*np.log10(plim), 11)),
                    }[s]
                pticklabels = {
                    0: np.power(10., np.linspace(*np.log10(plim), 9)),
                    1: np.power(10., np.linspace(*np.log10(plim), 5)),
                    2: np.power(10., np.linspace(*np.log10(plim), 5)),
                    4: np.power(10., np.linspace(*np.log10(plim), 5)),
                    5: np.power(10., np.linspace(*np.log10(plim), 11)),
                    }[s]
            else:
                plim = {
                    0: np.power(10., [-9, -8]),
                    1: np.power(10., [-9, -8]),
                    2: np.power(10., [-9, -8]),
                    4: np.power(10., [-9, -8]),
                    5: np.power(10., [-9, -8]),
                    }[s]
                pticks = {
                    0: pm_sym_ticks(np.power(10., np.linspace(*np.log10(plim), 5))),
                    1: pm_sym_ticks(np.power(10., np.linspace(*np.log10(plim), 5))),
                    2: pm_sym_ticks(np.power(10., np.linspace(*np.log10(plim), 5))),
                    4: pm_sym_ticks(np.power(10., np.linspace(*np.log10(plim), 5))),
                    5: pm_sym_ticks(np.power(10., np.linspace(*np.log10(plim), 5))),
                    }[s]
                pticklabels = pm_sym_ticks(plim)
            if s < dim_sAll:
                m = np.nansum(plotable_abs[:, s], axis=0)/plotable_abs.shape[0]
            else:
                m = np.nansum(plotable_abs, axis=1).mean(0)
            if pn < 2:
                m[m == 0] = np.nan
            if np.isnan(m).all():
                continue
            fk_s = f'{fk}_{s}'
            sk = f'{c}_map_{s}'
            no_c = 0 if pn < 2 else 1
            no_r = pn if pn < 2 else (pn+2) % len(scodes2)
            axs[fk_s][sk] = name_qch4_couple.plot.geographical(
                fig=figs[fk_s],
                label=sk,
                idata=m.copy(),
                grid=grid_sel,
                texts=[
                    {  # colourbar label
                        'x': (
                            (no_c)*fig_param['mpw']
                            + (no_c)*fig_param['mlmargin']
                            + 1*fig_param['mgap']
                            ) / fig_param['mw'],
                        'y': (
                            fig_param['mh']
                            - (no_r)*fig_param['mph']
                            - 3*fig_param['mgap']
                            ) / fig_param['mh'],
                        's': f'{alphabet[pn]})',
                        'ha': 'left', 'va': 'top', 'size': fontsize,
                        'transform': figs[fk_s].transFigure,
                        'fontweight': 'bold',
                        'bbox': {
                            'boxstyle': 'round', 'pad': 0.1,
                            'facecolor': '#ffffff', 'edgecolor': '#ffffff',
                            }
                        }
                    ],
                loc_plot=[
                    (
                        (no_c)*fig_param['mpw']
                        + (no_c)*fig_param['mlmargin']
                        ) / fig_param['mw'],
                    (
                        fig_param['mh']
                        - (no_r+1)*fig_param['mph']
                        ) / fig_param['mh'],
                    fig_param['mpw']/fig_param['mw'],
                    fig_param['mph']/fig_param['mh'],
                    ],
                loc_bar=[
                    (
                        (no_c+1)*fig_param['mpw']
                        + (no_c)*fig_param['mlmargin']
                        + 1*fig_param['mgap']
                        ) / fig_param['mw'],
                    (
                        fig_param['mh']
                        - (no_r+1)*fig_param['mph']
                        + 1*fig_param['mgap']
                        ) / fig_param['mh'],
                    0.2/fig_param['mw'],
                    (
                        fig_param['mph'] - 2 * fig_param['mgap']
                        ) / fig_param['mh'],
                    ],
                bbox=[-12., 3., 45., 65],
                tick_params={'labelsize': 6, 'pad': 0.05},
                shapefiles=[
                    '/home/ec5/name_qch4_couple/shape_files/'
                    'CNTR_BN_10M_2016_4326.shp'
                    ],
                colour='RdYlBu_r' if pn else 'viridis_r',
                vlim=[*pticks[[0, -1]], pticks, pticklabels],
                extend='both',
                )
            """
            figs[fk_s].axes[-1].set_yticklabels(
                [
                    f'${{{(np.sign(x)*10):+2.0f}}}$'
                    f'$^{{{np.log10(np.abs(x)):+03.0f}}}$'
                    if x != 0. else
                    '0.0'
                    for x in np.concatenate([
                        pticklabels
                        ])
                    ]
                )
            """
    for s in s_sel:
        fk_s = f'{fk}_{s}'
        figs[fk_s].text(
            x=(fig_param['mw']-fig_param['mgap'])/fig_param['mw'],
            y=fig_param['mgap']/fig_param['mh'],
            s='Units: g m$^{-2}$ s$^{-1}$',
            fontsize=fig_param['fontsize'],
            ha='right', va='bottom'
            )
        figs[fk_s].savefig(os.path.join(odir, f'{fk_s}_{scode}.png'))

fig_param['mph'] = fig_param_def['mph']
fig_param['mpw'] = fig_param_def['mpw']
fig_param['mh'] = fig_param_def['mh']
fig_param['mw'] = fig_param_def['mw']


plt.close('all')

# =============================================================================
# set1: mixture
# =============================================================================
setc = 'set1'
scodes1 = ['s4']
scodes2 = ['i2u0', 'i3u0', 'i3u1']
sites1 = ['hfd']

plt.close('all')

# Obs sensitivity
yticks = {}
yrange = {}
for scode in scodes1:                                                             
    codes_sen = [f'{scode}{i}' for i in scodes2]
    for site in sites1:
        for k in ['chi', 'd_13C', 'd_2H']:
            '''Target'''
            sk = f'{setc}_{scode}_{site}_{k}'
            ymin = np.nanmin(np.concatenate([
                *[obs_y[c][site][k] for c in codes_sen],
                ]))
            ymax = np.nanmax(np.concatenate([
                *[obs_y[c][site][k] for c in codes_sen],
                ]))
            ysf = np.floor(np.log10(np.abs([
                i if i != 0 else 1 for i in [ymin, ymax]
                ]))).max() - 1
            ysf = 1 if not ysf and k not in ['d_13C'] else ysf
            yticks[sk] = aticks(ymin, ymax, 3, 5, ysf, 1)
            yrange[sk] = yticks[sk][0][1] - yticks[sk][0][0]
            '''Model-Target'''
            sk = f'{setc}_{scode}_{site}_{k}_diff'
            ymin = np.nanmin(np.concatenate([
                *[- obs_y_err[c][site][k]**0.5 for c in codes_sen],
                *[
                    obs_y[c][site][k]
                    - prior_obs[c][site][k][nx_tH_tObs[site][k]]
                    for c in codes_sen
                    ],
                *[
                    obs_y[c][site][k]
                    - post_obs[c][site][k][nx_tH_tObs[site][k]]
                    for c in codes_sen
                    ],
                ]))
            ymax = np.nanmax(np.concatenate([
                *[+ obs_y_err[c][site][k]**0.5 for c in codes_sen],
                *[
                    obs_y[c][site][k]
                    - prior_obs[c][site][k][nx_tH_tObs[site][k]]
                    for c in codes_sen
                    ],
                *[
                    obs_y[c][site][k]
                    - post_obs[c][site][k][nx_tH_tObs[site][k]]
                    for c in codes_sen
                    ],
                ]))
            ysf = np.floor(np.log10(np.abs([
                i if i != 0 else 1 for i in [ymin, ymax]
                ]))).max() - 1
            ysf = 1 if not ysf and k not in ['d_13C'] else ysf
            yticks[sk] = aticks(ymin, ymax, 3, 5, ysf, 1)
            yrange[sk] = yticks[sk][0][1] - yticks[sk][0][0]
            if k == 'chi':
                yticks[sk] = [[-6.0, 4.0], 2]
                yrange[sk] = 10.0
            elif k == 'd_13C':
                yticks[sk] = [[-0.1, 0.1], 0.05]
                yrange[sk] = 0.2
            elif k == 'd_2H':
                yticks[sk] = [[-1.0, 1.0], 0.5]
                yrange[sk] = 2

subplot_hgt_obs0 = 2
fs = (
        subplot_wdt_obs0 + 2*subplot_xmargin,
        3*subplot_hgt_obs0 + 0.5
        )
figs['set1_obs'] = plt.figure(figsize=fs, dpi=300)
axs['set1_obs'] = {}

figs['set1_obs'].clf()
axs['set1_obs'][0] = name_qch4_couple.plot.generic2(
    fig=figs['set1_obs'],
    idata={
        **{
            f'obs_{c}_{site}_{k}': [
                'err',
                [],
                {
                    'x': dates_tObs[site][k],
                    'y': obs_y[c][site][k],
                    'yerr': obs_y_err[c][site][k]**.5,# if k in ['chi'] else None, #NCG[k],
                    'marker': 'o',
                    'c': '#ffffff', 'ls': '',
                    'markersize': 4.0, 'markeredgewidth': 0.5,
                    'markeredgecolor': '#000000',
                    'elinewidth': 1.0, 'ecolor': '#aaaaaa',
                    'zorder': 2
                    },
                {
                    'loc_plot': [
                        (subplot_xmargin + subplot_gap) / fs[0],
                        (fs[1] - (n+p+1)*3*subplot_hgt_obs0 + subplot_gap) / fs[1],
                        (subplot_wdt_obs0 - 2*subplot_gap) / fs[0],
                        (3*subplot_hgt_obs0 - 2*subplot_gap) / fs[1]
                        ],
                    'ylim': [
                        yticks[f'{setc}_{scode}_{site}_{k}'][0][0]
                        - (5-2*q)*yrange[f'{setc}_{scode}_{site}_{k}'],
                        yticks[f'{setc}_{scode}_{site}_{k}'][0][1]
                        + (2*q)*yrange[f'{setc}_{scode}_{site}_{k}'],
                        ],
                    'yticks': np.arange(
                        yticks[f'{setc}_{scode}_{site}_{k}'][0][0],
                        yticks[f'{setc}_{scode}_{site}_{k}'][0][1]
                        + yticks[f'{setc}_{scode}_{site}_{k}'][1],
                        yticks[f'{setc}_{scode}_{site}_{k}'][1]
                        ),
                    'tick_params': {
                        'axis': 'both', 'which': 'major', 'direction': 'in',
                        'labelsize': fontsize,
                        'left': True, 'bottom': False,
                        'right': False, 'top': False,
                        'labelleft': True, 'labelbottom': False,
                        'labelright': False, 'labeltop': False
                        },
                    'xlim': [
                        np.datetime64(dates_tHour[0]) - np.timedelta64(1, 'D'),
                        np.datetime64(dates_tHour[-1]) + np.timedelta64(1, 'D'),
                        ],
                    'xtick_params': [
                        True,
                        mdates.DateFormatter('%Y-%m-%d'),
                        mdates.DayLocator(bymonthday=(1, 16)),
                        ],
                    'label': f'obs_{scode}_{site}_{k}',
                    'projection': None,
                    'patch_alpha': 0.0,
                    },
                ]
            for n, scode in enumerate(scodes1)
            for o, c in enumerate([f'{scode}{i}' for i in [scodes2[0]]])
            for p, site in enumerate(sites1)
            for q, k in enumerate(['chi', 'd_13C', 'd_2H'])
            },
        **{
            f'{scode}_{site}_unc_{k}': [
                'err',
                [],
                {
                    'x': dates_tObs[site][k],
                    'y': np.zeros(dates_tObs[site][k].size),
                    'yerr': obs_y_err[f'{scode}{scodes2[0]}'][site][k]**0.5,
                    'marker': 'None',
                    'c': '#ffffff', 'ls': '',
                    'markersize': 5.0, 'markeredgewidth': 0.5,
                    'markeredgecolor': '#000000',
                    'elinewidth': 1.0, 'ecolor': '#aaaaaa',
                    },
                {
                    'loc_plot': [
                        (subplot_xmargin + subplot_gap) / fs[0],
                        (fs[1] - (n+p+1)*3*subplot_hgt_obs0 + subplot_gap) / fs[1],
                        (subplot_wdt_obs0 - 2*subplot_gap) / fs[0],
                        (3*subplot_hgt_obs0 - 2*subplot_gap) / fs[1]
                        ],
                    'ylim': [
                        yticks[f'{setc}_{scode}_{site}_{k}_diff'][0][0]
                        - (4-2*q)*yrange[f'{setc}_{scode}_{site}_{k}_diff'],
                        yticks[f'{setc}_{scode}_{site}_{k}_diff'][0][-1]
                        + (1+2*q)*yrange[f'{setc}_{scode}_{site}_{k}_diff'],
                        ],
                    'yticks': [
                        yticks[f'{setc}_{scode}_{site}_{k}_diff'][0][0],
                        yticks[f'{setc}_{scode}_{site}_{k}_diff'][0][1]
                        + yticks[f'{setc}_{scode}_{site}_{k}_diff'][1],
                        yticks[f'{setc}_{scode}_{site}_{k}_diff'][1]
                        ],
                    'tick_params': {
                        'axis': 'both', 'which': 'major', 'direction': 'in',
                        'labelsize': fontsize,
                        'left': False, 'bottom': False,
                        'right': False, 'top': False,
                        'labelleft': False,
                        'labelbottom': False,
                        'labelright': False, 'labeltop': False
                        },
                    'xlim': [
                        np.datetime64(dates_tHour[0]) - np.timedelta64(1, 'D'),
                        np.datetime64(dates_tHour[-1]) + np.timedelta64(1, 'D'),
                        ],
                    'xtick_params': [
                        True,
                        mdates.DateFormatter('%Y-%m-%d'),
                        mdates.DayLocator(bymonthday=(1, 16)),
                        ],
                    'label': f'{scode}_{site}_unc_{k}',
                    'projection': None,
                    'patch_alpha': 0.0,
                    }
                ]
            for n, scode in enumerate(scodes1)
            for p, site in enumerate(sites1)
            for q, k in enumerate(['chi', 'd_13C', 'd_2H'])
            if False
            },
        **{
            f'pri_{c}_{site}_{k}': [
                'line',
                [
                    dates_tObs[site][k],
                    obs_y[c][site][k]
                    - prior_obs[c][site][k][nx_tH_tObs[site][k]],
                    'o'
                    ],
                {
                    'c': colours[0], 'ls': '',
                    'ms': 4.0, 'mew': 0.0,#0.5, 'mec': '#000000',
                    'zorder': 1
                    },
                {
                    'loc_plot': [
                        (subplot_xmargin + subplot_gap) / fs[0],
                        (fs[1] - (n+p+1)*3*subplot_hgt_obs0 + subplot_gap) / fs[1],
                        (subplot_wdt_obs0 - 2*subplot_gap) / fs[0],
                        (3*subplot_hgt_obs0 - 2*subplot_gap) / fs[1]
                        ],
                    'ylim': [
                        yticks[f'{setc}_{scode}_{site}_{k}_diff'][0][0]
                        - (4-2*q)*yrange[f'{setc}_{scode}_{site}_{k}_diff'],
                        yticks[f'{setc}_{scode}_{site}_{k}_diff'][0][1]
                        + (1+2*q)*yrange[f'{setc}_{scode}_{site}_{k}_diff'],
                        ],
                    'yticks': np.arange(
                        yticks[f'{setc}_{scode}_{site}_{k}_diff'][0][0],
                        yticks[f'{setc}_{scode}_{site}_{k}_diff'][0][1]
                        + yticks[f'{setc}_{scode}_{site}_{k}_diff'][1],
                        yticks[f'{setc}_{scode}_{site}_{k}_diff'][1]
                        ),
                    'tick_params': {
                        'axis': 'both', 'which': 'major', 'direction': 'in',
                        'labelsize': fontsize,
                        'left': False, 'bottom': False,
                        'right': True, 'top': False,
                        'labelleft': False, 'labelbottom': False,
                        'labelright': True, 'labeltop': False
                        },
                    'xlim': [
                        np.datetime64(dates_tHour[0]) - np.timedelta64(1, 'D'),
                        np.datetime64(dates_tHour[-1]) + np.timedelta64(1, 'D'),
                        ],
                    'xtick_params': [
                        True,
                        mdates.DateFormatter('%Y-%m-%d'),
                        mdates.DayLocator(bymonthday=(1, 16)),
                        ],
                    'label': f'pri_{c}_{site}_{k}',
                    'projection': None,
                    'patch_alpha': 0.0,
                    },
                ]
            for n, scode in enumerate(scodes1)
            for o, c in enumerate([f'{scode}{i}' for i in [scodes2[0]]])
            for p, site in enumerate(sites1)
            for q, k in enumerate(['chi', 'd_13C', 'd_2H'])
            },
        **{
            f'pos_{c}_{site}_{k}': [
                'line',
                [
                    dates_tObs[site][k],
                    obs_y[c][site][k]
                    - post_obs[c][site][k][nx_tH_tObs[site][k]],
                    '+'
                    ],
                {
                    'c': colours[1+o], 'ls': '',
                    'ms': 5.0, 'mew': 0.5, #'mec': '#000000',
                    'zorder': (3+o)
                    },
                {
                    'loc_plot': [
                        (subplot_xmargin + subplot_gap) / fs[0],
                        (fs[1] - (n+p+1)*3*subplot_hgt_obs0 + subplot_gap) / fs[1],
                        (subplot_wdt_obs0 - 2*subplot_gap) / fs[0],
                        (3*subplot_hgt_obs0 - 2*subplot_gap) / fs[1]
                        ],
                    'ylim': [
                        yticks[f'{setc}_{scode}_{site}_{k}_diff'][0][0]
                        - (4-2*q)*yrange[f'{setc}_{scode}_{site}_{k}_diff'],
                        yticks[f'{setc}_{scode}_{site}_{k}_diff'][0][1]
                        + (1+2*q)*yrange[f'{setc}_{scode}_{site}_{k}_diff'],
                        ],
                    'yticks': np.arange(
                        yticks[f'{setc}_{scode}_{site}_{k}_diff'][0][0],
                        yticks[f'{setc}_{scode}_{site}_{k}_diff'][0][1]
                        + yticks[f'{setc}_{scode}_{site}_{k}_diff'][1],
                        yticks[f'{setc}_{scode}_{site}_{k}_diff'][1]
                        ),
                    'tick_params': {
                        'axis': 'both', 'which': 'major', 'direction': 'in',
                        'labelsize': fontsize,
                        'left': False, 'bottom': False,
                        'right': True, 'top': False,
                        'labelleft': False, 'labelbottom': False,
                        'labelright': True, 'labeltop': False
                        },
                    'xlim': [
                        np.datetime64(dates_tHour[0]) - np.timedelta64(1, 'D'),
                        np.datetime64(dates_tHour[-1]) + np.timedelta64(1, 'D'),
                        ],
                    'xtick_params': [
                        True,
                        mdates.DateFormatter('%Y-%m-%d'),
                        mdates.DayLocator(bymonthday=(1, 16)),
                        ],
                    'label': f'pos_{c}_{site}_{k}',
                    'projection': None,
                    'patch_alpha': 0.0,
                    },
                ]
            for n, scode in enumerate(scodes1)
            for o, c in enumerate([f'{scode}{i}' for i in scodes2])
            for p, site in enumerate(['hfd'])
            for q, k in enumerate(['chi', 'd_13C', 'd_2H'])
            },
        **{
            f'{scode}_{site}_vline_{i}': [
                'vline', [t], {'ls': ':', 'linewidth': 0.5, 'c': '#000000'},
                {
                    'loc_plot': [
                        (subplot_xmargin + subplot_gap) / fs[0],
                        (fs[1] - (n+p+1)*3*subplot_hgt_obs0 + subplot_gap) / fs[1],
                        (subplot_wdt_obs0 - 2*subplot_gap) / fs[0],
                        (3*subplot_hgt_obs0 - 2*subplot_gap) / fs[1]
                        ],
                    'ylim': [None, None],
                    'yticks': [],
                    'tick_params': {
                        'axis': 'both', 'which': 'major', 'direction': 'in',
                        'labelsize': fontsize,
                        'left': False, 'bottom': True,
                        'right': False, 'top': True,
                        'labelleft': False,
                        'labelbottom': (
                            True if p == len(sites1) - 1 and not i else False
                            ),
                        'labelright': False, 'labeltop': False
                        },
                    'xlim': [
                        np.datetime64(dates_tHour[0]) - np.timedelta64(1, 'D'),
                        np.datetime64(dates_tHour[-1]) + np.timedelta64(1, 'D'),
                        ],
                    'xtick_params': [
                        True,
                        mdates.DateFormatter('%Y-%m-%d'),
                        mdates.DayLocator(bymonthday=(1, 16)),
                        ],
                    'label': f'{scode}_{site}_vline_{i}',
                    'projection': None,
                    'patch_alpha': 0.0,
                    }
                ]
            for n, scode in enumerate(scodes1)
            for p, site in enumerate(sites1)
            for i, t in enumerate(pd.to_datetime(rdates)[1:])
            },
        **{
            f'{scode}_{site}_hline_{k}_{r}': [
                'hline', [-NCG[k] if r == 0 else 0. if r == 1 else NCG[k]],
                {'ls': ':' if r != 1 else '-', 'linewidth': 1.5, 'c': '#000000'},
                {
                    'loc_plot': [
                        (subplot_xmargin + subplot_gap) / fs[0],
                        (fs[1] - (n+p+1)*3*subplot_hgt_obs0 + subplot_gap) / fs[1],
                        (subplot_wdt_obs0 - 2*subplot_gap) / fs[0],
                        (3*subplot_hgt_obs0 - 2*subplot_gap) / fs[1]
                        ],
                    'ylim': [
                        yticks[f'{setc}_{scode}_{site}_{k}_diff'][0][0]
                        - (4-2*q)*yrange[f'{setc}_{scode}_{site}_{k}_diff'],
                        yticks[f'{setc}_{scode}_{site}_{k}_diff'][0][-1]
                        + (1+2*q)*yrange[f'{setc}_{scode}_{site}_{k}_diff'],
                        ],
                    'yticks': [
                        yticks[f'{setc}_{scode}_{site}_{k}_diff'][0][0],
                        yticks[f'{setc}_{scode}_{site}_{k}_diff'][0][1]
                        + yticks[f'{setc}_{scode}_{site}_{k}_diff'][1],
                        yticks[f'{setc}_{scode}_{site}_{k}_diff'][1]
                        ],
                    'tick_params': {
                        'axis': 'both', 'which': 'major', 'direction': 'in',
                        'labelsize': fontsize,
                        'left': False, 'bottom': False,
                        'right': False, 'top': False,
                        'labelleft': False,
                        'labelbottom': False,
                        'labelright': False, 'labeltop': False
                        },
                    'xlim': [
                        np.datetime64(dates_tHour[0]) - np.timedelta64(1, 'D'),
                        np.datetime64(dates_tHour[-1]) + np.timedelta64(1, 'D'),
                        ],
                    'xtick_params': [
                        True,
                        mdates.DateFormatter('%Y-%m-%d'),
                        mdates.DayLocator(bymonthday=(1, 16)),
                        ],
                    'label': f'{scode}_{site}_hline_{k}_{r}',
                    'projection': None,
                    'patch_alpha': 0.0,
                    }
                ]
            for n, scode in enumerate(scodes1)
            for p, site in enumerate(sites1)
            for q, k in enumerate(['chi', 'd_13C', 'd_2H'])
            for r in range(1, 2)
            },
        },
    texts=[
        *[
            {
                'x': subplot_gap / fs[0],
                'y': (
                    fs[1]
                    - (n+p)*3*subplot_hgt_obs0
                    - (3*subplot_hgt_obs0 - 2*subplot_gap)/3*(q+0.5)
                    - subplot_gap
                    - (subplot_gap if k == 'chi' else 0)
                    ) / fs[1],
                's': y_name[k],
                'ha': 'left',
                'va': 'center', 'size': fontsize, 'rotation': 90
                }
            for n, scode in enumerate(scodes1)
            for o, c in enumerate([f'{scode}{i}' for i in scodes2])
            for p, site in enumerate(sites1)
            for q, k in enumerate(['chi', 'd_13C', 'd_2H'])
            ],
        *[
            {
                'x': (subplot_xmargin+2*subplot_gap)/fs[0],
                'y': (fs[1] - 3*p*subplot_hgt_obs0 - 2*subplot_gap) / fs[1],
                's': f'{alphabet[p]})',
                'ha': 'left', 'va': 'top', 'size': fontsize,
                'fontweight': 'bold'
                }
            for p, site in enumerate(sites1)
            ]
        ],
    legend_params=[
        [
            'obs_s4i2u0_hfd_chi', 'pri_s4i2u0_hfd_chi',
            'pos_s4i2u0_hfd_chi', 'pos_s4i3u0_hfd_chi', 'pos_s4i3u1_hfd_chi'
            ],
        ['Target', 'Prior', 'NHI', 'WHI', 'WHI_HU'],
        {
            'loc': 'upper right',
            'bbox_to_anchor': [
                (fs[0] - subplot_xmargin - subplot_gap) / fs[0],
                (fs[1] - subplot_gap) / fs[1]
                ],
            'numpoints': 1, 'fontsize': fontsize, 'ncol': 5,
            'handletextpad': 0.4
            }
        ]
    )
for label in axs['set1_obs'][0]['s4_hfd_vline_0'].get_xticklabels():
    label.set_ha("right")
    label.set_rotation(30)
figs['set1_obs'].savefig(os.path.join(odir, 'set1_obs.png'))

subplot_hgt_obs0 = 1


# Q sensitivity
yticks = {}
yrange = {}
for scode in scodes1:
    codes_sen = [f'{scode}{i}' for i in scodes2]
    for s in s_sel:
        '''QA'''
        if s < dim_sAll:
            k1 = 'QA_sce'
            k2 = 'sce_QA_sce'
            s1 = s
        else:
            k1 = 'QA_sce_total'
            k2 = 'sce_QA_sce_total'
            s1 = 0
        sk = f'{scode}_{s}_QA'
        ymin = np.nanmin(np.concatenate([
            *[prior_state[c][k1][..., s1].flatten() for c in codes_sen],
            *[post_state[c][k1][..., s1].flatten() for c in codes_sen],
            *[prior_state[c][k2][..., s1].flatten() for c in codes_sen],
            ]))
        ymax = np.nanmax(np.concatenate([
            *[prior_state[c][k1][..., s1].flatten() for c in codes_sen],
            *[post_state[c][k1][..., s1].flatten() for c in codes_sen],
            *[prior_state[c][k2][..., s1].flatten() for c in codes_sen],
            ]))
        ysf = np.floor(np.log10(np.abs([
            i if i != 0 else 1 for i in [ymin, ymax]
            ]))).max() - 1
        ysf = 1 if not ysf and k not in ['d_13C'] else ysf
        yticks[sk] = aticks(ymin, ymax, 3, 5, ysf, 1)
        yrange[sk] = yticks[sk][0][1] - yticks[sk][0][0]
        if not s < dim_sAll:
            continue
        '''dQ'''
        for k in ['dQi1', 'dQi2']:
            k1 = f'v_{k}'
            sk = f'{scode}_{s}_{k}'
            ymin = np.nanmin(np.concatenate([
                *[prior_state[c][k1][..., s].flatten() for c in codes_sen],
                *[post_state[c][k1][..., s].flatten() for c in codes_sen],
                *[prior_state[c][k1][..., s].flatten() for c in codes_sen],
                ]))
            ymax = np.nanmax(np.concatenate([
                *[prior_state[c][k1][..., s].flatten() for c in codes_sen],
                *[post_state[c][k1][..., s].flatten() for c in codes_sen],
                *[prior_state[c][k1][..., s].flatten() for c in codes_sen],
                ]))
            ysf = np.floor(np.log10(np.abs([
                i if i != 0 else 1 for i in [ymin, ymax]
                ]))).max() - 1
            ysf = 1 if not ysf else ysf
            yticks[sk] = aticks(ymin, ymax, 3, 5, ysf, 1)
            yrange[sk] = yticks[sk][0][1] - yticks[sk][0][0]
dates_plot = [
    pd.to_datetime(rdates)
    + pd.to_timedelta(
        (
            pd.to_datetime(rdates).days_in_month * (i+1) / (len(scodes2)+2)
            ),
        'D'
        )
    for i in range(len(scodes2)+2)
    ]

fs = (
        1*subplot_wdt_obs0 + 1*subplot_xmargin,
        5*subplot_hgt_obs0 + 0.5
        )
figs['set1_state_Qgbie'] = plt.figure(figsize=fs, dpi=300)

axs['set1_state_Qgbie'] = {}
figs['set1_state_Qgbie'].clf()
axs['set1_state_Qgbie'][0] = name_qch4_couple.plot.generic2(
    fig=figs['set1_state_Qgbie'],
    idata={
        **{
            f'{c}_{th}_{s}': [
                'err',
                [],
                {
                    'x': dates_plot[o+1 if th != 'sce' else 0],
                    'y': (post_state if th == 'pos' else prior_state)[c][
                        ('QA_sce' if s < dim_sAll else 'QA_sce_total')
                        if th != 'sce' else
                        ('sce_QA_sce' if s < dim_sAll else 'sce_QA_sce_total')
                        ][
                            ...,
                            1 if ((s < dim_sAll) and (th != 'sce')) else
                            1 if s >= dim_sAll else
                            0,
                            s if s < dim_sAll else 0
                            ],
                    'yerr': np.diff(
                        (post_state if th == 'pos' else prior_state)[c][
                            ('QA_sce' if s < dim_sAll else 'QA_sce_total')
                            if th != 'sce' else
                            ('sce_QA_sce' if s < dim_sAll else 'sce_QA_sce_total')
                            ], axis=1
                        )[
                            ...,
                            s if s < dim_sAll else 0
                            ].T
                        if th != 'sce' else None,
                    'c': (
                        colours[0 if th == 'pri' else 1+o]
                        if th != 'sce' else
                        '#ffffff'
                        ),
                    'ls': '', 'marker': 'o',
                    **({
                        'markeredgecolor': '#000000',
                        'markeredgewidth': 0.5
                        } if th == 'sce' else {}),
                    'markersize': 5.0, 'elinewidth': 1.5,
                    'capsize': 4.0, 'capthick': 1.5,
                    'zorder': (
                        1 if th == 'pri' else
                        2 if th == 'sce' else
                        3+o
                        )
                    },
                {
                    'loc_plot': [
                        (subplot_xmargin + subplot_gap) / fs[0],
                        (fs[1] - (p+1)*subplot_hgt_obs0 + subplot_gap) / fs[1],
                        (subplot_wdt_obs0 - 2*subplot_gap) / fs[0],
                        (subplot_hgt_obs0 - 2*subplot_gap) / fs[1]
                        ],
                    'ylim': [
                        yticks[f'{scode}_{s}_QA'][0][0]
                        if yticks[f'{scode}_{s}_QA'][0][0] != 0 else
                        -yrange[f'{scode}_{s}_QA']/5,
                        yticks[f'{scode}_{s}_QA'][0][1]
                        if p != 0 else
                        yticks[f'{scode}_{s}_QA'][0][1] + yrange[f'{scode}_{s}_QA']/3
                        ],
                    'yticks': np.arange(
                        yticks[f'{scode}_{s}_QA'][0][0],
                        yticks[f'{scode}_{s}_QA'][0][1]
                        + yticks[f'{scode}_{s}_QA'][1],
                        yticks[f'{scode}_{s}_QA'][1]
                        ),
                    'tick_params': {
                        'axis': 'both', 'which': 'major', 'direction': 'in',
                        'labelsize': fontsize,
                        'left': True if th == 'sce' else False,
                        'bottom': True if th == 'sce' else False,
                        'right': True if th == 'sce' else False,
                        'top': False,
                        'labelleft': True if th == 'sce' else False,
                        'labelbottom': False,
                        'labelright': False, 'labeltop': False
                        },
                    'xlim': [
                        np.datetime64(dates_tHour[0]) - np.timedelta64(1, 'D'),
                        np.datetime64(dates_tHour[-1]) + np.timedelta64(1, 'D'),
                        ],
                    'xtick_params': [
                        True,
                        mdates.DateFormatter('%Y-%m-%d'),
                        mdates.DayLocator(bymonthday=(1)),
                        ],
                    'label': f'{c}_{th}_{s}',
                    'projection': None,
                    'patch_alpha': 0.0,
                    }
                ]
            for n, scode in enumerate(scodes1)
            for o, c in enumerate([f'{scode}{i}' for i in scodes2])
            for p, s in enumerate(s_sel)
            for th in ['pri', 'pos', 'sce']
            if c.endswith(scodes2[0]) or (th != 'sce')
            },
        **{
            f'{scode}_{p}_vline_{i}': [
                'vline', [t], {'ls': ':', 'linewidth': 0.5, 'c': '#000000'},
                {
                    'loc_plot': [
                        (subplot_xmargin + subplot_gap) / fs[0],
                        (fs[1] - (p+1)*subplot_hgt_obs0 + subplot_gap) / fs[1],
                        (subplot_wdt_obs0 - 2*subplot_gap) / fs[0],
                        (subplot_hgt_obs0 - 2*subplot_gap) / fs[1]
                        ],
                    'ylim': [None, None],
                    'yticks': [],
                    'tick_params': {
                        'axis': 'both', 'which': 'major', 'direction': 'in',
                        'labelsize': fontsize,
                        'left': False, 'bottom': True,
                        'right': False, 'top': True,
                        'labelleft': False,
                        'labelbottom': True if p == 4 and not i else False,
                        'labelright': False, 'labeltop': False
                        },
                    'xlim': [
                        np.datetime64(dates_tHour[0]) - np.timedelta64(1, 'D'),
                        np.datetime64(dates_tHour[-1]) + np.timedelta64(1, 'D'),
                        ],
                    'xtick_params': [
                        True,
                        mdates.DateFormatter('%Y-%m-%d'),
                        mdates.DayLocator(bymonthday=(1)),
                        ],
                    'label': f'{scode}_vline_{i}',
                    'projection': None,
                    'patch_alpha': 0.0,
                    }
                ]
            for n, scode in enumerate(scodes1)
            for p, s in enumerate(s_sel)
            for i, t in enumerate(pd.to_datetime(rdates)[1:])
            },
        },
    texts=[
        *[
            {
                'x': subplot_gap / fs[0],
                'y': (fs[1] - (p+0.5)*subplot_hgt_obs0 + subplot_gap) / fs[1],
                's': f'{sector_names[s][0]} (Gg yr$^{-1}$)',
                'ha': 'left', 'va': 'center', 'size': fontsize, 'rotation': 90
                }
            for p, s in enumerate(s_sel)
            ],
        ],
    legend_params=[
        [
            's4i2u0_sce_0', 's4i2u0_pri_0',
            's4i2u0_pos_0', 's4i3u0_pos_0', f's4i3u1_pos_0'
            ],
        ['Target', 'Prior', 'NHI', 'WHI', 'WHI_HU'],
        {
            'loc': 'upper right',
            'bbox_to_anchor': [
                (fs[0] - (subplot_gap)) / fs[0],
                (fs[1] - (subplot_gap)) / fs[1]
                ],
            'numpoints': 1, 'fontsize': fontsize, 'ncol': 5,
            'handletextpad': 0.2, 'columnspacing': 1.0,
            'borderpad': 0.2, 'borderaxespad': 0.2
            }
        ]
    )
for label in axs['set1_state_Qgbie'][0]['s4_4_vline_0'].get_xticklabels():
    label.set_ha("right")
    label.set_rotation(30)
figs['set1_state_Qgbie'].savefig(os.path.join(odir, 'set1_state_Qgbie.png'))


subplot_xmargin = 0.3
fs = (
        2*subplot_wdt_obs1 + 4*subplot_xmargin,
        4*subplot_hgt_obs1 + 0.5
        )
figs['set1_state_dQ'] = plt.figure(figsize=fs, dpi=300)
axs['set1_state_dQ'] = {}
figs['set1_state_dQ'].clf()
axs['set1_state_dQ'][0] = name_qch4_couple.plot.generic2(
    fig=figs['set1_state_dQ'],
    idata={
        **{
            f'{c}_{th}_{s}_{k}': [
                'err',
                [],
                (
                    {
                        'x': np.arange(1, 1 + len(rdates)),
                        'y': (post_state if th == 'pos' else prior_state)[c][
                            ('QA_sce' if s < dim_sAll else 'QA_sce_total')
                            if th != 'sce' else
                            ('sce_QA_sce' if s < dim_sAll else 'sce_QA_sce_total')
                            ][
                                ...,
                                1 if ((s < dim_sAll) and (th != 'sce')) else
                                1 if s >= dim_sAll else
                                0,
                                s if s < dim_sAll else 0
                                ],
                        'yerr': np.diff(
                            (post_state if th == 'pos' else prior_state)[c][
                                ('QA_sce' if s < dim_sAll else 'QA_sce_total')
                                if th != 'sce' else
                                ('sce_QA_sce' if s < dim_sAll else 'sce_QA_sce_total')
                                ], axis=1
                            )[
                                ...,
                                s if s < dim_sAll else 0
                                ].T
                            if th != 'sce' else None,
                        'c': (
                            colours[0 if th == 'pri' else 1+o]
                            if th != 'sce' else
                            '#ffffff'
                            ),
                        'ls': '', 'marker': 'o',
                        **({
                            'markeredgecolor': '#000000',
                            'markeredgewidth': 0.5
                            } if th == 'sce' else {}),
                        'markersize': 5.0, 'elinewidth': 1.5,
                        'capsize': 4.0, 'capthick': 1.5,
                        'zorder': (
                            1 if th == 'pri' else
                            2 if th == 'sce' else
                            3+o
                            )
                        }
                    if k == 'QA' else
                    {
                        'x': np.arange(1, 1 + len(rdates)),
                        'y': (post_state if th == 'pos' else prior_state)[c][
                            f'v_{k}' if th != 'sce' else f'sce_v_{k}'
                            ][..., 1, s],
                        'yerr': np.diff(
                            (post_state if th == 'pos' else prior_state)[c][
                                f'v_{k}' if th != 'sce' else f'sce_v_{k}'
                                ][..., s],
                            axis=1
                            ).T
                            if th != 'sce' else None,
                        'c': (
                            colours[0 if th == 'pri' else 1+o]
                            if th != 'sce' else
                            '#ffffff'
                            ),
                        'ls': '', 'marker': 'o',
                        **({
                            'markeredgecolor': '#000000',
                            'markeredgewidth': 0.5
                            } if th == 'sce' else {}),
                        'markersize': 5.0, 'elinewidth': 1.5,
                        'capsize': 4.0, 'capthick': 1.5,
                        'zorder': (
                            1 if th == 'pri' else
                            2 if th == 'sce' else
                            3+o
                            )
                        }
                    ),
                {
                    'loc_plot': [
                        (
                            (2*q+n+2)*subplot_xmargin
                            + (1*q+n)*subplot_wdt_obs1
                            + subplot_gap
                            ) / fs[0],
                        (fs[1] - (p+1)*subplot_hgt_obs1 + subplot_gap) / fs[1],
                        (subplot_wdt_obs1 - 2*subplot_gap) / fs[0],
                        (subplot_hgt_obs1 - 2*subplot_gap) / fs[1]
                        ],
                    'ylim': [
                        yticks[f'{scode}_{s}_{k}'][0][0]
                        if yticks[f'{scode}_{s}_{k}'][0][0] != 0 else
                        -yrange[f'{scode}_{s}_{k}']/5,
                        yticks[f'{scode}_{s}_{k}'][0][1]
                        ],
                    'yticks': np.arange(
                        yticks[f'{scode}_{s}_{k}'][0][0],
                        yticks[f'{scode}_{s}_{k}'][0][1]
                        + yticks[f'{scode}_{s}_{k}'][1],
                        yticks[f'{scode}_{s}_{k}'][1]
                        ),
                    'tick_params': {
                        'axis': 'both', 'which': 'major', 'direction': 'in',
                        'labelsize': fontsize,
                        'left': True if th == 'pri' else False,
                        'bottom': True if th == 'pri' else False,
                        'right': True if th == 'pri' else False,
                        'top': False,
                        'labelleft': True if th == 'pri' else False,
                        'labelbottom': True if p == len(s_sel)-2 else False,
                        'labelright': False, 'labeltop': False
                        },
                    'xlim': [0, 1 + len(rdates)],
                    'xtick_params': [
                        False,
                        np.arange(2 + len(rdates)),
                        ['', 'O', 'N', 'D', 'J', 'F', 'M', '']
                        ],
                    'label': f'{c}_{th}_{s}_{k}',
                    'projection': None,
                    'patch_alpha': 0.0,
                    }
                ]
            for n, scode in enumerate(scodes1)
            for o, c in enumerate([f'{scode}{i}' for i in scodes2])
            for p, s in enumerate(s_sel)
            for th in ['pri', 'pos', 'sce']
            for q, k in enumerate(['dQi1', 'dQi2'])
            if (
                    (c.endswith(scodes2[0]) or (th == 'pos'))
                    if k == 'QA' else
                    ((c.endswith(scodes2[0]) or (th == 'pos')) and s < dim_sAll)
                    )
            },
        },
    texts=[
        *[
            {
                'x': subplot_gap / fs[0],
                'y': (fs[1] - (p+0.5)*subplot_hgt_obs1 + subplot_gap) / fs[1],
                's': f'{sector_names[s][0]}',
                'ha': 'left', 'va': 'center', 'size': fontsize, 'rotation': 90
                }
            for n, scode in enumerate(scodes1[0])
            for p, s in enumerate(s_sel)
            ],
        *[
            {
                'x': (
                    (2*q)*subplot_xmargin
                    + (1*q)*subplot_wdt_obs1
                    + 3*subplot_gap
                    ) / fs[0],
                'y': (0.5 + (fs[1] - 0.5) / 2)/ fs[1],
                's': (
                    'Q (Gg yr$^{-1}$)' if k == 'QA' else
                    u'$\delta^{13}$C (\u2030)' if k == 'dQi1' else
                    u'$\delta^{2}$H (\u2030)'
                    ),
                'ha': 'left', 'va': 'center', 'size': fontsize, 'rotation': 90
                }
            for q, k in enumerate(['dQi1', 'dQi2'])
            ],
        *[
            {
                'x': (
                    (2*q+n+2)*subplot_xmargin
                    + (1*q+n)*subplot_wdt_obs1
                    + 2*subplot_gap
                    ) / fs[0],
                'y': (fs[1] - 2*subplot_gap) / fs[1],
                's': f'{alphabet[q]})',
                'ha': 'left', 'va': 'top', 'size': fontsize,
                'fontweight': 'bold',
                'bbox': {
                    'boxstyle': 'round', 'pad': 0.1,
                    'facecolor': '#ffffff', 'edgecolor': '#ffffff'
                    }
                }
            for n in range(len(scodes1))
            for q, k in enumerate(['dQi1', 'dQi2'])
            ],
        ],
    legend_params=[
        [
            f's4i2u0_sce_0_dQi1', f's4i2u0_pri_0_dQi1',
            f's4i2u0_pos_0_dQi1', f's4i3u0_pos_0_dQi1', f's4i3u1_pos_0_dQi1'
            ],
        ['Target', 'Prior', 'NHI', 'WHI', 'WHI_HU'],
        {
            'loc': 'lower center',
            'bbox_to_anchor': [
                fs[0] / 2 / fs[0],
                (subplot_gap) / fs[1]
                ],
            'numpoints': 1, 'fontsize': fontsize, 'ncol': 5,
            'handletextpad': 0.2, 'columnspacing': 1.0
            }
        ]
    )
figs['set1_state_dQ'].savefig(os.path.join(odir, 'set1_state_dQ.png'))

subplot_xmargin = 0.6


# Q sensitivity map
fk = 'set1_Qmap_panel'
fig_param['mph'] = 1.5
fig_param['mpw'] = 1
fig_param['mw'] = 2*fig_param['mpw'] + 2*fig_param['mlmargin']
fig_param['mh'] = min(2, len(scodes2))*fig_param['mph'] + 1*fig_param['mbmargin']
for s in s_sel:
    fk_s = f'{fk}_{s}'
    figs[fk_s] = plt.figure(figsize=(fig_param['mw'], fig_param['mh']), dpi=300)
    axs[fk_s] = {}

pm_sym_ticks = lambda x: np.concatenate([-x[::-1], x])
for n0, scode in enumerate(scodes1):
    for s in s_sel:
        fk_s = f'{fk}_{s}'
        figs[fk_s].clf()
    for pn in range(2+len(scodes2)):
        if pn < 3:
            c = f'{scode}{scodes2[0]}'
        else:
            c = f'{scode}{scodes2[pn-2]}'
        plotable_abs[:] = np.nan
        for n2, rdate in enumerate(rdates):
            for n3, reg in enumerate(
                    np.unique(inv_reg_maps[c][n2][reg_map_sel[c][n2]])
                    ):
                plotable_abs[n2][inv_reg_maps[c][n2] == reg] = (
                    (
                        prior_state[c]['sce_Q'][n2][n3]
                        - post_state[c]['v_Q'][n2][1][n3]
                        )
                    if pn > 1 else
                    (
                        prior_state[c]['sce_Q'][n2][n3]
                        - prior_state[c]['v_Q'][n2][n3]
                        )
                    if pn == 1 else
                    prior_state[c]['v_Q'][n2][n3]
                    )
            #plotable_abs[n2][reg_map_sel[c][n0]] *= inv_Q_dist[c][n0]
        for n4, s in enumerate(s_sel):
            if pn == 0:
                plim = {
                    0: np.power(10., [-12, -8]),
                    1: np.power(10., [-10, -8]),
                    2: np.power(10., [-10, -7]),
                    4: np.power(10., [-10, -9]),
                    5: np.power(10., [-12, -7]),
                    }[s]
                pticks = {
                    0: np.power(10., np.linspace(*np.log10(plim), 9)),
                    1: np.power(10., np.linspace(*np.log10(plim), 5)),
                    2: np.power(10., np.linspace(*np.log10(plim), 5)),
                    4: np.power(10., np.linspace(*np.log10(plim), 5)),
                    5: np.power(10., np.linspace(*np.log10(plim), 11)),
                    }[s]
                pticklabels = {
                    0: np.power(10., np.linspace(*np.log10(plim), 9)),
                    1: np.power(10., np.linspace(*np.log10(plim), 5)),
                    2: np.power(10., np.linspace(*np.log10(plim), 5)),
                    4: np.power(10., np.linspace(*np.log10(plim), 5)),
                    5: np.power(10., np.linspace(*np.log10(plim), 11)),
                    }[s]
            else:
                plim = {
                    0: np.power(10., [-9, -8]),
                    1: np.power(10., [-9, -8]),
                    2: np.power(10., [-9, -8]),
                    4: np.power(10., [-9, -8]),
                    5: np.power(10., [-9, -8]),
                    }[s]
                pticks = {
                    0: pm_sym_ticks(np.power(10., np.linspace(*np.log10(plim), 5))),
                    1: pm_sym_ticks(np.power(10., np.linspace(*np.log10(plim), 5))),
                    2: pm_sym_ticks(np.power(10., np.linspace(*np.log10(plim), 5))),
                    4: pm_sym_ticks(np.power(10., np.linspace(*np.log10(plim), 5))),
                    5: pm_sym_ticks(np.power(10., np.linspace(*np.log10(plim), 5))),
                    }[s]
                pticklabels = pm_sym_ticks(plim)
            if s < dim_sAll:
                m = np.nansum(plotable_abs[:, s], axis=0)/plotable_abs.shape[0]
            else:
                m = np.nansum(plotable_abs, axis=1).mean(0)
            if pn < 2:
                m[m == 0] = np.nan
            if np.isnan(m).all():
                continue
            fk_s = f'{fk}_{s}'
            sk = f'{c}_map_{s}'
            no_c = 0 if pn < 2 else 1
            no_r = pn if pn < 2 else (pn+2) % len(scodes2)
            axs[fk_s][sk] = name_qch4_couple.plot.geographical(
                fig=figs[fk_s],
                label=sk,
                idata=m.copy(),
                grid=grid_sel,
                texts=[
                    {  # colourbar label
                        'x': (
                            (no_c)*fig_param['mpw']
                            + (no_c)*fig_param['mlmargin']
                            + 1*fig_param['mgap']
                            ) / fig_param['mw'],
                        'y': (
                            fig_param['mh']
                            - (no_r)*fig_param['mph']
                            - 3*fig_param['mgap']
                            ) / fig_param['mh'],
                        's': f'{alphabet[pn]})',
                        'ha': 'left', 'va': 'top', 'size': fontsize,
                        'transform': figs[fk_s].transFigure,
                        'fontweight': 'bold',
                        'bbox': {
                            'boxstyle': 'round', 'pad': 0.1,
                            'facecolor': '#ffffff', 'edgecolor': '#ffffff',
                            }
                        }
                    ],
                loc_plot=[
                    (
                        (no_c)*fig_param['mpw']
                        + (no_c)*fig_param['mlmargin']
                        ) / fig_param['mw'],
                    (
                        fig_param['mh']
                        - (no_r+1)*fig_param['mph']
                        ) / fig_param['mh'],
                    fig_param['mpw']/fig_param['mw'],
                    fig_param['mph']/fig_param['mh'],
                    ],
                loc_bar=[
                    (
                        (no_c+1)*fig_param['mpw']
                        + (no_c)*fig_param['mlmargin']
                        + 1*fig_param['mgap']
                        ) / fig_param['mw'],
                    (
                        fig_param['mh']
                        - (no_r+1)*fig_param['mph']
                        + 1*fig_param['mgap']
                        ) / fig_param['mh'],
                    0.2/fig_param['mw'],
                    (
                        fig_param['mph'] - 2 * fig_param['mgap']
                        ) / fig_param['mh'],
                    ],
                bbox=[-12., 3., 45., 65],
                tick_params={'labelsize': 6, 'pad': 0.05},
                shapefiles=[
                    '/home/ec5/name_qch4_couple/shape_files/'
                    'CNTR_BN_10M_2016_4326.shp'
                    ],
                colour='RdYlBu_r' if pn else 'viridis_r',
                vlim=[*pticks[[0, -1]], pticks, pticklabels],
                extend='both',
                )
            """
            figs[fk_s].axes[-1].set_yticklabels(
                [
                    f'${{{(np.sign(x)*10):+2.0f}}}$'
                    f'$^{{{np.log10(np.abs(x)):+03.0f}}}$'
                    if x != 0. else
                    '0.0'
                    for x in np.concatenate([
                        pticklabels
                        ])
                    ]
                )
            """
    for s in s_sel:
        fk_s = f'{fk}_{s}'
        figs[fk_s].text(
            x=(fig_param['mw']-fig_param['mgap'])/fig_param['mw'],
            y=fig_param['mgap']/fig_param['mh'],
            s='Units: g m$^{-2}$ s$^{-1}$',
            fontsize=fig_param['fontsize'],
            ha='right', va='bottom'
            )
        figs[fk_s].savefig(os.path.join(odir, f'{fk_s}_{scode}.png'))

fig_param['mph'] = fig_param_def['mph']
fig_param['mpw'] = fig_param_def['mpw']
fig_param['mh'] = fig_param_def['mh']
fig_param['mw'] = fig_param_def['mw']


# =============================================================================
# set2: Future
# =============================================================================
setc = 'set2'
scodes1 = ['s5']
scodes2 = ['i3u0', 'i4u0']
sites1 = ['hfd', 'tac']
obs_y['s5i3u0']['tac']['d_13C'] = obs_y['s5i4u0']['tac']['d_13C']
obs_y['s5i3u0']['tac']['d_2H'] = obs_y['s5i4u0']['tac']['d_2H']
obs_y_err['s5i3u0']['tac']['d_13C'] = obs_y_err['s5i4u0']['tac']['d_13C']
obs_y_err['s5i3u0']['tac']['d_2H'] = obs_y_err['s5i4u0']['tac']['d_2H']

# Obs sensitivity
yticks = {}
yrange = {}
for scode in scodes1:                                                             
    codes_sen = [f'{scode}{i}' for i in scodes2]
    for site in sites1:
        for k in ['chi', 'd_13C', 'd_2H']:
            '''Target'''
            sk = f'{setc}_{scode}_{site}_{k}'
            ymin = np.nanmin(np.concatenate([
                *[obs_y[c][site][k] for c in codes_sen],
                ]))
            ymax = np.nanmax(np.concatenate([
                *[obs_y[c][site][k] for c in codes_sen],
                ]))
            ysf = np.floor(np.log10(np.abs([
                i if i != 0 else 1 for i in [ymin, ymax]
                ]))).max() - 1
            ysf = 1 if not ysf and k not in ['d_13C'] else ysf
            yticks[sk] = aticks(ymin, ymax, 3, 5, ysf, 1)
            yrange[sk] = yticks[sk][0][1] - yticks[sk][0][0]
            '''Model-Target'''
            sk = f'{setc}_{scode}_{site}_{k}_diff'
            ymin = np.nanmin(np.concatenate([
                *[- obs_y_err[c][site][k]**0.5 for c in codes_sen],
                *[
                    obs_y[c][site][k]
                    - prior_obs[c][site][k][nx_tH_tObs[site][k]]
                    for c in codes_sen
                    ],
                *[
                    obs_y[c][site][k]
                    - post_obs[c][site][k][nx_tH_tObs[site][k]]
                    for c in codes_sen
                    ],
                ]))
            ymax = np.nanmax(np.concatenate([
                *[+ obs_y_err[c][site][k]**0.5 for c in codes_sen],
                *[
                    obs_y[c][site][k]
                    - prior_obs[c][site][k][nx_tH_tObs[site][k]]
                    for c in codes_sen
                    ],
                *[
                    obs_y[c][site][k]
                    - post_obs[c][site][k][nx_tH_tObs[site][k]]
                    for c in codes_sen
                    ],
                ]))
            ysf = np.floor(np.log10(np.abs([
                i if i != 0 else 1 for i in [ymin, ymax]
                ]))).max() - 1
            ysf = 1 if not ysf and k not in ['d_13C'] else ysf
            yticks[sk] = aticks(ymin, ymax, 3, 5, ysf, 1)
            yrange[sk] = yticks[sk][0][1] - yticks[sk][0][0]
            if k == 'chi':
                yticks[sk] = [[-6.0, 4.0], 2]
                yrange[sk] = 10.0
            elif k == 'd_13C':
                yticks[sk] = [[-0.1, 0.1], 0.05]
                yrange[sk] = 0.2
            elif k == 'd_2H':
                yticks[sk] = [[-1.0, 1.0], 0.5]
                yrange[sk] = 2

subplot_hgt_obs0 = 1.5
fs = (
        subplot_wdt_obs0 + 2*subplot_xmargin,
        6*subplot_hgt_obs0 + 0.5
        )
figs['set2_obs'] = plt.figure(figsize=fs, dpi=300)
axs['set2_obs'] = {}

figs['set2_obs'].clf()
axs['set2_obs'][0] = name_qch4_couple.plot.generic2(
    fig=figs['set2_obs'],
    idata={
        **{
            f'pri_{c}_{site}_{k}': [
                'line',
                [
                    dates_tObs[site][k],
                    obs_y[c][site][k]
                    - prior_obs[c][site][k][nx_tH_tObs[site][k]],
                    'o'
                    ],
                {
                    'c': colours[0], 'ls': '',
                    'ms': 4.0, 'mew': 0.0,#0.5, 'mec': '#000000',
                    'zorder': 1
                    },
                {
                    'loc_plot': [
                        (subplot_xmargin + subplot_gap) / fs[0],
                        (fs[1] - (n+p+1)*3*subplot_hgt_obs0 + subplot_gap) / fs[1],
                        (subplot_wdt_obs0 - 2*subplot_gap) / fs[0],
                        (3*subplot_hgt_obs0 - 2*subplot_gap) / fs[1]
                        ],
                    'ylim': [
                        yticks[f'{setc}_{scode}_{site}_{k}_diff'][0][0]
                        - (4-2*q)*yrange[f'{setc}_{scode}_{site}_{k}_diff'],
                        yticks[f'{setc}_{scode}_{site}_{k}_diff'][0][1]
                        + (1+2*q)*yrange[f'{setc}_{scode}_{site}_{k}_diff'],
                        ],
                    'yticks': np.arange(
                        yticks[f'{setc}_{scode}_{site}_{k}_diff'][0][0],
                        yticks[f'{setc}_{scode}_{site}_{k}_diff'][0][1]
                        + yticks[f'{setc}_{scode}_{site}_{k}_diff'][1],
                        yticks[f'{setc}_{scode}_{site}_{k}_diff'][1]
                        ),
                    'tick_params': {
                        'axis': 'both', 'which': 'major', 'direction': 'in',
                        'labelsize': fontsize,
                        'left': False, 'bottom': False,
                        'right': True, 'top': False,
                        'labelleft': False, 'labelbottom': False,
                        'labelright': True, 'labeltop': False
                        },
                    'xlim': [
                        np.datetime64(dates_tHour[0]) - np.timedelta64(1, 'D'),
                        np.datetime64(dates_tHour[-1]) + np.timedelta64(1, 'D'),
                        ],
                    'xtick_params': [
                        True,
                        mdates.DateFormatter('%Y-%m-%d'),
                        mdates.DayLocator(bymonthday=(1, 16)),
                        ],
                    'label': f'pri_{c}_{site}_{k}',
                    'projection': None,
                    'patch_alpha': 0.0,
                    },
                ]
            for n, scode in enumerate(scodes1)
            for o, c in enumerate([f'{scode}{i}' for i in [scodes2[0]]])
            for p, site in enumerate(sites1)
            for q, k in enumerate(['chi', 'd_13C', 'd_2H'])
            },
        **{
            f'obs_{c}_{site}_{k}': [
                'err',
                [],
                {
                    'x': dates_tObs[site][k],
                    'y': obs_y[c][site][k],
                    'yerr': obs_y_err[c][site][k]**0.5,#NCG[k],
                    'marker': 'o',
                    'c': '#ffffff', 'ls': '',
                    'markersize': 4.0, 'markeredgewidth': 0.5,
                    'markeredgecolor': '#000000',
                    'elinewidth': 1.0, 'ecolor': '#aaaaaa',
                    'zorder': 2
                    },
                {
                    'loc_plot': [
                        (subplot_xmargin + subplot_gap) / fs[0],
                        (fs[1] - (n+p+1)*3*subplot_hgt_obs0 + subplot_gap) / fs[1],
                        (subplot_wdt_obs0 - 2*subplot_gap) / fs[0],
                        (3*subplot_hgt_obs0 - 2*subplot_gap) / fs[1]
                        ],
                    'ylim': [
                        yticks[f'{setc}_{scode}_{site}_{k}'][0][0]
                        - (5-2*q)*yrange[f'{setc}_{scode}_{site}_{k}'],
                        yticks[f'{setc}_{scode}_{site}_{k}'][0][1]
                        + (2*q)*yrange[f'{setc}_{scode}_{site}_{k}'],
                        ],
                    'yticks': np.arange(
                        yticks[f'{setc}_{scode}_{site}_{k}'][0][0],
                        yticks[f'{setc}_{scode}_{site}_{k}'][0][1]
                        + yticks[f'{setc}_{scode}_{site}_{k}'][1],
                        yticks[f'{setc}_{scode}_{site}_{k}'][1]
                        ),
                    'tick_params': {
                        'axis': 'both', 'which': 'major', 'direction': 'in',
                        'labelsize': fontsize,
                        'left': True, 'bottom': False,
                        'right': False, 'top': False,
                        'labelleft': True, 'labelbottom': False,
                        'labelright': False, 'labeltop': False
                        },
                    'xlim': [
                        np.datetime64(dates_tHour[0]) - np.timedelta64(1, 'D'),
                        np.datetime64(dates_tHour[-1]) + np.timedelta64(1, 'D'),
                        ],
                    'xtick_params': [
                        True,
                        mdates.DateFormatter('%Y-%m-%d'),
                        mdates.DayLocator(bymonthday=(1, 16)),
                        ],
                    'label': f'obs_{scode}_{site}_{k}',
                    'projection': None,
                    'patch_alpha': 0.0,
                    },
                ]
            for n, scode in enumerate(scodes1)
            for o, c in enumerate([f'{scode}{i}' for i in [scodes2[0]]])
            for p, site in enumerate(sites1)
            for q, k in enumerate(['chi', 'd_13C', 'd_2H'])
            },
        **{
            f'pos_{c}_{site}_{k}': [
                'line',
                [
                    dates_tObs[site][k],
                    obs_y[c][site][k]
                    - post_obs[c][site][k][nx_tH_tObs[site][k]],
                    '+'
                    ],
                {
                    'c': colours[2+o], 'ls': '',
                    'ms': 5.0, 'mew': 0.5, #'mec': '#000000',
                    'zorder': 1
                    },
                {
                    'loc_plot': [
                        (subplot_xmargin + subplot_gap) / fs[0],
                        (fs[1] - (n+p+1)*3*subplot_hgt_obs0 + subplot_gap) / fs[1],
                        (subplot_wdt_obs0 - 2*subplot_gap) / fs[0],
                        (3*subplot_hgt_obs0 - 2*subplot_gap) / fs[1]
                        ],
                    'ylim': [
                        yticks[f'{setc}_{scode}_{site}_{k}_diff'][0][0]
                        - (4-2*q)*yrange[f'{setc}_{scode}_{site}_{k}_diff'],
                        yticks[f'{setc}_{scode}_{site}_{k}_diff'][0][1]
                        + (1+2*q)*yrange[f'{setc}_{scode}_{site}_{k}_diff'],
                        ],
                    'yticks': np.arange(
                        yticks[f'{setc}_{scode}_{site}_{k}_diff'][0][0],
                        yticks[f'{setc}_{scode}_{site}_{k}_diff'][0][1]
                        + yticks[f'{setc}_{scode}_{site}_{k}_diff'][1],
                        yticks[f'{setc}_{scode}_{site}_{k}_diff'][1]
                        ),
                    'tick_params': {
                        'axis': 'both', 'which': 'major', 'direction': 'in',
                        'labelsize': fontsize,
                        'left': False, 'bottom': False,
                        'right': True, 'top': False,
                        'labelleft': False, 'labelbottom': False,
                        'labelright': True, 'labeltop': False
                        },
                    'xlim': [
                        np.datetime64(dates_tHour[0]) - np.timedelta64(1, 'D'),
                        np.datetime64(dates_tHour[-1]) + np.timedelta64(1, 'D'),
                        ],
                    'xtick_params': [
                        True,
                        mdates.DateFormatter('%Y-%m-%d'),
                        mdates.DayLocator(bymonthday=(1, 16)),
                        ],
                    'label': f'pos_{c}_{site}_{k}',
                    'projection': None,
                    'patch_alpha': 0.0,
                    },
                ]
            for n, scode in enumerate(scodes1)
            for o, c in enumerate([f'{scode}{i}' for i in scodes2])
            for p, site in enumerate(sites1)
            for q, k in enumerate(['chi', 'd_13C', 'd_2H'])
            },
        **{
            f'{scode}_{site}_hline_{k}_{r}': [
                'hline', [-NCG[k] if r == 0 else 0. if r == 1 else NCG[k]],
                {'ls': ':' if r != 1 else '-', 'linewidth': 1.5, 'c': '#000000'},
                {
                    'loc_plot': [
                        (subplot_xmargin + subplot_gap) / fs[0],
                        (fs[1] - (n+p+1)*3*subplot_hgt_obs0 + subplot_gap) / fs[1],
                        (subplot_wdt_obs0 - 2*subplot_gap) / fs[0],
                        (3*subplot_hgt_obs0 - 2*subplot_gap) / fs[1]
                        ],
                    'ylim': [
                        yticks[f'{setc}_{scode}_{site}_{k}_diff'][0][0]
                        - (4-2*q)*yrange[f'{setc}_{scode}_{site}_{k}_diff'],
                        yticks[f'{setc}_{scode}_{site}_{k}_diff'][0][-1]
                        + (1+2*q)*yrange[f'{setc}_{scode}_{site}_{k}_diff'],
                        ],
                    'yticks': [
                        yticks[f'{setc}_{scode}_{site}_{k}_diff'][0][0],
                        yticks[f'{setc}_{scode}_{site}_{k}_diff'][0][1]
                        + yticks[f'{setc}_{scode}_{site}_{k}_diff'][1],
                        yticks[f'{setc}_{scode}_{site}_{k}_diff'][1]
                        ],
                    'tick_params': {
                        'axis': 'both', 'which': 'major', 'direction': 'in',
                        'labelsize': fontsize,
                        'left': False, 'bottom': False,
                        'right': False, 'top': False,
                        'labelleft': False,
                        'labelbottom': False,
                        'labelright': False, 'labeltop': False
                        },
                    'xlim': [
                        np.datetime64(dates_tHour[0]) - np.timedelta64(1, 'D'),
                        np.datetime64(dates_tHour[-1]) + np.timedelta64(1, 'D'),
                        ],
                    'xtick_params': [
                        True,
                        mdates.DateFormatter('%Y-%m-%d'),
                        mdates.DayLocator(bymonthday=(1, 16)),
                        ],
                    'label': f'{scode}_{site}_hline_{k}_{r}',
                    'projection': None,
                    'patch_alpha': 0.0,
                    }
                ]
            for n, scode in enumerate(scodes1)
            for p, site in enumerate(sites1)
            for q, k in enumerate(['chi', 'd_13C', 'd_2H'])
            for r in range(1, 2)
            },
        **{
            '{scode}_{site}_unc_{k}': [
                'err',
                [],
                {
                    'x': dates_tObs[site][k],
                    'y': 0.,
                    'y': obs_y_err[c][site][k]**0.5,
                    'marker': 'None',
                    'c': '#ffffff', 'ls': '',
                    'markersize': 5.0, 'markeredgewidth': 0.5,
                    'markeredgecolor': '#000000',
                    'elinewidth': 5.0, 'ecolor': '#888888',
                    },
                {
                    'loc_plot': [
                        (subplot_xmargin + subplot_gap) / fs[0],
                        (fs[1] - (n+p+1)*3*subplot_hgt_obs0 + subplot_gap) / fs[1],
                        (subplot_wdt_obs0 - 2*subplot_gap) / fs[0],
                        (3*subplot_hgt_obs0 - 2*subplot_gap) / fs[1]
                        ],
                    'ylim': [
                        yticks[f'{setc}_{scode}_{site}_{k}_diff'][0][0]
                        - (4-2*q)*yrange[f'{setc}_{scode}_{site}_{k}_diff'],
                        yticks[f'{setc}_{scode}_{site}_{k}_diff'][0][-1]
                        + (1+2*q)*yrange[f'{setc}_{scode}_{site}_{k}_diff'],
                        ],
                    'yticks': [
                        yticks[f'{setc}_{scode}_{site}_{k}_diff'][0][0],
                        yticks[f'{setc}_{scode}_{site}_{k}_diff'][0][1]
                        + yticks[f'{setc}_{scode}_{site}_{k}_diff'][1],
                        yticks[f'{setc}_{scode}_{site}_{k}_diff'][1]
                        ],
                    'tick_params': {
                        'axis': 'both', 'which': 'major', 'direction': 'in',
                        'labelsize': fontsize,
                        'left': False, 'bottom': False,
                        'right': False, 'top': False,
                        'labelleft': False,
                        'labelbottom': False,
                        'labelright': False, 'labeltop': False
                        },
                    'xlim': [
                        np.datetime64(dates_tHour[0]) - np.timedelta64(1, 'D'),
                        np.datetime64(dates_tHour[-1]) + np.timedelta64(1, 'D'),
                        ],
                    'xtick_params': [
                        True,
                        mdates.DateFormatter('%Y-%m-%d'),
                        mdates.DayLocator(bymonthday=(1, 16)),
                        ],
                    'label': f'{scode}_{site}_unc_{k}',
                    'projection': None,
                    'patch_alpha': 0.0,
                    }
                ]
            for n, scode in enumerate(scodes1)
            for p, site in enumerate(sites1)
            for q, k in enumerate(['chi', 'd_13C', 'd_2H'])
            if False
            },
        **{
            f'{scode}_{site}_vline_{i}': [
                'vline', [t], {'ls': ':', 'linewidth': 0.5, 'c': '#000000'},
                {
                    'loc_plot': [
                        (subplot_xmargin + subplot_gap) / fs[0],
                        (fs[1] - (n+p+1)*3*subplot_hgt_obs0 + subplot_gap) / fs[1],
                        (subplot_wdt_obs0 - 2*subplot_gap) / fs[0],
                        (3*subplot_hgt_obs0 - 2*subplot_gap) / fs[1]
                        ],
                    'ylim': [None, None],
                    'yticks': [],
                    'tick_params': {
                        'axis': 'both', 'which': 'major', 'direction': 'in',
                        'labelsize': fontsize,
                        'left': False, 'bottom': True,
                        'right': False, 'top': True,
                        'labelleft': False,
                        'labelbottom': (
                            True if p == len(sites1) - 1 and not i else False
                            ),
                        'labelright': False, 'labeltop': False
                        },
                    'xlim': [
                        np.datetime64(dates_tHour[0]) - np.timedelta64(1, 'D'),
                        np.datetime64(dates_tHour[-1]) + np.timedelta64(1, 'D'),
                        ],
                    'xtick_params': [
                        True,
                        mdates.DateFormatter('%Y-%m-%d'),
                        mdates.DayLocator(bymonthday=(1, 16)),
                        ],
                    'label': f'{scode}_{site}_vline_{i}',
                    'projection': None,
                    'patch_alpha': 0.0,
                    }
                ]
            for n, scode in enumerate(scodes1)
            for p, site in enumerate(sites1)
            for i, t in enumerate(pd.to_datetime(rdates)[1:])
            },
        },
    texts=[
        *[
            {
                'x': subplot_gap / fs[0],
                'y': (
                    fs[1]
                    - (n+p)*3*subplot_hgt_obs0
                    - (3*subplot_hgt_obs0 - 2*subplot_gap)/3*(q+0.5)
                    - subplot_gap
                    - (subplot_gap if k == 'chi' else 0)
                    ) / fs[1],
                's': y_name[k],
                'ha': 'left',
                'va': 'center', 'size': fontsize, 'rotation': 90
                }
            for n, scode in enumerate(scodes1)
            for o, c in enumerate([f'{scode}{i}' for i in scodes2])
            for p, site in enumerate(sites1)
            for q, k in enumerate(['chi', 'd_13C', 'd_2H'])
            ],
        *[
            {
                'x': (subplot_xmargin+2*subplot_gap)/fs[0],
                'y': (fs[1] - 3*p*subplot_hgt_obs0 - 2*subplot_gap) / fs[1],
                's': f'{alphabet[p]})',
                'ha': 'left', 'va': 'top', 'size': fontsize,
                'fontweight': 'bold'
                }
            for p, site in enumerate(sites1)
            ]
        ],
    legend_params=[
        [
            'obs_s5i3u0_hfd_chi', 'pri_s5i3u0_hfd_chi',
            'pos_s5i3u0_hfd_chi', 'pos_s5i4u0_hfd_chi'
            ],
        ['Target', 'Prior', 'WHI', 'WTI'],
        {
            'loc': 'upper right',
            'bbox_to_anchor': [
                (fs[0] - subplot_xmargin - subplot_gap) / fs[0],
                (fs[1] - subplot_gap) / fs[1]
                ],
            'numpoints': 1, 'fontsize': fontsize, 'ncol': 5,
            'handletextpad': 0.4
            }
        ]
    )
for label in axs['set2_obs'][0]['s5_tac_vline_0'].get_xticklabels():
    label.set_ha("right")
    label.set_rotation(30)
figs['set2_obs'].savefig(os.path.join(odir, 'set2_obs.png'))

subplot_hgt_obs0 = 1.

# Q sensitivity
yticks = {}
yrange = {}
subplot_hgt_obs1 = 1.5
subplot_xmargin = 0.3
for scode in scodes1:
    codes_sen = [f'{scode}{i}' for i in scodes2]
    for s in s_sel:
        '''QA'''
        if s < dim_sAll:
            k1 = 'QA_sce'
            k2 = 'sce_QA_sce'
            s1 = s
        else:
            k1 = 'QA_sce_total'
            k2 = 'sce_QA_sce_total'
            s1 = 0
        sk = f'{scode}_{s}_QA'
        ymin = np.nanmin(np.concatenate([
            *[prior_state[c][k1][..., s1].flatten() for c in codes_sen],
            *[post_state[c][k1][..., s1].flatten() for c in codes_sen],
            *[prior_state[c][k2][..., s1].flatten() for c in codes_sen],
            ]))
        ymax = np.nanmax(np.concatenate([
            *[prior_state[c][k1][..., s1].flatten() for c in codes_sen],
            *[post_state[c][k1][..., s1].flatten() for c in codes_sen],
            *[prior_state[c][k2][..., s1].flatten() for c in codes_sen],
            ]))
        ysf = np.floor(np.log10(np.abs([
            i if i != 0 else 1 for i in [ymin, ymax]
            ]))).max() - 1
        ysf = 1 if not ysf and k not in ['d_13C'] else ysf
        yticks[sk] = aticks(ymin, ymax, 3, 5, ysf, 1)
        yrange[sk] = yticks[sk][0][1] - yticks[sk][0][0]
        if not s < dim_sAll:
            continue
        '''dQ'''
        for k in ['dQi1', 'dQi2']:
            k1 = f'v_{k}'
            sk = f'{scode}_{s}_{k}'
            ymin = np.nanmin(np.concatenate([
                *[prior_state[c][k1][..., s].flatten() for c in codes_sen],
                *[post_state[c][k1][..., s].flatten() for c in codes_sen],
                *[prior_state[c][k1][..., s].flatten() for c in codes_sen],
                ]))
            ymax = np.nanmax(np.concatenate([
                *[prior_state[c][k1][..., s].flatten() for c in codes_sen],
                *[post_state[c][k1][..., s].flatten() for c in codes_sen],
                *[prior_state[c][k1][..., s].flatten() for c in codes_sen],
                ]))
            ysf = np.floor(np.log10(np.abs([
                i if i != 0 else 1 for i in [ymin, ymax]
                ]))).max() - 1
            ysf = 1 if not ysf else ysf
            yticks[sk] = aticks(ymin, ymax, 3, 5, ysf, 1)
            yrange[sk] = yticks[sk][0][1] - yticks[sk][0][0]
fs = (
        3*subplot_wdt_obs1 + 6*subplot_xmargin,
        5*subplot_hgt_obs1 + 0.5
        )
figs['set2_state'] = plt.figure(figsize=fs, dpi=300)
axs['set2_state'] = {}
figs['set2_state'].clf()
axs['set2_state'][0] = name_qch4_couple.plot.generic2(
    fig=figs['set2_state'],
    idata={
        **{
            f'{c}_{th}_{s}_{k}': [
                'err',
                [],
                (
                    {
                        'x': np.arange(1, 1 + len(rdates)),
                        'y': (post_state if th == 'pos' else prior_state)[c][
                            ('QA_sce' if s < dim_sAll else 'QA_sce_total')
                            if th != 'sce' else
                            ('sce_QA_sce' if s < dim_sAll else 'sce_QA_sce_total')
                            ][
                                ...,
                                1 if ((s < dim_sAll) and (th != 'sce')) else
                                1 if s >= dim_sAll else
                                0,
                                s if s < dim_sAll else 0
                                ],
                        'yerr': np.diff(
                            (post_state if th == 'pos' else prior_state)[c][
                                ('QA_sce' if s < dim_sAll else 'QA_sce_total')
                                if th != 'sce' else
                                ('sce_QA_sce' if s < dim_sAll else 'sce_QA_sce_total')
                                ], axis=1
                            )[
                                ...,
                                s if s < dim_sAll else 0
                                ].T
                            if th != 'sce' else None,
                        'c': colours[
                            0 if th == 'pri' else
                            1 if th == 'sce' else
                            2+o
                            ],
                        'ls': '', 'marker': 'o',
                        'markersize': 5.0, 'elinewidth': 1.5,
                        'capsize': 4.0, 'capthick': 1.5,
                        'zorder': (
                            1 if th == 'pri' else
                            2 if th == 'sce' else
                            3+o
                            )
                        }
                    if k == 'QA' else
                    {
                        'x': np.arange(1, 1 + len(rdates)),
                        'y': (post_state if th == 'pos' else prior_state)[c][
                            f'v_{k}' if th != 'sce' else f'sce_v_{k}'
                            ][..., 1, s],
                        'yerr': np.diff(
                            (post_state if th == 'pos' else prior_state)[c][
                                f'v_{k}' if th != 'sce' else f'sce_v_{k}'
                                ][..., s],
                            axis=1
                            ).T
                            if th != 'sce' else None,
                        'c': colours[
                            0 if th == 'pri' else
                            1 if th == 'sce' else
                            2+o
                            ],
                        'ls': '', 'marker': 'o',
                        'markersize': 5.0, 'elinewidth': 1.5,
                        'capsize': 4.0, 'capthick': 1.5,
                        'zorder': (
                            1 if th == 'pri' else
                            2 if th == 'sce' else
                            3+o
                            )
                        }
                    ),
                {
                    'loc_plot': [
                        (
                            (2*q+n+2)*subplot_xmargin
                            + (1*q+n)*subplot_wdt_obs1
                            + subplot_gap
                            ) / fs[0],
                        (fs[1] - (p+1)*subplot_hgt_obs1 + subplot_gap) / fs[1],
                        (subplot_wdt_obs1 - 2*subplot_gap) / fs[0],
                        (subplot_hgt_obs1 - 2*subplot_gap) / fs[1]
                        ],
                    'ylim': [
                        yticks[f'{scode}_{s}_{k}'][0][0]
                        if yticks[f'{scode}_{s}_{k}'][0][0] != 0 else
                        -yrange[f'{scode}_{s}_{k}']/5,
                        yticks[f'{scode}_{s}_{k}'][0][1]
                        ],
                    'yticks': np.arange(
                        yticks[f'{scode}_{s}_{k}'][0][0],
                        yticks[f'{scode}_{s}_{k}'][0][1]
                        + yticks[f'{scode}_{s}_{k}'][1],
                        yticks[f'{scode}_{s}_{k}'][1]
                        ),
                    'tick_params': {
                        'axis': 'both', 'which': 'major', 'direction': 'in',
                        'labelsize': fontsize,
                        'left': True if th == 'pri' else False,
                        'bottom': True if th == 'pri' else False,
                        'right': True if th == 'pri' else False,
                        'top': False,
                        'labelleft': True if th == 'pri' else False,
                        'labelbottom': (
                            True if (p == len(s_sel)-1 and k == 'QA') else
                            True if (p == len(s_sel)-2 and k in ['dQi1', 'dQi2']) else
                            False
                            ),
                        'labelright': False, 'labeltop': False
                        },
                    'xlim': [0, 1 + len(rdates)],
                    'xtick_params': [
                        False,
                        np.arange(2 + len(rdates)),
                        ['', 'O', 'N', 'D', 'J', 'F', 'M', '']
                        ],
                    'label': f'{c}_{th}_{s}_{k}',
                    'projection': None,
                    'patch_alpha': 0.0,
                    }
                ]
            for n, scode in enumerate(scodes1)
            for o, c in enumerate([f'{scode}{i}' for i in scodes2])
            for p, s in enumerate(s_sel)
            for th in ['pri', 'pos', 'sce']
            for q, k in enumerate(['QA', 'dQi1', 'dQi2'])
            if (
                    (c.endswith(scodes2[0]) or (th == 'pos'))
                    if k == 'QA' else
                    ((c.endswith(scodes2[0]) or (th == 'pos')) and s < dim_sAll)
                    )
            },
        },
    texts=[
        *[
            {
                'x': subplot_gap / fs[0],
                'y': (fs[1] - (p+0.5)*subplot_hgt_obs1 + subplot_gap) / fs[1],
                's': f'{sector_names[s][0]}',
                'ha': 'left', 'va': 'center', 'size': fontsize, 'rotation': 90
                }
            for n, scode in enumerate(scodes1[0])
            for p, s in enumerate(s_sel)
            ],
        *[
            {
                'x': (
                    (2*q)*subplot_xmargin
                    + (1*q)*subplot_wdt_obs1
                    + 3*subplot_gap
                    ) / fs[0],
                'y': (0.5 + (fs[1] - 0.5) / 2)/ fs[1],
                's': (
                    'Q (Gg yr$^{-1}$)' if k == 'QA' else
                    u'$\delta^{13}$C (\u2030)' if k == 'dQi1' else
                    u'$\delta^{2}$H (\u2030)'
                    ),
                'ha': 'left', 'va': 'center', 'size': fontsize, 'rotation': 90
                }
            for q, k in enumerate(['QA', 'dQi1', 'dQi2'])
            ],
        *[
            {
                'x': (
                    (2*q+n+2)*subplot_xmargin
                    + (1*q+n)*subplot_wdt_obs1
                    + 2*subplot_gap
                    ) / fs[0],
                'y': (fs[1] - 2*subplot_gap) / fs[1],
                's': f'{alphabet[q]})',
                'ha': 'left', 'va': 'top', 'size': fontsize,
                'fontweight': 'bold',
                'bbox': {
                    'boxstyle': 'round', 'pad': 0.1,
                    'facecolor': '#ffffff', 'edgecolor': '#ffffff'
                    }
                }
            for n in range(len(scodes1))
            for q, k in enumerate(['QA', 'dQi1', 'dQi2'])
            ],
        ],
    legend_params=[
        [
            f's5i3u0_sce_0_QA', f's5i3u0_pri_0_QA',
            f's5i3u0_pos_0_QA', f's5i4u0_pos_0_QA'
            ],
        ['Target', 'Prior', 'WHI', 'WTI'],
        {
            'loc': 'lower center',
            'bbox_to_anchor': [
                fs[0] / 2 / fs[0],
                (subplot_gap) / fs[1]
                ],
            'numpoints': 1, 'fontsize': fontsize, 'ncol': 4,
            'handletextpad': 0.2, 'columnspacing': 1.0
            }
        ]
    )
figs['set2_state'].savefig(os.path.join(odir, 'set2_state.png'))

subplot_xmargin = 0.6


# Q sensitivity map
fk = 'set2_Qmap_panel'
fig_param['mph'] = 1.5
fig_param['mpw'] = 1
fig_param['mw'] = 2*fig_param['mpw'] + 2*fig_param['mlmargin']
fig_param['mh'] = min(2, len(scodes2))*fig_param['mph'] + 1*fig_param['mbmargin']
for s in s_sel:
    fk_s = f'{fk}_{s}'
    figs[fk_s] = plt.figure(figsize=(fig_param['mw'], fig_param['mh']), dpi=300)
    axs[fk_s] = {}

pm_sym_ticks = lambda x: np.concatenate([-x[::-1], x])
for n0, scode in enumerate(scodes1):
    for s in s_sel:
        fk_s = f'{fk}_{s}'
        figs[fk_s].clf()
    for pn in range(2+len(scodes2)):
        if pn < 3:
            c = f'{scode}{scodes2[0]}'
        else:
            c = f'{scode}{scodes2[pn-2]}'
        plotable_abs[:] = np.nan
        for n2, rdate in enumerate(rdates):
            for n3, reg in enumerate(
                    np.unique(inv_reg_maps[c][n2][reg_map_sel[c][n2]])
                    ):
                plotable_abs[n2][inv_reg_maps[c][n2] == reg] = (
                    (
                        prior_state[c]['sce_Q'][n2][n3]
                        - post_state[c]['v_Q'][n2][1][n3]
                        )
                    if pn > 1 else
                    (
                        prior_state[c]['sce_Q'][n2][n3]
                        - prior_state[c]['v_Q'][n2][n3]
                        )
                    if pn == 1 else
                    prior_state[c]['v_Q'][n2][n3]
                    )
            #plotable_abs[n2][reg_map_sel[c][n0]] *= inv_Q_dist[c][n0]
        for n4, s in enumerate(s_sel):
            if pn == 0:
                plim = {
                    0: np.power(10., [-12, -8]),
                    1: np.power(10., [-10, -8]),
                    2: np.power(10., [-10, -7]),
                    4: np.power(10., [-10, -9]),
                    5: np.power(10., [-12, -7]),
                    }[s]
                pticks = {
                    0: np.power(10., np.linspace(*np.log10(plim), 9)),
                    1: np.power(10., np.linspace(*np.log10(plim), 5)),
                    2: np.power(10., np.linspace(*np.log10(plim), 5)),
                    4: np.power(10., np.linspace(*np.log10(plim), 5)),
                    5: np.power(10., np.linspace(*np.log10(plim), 11)),
                    }[s]
                pticklabels = {
                    0: np.power(10., np.linspace(*np.log10(plim), 9)),
                    1: np.power(10., np.linspace(*np.log10(plim), 5)),
                    2: np.power(10., np.linspace(*np.log10(plim), 5)),
                    4: np.power(10., np.linspace(*np.log10(plim), 5)),
                    5: np.power(10., np.linspace(*np.log10(plim), 11)),
                    }[s]
            else:
                plim = {
                    0: np.power(10., [-9, -8]),
                    1: np.power(10., [-9, -8]),
                    2: np.power(10., [-9, -8]),
                    4: np.power(10., [-9, -8]),
                    5: np.power(10., [-9, -8]),
                    }[s]
                pticks = {
                    0: pm_sym_ticks(np.power(10., np.linspace(*np.log10(plim), 5))),
                    1: pm_sym_ticks(np.power(10., np.linspace(*np.log10(plim), 5))),
                    2: pm_sym_ticks(np.power(10., np.linspace(*np.log10(plim), 5))),
                    4: pm_sym_ticks(np.power(10., np.linspace(*np.log10(plim), 5))),
                    5: pm_sym_ticks(np.power(10., np.linspace(*np.log10(plim), 5))),
                    }[s]
                pticklabels = pm_sym_ticks(plim)
            if s < dim_sAll:
                m = np.nansum(plotable_abs[:, s], axis=0)/plotable_abs.shape[0]
            else:
                m = np.nansum(plotable_abs, axis=1).mean(0)
            if pn < 2:
                m[m == 0] = np.nan
            if np.isnan(m).all():
                continue
            fk_s = f'{fk}_{s}'
            sk = f'{c}_map_{s}'
            no_c = 0 if pn < 2 else 1
            no_r = pn if pn < 2 else (pn+2) % len(scodes2)
            axs[fk_s][sk] = name_qch4_couple.plot.geographical(
                fig=figs[fk_s],
                label=sk,
                idata=m.copy(),
                grid=grid_sel,
                texts=[
                    {  # colourbar label
                        'x': (
                            (no_c)*fig_param['mpw']
                            + (no_c)*fig_param['mlmargin']
                            + 1*fig_param['mgap']
                            ) / fig_param['mw'],
                        'y': (
                            fig_param['mh']
                            - (no_r)*fig_param['mph']
                            - 3*fig_param['mgap']
                            ) / fig_param['mh'],
                        's': f'{alphabet[pn]})',
                        'ha': 'left', 'va': 'top', 'size': fontsize,
                        'transform': figs[fk_s].transFigure,
                        'fontweight': 'bold',
                        'bbox': {
                            'boxstyle': 'round', 'pad': 0.1,
                            'facecolor': '#ffffff', 'edgecolor': '#ffffff',
                            }
                        }
                    ],
                loc_plot=[
                    (
                        (no_c)*fig_param['mpw']
                        + (no_c)*fig_param['mlmargin']
                        ) / fig_param['mw'],
                    (
                        fig_param['mh']
                        - (no_r+1)*fig_param['mph']
                        ) / fig_param['mh'],
                    fig_param['mpw']/fig_param['mw'],
                    fig_param['mph']/fig_param['mh'],
                    ],
                loc_bar=[
                    (
                        (no_c+1)*fig_param['mpw']
                        + (no_c)*fig_param['mlmargin']
                        + 1*fig_param['mgap']
                        ) / fig_param['mw'],
                    (
                        fig_param['mh']
                        - (no_r+1)*fig_param['mph']
                        + 1*fig_param['mgap']
                        ) / fig_param['mh'],
                    0.2/fig_param['mw'],
                    (
                        fig_param['mph'] - 2 * fig_param['mgap']
                        ) / fig_param['mh'],
                    ],
                bbox=[-12., 3., 45., 65],
                tick_params={'labelsize': 6, 'pad': 0.05},
                shapefiles=[
                    '/home/ec5/name_qch4_couple/shape_files/'
                    'CNTR_BN_10M_2016_4326.shp'
                    ],
                colour='RdYlBu_r' if pn else 'viridis_r',
                vlim=[*pticks[[0, -1]], pticks, pticklabels],
                extend='both',
                )
            """
            figs[fk_s].axes[-1].set_yticklabels(
                [
                    f'${{{(np.sign(x)*10):+2.0f}}}$'
                    f'$^{{{np.log10(np.abs(x)):+03.0f}}}$'
                    if x != 0. else
                    '0.0'
                    for x in np.concatenate([
                        pticklabels
                        ])
                    ]
                )
            """
    for s in s_sel:
        fk_s = f'{fk}_{s}'
        figs[fk_s].text(
            x=(fig_param['mw']-fig_param['mgap'])/fig_param['mw'],
            y=fig_param['mgap']/fig_param['mh'],
            s='Units: g m$^{-2}$ s$^{-1}$',
            fontsize=fig_param['fontsize'],
            ha='right', va='bottom'
            )
        figs[fk_s].savefig(os.path.join(odir, f'{fk_s}_{scode}.png'))

fig_param['mph'] = fig_param_def['mph']
fig_param['mpw'] = fig_param_def['mpw']
fig_param['mh'] = fig_param_def['mh']
fig_param['mw'] = fig_param_def['mw']



sys.exit()

























"""
subplot_wdt_Qmap = 2.1
fs = (
        2*subplot_wdt_Qmap + 1*subplot_xmargin,
        5*subplot_hgt_Qmap + 0.5
        )

figs['set2_state_Qmap'] = plt.figure(figsize=fs, dpi=300)
axs['set2_state_Qmap'] = {}
figs['set2_state_Qmap'].clf()
for n0, scode in enumerate(scodes1):
    ax_rel_bar = False
    for n1, c in enumerate([f'{scode}{i}' for i in scodes2]):
        plotable_rel[:] = np.nan
        for n2, rdate in enumerate(rdates):
            for n3, reg in enumerate(
                    np.unique(inv_reg_maps[c][n2][reg_map_sel[c][n2]])
                    ):
                plotable_rel[n2][inv_reg_maps[c][n2] == reg] = (
                    prior_state[c]['sce_Q'][n2][n3]
                    - post_state[c]['v_Q'][n2][1][n3]
                    ) / np.maximum(prior_state[c]['v_Q'][n2][n3], 1.e-12)
        for n4, s in enumerate(s_sel):
            if s < dim_sAll:
                m = np.nansum(plotable_rel[:, s], axis=0)/plotable_rel.shape[0]
            else:
                m = np.nansum(plotable_rel, axis=1).mean(0)
            m[m == 0] = np.nan
            if np.isnan(m).all():
                continue
            sk = f'{c}_map_{s}'
            axs['set2_state_Qmap'][sk] = name_qch4_couple.plot.geographical(
                fig=figs[f'set2_state_Qmap'],
                label=sk,
                idata=m.copy(),
                grid=grid_sel,
                texts=[
                    {  # colourbar label
                        'x': (
                            subplot_xmargin
                            + 1.0*subplot_wdt_Qmap
                            ) / fs[0],
                        'y': (subplot_gap) / fs[1],
                        's': '(Target - Posterior) / Prior',
                        'ha': 'center', 'va': 'bottom', 'size': fontsize,
                        'transform': figs[f'set2_state_Qmap'].transFigure
                        } if ax_rel_bar is False else {}

                    ],
                loc_plot=[
                    (
                        subplot_xmargin
                        + n1*subplot_wdt_Qmap
                        ) / fs[0],
                    (fs[1] - (n4+1)*subplot_hgt_Qmap) / fs[1],
                    subplot_wdt_Qmap/fs[0],
                    subplot_hgt_Qmap/fs[1],
                    ],
                loc_bar=(
                    [
                        (
                            subplot_xmargin
                            + 0.0*subplot_wdt_Qmap
                            + subplot_gap
                            ) / fs[0],
                        (0.4 - 1*subplot_gap) / fs[1],
                        (2*subplot_wdt_Qmap - 2*subplot_gap)/fs[0],
                        subplot_hgt_Qmap*0.1/fs[1],
                        ]
                    if ax_rel_bar is False else
                    False
                    ),
                bbox=[*grid_sel[0][[0, -1]], *grid_sel[1][[0, -1]]],
                tick_params={'labelsize': 6, 'pad': 0.05},
                shapefiles=[
                    '/home/ec5/name_qch4_couple/shape_files/'
                    'CNTR_BN_10M_2016_4326.shp'
                    ],
                colour='RdYlBu_r',
                vlim=[
                    ytick_rel[0][0], ytick_rel[0][-1],
                    ytick_rel[0], ytick_rel[1]
                    ],
                extend='both',
                )
            if ax_rel_bar is False:
                ax_rel_bar = figs[f'set2_state_Qmap'].axes[-1]
    if ax_rel_bar is not False:
        ax_rel_bar.set_xticklabels(
            [f'{x:3.2f}' for x in ytick_rel[1]]
            )
axs['set2_state_Qmap'][0] = name_qch4_couple.plot.generic2(
    fig=figs['set2_state_Qmap'],
    idata={},
    texts=[
        *[
            {
                'x': 0.5*subplot_xmargin / fs[0],
                'y': (fs[1] - (p+0.5)*subplot_hgt_Qmap + subplot_gap) / fs[1],
                's': f'{sector_names[s][0]}',
                'ha': 'center', 'va': 'center', 'size': fontsize, 'rotation': 90
                }
            for p, s in enumerate(s_sel)
            ],
        *[
            {
                'x': (
                    1*subplot_xmargin
                    + (i)*subplot_wdt_Qmap
                    + 2*subplot_gap
                    ) / fs[0],
                'y': (fs[1] - 2*subplot_gap) / fs[1],
                's': f'{alphabet[i]})',
                'ha': 'left', 'va': 'top', 'size': fontsize,
                'fontweight': 'bold',
                'bbox': {
                    'boxstyle': 'round', 'pad': 0.1,
                    'facecolor': '#ffffff', 'edgecolor': '#ffffff'
                    }
                }
            for i in range(len(scodes2))
            ],
        ],
    )
figs['set2_state_Qmap'].savefig(os.path.join(odir, 'set2_state_Qmap.png'))


fk = 'set2_state_Qmap_abs'
fs = (
        2*subplot_wdt_Qmap + 2.5*subplot_xmargin,
        5*subplot_hgt_Qmap
        )

figs[fk] = plt.figure(figsize=fs, dpi=300)
axs[fk] = {}

figs[fk].clf()
ax_abs_bar = [False for _ in s_sel]
for n0, scode in enumerate(scodes1):
    for n1, c in enumerate([f'{scode}{i}' for i in scodes2]):
        plotable_abs[:] = np.nan
        for n2, rdate in enumerate(rdates):
            for n3, reg in enumerate(
                    np.unique(inv_reg_maps[c][n2][reg_map_sel[c][n2]])
                    ):
                plotable_abs[n2][inv_reg_maps[c][n2] == reg] = (
                    prior_state[c]['sce_Q'][n2][n3]
                    - post_state[c]['v_Q'][n2][1][n3]
                    )
            #plotable_abs[n2][reg_map_sel[c][n0]] *= inv_Q_dist[c][n0]
        for n4, s in enumerate(s_sel):
            if s < dim_sAll:
                m = np.nansum(plotable_abs[:, s], axis=0)/plotable_abs.shape[0]
            else:
                m = np.nansum(plotable_abs, axis=1).mean(0)
            m[m == 0] = np.nan
            if np.isnan(m).all():
                continue
            sk = f'{c}_map_{s}'
            axs[fk][sk] = name_qch4_couple.plot.geographical(
                fig=figs[fk],
                label=sk,
                idata=m.copy(),
                grid=grid_sel,
                texts=[
                    {  # colourbar label
                        'x': (
                            2*subplot_xmargin
                            + 2*subplot_wdt_Qmap
                            + 1*subplot_gap
                            ) / fs[0],
                        'y': (fs[1] - (len(s_sel)/2)*subplot_hgt_Qmap) / fs[1],
                        's': 'Target - Posterior (g m$^{-2}$ s$^{-1}$)',
                        'ha': 'left', 'va': 'center', 'size': fontsize,
                        'rotation': 90,
                        'transform': figs[fk].transFigure
                        } if not any(ax_abs_bar) else {}
                    ],
                loc_plot=[
                    (
                        subplot_xmargin
                        + n1*subplot_wdt_Qmap
                        ) / fs[0],
                    (fs[1] - (n4+1)*subplot_hgt_Qmap) / fs[1],
                    subplot_wdt_Qmap/fs[0],
                    subplot_hgt_Qmap/fs[1],
                    ],
                loc_bar=(
                    [
                        (
                            subplot_xmargin
                            + 2.0*subplot_wdt_Qmap
                            + subplot_gap
                            ) / fs[0],
                        (fs[1] - (n4+1)*subplot_hgt_Qmap + subplot_gap) / fs[1],
                        subplot_wdt_Qmap*0.05/fs[0],
                        (1*subplot_hgt_Qmap - 2*subplot_gap)/fs[1],
                        ]
                    if ax_abs_bar[n4] is False else
                    False
                    ),
                bbox=[*grid_sel[0][[0, -1]], *grid_sel[1][[0, -1]]],
                tick_params={'labelsize': 6, 'pad': 0.05},
                shapefiles=[
                    '/home/ec5/name_qch4_couple/shape_files/'
                    'CNTR_BN_10M_2016_4326.shp'
                    ],
                colour='RdYlBu_r',
                vlim=(
                    [
                        -1.e-9, 1.e-9,
                        np.concatenate([
                            - np.power(10, np.linspace(-9, -12, 13)),
                            np.power(10, np.linspace(-12, -9, 13)),
                            ]),
                        np.concatenate([
                            - np.power(10, np.linspace(-9, -11, 3)),
                            np.power(10, np.linspace(-11, -9, 3)),
                            ]),
                        ]
                    if n4 == 2 else
                    [
                        -1.e-9, 1.e-9,
                        np.concatenate([
                            - np.power(10, np.linspace(-9, -12, 9)),
                            np.power(10, np.linspace(-12, -9, 9)),
                            ]),
                        np.concatenate([
                            - np.power(10, np.linspace(-9, -11, 3)),
                            np.power(10, np.linspace(-11, -9, 3)),
                            ]),
                        ]
                    if n4 == 3 else
                    [
                        -1.e-10, 1.e-10,
                        np.concatenate([
                            - np.power(10, np.linspace(-10, -12, 9)),
                            np.power(10, np.linspace(-12, -10, 9)),
                            ]),
                        np.concatenate([
                            - np.power(10, np.linspace(-10, -12, 3)),
                            np.power(10, np.linspace(-12, -10, 3)),
                            ]),
                        #ytick_abs[n4][0][0], ytick_abs[n4][0][-1],
                        #ytick_abs[n4][0], ytick_abs[n4][1]
                        ]
                    if n4 != 4 else
                    [
                        -1.e-8, 1.e-8,
                        np.concatenate([
                            - np.power(10, np.linspace(-8, -12, 9)),
                            np.power(10, np.linspace(-12, -8, 9)),
                            ]),
                        np.concatenate([
                            - np.power(10, np.linspace(-8, -12, 3)),
                            np.power(10, np.linspace(-12, -8, 3)),
                            ]),
                        ]
                    ),
                extend='both',
                )
            if ax_abs_bar[n4] is False:
                ax_abs_bar[n4] = figs[fk].axes[-1]
                ax_abs_bar[n4].set_yticklabels(
                    [
                        f'{(np.sign(float(x._text))*10):+2.0f}'
                        f'$^{{{np.log10(np.abs(float(x._text))):+03.0f}}}$'
                        if float(x._text) != 0. else
                        '0.0'
                        for x in ax_abs_bar[n4].get_yticklabels()#ytick_abs[n4][1]
                        ]
                    )
axs[fk][0] = name_qch4_couple.plot.generic2(
    fig=figs[fk],
    idata={},
    texts=[
        *[
            {
                'x': 0.5*subplot_xmargin / fs[0],
                'y': (fs[1] - (p+0.5)*subplot_hgt_Qmap + subplot_gap) / fs[1],
                's': f'{sector_names[s][0]}',
                'ha': 'center', 'va': 'center', 'size': fontsize, 'rotation': 90
                }
            for p, s in enumerate(s_sel)
            ],
        *[
            {
                'x': (
                    1*subplot_xmargin
                    + (i)*subplot_wdt_Qmap
                    + 2*subplot_gap
                    ) / fs[0],
                'y': (fs[1] - 2*subplot_gap) / fs[1],
                's': f'{alphabet[i]})',
                'ha': 'left', 'va': 'top', 'size': fontsize,
                'fontweight': 'bold',
                'bbox': {
                    'boxstyle': 'round', 'pad': 0.1,
                    'facecolor': '#ffffff', 'edgecolor': '#ffffff'
                    }
                }
            for i in range(len(scodes2))
            ],
        ],
    )
figs[fk].savefig(os.path.join(odir, f'{fk}.png'))









# Q sensitivity
s_sel = [0, 1, 2, 4, 5]
yticks = {}
yrange = {}
for scode in scodes1:
    codes_sen = [f'{scode}{i}' for i in scodes2]
    for s in s_sel:
        if s < dim_sAll:
            k1 = 'QA_sce'
            k2 = 'sce_QA_sce'
            s1 = s
        else:
            k1 = 'QA_sce_total'
            k2 = 'sce_QA_sce_total'
            s1 = 0
        sk = f'{scode}_{s}'
        ymin = np.nanmin(np.concatenate([
            *[prior_state[c][k1][..., s1].flatten() for c in codes_sen],
            *[post_state[c][k1][..., s1].flatten() for c in codes_sen],
            *[prior_state[c][k2][..., s1].flatten() for c in codes_sen],
            ]))
        ymax = np.nanmax(np.concatenate([
            *[prior_state[c][k1][..., s1].flatten() for c in codes_sen],
            *[post_state[c][k1][..., s1].flatten() for c in codes_sen],
            *[prior_state[c][k2][..., s1].flatten() for c in codes_sen],
            ]))
        ysf = np.floor(np.log10(np.abs([
            i if i != 0 else 1 for i in [ymin, ymax]
            ]))).max() - 1
        ysf = 1 if not ysf and k not in ['d_13C'] else ysf
        yticks[sk] = aticks(ymin, ymax, 3, 5, ysf, 1)
        yrange[sk] = yticks[sk][0][1] - yticks[sk][0][0]
fs = (
        6*subplot_wdt_Qgbie + 3*subplot_wdt_obs1 + 3*subplot_xmargin,
        5*subplot_hgt_Qmap + 0.5
        )
plotable_rel = np.full(
    (len(rdates), dim_sAll, grid_info['inv_nlat'], grid_info['inv_nlon']),
    np.nan
    )
grid_sel = [
    grid_vertex[0][inv_lon0:inv_lon1+1],
    grid_vertex[1][inv_lat0:inv_lat1+1],
    ]
ytick_rel = [
    np.array([
        *-np.linspace(1.2, 0., 13)[:-1],
        *np.linspace(0., 1.2, 13)[1:]
        ]),
    np.array([
        *-np.linspace(1.2, 0., 4)[:-1],
        0.,
        *np.linspace(0., 1.2, 4)[1:]
        ])
    ]

figs['set0_state'] = plt.figure(figsize=fs, dpi=300)
axs['set0_state'] = {}
figs['set0_state'].clf()
for n0, scode in enumerate(scodes1):
    ax_rel_bar = False
    for n1, c in enumerate([f'{scode}{i}' for i in scodes2]):
        plotable_rel[:] = np.nan
        for n2, rdate in enumerate(rdates):
            for n3, reg in enumerate(
                    np.unique(inv_reg_maps[c][n2][reg_map_sel[c][n2]])
                    ):
                plotable_rel[n2][inv_reg_maps[c][n2] == reg] = (
                    prior_state[c]['sce_Q'][n2][n3]
                    - post_state[c]['v_Q'][n2][1][n3]
                    ) / np.maximum(prior_state[c]['v_Q'][n2][n3], 1.e-12)
        for n4, s in enumerate(s_sel):
            if s < dim_sAll:
                m = np.nansum(plotable_rel[:, s], axis=0)/plotable_rel.shape[0]
            else:
                m = np.nansum(plotable_rel, axis=1).mean(0)
            m[m == 0] = np.nan
            if np.isnan(m).all():
                continue
            axs['set0_state'][f'map_{c}_{s}'] = name_qch4_couple.plot.geographical(
                fig=figs[f'set0_state'],
                label=f'map_{c}_{s}',
                idata=m.copy(),
                grid=grid_sel,
                texts=[
                    {  # colourbar label
                        'x': (
                            (n0+1)*subplot_xmargin
                            + (n0+1)*subplot_wdt_obs1
                            + (2*n0+1)*subplot_wdt_Qgbie
                            ) / fs[0],
                        'y': (subplot_gap) / fs[1],
                        's': '(Target - Posterior) / Prior',
                        'ha': 'center', 'va': 'bottom', 'size': fontsize,
                        'transform': figs[f'set0_state'].transFigure
                        } if ax_rel_bar is False else {}

                    ],
                loc_plot=[
                        (
                            (n0+1)*subplot_xmargin
                            + (n0+1)*subplot_wdt_obs1
                            + (2*n0+n1)*subplot_wdt_Qgbie
                            ) / fs[0],
                        (fs[1] - (n4+1)*subplot_hgt_Qmap) / fs[1],
                    subplot_wdt_Qgbie/fs[0],
                    subplot_hgt_Qmap/fs[1],
                    ],
                loc_bar=(
                    [
                        (
                            (n0+1)*subplot_xmargin
                            + (n0+1)*subplot_wdt_obs1
                            + (2*n0)*subplot_wdt_Qgbie
                            + subplot_gap
                            ) / fs[0],
                        (0.4 - 1*subplot_gap) / fs[1],
                        (2*subplot_wdt_Qgbie - 2*subplot_gap)/fs[0],
                        subplot_hgt_Qmap*0.1/fs[1],
                        ]
                    if ax_rel_bar is False else
                    False
                    ),
                bbox=[-12., 3., 45., 65],
                #bbox=[*grid_sel[0][[0, -1]], *grid_sel[1][[0, -1]]],
                tick_params={'labelsize': 6, 'pad': 0.05},
                shapefiles=[
                    '/home/ec5/name_qch4_couple/shape_files/'
                    'CNTR_BN_10M_2016_4326.shp'
                    ],
                colour='RdYlBu_r',
                vlim=[
                    ytick_rel[0][0], ytick_rel[0][-1],
                    ytick_rel[0], ytick_rel[1]
                    ],
                extend='both',
                )
            if ax_rel_bar is False:
                ax_rel_bar = figs[f'set0_state'].axes[-1]
    if ax_rel_bar is not False:
        ax_rel_bar.set_xticklabels(
            [f'{x:3.2f}' for x in ytick_rel[1]]
            )
axs['set0_state'][0] = name_qch4_couple.plot.generic2(
    fig=figs['set0_state'],
    idata={
        **{
            f'{c}_{th}_{s}': [
                'err',
                [],
                {
                    'x': np.arange(1, 1 + len(rdates)),
                    'y': (post_state if th == 'pos' else prior_state)[c][
                        ('QA_sce' if s < dim_sAll else 'QA_sce_total')
                        if th != 'sce' else
                        ('sce_QA_sce' if s < dim_sAll else 'sce_QA_sce_total')
                        ][
                            ...,
                            1 if ((s < dim_sAll) and (th != 'sce')) else
                            1 if s >= dim_sAll else
                            0,
                            s if s < dim_sAll else 0
                            ],
                    'yerr': np.diff(
                        (post_state if th == 'pos' else prior_state)[c][
                            ('QA_sce' if s < dim_sAll else 'QA_sce_total')
                            if th != 'sce' else
                            ('sce_QA_sce' if s < dim_sAll else 'sce_QA_sce_total')
                            ], axis=1
                        )[
                            ...,
                            s if s < dim_sAll else 0
                            ].T
                        if th != 'sce' else None,
                    'c': colours[
                        0 if th == 'pri' else
                        1 if th == 'sce' else
                        2+o
                        ],
                    'ls': '', 'marker': 'o',
                    'markersize': 5.0, 'elinewidth': 1.5,
                    'capsize': 4.0, 'capthick': 1.5,
                    'zorder': (
                        1 if th == 'pri' else
                        2 if th == 'sce' else
                        3+o
                        )
                    },
                {
                    'loc_plot': [
                        (
                            (n+1)*subplot_xmargin
                            + n*subplot_wdt_obs1
                            + 2*n*subplot_wdt_Qgbie
                            + subplot_gap
                            ) / fs[0],
                        (fs[1] - (p+1)*subplot_hgt_Qmap + subplot_gap) / fs[1],
                        (subplot_wdt_obs1 - 2*subplot_gap) / fs[0],
                        (subplot_hgt_Qmap - 2*subplot_gap) / fs[1]
                        ],
                    'ylim': [
                        yticks[f'{scode}_{s}'][0][0]
                        if yticks[f'{scode}_{s}'][0][0] != 0 else
                        -yrange[f'{scode}_{s}']/5,
                        yticks[f'{scode}_{s}'][0][1]
                        ],
                    'yticks': np.arange(
                        yticks[f'{scode}_{s}'][0][0],
                        yticks[f'{scode}_{s}'][0][1]
                        + yticks[f'{scode}_{s}'][1],
                        yticks[f'{scode}_{s}'][1]
                        ),
                    'tick_params': {
                        'axis': 'both', 'which': 'major', 'direction': 'in',
                        'labelsize': fontsize,
                        'left': True if th == 'pri' else False,
                        'bottom': True if th == 'pri' else False,
                        'right': True if th == 'pri' else False,
                        'top': False,
                        'labelleft': True if th == 'pri' else False,
                        'labelbottom': False,
                        'labelright': False, 'labeltop': False
                        },
                    'xlim': [0, 1 + len(rdates)],
                    'xtick_params': [
                        False,
                        np.arange(2 + len(rdates)),
                        ['', 'O', 'N', 'D', 'J', 'F', 'M', '']
                        ],
                    'label': f'{c}_{th}_{s}',
                    'projection': None,
                    'patch_alpha': 0.0,
                    }
                ]
            for n, scode in enumerate(scodes1)
            for o, c in enumerate([f'{scode}{i}' for i in scodes2])
            for p, s in enumerate(s_sel)
            for th in ['pri', 'pos', 'sce']
            if c.endswith(scodes2[0]) or (th == 'pos')
            },
        },
    texts=[
        *[
            {
                'x': (
                    n*subplot_xmargin
                    + n*subplot_wdt_obs1
                    + 2*n*subplot_wdt_Qgbie
                    + subplot_gap
                    ) / fs[0],
                'y': (fs[1] - (p+0.5)*subplot_hgt_Qmap + subplot_gap) / fs[1],
                's': f'{sector_names[s][0]} (Gg yr$^{-1}$)',
                'ha': 'left', 'va': 'center', 'size': fontsize, 'rotation': 90
                }
            for n, scode in enumerate(scodes1)
            for p, s in enumerate(s_sel)
            ],
        *[
            {
                'x': (
                    (n+1)*subplot_xmargin
                    + n*subplot_wdt_obs1
                    + 2*n*subplot_wdt_Qgbie
                    + 2*subplot_gap
                    ) / fs[0],
                'y': (fs[1] - 2*subplot_gap) / fs[1],
                's': s,
                'ha': 'left', 'va': 'top', 'size': fontsize,
                'bbox': {
                    'boxstyle': 'round', 'pad': 0.1,
                    'facecolor': '#ffffff', 'edgecolor': '#ffffff'
                    }
                }
            for n, s in enumerate(
                ['a)', 'd)', 'g)']
                )
            ],
        *[
            {
                'x': (
                    (n+1)*subplot_xmargin
                    + (n+1)*subplot_wdt_obs1
                    + 2*n*subplot_wdt_Qgbie
                    + 2*subplot_gap
                    ) / fs[0],
                'y': (fs[1] - 2*subplot_gap) / fs[1],
                's': s,
                'ha': 'left', 'va': 'top', 'size': fontsize,
                'bbox': {
                    'boxstyle': 'round', 'pad': 0.1,
                    'facecolor': '#ffffff', 'edgecolor': '#ffffff'
                    }
                }
            for n, s in enumerate(
                ['b)', 'e)', 'h)']
                )
            ],
        *[
            {
                'x': (
                    (n+1)*subplot_xmargin
                    + (n+1)*subplot_wdt_obs1
                    + (2*n+1)*subplot_wdt_Qgbie
                    + 2*subplot_gap
                    ) / fs[0],
                'y': (fs[1] - 2*subplot_gap) / fs[1],
                's': s,
                'ha': 'left', 'va': 'top', 'size': fontsize,
                'bbox': {
                    'boxstyle': 'round', 'pad': 0.1,
                    'facecolor': '#ffffff', 'edgecolor': '#ffffff'
                    }
                }
            for n, s in enumerate(
                ['c)', 'f)', 'i)']
                )
            ]
        ],
    legend_params=[
        [
            f's1i2u0_sce_0', f's1i2u0_pri_0',
            f's1i2u0_pos_0', f's1i3u0_pos_0'
            ],
        ['Target', 'Prior', 'NHI', 'WHI'],
        {
            'loc': 'lower left',
            'bbox_to_anchor': [
                (subplot_gap) / fs[0],
                (subplot_gap) / fs[1]
                ],
            'numpoints': 1, 'fontsize': fontsize, 'ncol': 2,
            'handletextpad': 0.2, 'columnspacing': 1.0
            }
        ]
    )
figs['set0_state'].savefig(os.path.join(odir, 'set0_state.png'))



# Q sensitivity
yticks = {}
yrange = {}
for scode in scodes1:
    codes_sen = [f'{scode}{i}' for i in scodes2]
    for s in s_sel:
        if s < dim_sAll:
            k1 = 'QA_sce'
            k2 = 'sce_QA_sce'
            s1 = s
        else:
            k1 = 'QA_sce_total'
            k2 = 'sce_QA_sce_total'
            s1 = 0
        sk = f'{scode}_{s}'
        ymin = np.nanmin(np.concatenate([
            *[prior_state[c][k1][..., s1].flatten() for c in codes_sen],
            *[post_state[c][k1][..., s1].flatten() for c in codes_sen],
            *[prior_state[c][k2][..., s1].flatten() for c in codes_sen],
            ]))
        ymax = np.nanmax(np.concatenate([
            *[prior_state[c][k1][..., s1].flatten() for c in codes_sen],
            *[post_state[c][k1][..., s1].flatten() for c in codes_sen],
            *[prior_state[c][k2][..., s1].flatten() for c in codes_sen],
            ]))
        ysf = np.floor(np.log10(np.abs([
            i if i != 0 else 1 for i in [ymin, ymax]
            ]))).max() - 1
        ysf = 1 if not ysf and k not in ['d_13C'] else ysf
        yticks[sk] = aticks(ymin, ymax, 3, 5, ysf, 1)
        yrange[sk] = yticks[sk][0][1] - yticks[sk][0][0]
fs = (
        2*subplot_wdt_Qmap + 1*subplot_wdt_obs1 + 1*subplot_xmargin,
        5*subplot_hgt_Qmap + 0.5
        )
figs['set2_state'] = plt.figure(figsize=fs, dpi=300)
axs['set2_state'] = {}
figs['set2_state'].clf()
for n0, scode in enumerate(scodes1):
    ax_rel_bar = False
    for n1, c in enumerate([f'{scode}{i}' for i in scodes2]):
        plotable_rel[:] = np.nan
        for n2, rdate in enumerate(rdates):
            for n3, reg in enumerate(
                    np.unique(inv_reg_maps[c][n2][reg_map_sel[c][n2]])
                    ):
                plotable_rel[n2][inv_reg_maps[c][n2] == reg] = (
                    prior_state[c]['sce_Q'][n2][n3]
                    - post_state[c]['v_Q'][n2][1][n3]
                    ) / np.maximum(prior_state[c]['v_Q'][n2][n3], 1.e-12)
        for n4, s in enumerate(s_sel):
            if s < dim_sAll:
                m = np.nansum(plotable_rel[:, s], axis=0)/plotable_rel.shape[0]
            else:
                m = np.nansum(plotable_rel, axis=1).mean(0)
            m[m == 0] = np.nan
            if np.isnan(m).all():
                continue
            axs['set2_state'][f'map_{c}_{s}'] = name_qch4_couple.plot.geographical(
                fig=figs[f'set2_state'],
                label=f'map_{c}_{s}',
                idata=m.copy(),
                grid=grid_sel,
                texts=[
                    {  # colourbar label
                        'x': (
                            (n0+1)*subplot_xmargin
                            + (n0+1)*subplot_wdt_obs1
                            + (2*n0+1)*subplot_wdt_Qmap
                            ) / fs[0],
                        'y': (subplot_gap) / fs[1],
                        's': '(Target - Posterior) / Prior',
                        'ha': 'center', 'va': 'bottom', 'size': fontsize,
                        'transform': figs[f'set2_state'].transFigure
                        } if ax_rel_bar is False else {}
                    ],
                loc_plot=[
                        (
                            (n0+1)*subplot_xmargin
                            + (n0+1)*subplot_wdt_obs1
                            + (2*n0+n1)*subplot_wdt_Qmap
                            ) / fs[0],
                        (fs[1] - (n4+1)*subplot_hgt_Qmap) / fs[1],
                    subplot_wdt_Qmap/fs[0],
                    subplot_hgt_Qmap/fs[1],
                    ],
                loc_bar=(
                    [
                        (
                            (n0+1)*subplot_xmargin
                            + (n0+1)*subplot_wdt_obs1
                            + (2*n0)*subplot_wdt_Qmap
                            + subplot_gap
                            ) / fs[0],
                        (0.4 - 1*subplot_gap) / fs[1],
                        (2*subplot_wdt_Qmap - 2*subplot_gap)/fs[0],
                        subplot_hgt_Qmap*0.1/fs[1],
                        ]
                    if ax_rel_bar is False else
                    False
                    ),
                bbox=[*grid_sel[0][[0, -1]], *grid_sel[1][[0, -1]]],
                tick_params={'labelsize': 6, 'pad': 0.05},
                shapefiles=[
                    '/home/ec5/name_qch4_couple/shape_files/'
                    'CNTR_BN_10M_2016_4326.shp'
                    ],
                colour='RdYlBu_r',
                vlim=[
                    ytick_rel[0][0], ytick_rel[0][-1],
                    ytick_rel[0], ytick_rel[1]
                    ],
                extend='both',
                )
            if ax_rel_bar is False:
                ax_rel_bar = figs[f'set2_state'].axes[-1]
    if ax_rel_bar is not False:
        ax_rel_bar.set_xticklabels(
            [f'{x:3.2f}' for x in ytick_rel[1]]
            )
axs['set2_state'][0] = name_qch4_couple.plot.generic2(
    fig=figs['set2_state'],
    idata={
        **{
            f'{c}_{th}_{s}': [
                'err',
                [],
                {
                    'x': np.arange(1, 1 + len(rdates)),
                    'y': (post_state if th == 'pos' else prior_state)[c][
                        ('QA_sce' if s < dim_sAll else 'QA_sce_total')
                        if th != 'sce' else
                        ('sce_QA_sce' if s < dim_sAll else 'sce_QA_sce_total')
                        ][
                            ...,
                            1 if ((s < dim_sAll) and (th != 'sce')) else
                            1 if s >= dim_sAll else
                            0,
                            s if s < dim_sAll else 0
                            ],
                    'yerr': np.diff(
                        (post_state if th == 'pos' else prior_state)[c][
                            ('QA_sce' if s < dim_sAll else 'QA_sce_total')
                            if th != 'sce' else
                            ('sce_QA_sce' if s < dim_sAll else 'sce_QA_sce_total')
                            ], axis=1
                        )[
                            ...,
                            s if s < dim_sAll else 0
                            ].T
                        if th != 'sce' else None,
                    'c': colours[
                        0 if th == 'pri' else
                        1 if th == 'sce' else
                        2+o
                        ],
                    'ls': '', 'marker': 'o',
                    'markersize': 5.0, 'elinewidth': 1.5,
                    'capsize': 4.0, 'capthick': 1.5,
                    'zorder': (
                        1 if th == 'pri' else
                        2 if th == 'sce' else
                        3+o
                        )
                    },
                {
                    'loc_plot': [
                        (
                            (n+1)*subplot_xmargin
                            + n*subplot_wdt_obs1
                            + 2*n*subplot_wdt_Qmap
                            + subplot_gap
                            ) / fs[0],
                        (fs[1] - (p+1)*subplot_hgt_Qmap + subplot_gap) / fs[1],
                        (subplot_wdt_obs1 - 2*subplot_gap) / fs[0],
                        (subplot_hgt_Qmap - 2*subplot_gap) / fs[1]
                        ],
                    'ylim': [
                        yticks[f'{scode}_{s}'][0][0]
                        if yticks[f'{scode}_{s}'][0][0] != 0 else
                        -yrange[f'{scode}_{s}']/5,
                        yticks[f'{scode}_{s}'][0][1]
                        ],
                    'yticks': np.arange(
                        yticks[f'{scode}_{s}'][0][0],
                        yticks[f'{scode}_{s}'][0][1]
                        + yticks[f'{scode}_{s}'][1],
                        yticks[f'{scode}_{s}'][1]
                        ),
                    'tick_params': {
                        'axis': 'both', 'which': 'major', 'direction': 'in',
                        'labelsize': fontsize,
                        'left': True if th == 'pri' else False,
                        'bottom': True if th == 'pri' else False,
                        'right': True if th == 'pri' else False,
                        'top': False,
                        'labelleft': True if th == 'pri' else False,
                        'labelbottom': False,
                        'labelright': False, 'labeltop': False
                        },
                    'xlim': [0, 1 + len(rdates)],
                    'xtick_params': [
                        False,
                        np.arange(2 + len(rdates)),
                        ['', 'O', 'N', 'D', 'J', 'F', 'M', '']
                        ],
                    'label': f'{c}_{th}_{s}',
                    'projection': None,
                    'patch_alpha': 0.0,
                    }
                ]
            for n, scode in enumerate(scodes1)
            for o, c in enumerate([f'{scode}{i}' for i in scodes2])
            for p, s in enumerate(s_sel)
            for th in ['pri', 'pos', 'sce']
            if c.endswith(scodes2[0]) or (th == 'pos')
            },
        },
    texts=[
        *[
            {
                'x': (
                    n*subplot_xmargin
                    + n*subplot_wdt_obs1
                    + 2*n*subplot_wdt_Qmap
                    + subplot_gap
                    ) / fs[0],
                'y': (fs[1] - (p+0.5)*subplot_hgt_Qmap + subplot_gap) / fs[1],
                's': f'{sector_names[s][0]} (Gg yr$^{-1}$)',
                'ha': 'left', 'va': 'center', 'size': fontsize, 'rotation': 90
                }
            for n, scode in enumerate(scodes1)
            for p, s in enumerate(s_sel)
            ],
        *[
            {
                'x': (
                    (n+1)*subplot_xmargin
                    + n*subplot_wdt_obs1
                    + 2*n*subplot_wdt_Qmap
                    + 2*subplot_gap
                    ) / fs[0],
                'y': (fs[1] - 2*subplot_gap) / fs[1],
                's': f'{alphabet[n*3]})',
                'ha': 'left', 'va': 'top', 'size': fontsize,
                'bbox': {
                    'boxstyle': 'round', 'pad': 0.1,
                    'facecolor': '#ffffff', 'edgecolor': '#ffffff'
                    }
                }
            for n in range(len(scodes1))
            ],
        *[
            {
                'x': (
                    (n+1)*subplot_xmargin
                    + (n+1)*subplot_wdt_obs1
                    + 2*n*subplot_wdt_Qmap
                    + 2*subplot_gap
                    ) / fs[0],
                'y': (fs[1] - 2*subplot_gap) / fs[1],
                's': f'{alphabet[n*3+1]})',
                'ha': 'left', 'va': 'top', 'size': fontsize,
                'bbox': {
                    'boxstyle': 'round', 'pad': 0.1,
                    'facecolor': '#ffffff', 'edgecolor': '#ffffff'
                    }
                }
            for n in range(len(scodes1))
            ],
        *[
            {
                'x': (
                    (n+1)*subplot_xmargin
                    + (n+1)*subplot_wdt_obs1
                    + (2*n+1)*subplot_wdt_Qmap
                    + 2*subplot_gap
                    ) / fs[0],
                'y': (fs[1] - 2*subplot_gap) / fs[1],
                's': f'{alphabet[n*3+2]})',
                'ha': 'left', 'va': 'top', 'size': fontsize,
                'bbox': {
                    'boxstyle': 'round', 'pad': 0.1,
                    'facecolor': '#ffffff', 'edgecolor': '#ffffff'
                    }
                }
            for n in range(len(scodes1))
            ]
        ],
    legend_params=[
        [
            f's5i3u0_sce_0', f's5i3u0_pri_0',
            f's5i3u0_pos_0', f's5i4u0_pos_0'
            ],
        ['Target', 'Prior', 'NHI', 'WHI'],
        {
            'loc': 'lower left',
            'bbox_to_anchor': [
                (subplot_gap) / fs[0],
                (subplot_gap) / fs[1]
                ],
            'numpoints': 1, 'fontsize': fontsize, 'ncol': 2,
            'handletextpad': 0.2, 'columnspacing': 1.0
            }
        ]
    )
figs['set2_state'].savefig(os.path.join(odir, 'set2_state.png'))



subplot_wdt_Qmap = 2.1
fs = (
        3*subplot_wdt_Qmap + 1*subplot_xmargin,
        5*subplot_hgt_Qmap + 0.5
        )
figs['set1_state_Qmap'] = plt.figure(figsize=fs, dpi=300)
axs['set1_state_Qmap'] = {}
figs['set1_state_Qmap'].clf()
for n0, scode in enumerate(scodes1):
    ax_rel_bar = False
    for n1, c in enumerate([f'{scode}{i}' for i in scodes2]):
        plotable_rel[:] = np.nan
        for n2, rdate in enumerate(rdates):
            for n3, reg in enumerate(
                    np.unique(inv_reg_maps[c][n2][reg_map_sel[c][n2]])
                    ):
                plotable_rel[n2][inv_reg_maps[c][n2] == reg] = (
                    prior_state[c]['sce_Q'][n2][n3]
                    - post_state[c]['v_Q'][n2][1][n3]
                    ) / np.maximum(prior_state[c]['v_Q'][n2][n3], 1.e-12)
        for n4, s in enumerate(s_sel):
            if s < dim_sAll:
                m = np.nansum(plotable_rel[:, s], axis=0)/plotable_rel.shape[0]
            else:
                m = np.nansum(plotable_rel, axis=1).mean(0)
            m[m == 0] = np.nan
            if np.isnan(m).all():
                continue
            sk = f'{c}_map_{s}'
            axs['set1_state_Qmap'][sk] = name_qch4_couple.plot.geographical(
                fig=figs[f'set1_state_Qmap'],
                label=sk,
                idata=m.copy(),
                grid=grid_sel,
                texts=[
                    {  # colourbar label
                        'x': (
                            subplot_xmargin
                            + 1.5*subplot_wdt_Qmap
                            ) / fs[0],
                        'y': (subplot_gap) / fs[1],
                        's': '(Target - Posterior) / Prior',
                        'ha': 'center', 'va': 'bottom', 'size': fontsize,
                        'transform': figs[f'set1_state_Qmap'].transFigure
                        } if ax_rel_bar is False else {}

                    ],
                loc_plot=[
                    (
                        subplot_xmargin
                        + n1*subplot_wdt_Qmap
                        ) / fs[0],
                    (fs[1] - (n4+1)*subplot_hgt_Qmap) / fs[1],
                    subplot_wdt_Qmap/fs[0],
                    subplot_hgt_Qmap/fs[1],
                    ],
                loc_bar=(
                    [
                        (
                            subplot_xmargin
                            + 0.5*subplot_wdt_Qmap
                            + subplot_gap
                            ) / fs[0],
                        (0.4 - 1*subplot_gap) / fs[1],
                        (2*subplot_wdt_Qmap - 2*subplot_gap)/fs[0],
                        subplot_hgt_Qmap*0.1/fs[1],
                        ]
                    if ax_rel_bar is False else
                    False
                    ),
                bbox=[*grid_sel[0][[0, -1]], *grid_sel[1][[0, -1]]],
                tick_params={'labelsize': 6, 'pad': 0.05},
                shapefiles=[
                    '/home/ec5/name_qch4_couple/shape_files/'
                    'CNTR_BN_10M_2016_4326.shp'
                    ],
                colour='RdYlBu_r',
                vlim=[
                    ytick_rel[0][0], ytick_rel[0][-1],
                    ytick_rel[0], ytick_rel[1]
                    ],
                extend='both',
                )
            if ax_rel_bar is False:
                ax_rel_bar = figs[f'set1_state_Qmap'].axes[-1]
    if ax_rel_bar is not False:
        ax_rel_bar.set_xticklabels(
            [f'{x:3.2f}' for x in ytick_rel[1]]
            )
axs['set1_state_Qmap'][0] = name_qch4_couple.plot.generic2(
    fig=figs['set1_state_Qmap'],
    idata={},
    texts=[
        *[
            {
                'x': 0.5*subplot_xmargin / fs[0],
                'y': (fs[1] - (p+0.5)*subplot_hgt_Qmap + subplot_gap) / fs[1],
                's': f'{sector_names[s][0]}',
                'ha': 'center', 'va': 'center', 'size': fontsize, 'rotation': 90
                }
            for p, s in enumerate(s_sel)
            ],
        *[
            {
                'x': (
                    1*subplot_xmargin
                    + (p)*subplot_wdt_Qmap
                    + 2*subplot_gap
                    ) / fs[0],
                'y': (fs[1] - 2*subplot_gap) / fs[1],
                's': s,
                'ha': 'left', 'va': 'top', 'size': fontsize,
                'fontweight': 'bold',
                'bbox': {
                    'boxstyle': 'round', 'pad': 0.1,
                    'facecolor': '#ffffff', 'edgecolor': '#ffffff'
                    }
                }
            for p, s in enumerate(['a)', 'b)', 'c)'])
            ],
        ],
    )
figs['set1_state_Qmap'].savefig(os.path.join(odir, 'set1_state_Qmap.png'))


fk = 'set1_state_Qmap_abs'
fs = (
        3*subplot_wdt_Qmap + 3*subplot_xmargin,
        5*subplot_hgt_Qmap
        )

figs[fk] = plt.figure(figsize=fs, dpi=300)
axs[fk] = {}

figs[fk].clf()
ax_abs_bar = [False for _ in s_sel]
for n0, scode in enumerate(scodes1):
    for n1, c in enumerate([f'{scode}{i}' for i in scodes2]):
        plotable_abs[:] = np.nan
        for n2, rdate in enumerate(rdates):
            for n3, reg in enumerate(
                    np.unique(inv_reg_maps[c][n2][reg_map_sel[c][n2]])
                    ):
                plotable_abs[n2][inv_reg_maps[c][n2] == reg] = (
                    prior_state[c]['sce_Q'][n2][n3]
                    - post_state[c]['v_Q'][n2][1][n3]
                    )
            #plotable_abs[n2][reg_map_sel[c][n0]] *= inv_Q_dist[c][n0]
        for n4, s in enumerate(s_sel):
            if s < dim_sAll:
                m = np.nansum(plotable_abs[:, s], axis=0)/plotable_abs.shape[0]
            else:
                m = np.nansum(plotable_abs, axis=1).mean(0)
            m[m == 0] = np.nan
            if np.isnan(m).all():
                continue
            sk = f'{c}_map_{s}'
            axs[fk][sk] = name_qch4_couple.plot.geographical(
                fig=figs[fk],
                label=sk,
                idata=m.copy(),
                grid=grid_sel,
                texts=[
                    {  # colourbar label
                        'x': (
                            2*subplot_xmargin
                            + 3*subplot_wdt_Qmap
                            + 1*subplot_gap
                            ) / fs[0],
                        'y': (fs[1] - (len(s_sel)/2)*subplot_hgt_Qmap) / fs[1],
                        's': 'Target - Posterior (g m$^{-2}$ s$^{-1}$)',
                        'ha': 'left', 'va': 'center', 'size': fontsize,
                        'rotation': 90,
                        'transform': figs[fk].transFigure
                        } if not any(ax_abs_bar) else {}
                    ],
                loc_plot=[
                    (
                        subplot_xmargin
                        + n1*subplot_wdt_Qmap
                        ) / fs[0],
                    (fs[1] - (n4+1)*subplot_hgt_Qmap) / fs[1],
                    subplot_wdt_Qmap/fs[0],
                    subplot_hgt_Qmap/fs[1],
                    ],
                loc_bar=(
                    [
                        (
                            subplot_xmargin
                            + 3.0*subplot_wdt_Qmap
                            + subplot_gap
                            ) / fs[0],
                        (fs[1] - (n4+1)*subplot_hgt_Qmap + subplot_gap) / fs[1],
                        subplot_wdt_Qmap*0.05/fs[0],
                        (1*subplot_hgt_Qmap - 2*subplot_gap)/fs[1],
                        ]
                    if ax_abs_bar[n4] is False else
                    False
                    ),
                bbox=[*grid_sel[0][[0, -1]], *grid_sel[1][[0, -1]]],
                tick_params={'labelsize': 6, 'pad': 0.05},
                shapefiles=[
                    '/home/ec5/name_qch4_couple/shape_files/'
                    'CNTR_BN_10M_2016_4326.shp'
                    ],
                colour='RdYlBu_r',
                vlim=(
                    [
                        -1.e-8, 1.e-8,
                        np.concatenate([
                            - np.power(10, np.linspace(-9, -12, 13)),
                            np.power(10, np.linspace(-12, -9, 13)),
                            ]),
                        np.concatenate([
                            - np.power(10, np.linspace(-9, -12, 4)),
                            np.power(10, np.linspace(-12, -9, 4)),
                            ]),
                        #ytick_abs[n4][0][0], ytick_abs[n4][0][-1],
                        #ytick_abs[n4][0], ytick_abs[n4][1]
                        ]
                    if n4 != 4 else
                    [
                        -1.e-8, 1.e-8,
                        np.concatenate([
                            - np.power(10, np.linspace(-8, -12, 9)),
                            np.power(10, np.linspace(-12, -8, 9)),
                            ]),
                        np.concatenate([
                            - np.power(10, np.linspace(-8, -12, 3)),
                            np.power(10, np.linspace(-12, -8, 3)),
                            ]),
                        ]
                    ),
                extend='both',
                )
            if ax_abs_bar[n4] is False:
                ax_abs_bar[n4] = figs[fk].axes[-1]
                ax_abs_bar[n4].set_yticklabels(
                    [
                        f'{(np.sign(float(x._text))*10):+2.0f}'
                        f'$^{{{np.log10(np.abs(float(x._text))):+03.0f}}}$'
                        if float(x._text) != 0. else
                        '0.0'
                        for x in ax_abs_bar[n4].get_yticklabels()#ytick_abs[n4][1]
                        ]
                    )
axs[fk][0] = name_qch4_couple.plot.generic2(
    fig=figs[fk],
    idata={},
    texts=[
        *[
            {
                'x': 0.5*subplot_xmargin / fs[0],
                'y': (fs[1] - (p+0.5)*subplot_hgt_Qmap + subplot_gap) / fs[1],
                's': f'{sector_names[s][0]}',
                'ha': 'center', 'va': 'center', 'size': fontsize, 'rotation': 90
                }
            for p, s in enumerate(s_sel)
            ],
        *[
            {
                'x': (
                    1*subplot_xmargin
                    + (i)*subplot_wdt_Qmap
                    + 2*subplot_gap
                    ) / fs[0],
                'y': (fs[1] - 2*subplot_gap) / fs[1],
                's': f'{alphabet[i]})',
                'ha': 'left', 'va': 'top', 'size': fontsize,
                'fontweight': 'bold',
                'bbox': {
                    'boxstyle': 'round', 'pad': 0.1,
                    'facecolor': '#ffffff', 'edgecolor': '#ffffff'
                    }
                }
            for i in range(len(scodes2))
            ],
        ],
    )
figs[fk].savefig(os.path.join(odir, f'{fk}.png'))





site_seq = ['hfd']  # ['hfd', 'tac']
obs_seq = ['chi', 'd_13C', 'd_2H']
site_obs_seq = [
    f'{site}_{obs}' for site in site_seq for obs in obs_seq
    if site in sites and obs in sites[site]
    ]
figsize={
    'obs': [
        subplot_wdt_obs0+subplot_wdt_obs1+1,
        subplot_hgt_obs0*len(site_obs_seq)+0.5
        ],
    **{
        f'QA_{rgroup}': [subplot_wdt_QA+0.5, subplot_hgt_QA*6+0.5]
        for rgroup in focus_rgroup_uniq[0]
        },
    'dQi1': [subplot_wdt_QA+0.5, subplot_hgt_QA*5+0.5],
    'dQi2': [subplot_wdt_QA+0.5, subplot_hgt_QA*5+0.5],
    **{
        f'dchi_{site}': [
            subplot_wdt_obs0+1,
            subplot_hgt_obs0*(
                3  # base
                + (1 if same_prior else len(idirs))  # pri
                + (1 if same_prior and sce else len(idirs) if sce else 0)
                + (sum(post))  # posteriors
                ) + 0.5
            ]
        for site in sites
        },
    **({
        f'Qmap_diff_{i}': [
            subplot_wdt_Qmap*(2*len(idirs))+0.5,
            subplot_hgt_Qmap*(4)+0.5
            ]
        for i in rdates} if sce and any(post) else {})
    }
plt.close('all')
figs = {k: plt.figure(figsize=v, dpi=300) for k, v in figsize.items()}
axs = {k: {} for k in figsize}

print('  Plot')
print('    Observations')
# Observation/difference plot
for site in site_seq:
    fs = figsize['obs']
    for k in obs_seq:
        sk = f'{site}_{k}'
        if sk not in site_obs_seq:
            continue
        s = site_obs_seq.index(sk)
        ymin = np.nanmin(np.concatenate([
            *[i[site][k] for i in obs_y],
            *[i[site][k][nx_tH_tObs[site][k]] for i in prior_obs],
            *[i[site][k][nx_tH_tObs[site][k]] for i in post_obs],
            ]))
        ymax = np.nanmax(np.concatenate([
            *[i[site][k] for i in obs_y],
            *[i[site][k][nx_tH_tObs[site][k]] for i in prior_obs],
            *[i[site][k][nx_tH_tObs[site][k]] for i in post_obs],
            ]))
        ysf = np.floor(np.log10(np.abs([
            i if i != 0 else 1 for i in [ymin, ymax]
            ]))).max() - 1
        ysf = 1 if not ysf and k not in ['d_13C'] else ysf
        ytick = aticks(ymin, ymax, 3, 5, ysf, 1)
        axs['obs'][sk] = name_qch4_couple.plot.generic(
            fig=figs['obs'],
            idata={
                **{
                    f'vline_{i}': [
                        'vline', [i],
                        {'ls': ':', 'linewidth': 0.5, 'c': '#000000'}
                        ]
                    for i in pd.to_datetime(rdates)[1:]
                    },
                **{f'obs_{n}': [
                    'err',
                    [],
                    {
                        'x': dates_tObs[site][k],
                        'y': obs_y[n][site][k],
                        'yerr': None,
                        'c': colours[0], 'ls': '', 'marker': 'o',
                        'markersize': 2.0, 'elinewidth': 0.5,
                        'capsize': 4.0, 'capthick': 0.5,
                        'zorder': (1 if any(post) else 0)
                        }
                    ]
                    for n, i in
                    enumerate(obs_y[slice(1 if same_obs else None)])
                    },
                **{f'prior_{n}': [
                    'err',
                    [],
                    {
                        'x': dates_tObs[site][k],
                        'y': (
                            i[site][k]
                            )[nx_tH_tObs[site][k]],
                        'yerr': None,
                        'c': colours[1] if same_prior else colours[2+n],
                        'ls': '', 'marker': 'o',
                        'markersize': 2.0, 'elinewidth': 0.5,
                        'capsize': 4.0, 'capthick': 0.5,
                        'zorder': (0 if any(post) else 1)
                        }
                    ]
                    for n, i in
                    enumerate(prior_obs[slice(1 if same_prior else None)])
                    },
                **{f'posterior_{n}': [
                    'err',
                    [],
                    {
                        'x': dates_tObs[site][k],
                        'y': (
                            post_obs[n][site][k]
                            )[nx_tH_tObs[site][k]],
                        'yerr': None,
                        'c': colours[2+n], 'ls': '', 'marker': '+',
                        'markeredgewidth': 0.5,
                        'markersize': 3.0, 'elinewidth': 0.5,
                        'capsize': 4.0, 'capthick': 0.5,
                        'zorder': 2
                        }
                    ]
                    for n, i in enumerate(post_obs) if post
                    },
                },
            xlim=[
                np.datetime64(dates_tHour[0]) - np.timedelta64(1, 'D'),
                np.datetime64(dates_tHour[-1]) + np.timedelta64(1, 'D'),
                ],
            ylim=ytick[0],
            texts=[
                {
                    'x': (0.5)/fs[0],
                    'y': (fs[1]-s*subplot_hgt_obs0-0.5)/fs[1],
                    's': y_name[k],
                    'ha': 'left', 'va': 'center', 'size': 6, 'rotation': 90
                    },
                *([
                {
                    'x': (0.05)/fs[0],
                    'y': (fs[1]-s*subplot_hgt_obs0-0.05)/fs[1],
                    's': site,
                    'ha': 'left', 'va': 'top', 'size': 6, 'rotation': 0
                    }
                    ] if k in 'chi' else [])
                ],
            yticks=np.arange(
                ytick[0][0],
                ytick[0][1] + ytick[1],
                ytick[1]
                ),
            loc_plot=[
                (fs[0]-subplot_wdt_obs0-subplot_wdt_obs1+0.1)/fs[0],
                (fs[1]-(s+1)*subplot_hgt_obs0+0.05)/fs[1],
                (subplot_wdt_obs0-0.1)/fs[0],
                (subplot_hgt_obs0-0.1)/fs[1]
                ],
            tick_fontsize=6,
            xtick_params=[
                True,
                mdates.DateFormatter('%Y-%m-%d'),
                mdates.DayLocator(bymonthday=(1, 16)),
                ],
            )
        '''
        axs['obs'][f'{sk}_c'] = name_qch4_couple.plot.compare(
            fig=figs['obs'],
            idata={
                'prior': [
                    [],
                    {
                        'x': obs_y[site][k],
                        'y': (
                            prior_obs[site][k]
                            )[nx_tH_tObs[site][k]],
                        'c': colours[1], 'marker': 'o',
                        's': 2.0,
                        'zorder': 1
                        }
                    ],
                'posterior': [
                    [],
                    {
                        'x': obs_y[site][k],
                        'y': (
                            post_obs[site][k]
                            )[nx_tH_tObs[site][k]],
                        'c': colours[2], 'marker': 'o',
                        's': 2.0,
                        'zorder': 2
                        }
                    ],
                },
            lim=ytick[0],
            err=NCG[k]*NCG_mul[k],
            texts=[],
            ticks=np.arange(
                ytick[0][0],
                ytick[0][1] + ytick[1],
                ytick[1]
                ),
            loc_plot=[
                (fs[0]-subplot_wdt_obs1+0.05)/fs[0],
                (fs[1]-(s+1)*subplot_hgt_obs1+0.05)/fs[1],
                (subplot_wdt_obs1-0.1)/fs[0],
                (subplot_hgt_obs1-0.1)/fs[1]
                ],
            fontsize=6,
            )
        '''
        if s != len(site_obs_seq) - 1:
            axs['obs'][sk].tick_params(
                labelleft=True, labelbottom=False,
                labelright=False, labeltop=False
                )
        else:
            for label in axs['obs'][sk].get_xticklabels():
                label.set_ha("right")
                label.set_rotation(30)
        '''
        axs['obs'][f'{sk}_c'].tick_params(
            labelleft=False, labelbottom=False,
            labelright=False, labeltop=False
            )
        '''

# Q
print('    Emissions')
for s in range(dim_sAll+1):
    for rgroup in focus_rgroup_uniq[0]:
        k0 = f'QA_{rgroup}'
        if s < dim_sAll:
            k1 = f'QA_{rgroup}'
            k2 = f'sce_QA_{rgroup}'
            s1 = s
            r1 = 0
        else:
            k1 = f'QA_{rgroup}_total'
            k2 = f'sce_QA_{rgroup}_total'
            s1 = 0
            r1 = 1
        ymin = np.nanmin(np.concatenate([
            *[i[k1][..., s1].flatten() for i in prior_state],
            *[i[k1][..., s1].flatten() for n, i in enumerate(post_state) if post[n]],
            *([i[k2][..., s1].flatten() for i in prior_state] if sce else []),
            ]))
        ymax = np.nanmax(np.concatenate([
            *[i[k1][..., s1].flatten() for i in prior_state],
            *[i[k1][..., s1].flatten() for n, i in enumerate(post_state) if post[n]],
            *([i[k2][..., s1].flatten() for i in prior_state] if sce else []),
            ]))
        if not ymin and not ymax:
            continue
        ysf = np.floor(np.log10(np.abs([
            i if i != 0 else 1 for i in [ymin, ymax]
            ]))).max() - 1
        ysf = 1 if not ysf else ysf
        ytick = aticks(ymin, ymax, 3, 5, ysf, 1)
        fs = figsize[k0]
        axs[k0][s] = name_qch4_couple.plot.generic(
            fig=figs[k0],
            idata={
                **{
                    f'vline_{i}': [
                        'vline', [i],
                        {'ls': ':', 'linewidth': 0.5, 'c': '#000000'}
                        ]
                    for i in pd.to_datetime(rdates)[1:]
                    },
                'target': [
                    'err',
                    [],
                    {
                        'x': dates_plot[0],
                        'y': prior_state[0][k2][..., r1, s1],
                        'yerr': None,
                        'c': colours[0], 'ls': '', 'marker': 'o',
                        'markersize': 2.0, 'elinewidth': 0.5,
                        'capsize': 4.0, 'capthick': 0.5,
                        'zorder': 2
                        }
                    ] if sce else ['', [], {}],
                **{f'prior_{n}': [
                    'err',
                    [],
                    {
                        'x': dates_plot[0 if same_prior else (n+bool(sce))],
                        'y': i[k1][..., 1, s1],
                        'yerr': np.diff(
                            i[k1], axis=1
                            )[..., 0, s1].T,
                        'c': (
                            colours[1]
                            if same_prior or post[n] else
                            colours[2+n]
                            ),
                        'ls': '', 'marker': 'o',
                        'markersize': 2.0, 'elinewidth': 0.5,
                        'capsize': 4.0, 'capthick': 0.5,
                        'zorder': 1
                        }
                    ]
                    for n, i in enumerate(prior_state[slice(1 if same_prior else None)])
                    },
                **{f'posterior_{n}': [
                    'err',
                    [],
                    {
                        'x': dates_plot[n+bool(sce)],
                        'y': i[k1][..., 1, s1],
                        'yerr': np.diff(
                            i[k1], axis=1
                            )[..., 0, s1].T,
                        'c': colours[2+n], 'ls': '', 'marker': 'o',
                        'markersize': 2.0, 'elinewidth': 0.5,
                        'capsize': 4.0, 'capthick': 0.5,
                        'zorder': 3
                        }
                    ]
                    for n, i in enumerate(post_state)
                    if post[n]
                    },
                },
            xlim=[
                np.datetime64(dates_tHour[0]) - np.timedelta64(1, 'D'),
                np.datetime64(dates_tHour[-1]) + np.timedelta64(1, 'D'),
                ],
            ylim=ytick[0],
            label='abs',
            texts=[
                {
                    'x': (0.05+0.1*(s%2))/fs[0],
                    'y': (fs[1]-s*subplot_hgt_QA-0.5)/fs[1],
                    's': (
                        f'{sector_names[s][1]} ({sector_names[s][0]})'
                        if s < dim_sAll else 'Total'
                        ),
                    'ha': 'left', 'va': 'center', 'size': 6, 'rotation': 90
                    }
                ],
            yticks=np.arange(
                ytick[0][0], ytick[0][1] + ytick[1], ytick[1]
                ),
            loc_plot=[
                (fs[0]-subplot_wdt_QA+0.05)/fs[0],
                (fs[1]-(s+1)*subplot_hgt_QA+0.05)/fs[1],
                (subplot_wdt_QA-0.1)/fs[0],
                (subplot_hgt_QA-0.1)/fs[1]
                ],
            tick_fontsize=6,
            xtick_params=[
                True,
                mdates.DateFormatter('%Y-%m-%d'),
                mdates.DayLocator(bymonthday=(1, 16))
                if tmode in [0, 1] else
                mdates.DayLocator(bymonthday=(1,)),
                ],
            )
        if s != 5:
            axs[k0][s].tick_params(
                labelleft=True, labelbottom=False,
                labelright=False, labeltop=False
                )
        else:
            for label in axs[k0][s].get_xticklabels():
                label.set_ha("right")
                label.set_rotation(30)
    for k in ['dQi1', 'dQi2']:
        if s >= dim_sAll:
            continue
        k1 = f'v_{k}'
        k2 = f'h_{k}'
        ymin = np.nanmin(np.concatenate([
            *[i[k1][..., s].flatten() for i in prior_state],
            *[i[k1][..., s].flatten() for n, i in enumerate(post_state) if post[n]],
            *([i[k1][..., s].flatten() for i in prior_state] if sce else []),
            ]))
        ymax = np.nanmax(np.concatenate([
            *[i[k1][..., s].flatten() for i in prior_state],
            *[i[k1][..., s].flatten() for n, i in enumerate(post_state) if post[n]],
            *([i[k1][..., s].flatten() for i in prior_state] if sce else []),
            ]))
        if not ymin and not ymax:
            continue
        ysf = np.floor(np.log10(np.abs([
            i if i != 0 else 1 for i in [ymin, ymax]
            ]))).max() - 1
        ysf = 1 if not ysf else ysf
        ytick = aticks(ymin, ymax, 3, 5, ysf, 1)
        #ytick = plot_params[f'{k}']['ytick']
        fs = figsize[k]
        axs[k][s] = name_qch4_couple.plot.generic(
            fig=figs[k],
            idata={
                **{
                    f'vline_{i}': [
                        'vline', [i],
                        {'ls': ':', 'linewidth': 0.5, 'c': '#000000'}
                        ]
                    for i in pd.to_datetime(rdates)[1:]
                    },
                'target': [
                    'err',
                    [],
                    {
                        'x': dates_plot[0],
                        'y': prior_state[0][f'sce_{k1}'][..., 1, s],
                        'yerr': None,
                        'c': colours[0], 'ls': '', 'marker': 'o',
                        'markersize': 2.0, 'elinewidth': 0.5,
                        'capsize': 4.0, 'capthick': 0.5,
                        'zorder': 2
                        }
                    ] if sce else ['', [], {}],
                **{f'prior_{n}': [
                    'err',
                    [],
                    {
                        'x': dates_plot[0 if same_prior else n+bool(sce)],
                        'y': i[k1][..., 1, s],
                        'yerr': np.diff(
                            i[k1][...,  s], axis=1
                            ).T,
                        'c': colours[1] if same_prior else colours[2+n],
                        'ls': '', 'marker': 'o',
                        'markersize': 2.0, 'elinewidth': 0.5,
                        'capsize': 4.0, 'capthick': 0.5,
                        'zorder': 1
                        }
                    ]
                    for n, i in enumerate(prior_state[slice(1 if same_prior else None)])
                    },
                **{f'posterior_{n}': [
                    'err',
                    [],
                    {
                        'x': dates_plot[n+bool(sce)],
                        'y': i[k1][..., 1, s],
                        'yerr': np.diff(
                            i[k1][..., s], axis=1
                            ).T,
                        'c': colours[2+n], 'ls': '', 'marker': 'o',
                        'markersize': 2.0, 'elinewidth': 0.5,
                        'capsize': 4.0, 'capthick': 0.5,
                        'zorder': 3
                        }
                    ]
                    for n, i in enumerate(post_state)
                    if post[n]
                    },
                },
            xlim=[
                np.datetime64(dates_tHour[0]) - np.timedelta64(1, 'D'),
                np.datetime64(dates_tHour[-1]) + np.timedelta64(1, 'D'),
                ],
            ylim=ytick[0],
            texts=[
                {
                    'x': (0.05+0.1*(s%2))/fs[0],
                    'y': (fs[1]-s*subplot_hgt_QA-0.5)/fs[1],
                    's': f'{sector_names[s][1]} ({sector_names[s][0]})',
                    'ha': 'left', 'va': 'center', 'size': 6, 'rotation': 90
                    }
                ],
            yticks=np.arange(
                ytick[0][0], ytick[0][1] + ytick[1], ytick[1]
                ),
            loc_plot=[
                (fs[0]-subplot_wdt_QA+0.05)/fs[0],
                (fs[1]-(s+1)*subplot_hgt_QA+0.05)/fs[1],
                (subplot_wdt_QA-0.1)/fs[0],
                (subplot_hgt_QA-0.1)/fs[1]
                ],
            tick_fontsize=6,
            xtick_params=[
                True,
                mdates.DateFormatter('%Y-%m-%d'),
                mdates.DayLocator(bymonthday=(1, 16))
                if tmode in [0, 1] else
                mdates.DayLocator(bymonthday=(1,)),
                ],
            )
        if s != 4:
            axs[k][s].tick_params(
                labelleft=True, labelbottom=False,
                labelright=False, labeltop=False
                )
        else:
            for label in axs[k][s].get_xticklabels():
                label.set_ha("right")
                label.set_rotation(30)

# dchi_s
print('    Enhancements')
dchi_s_pri = [
    {  # shape = (isotope, len([m0, m1, m2]), t, s)
        site:
        np.array(calc_final_d(
            [
                dm0_s[i][site],
                dm1_s[i][site],
                dm2_s_pri[i][site]
                ],
            [
                prior_state[i]['v_dQi1'][:, 1],
                prior_state[i]['v_dQi1'][:, 1],
                prior_state[i]['v_dQi1'][:, 1]
                ],
            [
                prior_state[i]['v_dQi2'][:, 1],
                prior_state[i]['v_dQi2'][:, 1],
                prior_state[i]['v_dQi2'][:, 1]
                ],
            R_std, M, rdates, dim_sAll
            ))
        for site in sites
        }
    for i in range(len(idirs))
    ]
dchi_dQi1_pri = [
    {
        site: (v[1].sum((0, 2)) / v[0].sum((0, 2)) / R_std[1] - 1.) * 1.e3
        for site, v in dchi_s_pri[i].items()
        }
    for i in range(len(idirs))
    ]
dchi_dQi2_pri = [
    {
        site: (v[2].sum((0, 2)) / v[0].sum((0, 2)) / R_std[2] - 1.) * 1.e3
        for site, v in dchi_s_pri[i].items()
        }
    for i in range(len(idirs))
    ]

if sce:
    dchi_s_sce = [
        {  # shape = (i , len(m), t, s)
            site:
            np.array(calc_final_d(
                [
                    dm0_s[i][site],
                    dm1_s[i][site],
                    dm2_s_sce[i][site]
                    ],
                [
                    prior_state[i]['v_dQi1'][:, 1],
                    prior_state[i]['v_dQi1'][:, 1],
                    prior_state[i]['v_dQi1'][:, 1]
                    ],
                [
                    prior_state[i]['v_dQi2'][:, 1],
                    prior_state[i]['v_dQi2'][:, 1],
                    prior_state[i]['v_dQi2'][:, 1]
                    ],
                R_std, M, rdates, dim_sAll
                ))
            for site in sites
            }
        for i in range(len(idirs))
        ]
    dchi_dQi1_sce = [
        {
            site: (v[1].sum((0, 2)) / v[0].sum((0, 2)) / R_std[1] - 1.) * 1.e3
            for site, v in dchi_s_sce[i].items()
            }
        for i in range(len(idirs))
        ]
    dchi_dQi2_sce = [
        {
            site: (v[2].sum((0, 2)) / v[0].sum((0, 2)) / R_std[2] - 1.) * 1.e3
            for site, v in dchi_s_sce[i].items()
            }
        for i in range(len(idirs))
        ]
if any(post):
    dchi_s_pos = [
        {  # shape = (i , len(m), t, s)
            site:
            np.array(calc_final_d(
                [
                    dm0_s[i][site],
                    dm1_s[i][site],
                    dm2_s_pos[i][site]
                    ],
                [
                    prior_state[i]['v_dQi1'][:, 1],
                    prior_state[i]['v_dQi1'][:, 1],
                    np.where(
                        np.isfinite(post_state[i]['v_dQi1'][:, 1]),
                        post_state[i]['v_dQi1'][:, 1],
                        prior_state[i]['v_dQi1'][:, 1]
                        )
                    ],
                [
                    prior_state[i]['v_dQi2'][:, 1],
                    prior_state[i]['v_dQi2'][:, 1],
                    np.where(
                        np.isfinite(post_state[i]['v_dQi2'][:, 1]),
                        post_state[i]['v_dQi2'][:, 1],
                        prior_state[i]['v_dQi2'][:, 1]
                        )
                    ],
                R_std, M, rdates, dim_sAll
                ))
            for site in sites
            }
        for i in range(len(idirs))
        if post[i]
        ]
    dchi_dQi1_pos = [
        {
            site: (v[1].sum((0, 2)) / v[0].sum((0, 2)) / R_std[1] - 1.) * 1.e3
            for site, v in dchi_s_pos[i].items()
            }
        for i in range(len(idirs))
        if post[i]
        ]
    dchi_dQi2_pos = [
        {
            site: (v[2].sum((0, 2)) / v[0].sum((0, 2)) / R_std[2] - 1.) * 1.e3
            for site, v in dchi_s_pos[i].items()
            }
        for i in range(len(idirs))
        if post[i]
        ]

for site in sites:
    fs = figsize[f'dchi_{site}']
    ymin0 = 0.
    ymax0 = np.nanmax([np.nanmax([
        dchi_s_pri[i][site].sum((0, 1, 3)),
        *([dchi_s_sce[i][site].sum((0, 1, 3))] if sce else []),
        *([dchi_s_pos[i][site].sum((0, 1, 3))] if post[i] else []),
        ]) for i in range(len(idirs))])
    ymind0 = np.nanmin([np.nanmin([
        *(
            [
                dchi_s_sce[i][site].sum((0, 1, 3))
                - dchi_s_pri[i][site].sum((0, 1, 3))
                ] if sce else []
            ),
        *(
            [
                dchi_s_pos[i][site].sum((0, 1, 3))
                - dchi_s_pri[i][site].sum((0, 1, 3))
                ] if post[i] else []),
        ]) for i in range(len(idirs))]) if sce and any(post) else None
    ymaxd0 = np.nanmax([np.nanmax([
        *(
            [
                dchi_s_sce[i][site].sum((0, 1, 3))
                - dchi_s_pri[i][site].sum((0, 1, 3))
                ] if sce else []
            ),
        *(
            [
                dchi_s_pos[i][site].sum((0, 1, 3))
                - dchi_s_pri[i][site].sum((0, 1, 3))
                ] if post[i] else []),
        ]) for i in range(len(idirs))]) if sce and any(post) else None
    ymin1 = np.nanmin([np.nanmin([
        dchi_dQi1_pri[i][site],
        *([dchi_dQi1_sce[i][site]] if sce else []),
        *([dchi_dQi1_pos[i][site]] if post[i] else []),
        ]) for i in range(len(idirs))])
    ymax1 = np.nanmax([np.nanmax([
        dchi_dQi1_pri[i][site],
        *([dchi_dQi1_sce[i][site]] if sce else []),
        *([dchi_dQi1_pos[i][site]] if post[i] else []),
        ]) for i in range(len(idirs))])
    ymind1 = np.nanmin([np.nanmin([
        *([dchi_dQi1_sce[i][site] - dchi_dQi1_pri[i][site]] if sce else []),
        *([dchi_dQi1_pos[i][site] - dchi_dQi1_pri[i][site]] if post[i] else []),
        ]) for i in range(len(idirs))]) if sce and any(post) else None
    ymaxd1 = np.nanmax([np.nanmax([
        *([dchi_dQi1_sce[i][site] - dchi_dQi1_pri[i][site]] if sce else []),
        *([dchi_dQi1_pos[i][site] - dchi_dQi1_pri[i][site]] if post[i] else []),
        ]) for i in range(len(idirs))]) if sce and any(post) else None
    ymin2 = np.nanmin([np.nanmin([
        dchi_dQi2_pri[i][site],
        *([dchi_dQi2_sce[i][site]] if sce else []),
        *([dchi_dQi2_pos[i][site]] if post[i] else []),
        ]) for i in range(len(idirs))])
    ymax2 = np.nanmax([np.nanmax([
        dchi_dQi2_pri[i][site],
        *([dchi_dQi2_sce[i][site]] if sce else []),
        *([dchi_dQi2_pos[i][site]] if post[i] else []),
        ]) for i in range(len(idirs))])
    ymind2 = np.nanmin([np.nanmin([
        *([dchi_dQi2_sce[i][site] - dchi_dQi2_pri[i][site]] if sce else []),
        *([dchi_dQi2_pos[i][site] - dchi_dQi2_pri[i][site]] if post[i] else []),
        ]) for i in range(len(idirs))]) if sce and any(post) else None
    ymaxd2 = np.nanmax([np.nanmax([
        *([dchi_dQi2_sce[i][site] - dchi_dQi2_pri[i][site]] if sce else []),
        *([dchi_dQi2_pos[i][site] - dchi_dQi2_pri[i][site]] if post[i] else []),
        ]) for i in range(len(idirs))]) if sce and any(post) else None
    rows = [
        *[
            (n, 'stack', 'pri')
            for n in range(1 if same_prior else len(idirs))
            ],
        *([(n, 'stack', 'sce')] if sce else []),
        *[
            (n, 'stack', 'pos')
            for n in range(len(idirs))
            if post[n]
            ],
        'Q', 'dQi1', 'dQi2'
        ]
    for s, sk in enumerate(rows):
        '''
        '''
        if isinstance(sk, tuple):
            n, name, dtype = sk
            key = f'Q_{dtype}{n}'
            Q_app = (
                np.array(pd.DataFrame(
                    (
                        dchi_s_pri[n][site] if dtype == 'pri' else
                        dchi_s_sce[n][site] if dtype == 'sce' else
                        dchi_s_pos[n][site]
                        ).sum((0, 1)),
                    index=dates_tHour
                    ).groupby(dates_tHour.date).mean()).T
                / np.array(pd.Series(
                    (
                        dchi_s_pri[n][site] if dtype == 'pri' else
                        dchi_s_sce[n][site] if dtype == 'sce' else
                        dchi_s_pos[n][site]
                        ).sum((0, 1, 3)),
                    index=dates_tHour
                    ).groupby(dates_tHour.date).mean())
                )
            Q_app[~np.isfinite(Q_app)] = 0.
            axs[f'dchi_{site}'][key] = name_qch4_couple.plot.generic(
                fig=figs[f'dchi_{site}'],
                idata={
                    **{
                        f'hline_{i}': [
                            'hline', [i], {'linewidth': 0.5, 'c': '#000000'}
                            ]
                        for i in np.linspace(0., 1., 6)[1:-1]
                        },
                    **{
                        f'vline_{i}': [
                            'vline', [i],
                            {'ls': ':', 'linewidth': 0.5, 'c': '#000000'}
                            ]
                        for i in pd.to_datetime(rdates)[1:]
                        },
                    **{f'bar_{i}': [
                        'bar',
                        [],
                        {
                            'x': dates_tDay,
                            'height': v,
                            'width': np.timedelta64(1, 'D'),
                            'bottom': Q_app[:i].sum(0),
                            'align': 'edge',
                            'color': matplotlib.cm.get_cmap(
                                'viridis_r'
                                )(0.1 + 0.2*i),
                            }
                        ]
                        for i, v in enumerate(Q_app)
                        },
                    },
                xlim=[
                    np.datetime64(dates_tHour[0]) - np.timedelta64(1, 'D'),
                    np.datetime64(dates_tHour[-1]) + np.timedelta64(1, 'D'),
                    ],
                ylim=[0., 1.],
                label='stack',
                texts=[
                    {
                        'x': (0.1)/fs[0],
                        'y': (fs[1]-s*subplot_hgt_obs0-0.5)/fs[1],
                        's': (
                            'Prior' if dtype == 'pri' else
                            'Target' if dtype == 'sce' else
                            'Posterior'
                            ),
                        'ha': 'left', 'va': 'center', 'size': 6, 'rotation': 90
                        },
                    ],
                yticks=np.linspace(0., 1., 6),
                loc_plot=[
                    (fs[0]-subplot_wdt_obs0+0.1)/2./fs[0],
                    (fs[1]-(s+1)*subplot_hgt_obs0+0.05)/fs[1],
                    (subplot_wdt_obs0-0.1)/fs[0],
                    (subplot_hgt_obs0-0.1)/fs[1]
                    ],
                tick_fontsize=6,
                xtick_params=[
                    True,
                    mdates.DateFormatter('%Y-%m-%d'),
                    mdates.DayLocator(bymonthday=(1, 16)),
                    ],
                )
        else:
            key = sk
            if sk == 'dQi1':
                ymin, ymax, ymind, ymaxd = ymin1, ymax1, ymind1, ymaxd1
            elif sk == 'dQi2':
                ymin, ymax, ymind, ymaxd = ymin2, ymax2, ymind2, ymaxd2
            else:
                ymin, ymax, ymind, ymaxd = ymin0, ymax0, ymind0, ymaxd0
            ysf = np.floor(np.log10(np.abs([
                i if i != 0 else 1 for i in [ymin, ymax]
                ]))).max() - 1
            ysf = 1 if not ysf and k not in ['d_13C'] else ysf
            ytick = aticks(ymin, ymax, 3, 5, ysf, 1)
            # Prior: normal
            axs[f'dchi_{site}'][key] = name_qch4_couple.plot.generic(
                fig=figs[f'dchi_{site}'],
                idata={
                    **{f'prior_{n}': [
                        'err',
                        [],
                        {
                            'x': dates_tHour,
                            'y': (
                                dchi_dQi1_pri[n][site] if sk == 'dQi1' else
                                dchi_dQi2_pri[n][site] if sk == 'dQi2' else
                                np.array(dchi_s_pri[n][site]).sum((0, 1, 3)).T
                                ),
                            'yerr': None,
                            'c': colours[1] if same_prior else colours[2+n],
                            'ls': '', 'marker': 'o',
                            'markeredgewidth': 0.5,
                            'markersize': 2.0, 'elinewidth': 0.5,
                            'capsize': 4.0, 'capthick': 0.5,
                            'zorder': 0
                            }
                        ]
                        for n in range(1 if same_prior else len(idirs))
                        },
                    **{
                        f'vline_{i}': [
                            'vline', [i],
                            {'ls': ':', 'linewidth': 0.5, 'c': '#000000'}
                            ]
                        for i in pd.to_datetime(rdates)[1:]
                        },
                    },
                xlim=[
                    np.datetime64(dates_tHour[0]) - np.timedelta64(1, 'D'),
                    np.datetime64(dates_tHour[-1]) + np.timedelta64(1, 'D'),
                    ],
                ylim=[ytick[0][0] - np.diff(ytick[0])*0.5, ytick[0][1]],
                texts=[
                    {
                        'x': (0.1)/fs[0],
                        'y': (fs[1]-s*subplot_hgt_obs0-0.5)/fs[1],
                        's': (
                            u'$\delta$$^{13}$C (\u2030)' if sk == 'dQi1' else
                            u'$\delta$$^{2}$H (\u2030)' if sk == 'dQi2' else
                            u'Enhancement (nmol mol$^{-1}$)'
                            ),
                        'ha': 'left', 'va': 'center', 'size': 6, 'rotation': 90
                        },
                    ],
                yticks=np.arange(
                    ytick[0][0],
                    ytick[0][1] + ytick[1],
                    ytick[1]
                    ),
                loc_plot=[
                    (fs[0]-subplot_wdt_obs0+0.1)/2./fs[0],
                    (fs[1]-(s+1)*subplot_hgt_obs0+0.05)/fs[1],
                    (subplot_wdt_obs0-0.1)/fs[0],
                    (subplot_hgt_obs0-0.1)/fs[1]
                    ],
                tick_fontsize=6,
                xtick_params=[
                    True,
                    mdates.DateFormatter('%Y-%m-%d'),
                    mdates.DayLocator(bymonthday=(1, 16)),
                    ],
                )
            # Observation/Target/Posterior: diff
            if sce and any(post):
                ysfd = np.floor(np.log10(np.abs([
                    i for i in [ymind, ymaxd] if i != 0
                    ]))).max() - 1
                ytickd0 = afloor(ymind, ysfd, 1)
                ytickd1 = aceil(ymaxd, ysfd, 1)
                if ytickd0 < 0 and 0 < ytickd1:
                    ytickdinc = adiv(ytickd0, ytickd1, 3, 5)
                    ytickd = [
                        [min(ytickd0, -ytickdinc), max(ytickd1, ytickdinc)],
                        ytickdinc
                        ]
                else:
                    ytickd = aticks(ymind, ymaxd, 3, 5, ysfd, 1)
                axs[f'dchi_{site}'][f'{key}_d'] = name_qch4_couple.plot.generic(
                    fig=figs[f'dchi_{site}'],
                    idata={
                        'hline': ['hline', [0], {'c': colours[1], 'ls': '-'}],
                        **{f'posterior_{n}': [
                            'err',
                            [],
                            {
                                'x': dates_tHour,
                                'y': (
                                    (
                                        dchi_dQi1_pos[n][site]
                                        - dchi_dQi1_pri[0 if same_prior else n][site]
                                        )
                                    if sk == 'dQi1' else
                                    (
                                        dchi_dQi2_pos[n][site]
                                        - dchi_dQi2_pri[0 if same_prior else n][site]
                                        )
                                    if sk == 'dQi2' else
                                    (
                                        np.array(dchi_s_pos[n][site]).sum((0, 1, 3)).T
                                        - np.array(
                                            dchi_s_pri[0 if same_prior else n][site]
                                            ).sum((0, 1, 3)).T
                                        )
                                    ),
                                'yerr': None,
                                'c': colours[2+n], 'ls': '', 'marker': '+',
                                'markeredgewidth': 0.5,
                                'markersize': 3.0, 'elinewidth': 0.5,
                                'capsize': 4.0, 'capthick': 0.5,
                                'zorder': 2
                                }
                            ]
                            for n in range(len(idirs))
                            if post[n]
                            },
                        **{f'prescribed_{n}': [
                            'err',
                            [],
                            {
                                'x': dates_tHour,
                                'y': (
                                    (dchi_dQi1_sce[n][site] - dchi_dQi1_pri[n][site])
                                    if sk == 'dQi1' else
                                    (dchi_dQi2_sce[n][site] - dchi_dQi2_pri[n][site])
                                    if sk == 'dQi2' else
                                    (
                                        np.array(dchi_s_sce[n][site]).sum((0, 1, 3)).T
                                        - np.array(dchi_s_pri[n][site]).sum((0, 1, 3)).T
                                        )
                                    ),
                                'yerr': None,
                                'c': colours[0], 'ls': '', 'marker': 'o',
                                'markeredgewidth': 0.5,
                                'markersize': 2.0, 'elinewidth': 0.5,
                                'capsize': 4.0, 'capthick': 0.5,
                                'zorder': 1
                                }
                            ]
                            for n in range(1)
                            if sce
                            },
                        },
                    xlim=[
                        np.datetime64(dates_tHour[0]) - np.timedelta64(1, 'D'),
                        np.datetime64(dates_tHour[-1]) + np.timedelta64(1, 'D'),
                        ],
                    ylim=[ytickd[0][0], ytickd[0][1] + np.diff(ytickd[0])*0.5],
                    label=f'diff_{sk}',
                    texts=[],
                    yticks=(
                        np.concatenate([
                            np.arange(0., ytickd[0][0]-ytickd[1], -ytickd[1])[::-1],
                            np.arange(0., ytickd[0][1]+ytickd[1], ytickd[1])[1:]
                            ])
                        if ytickd[0][0] < 0 and 0 < ytickd[0][1] else
                        np.arange(
                            ytickd[0][0 if ytickd[0][1] > 0 else 1],
                            ytickd[0][1 if ytickd[0][1] > 0 else 0]
                            + ytickd[1] * np.sign(ytickd[0][1]),
                            ytickd[1] * (
                                np.sign(ytickd[0][1])
                                if ytickd[0][1] != 0 else
                                np.sign(ytickd[0][0])
                                )
                            )
                        ),
                    loc_plot=[
                        (fs[0]-subplot_wdt_obs0+0.1)/2./fs[0],
                        (fs[1]-(s+1)*subplot_hgt_obs0+0.05)/fs[1],
                        (subplot_wdt_obs0-0.1)/fs[0],
                        (subplot_hgt_obs0-0.1)/fs[1]
                        ],
                    tick_fontsize=6,
                    xtick_params=[
                        True,
                        mdates.DateFormatter('%Y-%m-%d'),
                        mdates.DayLocator(bymonthday=(1, 16)),
                        ],
                    )
                axs[f'dchi_{site}'][f'{key}_d'].tick_params(
                    left=False, bottom=False,
                    right=True, top=False,
                    labelleft=False, labelbottom=False,
                    labelright=True if s != 0 else False, labeltop=False
                    )
                axs[f'dchi_{site}'][f'{key}_d'].patch.set_alpha(0.)
        if s == 0:  # Legend
            axs[f'dchi_{site}']['legend'] = figs[f'dchi_{site}'].add_axes(
                [
                    (fs[0]-0.5)/fs[0],
                    (fs[1]-(s+1)*subplot_hgt_obs0+0.05)/fs[1],
                    0.45/fs[0],
                    (subplot_hgt_obs0-0.1)/fs[1]
                    ])
            llabels = ['FF', 'WM', 'MA', 'BB', 'MN']
            lcolours = [
                *matplotlib.cm.get_cmap('viridis_r')(np.linspace(0.1, 0.9, 5))
                ]
            lhandles = []
            for i, j in zip(llabels, lcolours):
                lhandles.append(axs[f'dchi_{site}']['legend'].plot(
                    [], [],
                    **{
                        'c': j, 'ls': '-', 'marker': '',
                        'linewidth': 2.0, 'label': i
                        }
                    )[0])
            axs[f'dchi_{site}']['legend'].axis('off')
            axs[f'dchi_{site}']['legend'].legend(
                loc='center',
                handles=lhandles[::-1],
                handlelength=1.0,
                numpoints=1,
                fontsize=6
                )
        if s != len(rows) - 1:
            axs[f'dchi_{site}'][key].tick_params(
                labelleft=True, labelbottom=False,
                labelright=False, labeltop=False
                )
        else:
            for label in axs[f'dchi_{site}'][key].get_xticklabels():
                label.set_ha("right")
                label.set_rotation(30)

# Q maps
if sce and any(post):
    print('    Emission maps')
    plotable_abs = [
        np.full((dim_sAll, grid_info['inv_nlat'], grid_info['inv_nlon']), np.nan)
        for _ in range(len(idirs))
        ]
    plotable_rel = [
        np.full((dim_sAll, grid_info['inv_nlat'], grid_info['inv_nlon']), np.nan)
        for _ in range(len(idirs))
        ]
    plotable_dis = [
        np.full((dim_sAll, grid_info['inv_nlat'], grid_info['inv_nlon']), np.nan)
        for _ in range(len(idirs))
        ]
    grid_sel = [
        grid_vertex[0][inv_lon0:inv_lon1+1],
        grid_vertex[1][inv_lat0:inv_lat1+1],
        ]
    ytick_abs = [
        np.array([
            *-np.power(10., np.linspace(-6., -12., 7)),
            *np.power(10., np.linspace(-12., -6., 7))
            ]),
        np.array([
            *-np.power(10., np.linspace(-6., -12., 4)),
            *np.power(10., np.linspace(-12., -6., 4))
            ])
        ]
    ytick_rel = [
        np.array([
            *-np.linspace(1.2, 0., 13)[:-1],
            *np.linspace(0., 1.2, 13)[1:]
            ]),
        np.array([
            *-np.linspace(1.2, 0., 4)[:-1],
            0.,
            *np.linspace(0., 1.2, 4)[1:]
            ])
        ]
    for n0, rdate in enumerate(rdates):
        for n1 in range(len(idirs)):
            plotable_abs[n1][:] = np.nan
            plotable_rel[n1][:] = np.nan
            if not post[n1]:
                continue
            for n2, i in enumerate(
                    np.unique(inv_reg_maps[n1][n0][reg_map_sel[n1][n0]])
                    ):
                plotable_abs[n1][inv_reg_maps[n1][n0] == i] = (
                    prior_state[n1]['sce_Q'][n0][n2]
                    - post_state[n1]['v_Q'][n0][1][n2]
                    )
                plotable_abs[n1][~reg_map_sel[n1][n0]] == np.nan
                plotable_rel[n1][inv_reg_maps[n1][n0] == i] = (
                    prior_state[n1]['sce_Q'][n0][n2]
                    - post_state[n1]['v_Q'][n0][1][n2]
                    ) / prior_state[n1]['v_Q'][n0][n2]
            plotable_abs[n1][reg_map_sel[n1][n0]] *= inv_Q_dist[n1][n0]
        row = -1
        row_max = 3
        col_cnt = len(idirs)
        fs = figsize[f'Qmap_diff_{rdate}']
        figs[f'Qmap_diff_{rdate}'].clf()
        ax_abs_bar = False
        ax_rel_bar = False
        for s in range(dim_sAll):
            if all([np.isnan(m[s]).all() for m in plotable_abs]):
                continue
            row += 1
            row_name = False
            for n1, m in enumerate(plotable_abs):
                if np.isnan(m[s]).all():
                    continue
                name_qch4_couple.plot.geographical(
                    fig=figs[f'Qmap_diff_{rdate}'],
                    label=f'{n1}_sector{s}',
                    idata=m[s].copy(),
                    grid=grid_sel,
                    texts=[
                        {  # row name
                            'x': (0.05+0.15*(row%2))/fs[0],
                            'y': (fs[1]-subplot_hgt_Qmap*(row+0.5))/fs[1],
                            's': (
                                f'{sector_names[s][1]} ({sector_names[s][0]})'
                                if s < dim_sAll else 'Total'
                                ),
                            'ha': 'left', 'va': 'center', 'size': 8,
                            'rotation': 90,
                            'transform': figs[f'Qmap_diff_{rdate}'].transFigure
                            } if not row_name else {},
                        {  # colourbar label
                            'x': (subplot_wdt_Qmap*(col_cnt*0+1)+0.5)/fs[0],
                            'y': (fs[1]-subplot_hgt_Qmap*(row_max+1+0.2))/fs[1],
                            's': 'Target - Posterior (kg m$^{-2}$ s$^{-1}$)',
                            'ha': 'center', 'va': 'top', 'size': 8,
                            'transform': figs[f'Qmap_diff_{rdate}'].transFigure
                            } if ax_abs_bar is False else {}
                        ],
                    loc_plot=[
                        (subplot_wdt_Qmap*(col_cnt*0+n1)+0.5)/fs[0],
                        (fs[1]-subplot_hgt_Qmap*(row+1))/fs[1],
                        subplot_wdt_Qmap/fs[0],
                        subplot_hgt_Qmap/fs[1],
                        ],
                    loc_bar=(
                        [
                            (subplot_wdt_Qmap*(col_cnt*0+0.1)+0.5)/fs[0],
                            (fs[1]-subplot_hgt_Qmap*(row_max+1+0.1))/fs[1],
                            subplot_wdt_Qmap*(col_cnt-0.2)/fs[0],
                            subplot_hgt_Qmap*0.1/fs[1],
                            ]
                        if ax_abs_bar is False else
                        False
                        ),
                    bbox=[*grid_sel[0][[0, -1]], *grid_sel[1][[0, -1]]],
                    tick_params={'labelsize': 6, 'pad': 0.05},
                    shapefiles=[
                        '/home/ec5/name_qch4_couple/shape_files/'
                        'CNTR_BN_10M_2016_4326.shp'
                        ],
                    colour='RdYlBu_r',
                    vlim=[
                        ytick_abs[0][0], ytick_abs[0][-1],
                        ytick_abs[0], ytick_abs[1]
                        ],
                    extend='both',
                    )
                if ax_abs_bar is False:
                    ax_abs_bar = figs[f'Qmap_diff_{rdate}'].axes[-1]
                if not row_name:
                    row_name = True
            for n1, m in enumerate(plotable_rel):
                if np.isnan(m[s]).all():
                    continue
                name_qch4_couple.plot.geographical(
                    fig=figs[f'Qmap_diff_{rdate}'],
                    label=f'{n1}_sector{s}',
                    idata=m[s].copy(),
                    grid=grid_sel,
                    texts=[
                        {  # row name
                            'x': (0.05+0.15*(row%2))/fs[0],
                            'y': (fs[1]-subplot_hgt_Qmap*(row+0.5))/fs[1],
                            's': (
                                f'{sector_names[s][1]} ({sector_names[s][0]})'
                                if s < dim_sAll else 'Total'
                                ),
                            'ha': 'left', 'va': 'center', 'size': 8,
                            'rotation': 90,
                            'transform': figs[f'Qmap_diff_{rdate}'].transFigure
                            } if not row_name else {},
                        {  # colourbar label
                            'x': (subplot_wdt_Qmap*(col_cnt*1+1)+0.5)/fs[0],
                            'y': (fs[1]-subplot_hgt_Qmap*(row_max+1+0.2))/fs[1],
                            's': '(Target - Posterior) / Prior',
                            'ha': 'center', 'va': 'top', 'size': 8,
                            'transform': figs[f'Qmap_diff_{rdate}'].transFigure
                            } if ax_rel_bar is False else {}

                        ],
                    loc_plot=[
                        (subplot_wdt_Qmap*(col_cnt*1+n1)+0.5)/fs[0],
                        (fs[1]-subplot_hgt_Qmap*(row+1))/fs[1],
                        subplot_wdt_Qmap/fs[0],
                        subplot_hgt_Qmap/fs[1],
                        ],
                    loc_bar=(
                        [
                            (subplot_wdt_Qmap*(col_cnt*1+0.1)+0.5)/fs[0],
                            (fs[1]-subplot_hgt_Qmap*(row_max+1+0.1))/fs[1],
                            subplot_wdt_Qmap*(col_cnt-0.2)/fs[0],
                            subplot_hgt_Qmap*0.1/fs[1],
                            ]
                        if ax_rel_bar is False else
                        False
                        ),
                    bbox=[*grid_sel[0][[0, -1]], *grid_sel[1][[0, -1]]],
                    tick_params={'labelsize': 6, 'pad': 0.05},
                    shapefiles=[
                        '/home/ec5/name_qch4_couple/shape_files/'
                        'CNTR_BN_10M_2016_4326.shp'
                        ],
                    colour='RdYlBu_r',
                    vlim=[
                        ytick_rel[0][0], ytick_rel[0][-1],
                        ytick_rel[0], ytick_rel[1]
                        ],
                    extend='both',
                    )
                if ax_rel_bar is False:
                    ax_rel_bar = figs[f'Qmap_diff_{rdate}'].axes[-1]
                if not row_name:
                    row_name = True
        if ax_abs_bar is not False:
            ax_abs_bar.set_xticklabels(
                [
                    #f'{(np.sign(x)*10):+2.0f}'
                    f'{(np.sign(x)*10):2.0f}'
                    f'$^{{{np.log10(np.abs(x)):+03.0f}}}$'
                    if x != 0. else
                    '0.0'
                    for x in ytick_abs[1]
                    ]
                )
        if ax_rel_bar is not False:
            ax_rel_bar.set_xticklabels(
                [f'{x:3.2f}' for x in ytick_rel[1]]
                )

print('    Save Figures')
for k, fig in figs.items():
    fig.savefig(os.path.join(odir, f'post_{k}.jpeg'))



subplot_wdt_Qmap = 1.0

fs = (
        6*subplot_wdt_Qmap + 1*subplot_xmargin,
        5*subplot_hgt_Qmap + 0.5
        )
figs['set0_state_Qmap'] = plt.figure(figsize=fs, dpi=300)
axs['set0_state_Qmap'] = {}
figs['set0_state_Qmap'].clf()
ax_rel_bar = False
for n0, scode in enumerate(scodes1):
    for n1, c in enumerate([f'{scode}{i}' for i in scodes2]):
        plotable_rel[:] = np.nan
        for n2, rdate in enumerate(rdates):
            for n3, reg in enumerate(
                    np.unique(inv_reg_maps[c][n2][reg_map_sel[c][n2]])
                    ):
                plotable_rel[n2][inv_reg_maps[c][n2] == reg] = (
                    prior_state[c]['sce_Q'][n2][n3]
                    - post_state[c]['v_Q'][n2][1][n3]
                    ) / np.maximum(prior_state[c]['v_Q'][n2][n3], 1.e-12)
        for n4, s in enumerate(s_sel):
            if s < dim_sAll:
                m = np.nansum(plotable_rel[:, s], axis=0)/plotable_rel.shape[0]
            else:
                m = np.nansum(plotable_rel, axis=1).mean(0)
            m[m == 0] = np.nan
            if np.isnan(m).all():
                continue
            sk = f'{c}_map_{s}'
            axs['set0_state_Qmap'][sk] = name_qch4_couple.plot.geographical(
                fig=figs[f'set0_state_Qmap'],
                label=sk,
                idata=m.copy(),
                grid=grid_sel,
                texts=[
                    {  # colourbar label
                        'x': (
                            subplot_xmargin
                            + 3.0*subplot_wdt_Qmap
                            ) / fs[0],
                        'y': (subplot_gap) / fs[1],
                        's': '(Target - Posterior) / Prior',
                        'ha': 'center', 'va': 'bottom', 'size': fontsize,
                        'transform': figs[f'set0_state_Qmap'].transFigure
                        } if ax_rel_bar is False else {}

                    ],
                loc_plot=[
                    (
                        subplot_xmargin
                        + (2*n0+n1)*subplot_wdt_Qmap
                        ) / fs[0],
                    (fs[1] - (n4+1)*subplot_hgt_Qmap) / fs[1],
                    subplot_wdt_Qmap/fs[0],
                    subplot_hgt_Qmap/fs[1],
                    ],
                loc_bar=(
                    [
                        (
                            subplot_xmargin
                            + 1.0*subplot_wdt_Qmap
                            + subplot_gap
                            ) / fs[0],
                        (0.4 - 1*subplot_gap) / fs[1],
                        (4*subplot_wdt_Qmap - 2*subplot_gap)/fs[0],
                        subplot_hgt_Qmap*0.1/fs[1],
                        ]
                    if ax_rel_bar is False else
                    False
                    ),
                bbox=[-12., 3., 45., 65],
                tick_params={'labelsize': 6, 'pad': 0.05},
                shapefiles=[
                    '/home/ec5/name_qch4_couple/shape_files/'
                    'CNTR_BN_10M_2016_4326.shp'
                    ],
                colour='RdYlBu_r',
                vlim=[
                    ytick_rel[0][0], ytick_rel[0][-1],
                    ytick_rel[0], ytick_rel[1]
                    ],
                extend='both',
                )
            if ax_rel_bar is False:
                ax_rel_bar = figs[f'set0_state_Qmap'].axes[-1]
    if ax_rel_bar is not False:
        ax_rel_bar.set_xticklabels(
            [f'{x:3.2f}' for x in ytick_rel[1]]
            )
axs['set0_state_Qmap'][0] = name_qch4_couple.plot.generic2(
    fig=figs['set0_state_Qmap'],
    idata={},
    texts=[
        *[
            {
                'x': 0.5*subplot_xmargin / fs[0],
                'y': (fs[1] - (p+0.5)*subplot_hgt_Qmap + subplot_gap) / fs[1],
                's': f'{sector_names[s][0]}',
                'ha': 'center', 'va': 'center', 'size': fontsize, 'rotation': 90
                }
            for p, s in enumerate(s_sel)
            ],
        *[
            {
                'x': (
                    1*subplot_xmargin
                    + (i*2+j)*subplot_wdt_Qmap
                    + 2*subplot_gap
                    ) / fs[0],
                'y': (fs[1] - 2*subplot_gap) / fs[1],
                's': f'{alphabet[i]}{j+1})',
                'ha': 'left', 'va': 'top', 'size': fontsize,
                'fontweight': 'bold',
                'bbox': {
                    'boxstyle': 'round', 'pad': 0.1,
                    'facecolor': '#ffffff', 'edgecolor': '#ffffff',
                    }
                }
            for i in range(len(scodes1))
            for j in range(len(scodes2))
            ],
        ],
    )
figs['set0_state_Qmap'].savefig(os.path.join(odir, 'set0_state_Qmap.png'))

fk = 'set0_state_Qmap_abs'
fs = (
        6*subplot_wdt_Qmap + 3*subplot_xmargin,
        5*subplot_hgt_Qmap
        )

figs[fk] = plt.figure(figsize=fs, dpi=300)
axs[fk] = {}

figs[fk].clf()
ax_abs_bar = [False for _ in s_sel]
for n0, scode in enumerate(scodes1):
    for n1, c in enumerate([f'{scode}{i}' for i in scodes2]):
        plotable_abs[:] = np.nan
        for n2, rdate in enumerate(rdates):
            for n3, reg in enumerate(
                    np.unique(inv_reg_maps[c][n2][reg_map_sel[c][n2]])
                    ):
                plotable_abs[n2][inv_reg_maps[c][n2] == reg] = (
                    prior_state[c]['sce_Q'][n2][n3]
                    - post_state[c]['v_Q'][n2][1][n3]
                    )
            #plotable_abs[n2][reg_map_sel[c][n0]] *= inv_Q_dist[c][n0]
        for n4, s in enumerate(s_sel):
            if s < dim_sAll:
                m = np.nansum(plotable_abs[:, s], axis=0)/plotable_abs.shape[0]
            else:
                m = np.nansum(plotable_abs, axis=1).mean(0)
            m[m == 0] = np.nan
            if np.isnan(m).all():
                continue
            sk = f'{c}_map_{s}'
            axs[fk][sk] = name_qch4_couple.plot.geographical(
                fig=figs[fk],
                label=sk,
                idata=m.copy(),
                grid=grid_sel,
                texts=[
                    {  # colourbar label
                        'x': (
                            2*subplot_xmargin
                            + 6*subplot_wdt_Qmap
                            + 1*subplot_gap
                            ) / fs[0],
                        'y': (fs[1] - (len(s_sel)/2)*subplot_hgt_Qmap) / fs[1],
                        's': 'Target - Posterior (g m$^{-2}$ s$^{-1}$)',
                        'ha': 'left', 'va': 'center', 'size': fontsize,
                        'rotation': 90,
                        'transform': figs[fk].transFigure
                        } if not any(ax_abs_bar) else {}
                    ],
                loc_plot=[
                    (
                        subplot_xmargin
                        + (2*n0+n1)*subplot_wdt_Qmap
                        ) / fs[0],
                    (fs[1] - (n4+1)*subplot_hgt_Qmap) / fs[1],
                    subplot_wdt_Qmap/fs[0],
                    subplot_hgt_Qmap/fs[1],
                    ],
                loc_bar=(
                    [
                        (
                            subplot_xmargin
                            + 6.0*subplot_wdt_Qmap
                            + subplot_gap
                            ) / fs[0],
                        (fs[1] - (n4+1)*subplot_hgt_Qmap + subplot_gap) / fs[1],
                        subplot_wdt_Qmap*0.05/fs[0],
                        (1*subplot_hgt_Qmap - 2*subplot_gap)/fs[1],
                        ]
                    if ax_abs_bar[n4] is False else
                    False
                    ),
                bbox=[-12., 3., 45., 65],
                tick_params={'labelsize': 6, 'pad': 0.05},
                shapefiles=[
                    '/home/ec5/name_qch4_couple/shape_files/'
                    'CNTR_BN_10M_2016_4326.shp'
                    ],
                colour='RdYlBu_r',
                vlim=[
                    ytick_abs[n4][0][0], ytick_abs[n4][0][-1],
                    ytick_abs[n4][0], ytick_abs[n4][1]
                    ],
                extend='both',
                )
            if ax_abs_bar[n4] is False:
                ax_abs_bar[n4] = figs[fk].axes[-1]
            if ax_abs_bar[n4] is not False:
                ax_abs_bar[n4].set_yticklabels(
                    [
                        f'{(np.sign(x)*10):+2.0f}'
                        f'$^{{{np.log10(np.abs(x)):+03.0f}}}$'
                        if x != 0. else
                        '0.0'
                        for x in ytick_abs[n4][1]
                        ]
                    )
axs[fk][0] = name_qch4_couple.plot.generic2(
    fig=figs[fk],
    idata={},
    texts=[
        *[
            {
                'x': 0.5*subplot_xmargin / fs[0],
                'y': (fs[1] - (p+0.5)*subplot_hgt_Qmap + subplot_gap) / fs[1],
                's': f'{sector_names[s][0]}',
                'ha': 'center', 'va': 'center', 'size': fontsize, 'rotation': 90
                }
            for p, s in enumerate(s_sel)
            ],
        *[
            {
                'x': (
                    1*subplot_xmargin
                    + (i*2+j)*subplot_wdt_Qmap
                    + 2*subplot_gap
                    ) / fs[0],
                'y': (fs[1] - 2*subplot_gap) / fs[1],
                's': f'{alphabet[i]}{j+1})',
                'ha': 'left', 'va': 'top', 'size': fontsize,
                'fontweight': 'bold',
                'bbox': {
                    'boxstyle': 'round', 'pad': 0.1,
                    'facecolor': '#ffffff', 'edgecolor': '#ffffff',
                    }
                }
            for i in range(len(scodes1))
            for j in range(len(scodes2))
            ],
        ],
    )
figs[fk].savefig(os.path.join(odir, f'{fk}.png'))

fk = 'set0_state_Qmap_FFabs'
fs = (
        2*subplot_wdt_Qmap + 2.5*subplot_xmargin,
        1*subplot_hgt_Qmap
        )

figs[fk] = plt.figure(figsize=fs, dpi=300)
axs[fk] = {}

figs[fk].clf()
ax_abs_bar = [False for _ in s_sel]
for n0, scode in enumerate(scodes1):
    if n0:
        continue
    for n1, c in enumerate([f'{scode}{i}' for i in scodes2]):
        plotable_abs[:] = np.nan
        for n2, rdate in enumerate(rdates):
            for n3, reg in enumerate(
                    np.unique(inv_reg_maps[c][n2][reg_map_sel[c][n2]])
                    ):
                plotable_abs[n2][inv_reg_maps[c][n2] == reg] = (
                    prior_state[c]['sce_Q'][n2][n3]
                    - post_state[c]['v_Q'][n2][1][n3]
                    )
            #plotable_abs[n2][reg_map_sel[c][n0]] *= inv_Q_dist[c][n0]
        for n4, s in enumerate(s_sel):
            if n4:
                continue
            if s < dim_sAll:
                m = np.nansum(plotable_abs[:, s], axis=0)/plotable_abs.shape[0]
            else:
                m = np.nansum(plotable_abs, axis=1).mean(0)
            m[m == 0] = np.nan
            if np.isnan(m).all():
                continue
            sk = f'{c}_map_{s}'
            axs[fk][sk] = name_qch4_couple.plot.geographical(
                fig=figs[fk],
                label=sk,
                idata=m.copy(),
                grid=grid_sel,
                texts=[
                    {  # colourbar label
                        'x': (
                            2*subplot_xmargin
                            + 2*subplot_wdt_Qmap
                            + 0*subplot_gap
                            ) / fs[0],
                        'y': (fs[1] - (1/2)*subplot_hgt_Qmap) / fs[1],
                        's': 'Target - Posterior\n(g m$^{-2}$ s$^{-1}$)',
                        'ha': 'left', 'va': 'center', 'size': fontsize,
                        'ma': 'center',
                        'rotation': 90,
                        'transform': figs[fk].transFigure
                        } if not any(ax_abs_bar) else {}
                    ],
                loc_plot=[
                    (
                        subplot_xmargin
                        + (2*n0+n1)*subplot_wdt_Qmap
                        ) / fs[0],
                    (fs[1] - (n4+1)*subplot_hgt_Qmap) / fs[1],
                    subplot_wdt_Qmap/fs[0],
                    subplot_hgt_Qmap/fs[1],
                    ],
                loc_bar=(
                    [
                        (
                            subplot_xmargin
                            + 2*subplot_wdt_Qmap
                            + subplot_gap
                            ) / fs[0],
                        (fs[1] - 1*subplot_hgt_Qmap + subplot_gap) / fs[1],
                        0.1/fs[0],
                        (1*subplot_hgt_Qmap - 2*subplot_gap)/fs[1],
                        ]
                    if ax_abs_bar[n4] is False else
                    False
                    ),
                bbox=[-12., 3., 45., 65],
                tick_params={'labelsize': 6, 'pad': 0.05},
                shapefiles=[
                    '/home/ec5/name_qch4_couple/shape_files/'
                    'CNTR_BN_10M_2016_4326.shp'
                    ],
                colour='RdYlBu_r',
                vlim=[
                    -1.e-8, 1.e-8,
                    np.concatenate([
                        - np.power(10, np.linspace(-8, -9, 5)),
                        np.power(10, np.linspace(-9, -8, 5)),
                        ]),
                    np.concatenate([
                        - np.power(10, np.linspace(-8, -9, 2)),
                        np.power(10, np.linspace(-9, -8, 2)),
                        ])
                    ],
                extend='both',
                )
            if ax_abs_bar[n4] is False:
                ax_abs_bar[n4] = figs[fk].axes[-1]
            if ax_abs_bar[n4] is not False:
                ax_abs_bar[n4].set_yticklabels(
                    [
                        f'${{{(np.sign(x)*10):+2.0f}}}$'
                        f'$^{{{np.log10(np.abs(x)):+03.0f}}}$'
                        if x != 0. else
                        '0.0'
                        for x in np.concatenate([
                            - np.power(10, np.linspace(-8, -9, 2)),
                            np.power(10, np.linspace(-9, -8, 2)),
                            ])
                        ]
                    )
axs[fk][0] = name_qch4_couple.plot.generic2(
    fig=figs[fk],
    idata={},
    texts=[
        *[
            {
                'x': 0.5*subplot_xmargin / fs[0],
                'y': (fs[1] - (p+0.5)*subplot_hgt_Qmap + subplot_gap) / fs[1],
                's': f'{sector_names[s][0]}',
                'ha': 'center', 'va': 'center', 'size': fontsize, 'rotation': 90
                }
            for p, s in enumerate(s_sel)
            if not p
            ],
        *[
            {
                'x': (
                    1*subplot_xmargin
                    + (i*2+j)*subplot_wdt_Qmap
                    + 2*subplot_gap
                    ) / fs[0],
                'y': (fs[1] - 2*subplot_gap) / fs[1],
                's': f'{alphabet[j]})',
                'ha': 'left', 'va': 'top', 'size': fontsize,
                'fontweight': 'bold',
                'bbox': {
                    'boxstyle': 'round', 'pad': 0.1,
                    'facecolor': '#ffffff', 'edgecolor': '#ffffff',
                    }
                }
            for i in range(len(scodes1))
            for j in range(len(scodes2))
            if not i
            ],
        ],
    )
figs[fk].savefig(os.path.join(odir, f'{fk}.png'))

subplot_wdt_Qmap = 2.1






                            's': (
                                'Prior (g m$^{-2}$ s$^{-1}$)'
                                if pn == 0 else
                                'Target - Prior\n(g m$^{-2}$ s$^{-1}$)'
                                if pn == 1 else
                                'Target - Posterior\n(g m$^{-2}$ s$^{-1}$)'
                                ),
                            'ha': 'left', 'va': 'center', 'size': fontsize,
                            'ma': 'center',
                            'rotation': 90,
                            'transform': figs[fk_s].transFigure
                            }
"""


