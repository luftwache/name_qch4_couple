r"""

Radon/CH4 Forward Model

"""
# Standard Library imports
import argparse
import gzip
import matplotlib.dates as mdates
import matplotlib.pyplot as plt
import netCDF4
import numpy as np
import os
import pandas as pd
import sys
import xarray as xr

# Third party imports
from collections import OrderedDict
from datetime import datetime
from sklearn import linear_model

# Semi-local imports
import name_qch4_couple.io
import name_qch4_couple.name
import name_qch4_couple.plot

# Local imports
import routines


# Argument Parser
parser = argparse.ArgumentParser()
parser.add_argument("-site", required=True)
parser.add_argument("-species", required=True)
parser.add_argument("-date", required=True)
parser.add_argument("-odir", required=True)
args = parser.parse_args()

site = args.site
species = args.species
date = args.date
odir = args.odir

site1 = site.replace('_', '-')

# Dates
dates_tHour = pd.date_range(
    pd.to_datetime(date),
    pd.to_datetime(date) + pd.DateOffset(months=1),
    closed='left',
    freq='1H'
    )
date_nodash = datetime.strptime(date, '%Y-%m').strftime('%Y%m')

# Grid
grid_info = routines.define_grid()
inv_reg_map0 = grid_info['inv_reg_map']
nlat = grid_info['nlat']
nlon = grid_info['nlon']
area = grid_info['area']
grid_centre = grid_info['grid_centre']
grid_vertex = grid_info['grid_vertex']
inv_reg_uniq = grid_info['inv_reg_uniq']

# Standard atmospheric conditions
p_std = 1013.25
T_std = 15

# Species
if species == 'ch4':
    from chem_ch4 import read_Q, read_obs, read_baseline
    name="chi_CH4"
    Dfile = (
        f'/home/ec5/hpc-work/data_archive/decc/EUROPE_UKV_{site}/'
        f'{site1}_UKV_EUROPE_{date_nodash}.nc'
        )
    Q2obs = 1.e9 / 16.043  # M_CH4 (g mol-1) - IUPAC
    ylabel = u'$\chi$ CH$_{4}$ (nmol mol$^{-1}$)'
    ylim = [1800., 2500.]
    yticks = np.arange(1800., 2600., 100.)
elif species == '222rn':
    from chem_222rn import read_Q, read_obs, read_baseline
    name = 'C_222Rn'
    Dfile = (
        f'/home/ec5/hpc-work/data_archive/decc/EUROPE_UKV_{site}/'
        f'{site1}_UKV_rn_EUROPE_{date_nodash}.nc'
        )
    Q2obs = (p_std * 100.) / 8.314 / (273.15 + T_std)
    ylabel = u'$^{222}$Rn Activity Concentration (mBq m$^{-3}$)'
    ylim = [0., 8000.]
    yticks = np.arange(0., 8000., 1000.)

# Fooprints
print('Read Footprints')
with xr.open_dataset(Dfile) as ds_read:
    with ds_read.load() as Din:
        D = Din.fp.transpose('time', 'lat', 'lon').values
        mean_age_xr = (
            (
                Din.particle_locations_n * Din.mean_age_particles_n
                ).sum(('height', 'lon'))
            + (
                Din.particle_locations_s * Din.mean_age_particles_s
                ).sum(('height', 'lon'))
            + (
                Din.particle_locations_e * Din.mean_age_particles_e
                ).sum(('height', 'lat'))
            + (
                Din.particle_locations_w * Din.mean_age_particles_w
                ).sum(('height', 'lat'))
            )
        mean_age = np.array(mean_age_xr)
        T = Din.temperature.values
        p = Din.pressure.values
        ws = Din.wind_speed.values
        wd = Din.wind_direction.values
        wu = ws * np.sin(np.deg2rad(wd))
        wv = ws * np.cos(np.deg2rad(wd))
        pblh = Din.PBLH.values
        rel_lat = Din.release_lat.values
        rel_lon = Din.release_lon.values
        rel_px = (
            np.argmin(np.abs(grid_centre[1][:, np.newaxis]-rel_lat), axis=0),
            np.argmin(np.abs(grid_centre[0][:, np.newaxis]-rel_lon), axis=0),
            )

# Emissions
print('Read Emissions')
if read_Q(dates_tHour).ndim == 3:
    Q = read_Q(dates_tHour).sum(0)
else:
    Q = read_Q(dates_tHour)

# Baseline
print('Read Baseline')
base, var_base = read_baseline(dates_tHour, site)

# Observations
print('Read Observations')
obs, sigma_obs = read_obs(dates_tHour, site)

# Calculate
print('Calculate')
mod = pd.Series(
    base.values + np.nansum(Q * D, axis=(1, 2)) * Q2obs,
    index=dates_tHour
    )

# Save
pd.concat([
    pd.Series(obs.values, index=dates_tHour, name=f'{name}(obs)'),
    pd.Series(mod.values, index=dates_tHour, name=f'{name}(mod)'),
    ], axis=1).to_csv(os.path.join(odir, f'{site}-{species}-{date}.csv'))

# Plot
fig = {}
ax = {}
pobjs = {}

colours = {
    'obs': '#000000',
    'mo1': '#8888FF',
    'bas': '#886600',
    'mo2': '#FF8800',
    }
fig_param = {
    'mw': 6, 'mh': 5,
    'mpw': 5.5, 'mph': 4.5,
    'mgap': 0.05,
    'mlmargin': 0.5, 'mbmargin': 0.5,
    'ylblx': 0.05, 'ylbly': 1.5,  # left, centre aligned
    'fontsize': 8,
    }
plt.close('all')

fig['main'] = plt.figure(figsize=(fig_param['mw'], fig_param['mh']), dpi=300)
ax['main'] = {}
pobjs['main'] = {}
name_qch4_couple.plot.generic3(
    fig=fig['main'],
    axs=ax['main'],
    pobjs=pobjs['main'],
    new_axs={
        'date1': [
            dict(
                rect=[
                    (1 * fig_param['mlmargin']
                        + fig_param['mgap'])
                    / fig_param['mw'],
                    (fig_param['mh']
                        - fig_param['mph']
                        + fig_param['mgap'])
                    / fig_param['mh'],
                    (fig_param['mpw']
                        - 2*fig_param['mgap'])
                    / fig_param['mw'],
                    (fig_param['mph']
                        - 2*fig_param['mgap'])
                    / fig_param['mh']
                    ],
                label='date1',
                projection=None
                ),
            {
                #set_yticks=[[], {}],
                "set_yticks": [[yticks], {}],
                "set_xlim": [[
                    dates_tHour[0] - pd.to_timedelta('1 day'),
                    dates_tHour[-1] + pd.to_timedelta('1 day')
                    ], {}],
                "set_ylim": [[ylim], {}],
                "tick_params": [[], dict(
                    axis='both', which='major', direction='in',
                    labelsize=fig_param['fontsize'],
                    left=True, bottom=True,
                    right=False, top=False,
                    labelleft=True, labelbottom=True,
                    labelright=False, labeltop=False,
                    )],
                "xaxis.set_major_locator": [
                    [mdates.WeekdayLocator(byweekday=6)], {}
                    ],
                "xaxis.set_major_formatter": [
                    [mdates.DateFormatter('%Y-%m-%d')], {}
                    ],
                },
            dict(
                patch_alpha=0.0
                )
            ]
        },
    new_pobjs={
        'obs': [
            'date1', 'plot', [obs.index, np.array(obs), '-'],
            {'c': colours['obs'], 'lw': 0.5, 'label': 'Measured'}
            ],
        'mod': [
            'date1', 'plot', [mod.index, np.array(mod), '-'],
            {'c': colours['mo1'], 'lw': 0.5, 'label': 'Modelled'}
            ],
        'bas': [
            'date1', 'plot', [base.index, np.array(base), '-'],
            {'c': colours['bas'], 'lw': 0.5, 'label': 'Baseline'}
            ],
        'legend':[
            'date1', 'legend', [],
            dict(
                loc='upper right',
                numpoints=2, fontsize=fig_param['fontsize'], ncol=3,
                markerscale=5.0/3.5, handletextpad=0.2, columnspacing=1.0,
                borderpad=0.2, borderaxespad=0.2
                )
            ]
        },
    texts=[
        {
            'x': fig_param['mgap'] / fig_param['mw'],
            'y': (fig_param['mh']
                    - 1/2*fig_param['mph'])
                / fig_param['mh'],
            's': ylabel,
            'ha': 'left', 'va': 'center',
            'size': fig_param['fontsize'], 'rotation': 90
            }
        ],
    legend_params=[
        [],
        [],
        {}
        ]
    )
fig['main'].savefig(os.path.join(odir, f'{site}-{species}-{date}.png'))
