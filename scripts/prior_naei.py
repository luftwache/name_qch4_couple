import argparse
import os
import re
import name_qch4_couple.prior


parser = argparse.ArgumentParser()
parser.add_argument("-idir", required=True)
args = parser.parse_args()
idir = args.idir
file_regex = 'naei_[0-9]{4}_([A-Za-z0-9]+)_f2\.nc$'
ifiles = [f for f in os.listdir(idir) if re.match(file_regex, f)]
codes = set([re.match(file_regex, f)[1] for f in ifiles])
for code in codes:
    a, b = name_qch4_couple.prior.naei_aggregate_nc(
            idir,
            rf'naei_year_{code}_f2\.nc$',
            [2017, 2017],
            os.path.join(idir, f'naei_{code}_f.nc')
            )

