r"""

Radon/CH4 Forward Model

"""
# Standard Library imports
import argparse
import gzip
import matplotlib.dates as mdates
import matplotlib.pyplot as plt
import netCDF4
import numpy as np
import os
import pandas as pd
import sys
import xarray as xr

# Third party imports
from collections import OrderedDict
from datetime import datetime
from sklearn import linear_model

# Semi-local imports
import name_qch4_couple.io
import name_qch4_couple.name
import name_qch4_couple.plot

# Local imports
import routines


# Argument Parser
parser = argparse.ArgumentParser()
parser.add_argument("-date", required=True)
parser.add_argument("-Qtype", required=True, type=int)
parser.add_argument("-odir", required=True)
args = parser.parse_args()

date = args.date
Qtype = args.Qtype
odir = args.odir

# Dates
dates_tHour = pd.date_range(
    pd.to_datetime(date),
    pd.to_datetime(date) + pd.DateOffset(months=1),
    closed='left',
    freq='1H'
    )

# Grid
grid_info = routines.define_grid()
inv_reg_map0 = grid_info['inv_reg_map']
nlat = grid_info['nlat']
nlon = grid_info['nlon']
area = grid_info['area']
grid_centre = grid_info['grid_centre']
grid_vertex = grid_info['grid_vertex']
inv_reg_uniq = grid_info['inv_reg_uniq']

# Standard atmospheric conditions
p_std = 1013.25
T_std = 15

# Q - 222Rn
if Qtype in [0]:
    '''
    '''
    # European emissions
    Qfile = '/home/ec5/hpc-work/data_archive/rn_flux/noah.nc'
    with xr.open_dataset(Qfile) as ds_read:
        with ds_read.load() as Qin:
            t = pd.to_datetime(date, format='%Y-%m')
            t_Q = pd.to_datetime(np.array(Qin.time), format='%Y%m%d')
            tin = min(t_Q[t_Q.month==t.month], key=lambda x: abs(x - t))
        Q_Rn = np.array(Qin.rn_flux.sel(time=tin.strftime('%Y%m%d')))
    # Global emissions
    Qfile = '/home/ec5/hpc-work/data_archive/rn_flux_zhang2011/zhang2011_E3.nc'
    with xr.open_dataset(Qfile) as ds_read:
        with ds_read.load() as Qin:
            t = pd.to_datetime(date, format='%Y-%m')
            Q_Rn[~np.isfinite(Q_Rn)] = (
                np.array(Qin.rnemis[t.month-1])[~np.isfinite(Q_Rn)]
                * 1.e4*np.log(2)/3.822/86400*1e3
                )
elif Qtype in [1]:
    '''
    '''
    # Land-water/ocean emissions
    lwfile = '/home/ec5/hpc-work/data_archive/islscp_ii/land_mask.nc'
    with xr.open_dataset(lwfile) as ds_read:
        lwin = ds_read.load()
    Q_land = np.array(lwin.lo_land)/100 * 1.e4*np.log(2)/3.822/86400*1e3  # 1 atom cm-2 s-1
    Q_ocean = np.array(lwin.lo_ocean)/100 * 0.0382

    # European emissions
    Qfile = '/home/ec5/hpc-work/data_archive/rn_flux/noah.nc'
    with xr.open_dataset(Qfile) as ds_read:
        Qin = ds_read.load()
        t = pd.to_datetime(date, format='%Y-%m')
        t_Q = pd.to_datetime(np.array(Qin.time), format='%Y%m%d')
        tin = min(t_Q[t_Q.month==t.month], key=lambda x: abs(x - t))
    Q_Rn = np.array(Qin.rn_flux.sel(time=tin.strftime('%Y%m%d')))

    Q_Rn[~np.isfinite(Q_Rn)] = Q_land[~np.isfinite(Q_Rn)] + Q_ocean[~np.isfinite(Q_Rn)]
elif Qtype in [2]:
    '''
    '''
    # Global emissions
    Qfile = '/home/ec5/hpc-work/data_archive/rn_flux_zhang2011/zhang2011_E3.nc'
    with xr.open_dataset(Qfile) as ds_read:
        Qin = ds_read.load()
        t = pd.to_datetime(date, format='%Y-%m')
    Q_Rn = np.array(Qin.rnemis[t.month-1]) * 1.e4*np.log(2)/3.822/86400*1e3

# Dilution matrix - 222Rn
date_nodash = datetime.strptime(date, '%Y-%m').strftime('%Y%m')
Dfile_Rn = (
    '/home/ec5/hpc-work/data_archive/decc/EUROPE_UKV_HFD_100magl/'
    f'HFD-100magl_UKV_rn_EUROPE_{date_nodash}.nc'
    )
with xr.open_dataset(Dfile_Rn) as ds_read:
    with ds_read.load() as Din:
        D_Rn = Din.fp.transpose('time', 'lat', 'lon').values
        mean_age_xr = (
            (
                Din.particle_locations_n * Din.mean_age_particles_n
                ).sum(('height', 'lon'))
            + (
                Din.particle_locations_s * Din.mean_age_particles_s
                ).sum(('height', 'lon'))
            + (
                Din.particle_locations_e * Din.mean_age_particles_e
                ).sum(('height', 'lat'))
            + (
                Din.particle_locations_w * Din.mean_age_particles_w
                ).sum(('height', 'lat'))
            )
        mean_age = np.array(mean_age_xr)
        T = Din.temperature.values
        p = Din.pressure.values
        ws = Din.wind_speed.values
        wd = Din.wind_direction.values
        pblh = Din.PBLH.values
        rel_lat = Din.release_lat.values
        rel_lon = Din.release_lon.values
        rel_px = (
            np.argmin(np.abs(grid_centre[1][:, np.newaxis]-rel_lat), axis=0),
            np.argmin(np.abs(grid_centre[0][:, np.newaxis]-rel_lon), axis=0),
            )

# Observations - 222Rn
io_date_range = [
    pd.to_datetime(date) - pd.to_timedelta('30min'),
    pd.to_datetime(date) + pd.DateOffset(months=1),
    #pd.to_datetime(date) + pd.tseries.offsets.MonthEnd(0) + pd.to_timedelta('1d')
    ]
obs_Rn_file = '/home/ec5/hpc-work/data_archive/decc/obs/hfd_radon.dat'
obs_Rn_raw = (
    pd.read_csv(
        obs_Rn_file,
        sep='\t', skipinitialspace=True,
        parse_dates={'datetime': ['date', 'time']},
        date_parser=lambda x: datetime.strptime(x, '%y%m%d %H%M'),
        index_col='datetime'
        ).loc[io_date_range[0]:io_date_range[1]]
    if os.path.exists(obs_Rn_file) else
    pd.Series(np.nan, index=dates_tHour)
    )
obs_Rn_deconv = pd.read_csv(
    '/hpc-work/ec5/data_archive/decc/obs/'
    #'hfd_decRn_STP_202007-202104.csv',
    'Heathfield_RT_corrected_radon_30min_preliminary.csv',
    sep=',', skipinitialspace=True,
    #date_parser=lambda x: pd.to_datetime(x, format='%d/%m/%Y %H:%M'),
    parse_dates={'Datetime': ['time']},
    index_col='Datetime'
    ).loc[io_date_range[0]:io_date_range[1]]
obs_Rn_deconv_flag = pd.read_csv(
    '/hpc-work/ec5/data_archive/decc/obs/'
    'hfd_decRn_STP_202007-202104.csv',
    #'Heathfield_RT_corrected_radon_30min_preliminary.csv',
    sep=',', skipinitialspace=True,
    date_parser=lambda x: pd.to_datetime(x, format='%d/%m/%Y %H:%M'),
    parse_dates={'Datetime': ['date']},
    index_col='Datetime'
    ).loc[io_date_range[0]:io_date_range[1]].isnull().any(1)
obs_Rn_deconv.loc[obs_Rn_deconv_flag] = np.nan
obs_Rn = (
    obs_Rn_deconv['radon_deconv_emcee'].loc[date].rename()
    * (273.15 + obs_Rn_deconv['temp'].shift(-1, '30min').loc[date]).rename()
    / obs_Rn_deconv['press'].shift(-1, '30min').loc[date].rename()
    * p_std
    / (273.15 + T_std) 
    ).resample('1H').mean() * 1.e3
'''
obs_Rn_decon_file = (
    '/home/ec5/hpc-work/data_archive/decc/obs/'
    f'hfd_radon_decon30min_{date_nodash}.csv'
    )
obs_Rn_decon = (
    pd.read_csv(
        obs_Rn_decon_file,
        sep=',', skipinitialspace=True,
        parse_dates=['date'],
        date_parser=lambda x: datetime.strptime(x, '%d/%m/%Y %H:%M'),
        index_col='date'
        ).loc[date].resample('1H').mean()
    if os.path.exists(obs_Rn_decon_file) else
    pd.DataFrame(np.nan, index=dates_tHour, columns=['deconvRn'])
    )
'''

# =============================================================================
#   CH4
# =============================================================================
M_CH4 = 16.043  # g mol-1 - IUPAC
# Q - CH4
Qfiles_CH4 = OrderedDict([
    (0, [
        ['/home/ec5/name_qch4_couple/priors/prior_edgar_v6_0_SNAP01.nc', 'CH4_emissions', '1M'],
        ['/home/ec5/name_qch4_couple/priors/prior_edgar_v6_0_SNAP02.nc', 'CH4_emissions', '1M'],
        ['/home/ec5/name_qch4_couple/priors/prior_edgar_v6_0_SNAP03.nc', 'CH4_emissions', '1M'],
        ['/home/ec5/name_qch4_couple/priors/prior_edgar_v6_0_SNAP04.nc', 'CH4_emissions', '1M'],
        ['/home/ec5/name_qch4_couple/priors/prior_edgar_v6_0_SNAP05.nc', 'CH4_emissions', '1M'],
        ['/home/ec5/name_qch4_couple/priors/prior_edgar_v6_0_SNAP07.nc', 'CH4_emissions', '1M'],
        ['/home/ec5/name_qch4_couple/priors/prior_edgar_v6_0_SNAP08.nc', 'CH4_emissions', '1M'],
        ]),
    (1, [
        ['/home/ec5/name_qch4_couple/priors/prior_edgar_v6_0_SNAP09.nc', 'CH4_emissions', '1M'],
        ]),
    (2, [
        ['/home/ec5/name_qch4_couple/priors/prior_edgar_v6_0_SNAP10.nc', 'CH4_emissions', '1M'],
        ]),
    (3, [
        ['/home/ec5/name_qch4_couple/priors/prior_gfed.nc', 'CH4_emissions', '1M'],
        ]),
    (4, [
        ['/home/ec5/name_qch4_couple/priors/prior_wetcharts.nc', 'CH4_emissions', '1M']
        ]),
    ])
Q_CH4 = np.zeros((nlat, nlon))
for s, vs in Qfiles_CH4.items():
    for v in vs:
        with xr.open_dataset(v[0]) as ds_read:
            with ds_read.load() as Q_in:
                t_Q = Q_in['time']
                if v[2] == '1Y':
                    t = np.datetime64(
                        dates_tHour[0].floor('d').replace(month=1, day=1)
                        )
                    t_in = min(t_Q, key=lambda x: abs(x - t))
                else:
                    t = np.datetime64(
                        dates_tHour[0].floor('d').replace(day=1)
                        )
                    t_in = min(
                        t_Q[t_Q.dt.month==dates_tHour[0].month],
                        key=lambda x: abs(x - t)
                        )
                Q_CH4 += Q_in[v[1]].sel(time=t_in).values * 1.e3  # kg -> g

# Dilution matrix - CH4
Dfile_CH4 = (
    '/home/ec5/hpc-work/data_archive/decc/EUROPE_UKV_HFD_100magl/'
    f'HFD-100magl_UKV_EUROPE_{date_nodash}.nc'
    )
with xr.open_dataset(Dfile_CH4) as ds_read:
    with ds_read.load() as Din:
        D_CH4 = Din.fp.transpose('time', 'lat', 'lon').values

# Baseline - CH4
'''
chi0_CH4 = pd.concat(
    [
        pd.read_hdf('baseline/rtm/mhd_all.hdf', 'chi').loc[date],
        name_qch4_couple.io.r_agage(
            '/home/ec5/hpc-work/data_archive/decc/obs/mhd.csv'
            ).loc[date].resample('1H').mean().reindex(dates_tHour)
        ],
    axis=1
    ).min(1)
'''
if pd.to_datetime(date) < pd.to_datetime('2020-07'):
    bmonth = '2020-07'
    bflag = 1
elif pd.to_datetime(date) > pd.to_datetime('2020-12'):
    bmonth = '2020-12'
    bflag = 2
else:
    bmonth = date
    bflag = 0
m_sta = (pd.to_datetime(bmonth)).date().strftime('%Y%m%d')
m_end = (
    pd.to_datetime(bmonth)+pd.tseries.offsets.MonthEnd(0)
    ).date().strftime('%Y%m%d')
chi0file_CH4 = (
    '/home/ec5/hpc-work/data_archive/decc/EUROPE_UKV_HFD_100magl/Pos_CH4/'
    'H1_C_MHT1T2R1ANH1B2CBWB_ch4_OBUSEXL_4h_Fnc10_'
    f'{m_sta}-{m_end}_average_f.gz'
    )
with gzip.open(chi0file_CH4, mode='r') as chi0in:
    chi0_CH4_0 = pd.read_csv(
        chi0in, sep=' ', skipinitialspace=True, skiprows=[5], header=4,
        parse_dates={'datetime': ['YYYY', 'MM', 'DD', 'HH', 'MI']},
        date_parser=lambda x: datetime.strptime(x, '%Y %m %d %H %M'),
        index_col='datetime'
        )['BasePos']
if bflag == 1:
    chi0_CH4 = pd.Series(chi0_CH4_0.iloc[-1], index=dates_tHour)
elif bflag == 2:
    chi0_CH4 = pd.Series(chi0_CH4_0.iloc[0], index=dates_tHour)
else:
    chi0_CH4 = chi0_CH4_0.reindex(dates_tHour).interpolate(
        method='time', limit_direction='both'
        )

# Observations - CH4
obs_CH4 = name_qch4_couple.io.r_decc(
    '/home/ec5/hpc-work/data_archive/decc/obs/hfd.csv'
    ).loc[date].resample('1H').mean().reindex(dates_tHour)

mod_Rn = pd.Series(
    np.nansum(D_Rn * Q_Rn, axis=(1, 2))
    * (p_std * 100.) / 8.314 / (273.15 + T_std),
    index=dates_tHour
    )

mod_CH4 = pd.Series(
    chi0_CH4.values + (D_CH4 * Q_CH4).sum((1, 2)) / M_CH4 * 1e9,
    index=dates_tHour
    )
"""
# Save
name_qch4_couple.io.w_nc(
    dim={
        'time': [
            (dates_tHour - dates_tHour[0]).astype('timedelta64[s]'),
            None, np.float64,
            {
                'units': f'seconds since {dates_tHour[0]}',
                'long_name': 'time',
                'calendar': 'gregorian',
                }
            ],
        'lat': [
            grid_centre[1], grid_centre[1].size, np.float32,
            {
                'units': 'degrees_north',
                'long_name': 'latitude'
                }
            ],
        'lon': [
            grid_centre[0], grid_centre[0].size, np.float32,
            {
                'units': 'degrees_east',
                'long_name': 'longitude'
                }
            ],
        },
    var={
        'rho_222Rn': [
            mod_Rn, ('time'), np.float64,
            {
                'units': 'mBq m-3',
                'long_name': '222Rn activity concentration'
                }
            ],
        'chi_CH4': [
            mod_CH4, ('time'), np.float64,
            {
                'units': 'ppb',
                'long_name': 'CH4 mixing ratio'
                }
            ],
        },
    ofile=os.path.join(odir, f'{date}_{Qtype}.nc')
    )

pd.concat([
    pd.Series(mod_Rn, index=dates_tHour, name='rho_222Rn'),
    pd.Series(mod_CH4, index=dates_tHour, name='chi_CH4')
    ], axis=1).to_csv(f'{date}_{Qtype}.csv')
"""
# Plots
fig = {}
ax = {}
colours = {
    'obs': '#000000',
    'mo1': '#8888FF',
    'bas': '#886600',
    'mo2': '#FF8800',
    }
fig_param = {
    'w': 6, 'h': 3,
    'px0': 0.80, 'py0': 0.50,
    'pw': 5.15, 'ph': 2.45,
    'ylblx': 0.05, 'ylbly': 1.5,  # left, centre aligned
    'fontsize': 8,
    }
plt.close('all')

# Concentration
fig['main'] = plt.figure(figsize=(fig_param['w'], fig_param['h']), dpi=300)
for i in ['Rn', 'CH4']:
    fig['main'].clf()
    ax['main'] =  name_qch4_couple.plot.generic(
        fig=fig['main'],
        idata={
            'obs': [
                'line',
                [obs_Rn.index, np.array(obs_Rn), '-']
                if i == 'Rn' else
                [obs_CH4.index, np.array(obs_CH4.iloc[:, 0]), '-'],
                {'c': colours['obs'], 'lw': 0.5, 'label': 'Measured'}
                ],
            'mod': [
                'line',
                [obs_Rn.index, np.array(mod_Rn), '-']
                if i == 'Rn' else
                [mod_CH4.index, np.array(mod_CH4), '-'],
                {'c': colours['mo1'], 'lw': 0.5, 'label': 'Modelled'}
                ],
            **({'bas': [
                'line',
                [chi0_CH4.index, np.array(chi0_CH4), '-'],
                {'c': colours['bas'], 'lw': 0.5, 'label': 'Baseline'}
                ]} if i == 'CH4' else {}),
            },
        texts=[
            {
                'x': fig_param['ylblx'] / fig_param['w'],
                'y': fig_param['ylbly'] / fig_param['h'],
                's': (
                    u'$^{222}$Rn Activity Concentration (mBq m$^{-3}$)'
                    if i == 'Rn' else
                    u'$\chi$ CH$_{4}$ (nmol mol$^{-1}$)'
                    ),
                'ha': 'left', 'va': 'center',
                'size': fig_param['fontsize'], 'rotation': 90
                }
            ],
        xlim=[
            pd.to_datetime(date), 
            pd.to_datetime(date) + pd.DateOffset(months=1), 
            ],
        ylim=(
            [0., 8000.]
            if i == 'Rn' else
            [1800., 2500.]
            ),
        yticks=(
            np.arange(0., 9000., 1000.)
            if i == 'Rn' else
            np.arange(1800., 2600., 100.)
            ),
        tick_fontsize=fig_param['fontsize'],
        loc_plot=[
            fig_param['px0'] / fig_param['w'],
            fig_param['py0'] / fig_param['h'],
            fig_param['pw'] / fig_param['w'],
            fig_param['ph'] / fig_param['h']
            ],
        xtick_params=[
            True,
            mdates.DateFormatter('%Y-%m-%d'),
            mdates.WeekdayLocator(byweekday=6),
            ]
        )
    for l in ax['main'].get_xticklabels():
        l.set_ha("right")
        l.set_rotation(30)
    ax['main'].legend(
        loc='upper right', ncol=4, fontsize=fig_param['fontsize']
        )
    fig['main'].savefig(os.path.join(odir, f'{date}_{i}_{Qtype}.png'))
"""
# Mean particle age
fig['age'] = plt.figure(figsize=(6, 3), dpi=300)
ax['age'] =  name_qch4_couple.plot.generic(
    fig=fig['age'],
    idata={
        'age': [
            'line',
            [obs_Rn.index, np.array(mean_age), '-'],
            {'c': '#000000', 'lw': 0.5}
            ],
        },
    texts=[
        {
            'x': 0.02, 'y':.5,
            's': u'Mean age of Particles (hr)',
            'ha': 'left', 'va': 'center', 'size': 6, 'rotation': 90
            }
        ],
    xlim=[
        pd.to_datetime(date), 
        pd.to_datetime(date) + pd.DateOffset(months=1), 
        ],
    ylim=[0., 750.],
    yticks=np.arange(0, 750., 100.),
    tick_fontsize=6,
    loc_plot=[0.1, 0.1, 0.85, 0.85],
    xtick_params=[
        True,
        mdates.DateFormatter('%Y-%m-%d'),
        mdates.WeekdayLocator(byweekday=6),
        ]
    )
fig['age'].savefig(os.path.join(odir, f'{date}_age_{Qtype}.png'))
"""

# =============================================================================
# Further Analysis - Scaling GHG enhancements with obs-mod Rn ratio
# =============================================================================
Rn_cols = ['Rn_stp']
Rn_data = [obs_Rn]
grid_gb_eng = np.full((nlat, nlon), False)
grid_gb_eng[
    grid_info['inv_lat0']:grid_info['inv_lat1'],
    grid_info['inv_lon0']:grid_info['inv_lon1']
    ][np.isin(
        inv_reg_map0,
        tuple([
            grid_info['inv_reg_uniq'][n]
            for n, i in enumerate(grid_info['inv_reg_codes'])
            if i.startswith('gb_eng')
            ])
        )] = True

mod_Rn_enh = mod_Rn 
mod_Rn_enh_gb_eng = pd.Series(
    np.nansum((D_Rn * Q_Rn)[..., grid_gb_eng], axis=(1))
    * (p_std * 100.) / 8.314 / (273.15 + T_std),
    index=dates_tHour
    )
mod_CH4_enh = pd.Series(
    (D_CH4 * Q_CH4).sum((1, 2)) / M_CH4 * 1e9,
    index=dates_tHour
    )
mod_CH4_enh_gb_eng = pd.Series(
    (D_CH4 * Q_CH4)[..., grid_gb_eng].sum(1) / M_CH4 * 1e9,
    index=dates_tHour
    )

threshold = 0.8
dom_gb_eng = (
    np.isfinite(obs_Rn)
    & ((mod_Rn_enh_gb_eng / mod_Rn_enh) > threshold)
    & np.isfinite(obs_CH4.iloc[:, 0])
    & ((mod_CH4_enh_gb_eng / mod_CH4_enh) > threshold)
    )

grid_mulfacs = [np.ones((dom_gb_eng.sum(), nlat, nlon)) for Rn_col in Rn_cols]
for n, Rn_col in enumerate(Rn_cols):
    grid_mulfacs[n][:, grid_gb_eng] = np.array(
        Rn_data[n].loc[dom_gb_eng] / mod_Rn.loc[dom_gb_eng]
        )[:, np.newaxis]
mod_CH4_Rn = [
    pd.Series(
        chi0_CH4[dom_gb_eng].values
        + (D_CH4[dom_gb_eng] * grid_mulfac * Q_CH4).sum((1, 2)) / M_CH4 * 1e9,
        index=dates_tHour[dom_gb_eng]
        )
    for grid_mulfac in grid_mulfacs
    ]

# Wind Catetory
wu = ws * np.sin(np.deg2rad(wd))
wv = ws * np.cos(np.deg2rad(wd))

# Octant Categories
bearing = np.arctan2(  # arctan(lat / lon)
    np.tile((grid_centre[0] - rel_lon[:, np.newaxis])[:, np.newaxis], (1, nlat, 1)),
    np.tile((grid_centre[1] - rel_lat[:, np.newaxis])[:, :, np.newaxis], (1, 1, nlon))
    )
octant = np.zeros(bearing.shape, dtype='int32')
for i in range(8):
    if i < 4:
        ang0, ang1 = 0. + np.pi/4 * np.array([i, i+1])
    else:
        ang0, ang1 = np.pi/4 * (np.array([i, i+1]) - 8)
    octant[(ang0 <= bearing) & (bearing < ang1)] = i

octcat_Rn = pd.DataFrame(
    0., index=dates_tHour, columns=[f'ac_222Rn[{i}]' for i in range(8)]
    )
for n, QD in enumerate(Q_Rn * D_Rn):
    octcat_Rn.iloc[n] = np.bincount(
        octant[n, grid_gb_eng], QD[grid_gb_eng], minlength=8
        )
octcat_CH4 = pd.DataFrame(
    0., index=dates_tHour, columns=[f'chi_CH4[{i}]' for i in range(8)]
    )
for n, QD in enumerate(Q_CH4 * D_CH4):
    octcat_CH4.iloc[n] = np.bincount(
        octant[n, grid_gb_eng], QD[grid_gb_eng], minlength=8
        )
octcat_CH4_Rn = [
    pd.DataFrame(
        0., index=dates_tHour, columns=[f'chi_CH4_{l}[{i}]' for i in range(8)]
        )
    for l in Rn_cols
    ]
for i, Rn_col in enumerate(Rn_cols):
    for n, QD in enumerate((D_CH4[dom_gb_eng] * grid_mulfacs[i] * Q_CH4)):
        l = dates_tHour.get_loc(dates_tHour[dom_gb_eng][n])
        octcat_CH4_Rn[i].loc[dates_tHour[dom_gb_eng][n]] = np.bincount(
            octant[l, grid_gb_eng], QD[grid_gb_eng], minlength=8
            )

# Save data
pd.concat([
    pd.Series(mod_Rn, index=dates_tHour, name='ac_222Rn'),
    pd.Series(mod_CH4, index=dates_tHour, name='chi_CH4'),
    *[
        pd.Series(mod_CH4_Rn[n], index=dates_tHour, name=f'chi_CH4_{i}')
        for n, i in enumerate(Rn_cols)
        ],
    octcat_Rn, octcat_CH4, *octcat_CH4_Rn,
    pd.Series(wd, index=dates_tHour, name='wind_direction'),
    pd.Series(ws, index=dates_tHour, name='wind_speed'),
    pd.Series(wu, index=dates_tHour, name='x_wind'),
    pd.Series(wv, index=dates_tHour, name='y_wind'),
    pd.Series(T, index=dates_tHour, name='T'),
    pd.Series(p, index=dates_tHour, name='p'),
    pd.Series(pblh, index=dates_tHour, name='pblh'),
    ], axis=1).to_csv(os.path.join(odir, f'{date}_mod{Qtype}.csv'))
pd.concat([
    pd.Series(obs_CH4.iloc[:, 0], name='obs_CH4'),
    #pd.Series(obs_Rn, name='obs_222Rn'),
    *[
        pd.Series(Rn_data[n], name=f'obs_{i}')
        for n, i in enumerate(Rn_cols)
        ],
    ], axis=1).to_csv(os.path.join(odir, f'{date}_obs.csv')) 

plt.close('all')
fig['CH4_Rn1'] = plt.figure(figsize=(fig_param['w'], fig_param['h']), dpi=300)
fig['CH4_Rn2'] = plt.figure(figsize=(fig_param['w'], fig_param['h']), dpi=300)
fig['CH4_Rn3'] = plt.figure(figsize=(fig_param['w'], fig_param['h']), dpi=300)
for n, i in enumerate(mod_CH4_Rn):
    '''
    '''
    # Unlabelled
    k = f'CH4_{Rn_cols[n]}'
    fig['CH4_Rn1'].clf()
    ax['CH4_Rn1'] =  name_qch4_couple.plot.generic(
        fig=fig['CH4_Rn1'],
        idata={
            'obs': [
                'line',
                [obs_CH4.index[dom_gb_eng], np.array(obs_CH4)[dom_gb_eng], 'o'],
                {'c': colours['obs'], 'ms': 1.5, 'mew': 0.}
                ],
            'mod_CH4': [
                'line',
                [mod_CH4.index[dom_gb_eng], np.array(mod_CH4)[dom_gb_eng], 'o'],
                {'c': colours['mo2'], 'ms': 1.5, 'mew': 0.}
                ],
            'mod_CH4_Rn': [
                'line',
                [mod_CH4.index[dom_gb_eng], np.array(i), 'o'],
                {'c': colours['mo1'], 'ms': 1.5, 'mew': 0.}
                ],
            'chi0_CH4': [
                'line',
                [chi0_CH4.index[dom_gb_eng], np.array(chi0_CH4)[dom_gb_eng], 'o'],
                {'c': colours['bas'], 'ms': 1.5, 'mew': 0.}
                ],
            },
        texts=[
            {
                'x': fig_param['ylblx'] / fig_param['w'],
                'y': fig_param['ylbly'] / fig_param['h'],
                's': u'$\chi$ CH$_{4}$ (nmol mol$^{-1}$)',
                'ha': 'left', 'va': 'center', 'size': 6, 'rotation': 90
                }
            ],
        xlim=[
            pd.to_datetime(date), 
            pd.to_datetime(date) + pd.DateOffset(months=1), 
            ],
        ylim=[1800., 2500.],
        yticks=np.arange(1800., 2600., 100.),
        tick_fontsize=6,
        loc_plot=[0.1, 0.1, 0.85, 0.85],
        xtick_params=[
            True,
            mdates.DateFormatter('%Y-%m-%d'),
            mdates.WeekdayLocator(byweekday=6),
            ]
        )
    fig['CH4_Rn1'].savefig(os.path.join(odir, f'{date}_{k}1_{Qtype}.png'))

    # Labelled
    fig['CH4_Rn2'].clf()
    ax['CH4_Rn2'] =  name_qch4_couple.plot.generic(
        fig=fig['CH4_Rn2'],
        idata={
            'Measured': [
                'line',
                [obs_CH4.index[dom_gb_eng], np.array(obs_CH4)[dom_gb_eng, 0], 'o'],
                {'c': colours['obs'], 'ms': 2.5, 'mew': 0., 'label': 'Measured'}
                ],
            'Initial': [
                'line',
                [mod_CH4.index[dom_gb_eng], np.array(mod_CH4)[dom_gb_eng], 'o'],
                {'c': colours['mo2'], 'ms': 2.5, 'mew': 0., 'label': 'Initial'}
                ],
            'Corrected': [
                'line',
                [mod_CH4.index[dom_gb_eng], np.array(i), 'o'],
                {'c': colours['mo1'], 'ms': 2.5, 'mew': 0., 'label': 'Corrected'}
                ],
            'Baseline': [
                'line',
                [chi0_CH4.index[dom_gb_eng], np.array(chi0_CH4)[dom_gb_eng], 'o'],
                {'c': colours['bas'], 'ms': 2.5, 'mew': 0., 'label': 'Baseline'}
                ],
            },
        texts=[
            {
                'x': fig_param['ylblx'] / fig_param['w'],
                'y': fig_param['ylbly'] / fig_param['h'],
                's': u'$\chi$ CH$_{4}$ (nmol mol$^{-1}$)',
                'ha': 'left', 'va': 'center',
                'size': fig_param['fontsize'], 'rotation': 90, 'label': ''
                }
            ],
        xlim=[
            pd.to_datetime(date), 
            pd.to_datetime(date) + pd.DateOffset(months=1), 
            ],
        ylim=[1900., 2100.],
        yticks=np.arange(1900., 2150., 50.),
        tick_fontsize=fig_param['fontsize'],
        loc_plot=[
            fig_param['px0'] / fig_param['w'],
            fig_param['py0'] / fig_param['h'],
            fig_param['pw'] / fig_param['w'],
            fig_param['ph'] / fig_param['h']
            ],
        xtick_params=[
            True,
            mdates.DateFormatter('%Y-%m-%d'),
            mdates.WeekdayLocator(byweekday=6),
            ]
        )
    for l in ax['CH4_Rn2'].get_xticklabels():
        l.set_ha("right")
        l.set_rotation(30)
    ax['CH4_Rn2'].legend(
        loc='upper right', ncol=4, fontsize=fig_param['fontsize']
        )
    fig['CH4_Rn2'].savefig(os.path.join(odir, f'{date}_{k}2_{Qtype}.png'))

fig['CH4_Rn3'].clf()
ax['CH4_Rn3'] =  name_qch4_couple.plot.generic(
    fig=fig['CH4_Rn3'],
    idata={
        'hline': ['hline', [0.0], {'c': '#000000'}],
        'Initial': [
            'line',
            [
                mod_CH4.index[dom_gb_eng],
                np.array(mod_CH4-obs_CH4.iloc[:, 0])[dom_gb_eng],
                'o'
                ],
            {'c': colours['obs'], 'ms': 3.5, 'mew': 0., 'label': 'Initial'}
            ],
        **{
            Rn_cols[n]: [
                'line',
                [
                    mod_CH4.index[dom_gb_eng],
                    np.array(i-obs_CH4.iloc[:, 0])[dom_gb_eng],
                    'o'
                    ],
                {'c': colours[f'mo{n+1}'], 'ms': 3.5, 'mew': 0., 'label': Rn_cols[n]}
                ]
            for n, i in enumerate(mod_CH4_Rn)
            },
        },
    texts=[
        {
            'x': fig_param['ylblx'] / fig_param['w'],
            'y': fig_param['ylbly'] / fig_param['h'],
            's': u'$\chi$ CH$_{4}$ (nmol mol$^{-1}$)',
            'ha': 'left', 'va': 'center',
            'size': fig_param['fontsize'], 'rotation': 90, 'label': ''
            }
        ],
    xlim=[
        pd.to_datetime(date), 
        pd.to_datetime(date) + pd.DateOffset(months=1), 
        ],
    ylim=[-100., 100.],
    yticks=np.arange(-100., 150., 50.),
    tick_fontsize=fig_param['fontsize'],
    loc_plot=[
        fig_param['px0'] / fig_param['w'],
        fig_param['py0'] / fig_param['h'],
        fig_param['pw'] / fig_param['w'],
        fig_param['ph'] / fig_param['h']
        ],
    xtick_params=[
        True,
        mdates.DateFormatter('%Y-%m-%d'),
        mdates.WeekdayLocator(byweekday=6),
        ]
    )
for l in ax['CH4_Rn3'].get_xticklabels():
    l.set_ha("right")
    l.set_rotation(30)
ax['CH4_Rn3'].legend(
    loc='upper right', ncol=4, fontsize=fig_param['fontsize']
    )
fig['CH4_Rn3'].savefig(os.path.join(odir, f'{date}_dCH4_Rn_{Qtype}.png'))

fig['CH4_Rn3'].clf()
ax['CH4_Rn3'] =  name_qch4_couple.plot.generic(
    fig=fig['CH4_Rn3'],
    idata={
        'hline': ['hline', [0.0], {'c': '#000000'}],
        'Initial': [
            'line',
            [
                mod_CH4.index[dom_gb_eng],
                np.array(mod_CH4-obs_CH4.iloc[:, 0])[dom_gb_eng],
                'o'
                ],
            {'c': colours['obs'], 'ms': 3.5, 'mew': 0., 'label': 'Initial'}
            ],
        **{
            Rn_cols[n]: [
                'line',
                [
                    mod_CH4.index[dom_gb_eng],
                    np.array(i-obs_CH4.iloc[:, 0])[dom_gb_eng],
                    'o'
                    ],
                {'c': colours[f'mo{n+1}'], 'ms': 3.5, 'mew': 0., 'label': Rn_cols[n]}
                ]
            for n, i in enumerate(mod_CH4_Rn)
            },
        },
    texts=[
        {
            'x': fig_param['ylblx'] / fig_param['w'],
            'y': fig_param['ylbly'] / fig_param['h'],
            's': u'$\Delta\chi$ CH$_{4}$ (nmol mol$^{-1}$)',
            'ha': 'left', 'va': 'center',
            'size': fig_param['fontsize'], 'rotation': 90, 'label': ''
            }
        ],
    xlim=[
        pd.to_datetime(date), 
        pd.to_datetime(date) + pd.DateOffset(months=1), 
        ],
    ylim=[-100., 100.],
    yticks=np.arange(-100., 150., 50.),
    tick_fontsize=fig_param['fontsize'],
    loc_plot=[
        fig_param['px0'] / fig_param['w'],
        fig_param['py0'] / fig_param['h'],
        fig_param['pw'] / fig_param['w'],
        fig_param['ph'] / fig_param['h']
        ],
    xtick_params=[
        True,
        mdates.DateFormatter('%Y-%m-%d'),
        mdates.WeekdayLocator(byweekday=6),
        ]
    )
for l in ax['CH4_Rn3'].get_xticklabels():
    l.set_ha("right")
    l.set_rotation(30)
ax['CH4_Rn3'].legend(
    loc='upper right', ncol=4, fontsize=fig_param['fontsize']
    )
fig['CH4_Rn3'].savefig(os.path.join(odir, f'{date}_dCH4_Rn_{Qtype}.png'))

for k, v in {
        'wd': [
            'Wind Direction',
            wd, np.arange(0., 361., 45.),
            [f'{i:03.0f}' for i in np.arange(0., 361., 45.)]
            ],
        'ws': [
            'Wind Speed (m s$^{-1}$)',
            ws, np.arange(0., 31., 5.), np.arange(0., 31., 5.)
            ],
        'T': [
            u'Temperature (\u00B0C)',
            T, np.arange(-20., 41., 10.), np.arange(-20., 41., 10.)
            ],
        'p': [
            u'Pressure (hPa)',
            p, np.arange(960., 1041., 10.), np.arange(960., 1041., 10.)
            ],
        'PBLH': [
            u'PBLH (m)',
            pblh, np.arange(0., 5001., 1000.), np.arange(0., 5001., 1000.)
            ],
        }.items():
    fig['CH4_Rn3'].clf()
    ax['CH4_Rn3'] =  name_qch4_couple.plot.generic(
        fig=fig['CH4_Rn3'],
        idata={
            'hline': ['hline', [0.0], {'c': '#000000'}],
            'Initial': [
                'line',
                [
                    v[1][dom_gb_eng],
                    np.array(mod_CH4-obs_CH4.iloc[:, 0])[dom_gb_eng],
                    'o'
                    ],
                {'c': colours['obs'], 'ms': 3.5, 'mew': 0., 'label': 'Initial'}
                ],
            **{
                Rn_cols[n]: [
                    'line',
                    [
                        v[1][dom_gb_eng],
                        np.array(i-obs_CH4.iloc[:, 0])[dom_gb_eng],
                        'o'
                        ],
                    {'c': colours[f'mo{n+1}'], 'ms': 3.5, 'mew': 0., 'label': Rn_cols[n]}
                    ]
                for n, i in enumerate(mod_CH4_Rn)
                },
            },
        texts=[
            {
                'x': fig_param['ylblx'] / fig_param['w'],
                'y': fig_param['ylbly'] / fig_param['h'],
                's': u'$\Delta\chi$ CH$_{4}$ (nmol mol$^{-1}$)',
                'ha': 'left', 'va': 'center',
                'size': fig_param['fontsize'], 'rotation': 90, 'label': ''
                },
            {
                'x': (fig_param['px0'] + 0.5*fig_param['pw']) / fig_param['w'],
                'y': 0.05 / fig_param['h'],
                's': v[0],
                'ha': 'center', 'va': 'bottom',
                'size': fig_param['fontsize'], 'label': ''
                }
            ],
        xlim=[
            v[2][0] - np.diff(v[2][[0, -1]])[0]*0.01,
            v[2][-1] + np.diff(v[2][[0, -1]])[0]*0.01
            ],
        ylim=[-100., 100.],
        yticks=np.arange(-100., 150., 50.),
        tick_fontsize=fig_param['fontsize'],
        loc_plot=[
            fig_param['px0'] / fig_param['w'],
            fig_param['py0'] / fig_param['h'],
            fig_param['pw'] / fig_param['w'],
            fig_param['ph'] / fig_param['h']
            ],
        xtick_params=[False, v[2], v[3]]
        )
    for l in ax['CH4_Rn3'].get_xticklabels():
        l.set_ha("right")
        l.set_rotation(30)
    ax['CH4_Rn3'].legend(
        loc='upper right', ncol=4, fontsize=fig_param['fontsize']
        )
    fig['CH4_Rn3'].savefig(os.path.join(odir, f'{date}_met_{k}_dCH4_Rn_{Qtype}.png'))

# Q
fs = [5, 2.5]
fpd = 0.04
flp = [fpd, 0.24]
flb = [fs[0]-0.70, 0.24]
fbs = [0.1, 1.5]
ffs = 8
fbll = 2.24  # bar label y location
fbul = 1.94  # bar unit y location
fbfs = 6  # bar tick label fontsize
fbpd = 0.05  # pad between bar and tick label
fig['Q'] = plt.figure(figsize=fs, dpi=300)
for species, Q in {'Rn': Q_Rn, 'CH4': Q_CH4}.items():
    Q_plot = Q.copy()
    Q_plot[Q == 0.] = np.nan
    fig['Q'].clf()
    ax['Q'] = name_qch4_couple.plot.geographical(
        fig=fig['Q'],
        idata=Q_plot,
        grid=[grid_vertex[0], grid_vertex[1]],
        texts=[
            {
                'x': (flb[0]-fpd)/fs[0], 'y': (flp[1]-fpd)/fs[1],
                's': f'{date}',
                'ha': 'right', 'va': 'top', 'size': ffs,
                'transform': fig['Q'].transFigure
                },
            {
                'x': flb[0]/fs[0], 'y': fbll/fs[1],
                's': 'Emission',
                'ha': 'left', 'va': 'top', 'size': ffs,
                'transform': fig['Q'].transFigure
                },
            {
                'x': flb[0]/fs[0], 'y': fbul/fs[1],
                's': (
                    '(mBq m${^{-2}}$ s$^{-1}$)'
                    if species == 'Rn' else
                    '(g m${^{-2}}$ s$^{-1}$)'
                    ),
                'ha': 'left', 'va': 'top', 'size': fbfs,
                'transform': fig['Q'].transFigure
                }
            ],
        loc_plot=[
            flp[0]/fs[0], flp[1]/fs[1],
            (flb[0]-flp[0]-fpd)/fs[0], (fs[1]-flp[1]-fpd)/fs[1]
            ],
        loc_bar=[
            flb[0]/fs[0], flb[1]/fs[1], 
            fbs[0]/fs[0], fbs[1]/fs[1]
            ],
        bbox=[
            grid_vertex[0][0], grid_vertex[0][-1],
            grid_vertex[1][0], grid_vertex[1][-1]
            ],
        tick_params={
            'labelsize': fbfs, 'pad': fbpd
            },
        shapefiles=[
            '/home/ec5/name_qch4_couple/shape_files'
            '/CNTR_BN_10M_2016_4326.shp'
            ],
        colour='viridis_r',
        vlim=(
            [
                0.0, 100.0,
                np.array([0.0, 0.1, *np.arange(10, 110, 10)]),
                np.array([0.0, 0.1, *np.arange(10, 110, 10)]),
                ]
            if species == 'Rn' else
            [
                1.e-12, 1.e-5,
                np.power(10, np.linspace(-12, -5, 15)),
                np.power(10, np.linspace(-12, -5, 8)),
                ]
            ),
        extend='max',
        )
    fig['Q']._axstack._elements[1][1][1].set_yticklabels(
        [f'{i:5.1f}' for i in np.array([0.0, 0.1, *np.arange(10, 110, 10)])]
        if species == 'Rn' else
        [f'10$^{{{i:+03.0f}}}$' for i in np.linspace(-12, -5, 8)]
        )
    fig['Q'].savefig(os.path.join(odir, f'{date}_Q_{species}_{Qtype}.png'))

# Footprints for CH4_Rn
fig['fp'] = plt.figure(figsize=fs, dpi=300)
pdata = np.zeros((grid_info['nlat'], grid_info['nlon']))
for n, i in enumerate(dates_tHour[dom_gb_eng]):
    species = 'Rn'
    date_str = i.strftime('%Y%m%d_%H%M%S')
    fname = os.path.join(odir, 'fp', f'D_{species}_{date_str}.png')
    if os.path.exists(fname):
        continue
    fig['fp'].clf()
    pdata[:] = D_Rn[dom_gb_eng][n].copy()
    pdata[pdata == 0] = np.nan
    ax['fp'] = name_qch4_couple.plot.geographical(
        fig=fig['fp'],
        idata=pdata,
        grid=[grid_vertex[0], grid_vertex[1]],
        texts=[
            {
                'x': fpd/fs[0], 'y': (flp[1]-fpd)/fs[1],
                's': f'Release date',
                'ha': 'left', 'va': 'top', 'size': ffs,
                'transform': fig['fp'].transFigure
                },
            {
                'x': (flb[0]-fpd)/fs[0], 'y': (flp[1]-fpd)/fs[1],
                's': f"{i.strftime('%Y-%m-%d %H:%M:%S')}",
                'ha': 'right', 'va': 'top', 'size': ffs,
                'transform': fig['fp'].transFigure
                },
            {
                'x': flb[0]/fs[0], 'y': (fbll)/fs[1],
                's': 'Dilution\nMatrix\n(mol$^{-1}$ m$^{2}$ s)',
                'ha': 'left', 'va': 'top', 'size': ffs,
                'transform': fig['fp'].transFigure
                }
            ],
        loc_plot=[
            flp[0]/fs[0], flp[1]/fs[1],
            (flb[0]-flp[0]-fpd)/fs[0], (fs[1]-flp[1]-fpd)/fs[1]
            ],
        loc_bar=[
            flb[0]/fs[0], flb[1]/fs[1], 
            fbs[0]/fs[0], fbs[1]/fs[1]
            ],
        bbox=[
            grid_vertex[0][0], grid_vertex[0][-1],
            grid_vertex[1][0], grid_vertex[1][-1]
            ],
        tick_params={
            'labelsize': fbfs, 'pad': fbpd
            },
        shapefiles=[
            '/home/ec5/name_qch4_couple/shape_files'
            '/CNTR_BN_10M_2016_4326.shp'
            ],
        colour='viridis_r',
        vlim=[
            1.e-5, 1.e0,
            np.power(10, np.linspace(-5, 0, 11)),
            np.power(10, np.linspace(-5, 0, 6)),
            ],
        extend='both',
        )
    fig['fp'].axes[1].set_yticklabels(
        [f'10$^{{{x:+1.0f}}}$' for x in np.linspace(-5, 0, 6)]
        )
    fig['fp'].savefig(fname)

