import numpy as np
import pandas as pd
from agage_box_model.CH4_table import CH4_table


cat = {
        1: ['Natural_gas', 'Coal', 'Gas_hydrates'],
        2: ['Natural_gas', 'Coal', 'Gas_hydrates'],
        3: ['Natural_gas', 'Coal', 'Gas_hydrates'],
        4: ['Natural_gas', 'Coal', 'Gas_hydrates'],
        5: ['Natural_gas', 'Coal', 'Gas_hydrates'],
        7: ['Natural_gas', 'Coal', 'Gas_hydrates'],
        8: ['Natural_gas', 'Coal', 'Gas_hydrates'],
        9: ['Landfills'],
        10: ['Rice_paddies', 'Ruminants'],
        }


sectors = {
        'Rice_paddies': [110, −63, −320, −528.8, −359.2],
        'Ruminants': [80, −60.5, −330, −549.7, −369.5],
        'Natural_gas': [45, −44, −180, −322.5, −213.7],
        'Coal': [35, −37, −140, −254.8, −169.4],
        'Biomass_burning': [41, −24.6, −225, −394.83, −241.8],
        'Boreal_wetlands': [38, −62, −380, −607.2, −414.87],
        'Tropical_wetlands': [77, −58.9, −360, −582, −394.1],
        'Termites': [16, −63, −390, −620.3, −425],
        'Landfills': [40, −55, −310, −522.5, −346.9],
        'Ocean': [10, −58, −220, −379.1, −260.9],
        'Fresh_water': [4, −53.8, −385, −614.5, −414.8],
        'Gas_hydrates': [4, −62.5, −190, −338.9, −238.4],
        'Geological': [50, −41.8, −200, −355.2, −231.2],
        'AMP': [40, −51.2, −260, −450.7, −296.8],
        }

s = pd.DataFrame.from_dict(sectors, orient='rows', columns=['q', 1, 2, 3, 4]}

R = pd.DataFrame(0, rows=s.rows, columns=s.columns)

d = np.zeros(s.shape)
d[:, 1:] = np.array(s)[:, 1:]
R_std = np.array(CH4_table['R_std'])

R = (d * 1.e-3 + 1) * R_std

A = R / R.sum(axis=1, keepdims=True)

A_pd = pd.DataFrame(A, rows=s.rows, columns=[0, 1, 2, 3, 4])

