#!/bin/bash

set -e

sector=$1
idir=$2
gfile=$3
settaxisfile=$4

tdir=${idir}/${sector}

#: << 'END'
echo "Unzip files"
for f in $(find $idir -name "*${sector}*zip") ; do
    echo $f
    unzip -q -u -d $tdir $f -x *html
done
echo

echo "Process files"
for f in $(find $tdir -name "*${sector}*nc") ; do
    year=$(echo $f | sed -n "s/.*_\([0-9]\{4\}\)_\([0-9]\{1,2\}\).*/\1/p")
    month=$(printf "%02d" $(echo $f | sed -n "s/.*_\([0-9]\{4\}\)_\([0-9]\{1,2\}\).*/\2/p"))
    ofile=${tdir}/${sector}_${year}_${month}.nc
    if [[ ! -f $ofile ]] ; then
        echo $f
        $settaxisfile $f $gfile ${ofile}
    fi
    rm $f
done
#END

echo "Merge files"
#echo $(find $tdir -maxdepth 1 -name "${sector}*nc" | sort)
cdo chname,emi_ch4,CH4_emissions -cat $(find $tdir -maxdepth 1 -name "${sector}*nc" | sort) ${idir}/prior_edgar_v6_0_${sector}.nc
rm -r ${tdir}
echo done
