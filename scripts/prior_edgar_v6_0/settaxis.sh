#!/bin/bash

ifile=$1
gfile=$2
ofile=$3

year=$(echo $ifile | sed -n "s/.*_\([0-9]\{4\}\)_\([0-9]\{1,2\}\).*/\1/p")
month=$(printf "%02d" $(echo $ifile | sed -n "s/.*_\([0-9]\{4\}\)_\([0-9]\{1,2\}\).*/\2/p"))

cdo -s \
    -setreftime,1970-01-01,00:00:00,1days \
    -settaxis,${year}-${month}-01,00:00:00,1days \
    -remapcon,$gfile \
    $ifile $ofile
