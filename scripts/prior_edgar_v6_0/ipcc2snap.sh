#!/bin/bash

prefix0=$1
prefix1=$2

# 01
printf "01\n"
cdo enssum \
        ${prefix0}_ENE.nc \
        ${prefix0}_FFF.nc \
        ${prefix0}_REF_TRF.nc \
    ${prefix1}_SNAP01.nc
# 02
printf "02\n"
cp \
    ${prefix0}_RCO.nc \
    ${prefix1}_SNAP02.nc
# 03
printf "03\n"
cp \
    ${prefix0}_IND.nc \
    ${prefix1}_SNAP03.nc
# 04
printf "04\n"
cdo enssum \
        ${prefix0}_CHE.nc \
        ${prefix0}_IRO.nc \
    ${prefix1}_SNAP04.nc
# 05
printf "05\n"
cp \
    ${prefix0}_PRO.nc \
    ${prefix1}_SNAP05.nc
# 07
printf "07\n"
cp \
    ${prefix0}_TRO_noRES.nc \
    ${prefix1}_SNAP07.nc
# 08
printf "08\n"
suffix=(
        _TNR_Aviation_CDS.nc
        _TNR_Aviation_CRS.nc
        _TNR_Aviation_LTO.nc
        _TNR_Aviation_SPS.nc
        _TNR_Other.nc
        _TNR_Ship.nc
        )
files=()
for i in ${suffix[@]} ; do
    f=${prefix0}${i}
    if [ -f $f ] ; then
        files+=($f)
    fi
done
cdo enssum ${files[@]} \
    ${prefix1}_SNAP08.nc
# 09
printf "09\n"
cdo enssum \
        ${prefix0}_SWD_LDF.nc \
        ${prefix0}_SWD_INC.nc \
        ${prefix0}_WWT.nc \
    ${prefix1}_SNAP09.nc
# 10
printf "10\n"
cdo enssum \
        ${prefix0}_ENF.nc \
        ${prefix0}_MNM.nc \
        ${prefix0}_AWB.nc \
        ${prefix0}_AGS.nc \
    ${prefix1}_SNAP10.nc

