r"""

Forward Model

"""
# Standard Library imports
import gzip
import numpy as np
import pandas as pd
import xarray as xr

# Third party imports
from collections import OrderedDict

# Semi-local imports
import name_qch4_couple.io

# Local imports
import routines


def read_Q(timestamps):
    grid_info = routines.define_grid()
    nlat = grid_info['nlat']
    nlon = grid_info['nlon']
    Qfiles_CH4 = OrderedDict([
        (0, [
            ['/home/ec5/name_qch4_couple/priors/prior_edgar_v6_0_SNAP01.nc', 'CH4_emissions', '1M'],
            ['/home/ec5/name_qch4_couple/priors/prior_edgar_v6_0_SNAP02.nc', 'CH4_emissions', '1M'],
            ['/home/ec5/name_qch4_couple/priors/prior_edgar_v6_0_SNAP03.nc', 'CH4_emissions', '1M'],
            ['/home/ec5/name_qch4_couple/priors/prior_edgar_v6_0_SNAP04.nc', 'CH4_emissions', '1M'],
            ['/home/ec5/name_qch4_couple/priors/prior_edgar_v6_0_SNAP05.nc', 'CH4_emissions', '1M'],
            ['/home/ec5/name_qch4_couple/priors/prior_edgar_v6_0_SNAP07.nc', 'CH4_emissions', '1M'],
            ['/home/ec5/name_qch4_couple/priors/prior_edgar_v6_0_SNAP08.nc', 'CH4_emissions', '1M'],
            ]),
        (1, [
            ['/home/ec5/name_qch4_couple/priors/prior_edgar_v6_0_SNAP09.nc', 'CH4_emissions', '1M'],
            ]),
        (2, [
            ['/home/ec5/name_qch4_couple/priors/prior_edgar_v6_0_SNAP10.nc', 'CH4_emissions', '1M'],
            ]),
        (3, [
            ['/home/ec5/name_qch4_couple/priors/prior_gfed.nc', 'CH4_emissions', '1M'],
            ]),
        (4, [
            ['/home/ec5/name_qch4_couple/priors/prior_wetcharts.nc', 'CH4_emissions', '1M']
            ]),
        ])
    Q_CH4 = np.zeros((len(Qfiles_CH4), nlat, nlon))
    for s, vs in Qfiles_CH4.items():
        for v in vs:
            with xr.open_dataset(v[0]) as ds_read:
                with ds_read.load() as Q_in:
                    t_Q = Q_in['time']
                    if v[2] == '1Y':
                        t = np.datetime64(
                            timestamps[0].floor('d').replace(month=1, day=1)
                            )
                        t_in = min(t_Q, key=lambda x: abs(x - t))
                    else:
                        t = np.datetime64(
                            timestamps[0].floor('d').replace(day=1)
                            )
                        t_in = min(
                            t_Q[t_Q.dt.month==timestamps[0].month],
                            key=lambda x: abs(x - t)
                            )
                    Q_CH4[s] += Q_in[v[1]].sel(time=t_in).values * 1.e3  # kg -> g
    return Q_CH4


def read_obs(timestamps, site, resample='1H'):
    date = timestamps[0].strftime('%Y-%m')
    t0 = timestamps[0].strftime('%Y-%m-%d %H')
    t1 = timestamps[-1].strftime('%Y-%m-%d %H')
    if site == 'HFD_100magl':
        ifile = '/home/ec5/hpc-work/data_archive/decc/obs/hfd-g2401_10.csv'
        col_or_no = 'ch4_c'
        sigma_col_or_no = 'ch4_stdev'
    elif site == 'MHD_10magl':
        ifile = '/home/ec5/hpc-work/data_archive/decc/obs/mhd-md_01.csv'
        col_or_no = 'ch4_c'
        sigma_col_or_no = 0.2
    else:
        ifile = False
        col_or_no = np.nan
        sigma_col_or_no = np.nan
    if ifile:
        all_obs_raw = name_qch4_couple.io.r_decc(ifile).loc[t0:t1]
        obs_raw = all_obs_raw[col_or_no]
        sigma_obs_raw = (all_obs_raw[sigma_col_or_no]
                         if isinstance(sigma_col_or_no, str) else
                         pd.Series(sigma_col_or_no, index=all_obs_raw.index))
    if isinstance(col_or_no, str):
        obs = (obs_raw
               if resample is False else
               obs_raw.resample('1H').mean().reindex(timestamps))
    else:
        obs = pd.Series(col_or_no, index=timestamps)
    if isinstance(sigma_col_or_no, str) or isinstance(col_or_no, str):
        sigma_obs = (
            sigma_obs_raw
            if resample is False else
            sigma_obs_raw.resample('1H').apply(
                lambda x: np.sum(x**2)).reindex(timestamps))
    else:
        sigma_obs = pd.Series(sigma_col_or_no, index=timestamps)
    return obs, sigma_obs


def read_baseline(timestamps, site, btype="default"):
    date = timestamps[0].strftime('%Y-%m')
    year = timestamps[0].strftime('%Y')
    if site == 'HFD_100magl':
        if btype == 'default':
            chi0file = (
                '/home/ec5/hpc-work/data_archive/decc/'
                'baseline/baseline-mhd-ch4-{year}.nc'
                )
            with xr.open_dataset(chi0file) ds_read:
                with ds_read.load() as ds:
                    chi0 = ds.chi_ch4.sel(time=date).to_series()
                    var_chi0 = ds.var_chi_ch4.sel(time=date).to_series()
        elif btype == 'intem':
            if timestamps[0] < pd.to_datetime('2020-07'):
                bmonth = '2020-07'
                bflag = 1
            elif timestamps[0] > pd.to_datetime('2020-12'):
                bmonth = '2020-12'
                bflag = 2
            else:
                bmonth = date
                bflag = 0
            m_sta = (pd.to_datetime(bmonth)).date().strftime('%Y%m%d')
            m_end = (
                pd.to_datetime(bmonth)+pd.tseries.offsets.MonthEnd(0)
                ).date().strftime('%Y%m%d')
            chi0file = (
                '/home/ec5/hpc-work/data_archive/decc/'
                'EUROPE_UKV_HFD_100magl/Pos_CH4/'
                'H1_C_MHT1T2R1ANH1B2CBWB_ch4_OBUSEXL_4h_Fnc10_'
                f'{m_sta}-{m_end}_average_f.gz'
                )
            with gzip.open(chi0file, mode='r') as chi0in:
                chi0_0all = pd.read_csv(
                    chi0in, sep=' ', skipinitialspace=True,
                    skiprows=[5], header=4,
                    parse_dates={'datetime': ['YYYY', 'MM', 'DD', 'HH', 'MI']},
                    #converters={
                    #    'datetime': lambda Y, m, d, H, M:
                    #        pd.to_datetime(f'{Y} {m} {d} {H} {M}',
                    #                       format='%Y %m %d %H %M'),
                    #    },
                    #index_col='datetime'
                    )
                chi0_0all.index = pd.to_datetime(
                        chi0_0all['datetime'], format='%Y %m %d %H %M')
                chi0_0 = chi0_0all['BasePos']
                var_chi0_0 = chi0_0all['BasePos']
            if bflag == 1:
                chi0 = pd.Series(chi0_0.iloc[-1], index=timestamps)
                var_chi0 = pd.Series(var_chi0_0.iloc[-1], index=timestamps)
            elif bflag == 2:
                chi0 = pd.Series(chi0_0.iloc[0], index=timestamps)
                var_chi0 = pd.Series(var_chi0_0.iloc[0], index=timestamps)
            else:
                chi0 = chi0_0.reindex(timestamps).interpolate(
                    method='time', limit_direction='both'
                    )
                var_chi0 = var_chi0_0.reindex(timestamps).interpolate(
                    method='time', limit_direction='both'
                    )
    else:
        chi0 = pd.Series(1900., index=timestamps)
        var_chi0 = pd.Series(0.1, index=timestamps)
    return chi0

