import numpy as np

tau = 11*365.25*86400
dt = 30*86400
chi0 = 1900
d0_1 = -47
d0_2 = -92
KIE_1 = 1.0060
KIE_2 = 1.3208
#KIE = 1.0280
#KIE = 1.4100

for tau in (np.array([0.5, 1.0, 1.5, 2.0]) *11*365.25*86400):
    chi1 = chi0 * np.exp(-dt/tau)
    d1_1 = ((d0_1*1.e-3 + 1) * np.exp(-dt/tau)**(1/KIE_1 - 1) - 1) * 1.e3
    d1_2 = ((d0_2*1.e-3 + 1) * np.exp(-dt/tau)**(1/KIE_2 - 1) - 1) * 1.e3
    print(
        ' '*10 +
        '{:10s}  '.format('before') +
        '{:10s}  '.format('after') +
        '{:10s}  '.format('diff') +
        '{:10s}  '.format('diff ratio')
        )
    print(f'chi       {chi0:10.4f}  {chi1:10.4f}  {chi0-chi1:10.4f}  {(chi0-chi1)/chi1:10.4f}')
    print(f'd_13CH4   {d0_1:10.4f}  {d1_1:10.4f}  {d0_1-d1_1:10.4f}  {(d0_1-d1_1)/d0_1:10.4f}')
    print(f'd_12CH3D  {d0_2:10.4f}  {d1_2:10.4f}  {d0_2-d1_2:10.4f}  {(d0_2-d1_2)/d0_2:10.4f}')






#chi1 = chi0 * (1 - dt/tau)
#d1_1 = ((d0_1*1.e-3 + 1) * (1 - dt/tau)**(1/KIE_1 - 1) - 1) * 1.e3
#d1_2 = ((d0_2*1.e-3 + 1) * (1 - dt/tau)**(1/KIE_2 - 1) - 1) * 1.e3
