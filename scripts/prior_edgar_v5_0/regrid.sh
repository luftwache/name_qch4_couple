#!/bin/bash

prefix0=$1
prefix1=$2

# 01
printf "01\n"
cdo remapcon,name_grid \
    -enssum \
        ${prefix0}_ENE.0.1x0.1.nc \
        ${prefix0}_REF_TRF.0.1x0.1.nc \
    ${prefix1}_SNAP01.nc
# 02
printf "02\n"
cdo remapcon,name_grid \
    ${prefix0}_RCO.0.1x0.1.nc \
    ${prefix1}_SNAP02.nc
# 03
printf "03\n"
cdo remapcon,name_grid \
    ${prefix0}_IND.0.1x0.1.nc \
    ${prefix1}_SNAP03.nc
# 04
printf "04\n"
cdo remapcon,name_grid \
    -enssum \
        ${prefix0}_CHE.0.1x0.1.nc \
        ${prefix0}_IRO.0.1x0.1.nc \
    ${prefix1}_SNAP04.nc
# 05
printf "05\n"
cdo remapcon,name_grid \
    ${prefix0}_PRO.0.1x0.1.nc \
    ${prefix1}_SNAP05.nc
# 07
printf "07\n"
cdo remapcon,name_grid \
    ${prefix0}_TRO.0.1x0.1.nc \
    ${prefix1}_SNAP07.nc
# 08
printf "08\n"
suffix=(
        _TNR_Aviation_CDS.0.1x0.1.nc
        _TNR_Aviation_CRS.0.1x0.1.nc
        _TNR_Aviation_LTO.0.1x0.1.nc
        _TNR_Aviation_SPS.0.1x0.1.nc
        _TNR_Other.0.1x0.1.nc
        _TNR_Ship.0.1x0.1.nc
        )
files=()
for i in ${suffix[@]} ; do
    f=${prefix0}${i}
    if [ -f $f ] ; then
        files+=($f)
    fi
done
cdo remapcon,name_grid \
    -enssum ${files[@]} \
    ${prefix1}_SNAP08.nc
# 09
printf "09\n"
cdo remapcon,name_grid \
    -enssum \
        ${prefix0}_SWD_LDF.0.1x0.1.nc \
        ${prefix0}_SWD_INC.0.1x0.1.nc \
        ${prefix0}_WWT.0.1x0.1.nc \
    ${prefix1}_SNAP09.nc
# 10
printf "10\n"
cdo remapcon,name_grid \
    -enssum \
        ${prefix0}_ENF.0.1x0.1.nc \
        ${prefix0}_MNM.0.1x0.1.nc \
        ${prefix0}_AWB.0.1x0.1.nc \
        ${prefix0}_AGS.0.1x0.1.nc \
    ${prefix1}_SNAP10.nc

