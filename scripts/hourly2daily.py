import argparse
import datetime
import gzip
import matplotlib.pyplot as plt
import matplotlib.dates as mdate
import netCDF4
import numpy as np
import os
import pandas as pd
import time
import sys
import xarray as xr

import name_qch4_couple.io
import name_qch4_couple.name
import name_qch4_couple.plot


def zero2nan(d):
    out = d.copy()
    out[d == 0.] = np.nan
    return out


# Argument Parser
parser = argparse.ArgumentParser()
parser.add_argument("-date", required=True)
parser.add_argument("-tau", required=True, type=float)
parser.add_argument("-half", type=int, default=0)
parser.add_argument("-idir", required=True)
parser.add_argument("-ipat", required=True)
parser.add_argument("-ofile", required=True)
args = parser.parse_args()

date = args.date
if args.tau == 0:
    tau = np.inf
else:
    tau = args.tau
half = args.half
idir = args.idir
if idir is None and Qtype == 9:
    raise Exception('Input directory is required if Qtype is 9.')
ipat = args.ipat
ofile = args.ofile

# Decay constant
# 222Rn halflife = 3.8232 * 24
tau_e = tau/(np.log(2) if half else 1.) if not np.isinf(tau) else tau
l = (np.log(2) if half else 1.)/tau if not np.isinf(tau) else 0.

# Dates
dates_H = pd.date_range(
    f"{date}-01",
    pd.to_datetime(f"{date}-01") + pd.DateOffset(months=1),
    freq='1H',
    closed='left'
    )
dates_d = pd.date_range(
    f"{date}-01",
    pd.to_datetime(f"{date}-01") + pd.DateOffset(months=1),
    freq='1d',
    closed='left'
    )

# Grid
lon0 = -98.07600
lat0 = 10.61200
dlon = 0.3520000
dlat = 0.2340000
nlon = 391
nlat = 293
grid = [
    np.linspace(lon0 + dlon/2, lon0 + dlon*(nlon - 0.5), nlon),
    np.linspace(lat0 + dlat/2, lat0 + dlat*(nlat - 0.5), nlat)
    ]
grid2 = [
    np.linspace(lon0, lon0 + dlon*nlon, nlon+1),
    np.linspace(lat0, lat0 + dlat*nlat, nlat+1)
    ]
area = name_qch4_couple.name.calc_cell_area(
    grid2[1][:-1],
    grid2[1][1:],
    dlon
    )[:, np.newaxis]

f_fields_paths = pd.Series(
    dates_d.strftime(
        os.path.join(idir, ipat)
        ).to_series(index=dates_d).apply(np.vectorize(
            lambda x: x if os.path.exists(x) else False
            ))
    )

dosage0 = np.zeros((dates_H.size, nlat, nlon))
dosage1 = np.zeros((dates_H.size, nlat, nlon))
pt0 = time.process_time()
for n, hfile in enumerate(f_fields_paths):
    if not hfile:
        continue
    else:
        print(f'    {os.path.basename(hfile)}')
    with gzip.open(hfile) as gz:
        with netCDF4.Dataset('dummy', mode='r', memory=gz.read()) as ds:
            # Chung
            dt = ds.ReleaseDurationHours
            """
            t_rs = [
                i.replace('ReleaseTime', '')
                for i in ds.ncattrs()
                if i.startswith('ReleaseTime')
                ]
            """
            fp_parts = [i for i in ds.variables if i.startswith('NAMEdata')]
            for fp_part in fp_parts:
                fp_t = ds[fp_part]
                tstamp = fp_part.replace('NAMEdata_', '')
                t_o, t_r = [int(i) for i in tstamp.split('_')]
                t_t = 720 - (t_r - t_o) + dt/2.0
                x = ds[f'Xindex_{tstamp}'][:]-1
                y = ds[f'Yindex_{tstamp}'][:]-1
                dosage0[n*24+t_r-1, y, x] += fp_t[:] * np.exp(-l*(t_t))
            """
            for t_r in range(24):  # release time
                for t in range(720+1):  # time since final output time
                    tstamp = f'{t+t_r+1:003d}_{t_r+1:02d}'
                    try:
                        t_t = 720 - t + dt/2.0  # travel time
                        x = ds[f'Xindex_{tstamp}'][:]-1
                        y = ds[f'Yindex_{tstamp}'][:]-1
                        dosage0[n*24+t_r, y, x] += (
                            np.array(ds[f'NAMEdata_{tstamp}'])
                            * np.exp(-l*(t_t))
                            )
                    except:
                        print(f'Error on {tstamp}')
            """
            """
            # Ganesan
            lons = np.array(ds.variables["Longitude"][:])
            lats = np.array(ds.variables["Latitude"][:])
            attributes = ds.ncattrs()
            releasetime_str = [s for s in attributes if 'ReleaseTime' in s]
            releasetime = [f.split("ReleaseTime")[1] for f in releasetime_str]
            times = [
                datetime.datetime.strptime(f, '%Y%m%d%H%M')
                for f in releasetime
                ]
            levs = (
                ['From     0 -    40m agl']
                ) # not in the file, not sure if needed, placeholder
            timeStep = ds.getncattr('ReleaseDurationHours')
            for t_r, rtime in enumerate(releasetime):
                rt_dt = datetime.datetime.strptime(rtime, '%Y%m%d%H%M')
                fp_grid = np.zeros((len(lats), len(lons)))
                fields_vars = ds.get_variables_by_attributes(ReleaseTime=rtime)
                outputtime=[]            
                for ii in range(len(fields_vars)):
                    outputtime.append(fields_vars[ii].getncattr('OutputTime'))
                outputtime = list(sorted(set(outputtime)))
                
                for ot in outputtime:
                    data = [
                        f for f in fields_vars
                        if f.getncattr('OutputTime') == ot
                        ]
                    xindex = [
                        f for f in data if 'Xindex' in f.name
                        ][0][:]-1 # Alistair's files index from 1
                    yindex = [
                        f for f in data if 'Yindex' in f.name
                        ][0][:]-1 # Alistair's files index from 1
                    fp_vals = [f for f in data if 'NAMEdata' in f.name][0][:]
                    fp_grid_temp = np.zeros((len(lats), len(lons)))
                    ot_dt = datetime.datetime.strptime(ot, '%Y%m%d%H%M')
                    fp_timedelta_hrs = (
                        (rt_dt - ot_dt).total_seconds()/3600 + timeStep/2
                        ) # average time elapsed in hours
                    # turn this data into a grid
                    for ii in range(len(xindex)):
                        fp_grid_temp[yindex[ii], xindex[ii]]=fp_vals[ii]
                    
                    # add to the total for that release time    
                    fp_gridi += (
                        fp_grid_temp*np.exp(-l*fp_timedelta_hrs)
                        )# lifetime applied
                dosage2[n*24+t_r] = fp_grid
            """

pt1 = time.process_time()
print(pt1-pt0)
dilution_matrix0 = dosage0*area/3600*1e-6

datestr = dates_d[0].strftime('%Y%m')

name_qch4_couple.io.w_nc(
    dim={
        'time': [
            (dates_H - dates_H[0]).astype('timedelta64[s]'),
            None, np.float32,
            {
                'units': f'seconds since {dates_H[0]}',
                'long_name': 'time',
                'calendar': 'gregorian',
                }
            ],
        'lat': [
            grid[1], grid[1].size, np.float32,
            {
                'units': 'degrees_north',
                'long_name': 'latitude',
                }
            ],
        'lon': [
            grid[0], grid[0].size, np.float32,
            {
                'units': 'degrees_east',
                'long_name': 'longitude',
                }
            ]
        },
    var={
        'fp': [
            dilution_matrix0, ('time', 'lat', 'lon'), np.float32,
            {
                'units': '(mol/mol)(s)(m2)/(mol)',
                'long_name': 'dilution matrix',
                'loss_lifetime_hrs': tau_e, 
                }
            ]
        },
    ofile=ofile#os.path.join(odir, f'{1/l:.2f}_{datestr}.nc')
    )

"""
tfile = (
    '/home/ec5/hpc-work/data_archive/decc/EUROPE_UKV_HFD_100magl/Rn/'
    f'HFD-UKV-100magl-rn_EUROPE_{datestr}.nc'
    )
with xr.open_dataset(tfile) as ds_read:
    with ds_read.load() as Din:
        target = np.moveaxis(np.array(Din.fp.sel(time=dates_H)), 2, 0)

fs = [5, 2.5]
fpd = 0.04
flp = [fpd, 0.24]
flb = [fs[0]-0.6, 0.24]
fbs = [0.1, 1.5]
fig = plt.figure(figsize=fs, dpi=300)
for k, v in {
        0: dosage0,
        6: target/area*3600*1e6
        }.items():
    fig.clf()
    name_qch4_couple.plot.geographical(
        fig=fig,
        idata=zero2nan(v[0]),
        grid=[grid2[0], grid2[1]],
        texts=[
            {
                'x': flb[0]/fs[0], 'y': (flb[1]+2)/fs[1],
                's': 'Dosage\n(ppm s)',
                'ha': 'left', 'va': 'top', 'size':8
                }
            ],
        loc_plot=[
            flp[0]/fs[0], flp[1]/fs[1],
            (flb[0]-flp[0]-fpd)/fs[0], (fs[1]-flp[1]-fpd)/fs[1]
            ],
        loc_bar=[
            flb[0]/fs[0], flb[1]/fs[1], 
            fbs[0]/fs[0], fbs[1]/fs[1]
            ],
        bbox=[
            grid2[0][0], grid2[0][-1],
            grid2[1][0], grid2[1][-1]
            ],
        tick_params={
            'labelsize': 6, 'pad': 0.05
            },
        shapefiles=[
            '/home/ec5/name_qch4_couple/shape_files'
            '/CNTR_BN_10M_2016_4326.shp'
            ],
        colour='viridis_r',
        vlim=[
            1.e-4, 1.e0,
            np.power(10, np.linspace(-4, 0, 9)),
            np.power(10, np.linspace(-4, 0, 5)),
            ],
        extend='both',
        )
    fig.savefig(f'dosage{k}.png')
"""

