r"""

Pseudo-inverse Model

"""
# Standard Library imports
import argparse
import datetime
import json
import matplotlib.dates as mdates
import matplotlib.pyplot as plt
import netCDF4
import numpy as np
import os
import pandas as pd
import psutil
import pymc3 as pm
import re
import sys
import theano
import theano.tensor as tt
import xarray as xr

# Third party imports
from collections import OrderedDict

# Semi-local imports
import name_qch4_couple.io
import name_qch4_couple.name
import name_qch4_couple.plot
import name_qch4_couple.region_EU
import name_qch4_couple.routines
import name_qch4_couple.util

# Local imports
import routines


def part18end(n, e, s, w, y, x, h):
    r"""
    """
    lt = np.where(h <= 3.e3)[0]
    ut = np.where((3.e3 < h) & (h <= 8.e3))[0]
    st = np.where(8.e3 < h)[0]
    t = n.shape[0]
    yinc = all(np.diff(y) > 0)
    xinc = all(np.diff(x) > 0)
    ny = y.size
    nx = x.size
    dn = np.s_[-(ny//2):] if yinc else np.s_[:(ny//2)]
    de = np.s_[-(nx//2):] if xinc else np.s_[:(nx//2)]
    ds = np.s_[:(ny//2)] if yinc else np.s_[-(ny//2):]
    dw = np.s_[:(nx//2)] if xinc else np.s_[-(nx//2):]
    o = np.zeros((t, 18))
    f = lambda x: (x.shape, x)
    for i, vl in enumerate([lt, ut]):
        o[:, 8*i+1] = (
            n[:, vl, de].sum((1, 2))
            + (n[:, vl, (nx//2)].sum(1)/2 if nx & 0x1 else 0.)
            )
        o[:, 8*i+2] = (
            e[:, vl, dn].sum((1, 2))
            + (e[:, vl, (ny//2)].sum(1)/2 if ny & 0x1 else 0.)
            )
        o[:, 8*i+3] = (
            e[:, vl, ds].sum((1, 2))
            + (e[:, vl, (ny//2)].sum(1)/2 if ny & 0x1 else 0.)
            )
        o[:, 8*i+4] = (
            s[:, vl, de].sum((1, 2))
            + (s[:, vl, (nx//2)].sum(1)/2 if nx & 0x1 else 0.)
            )
        o[:, 8*i+5] = (
            s[:, vl, dw].sum((1, 2))
            + (s[:, vl, (nx//2)].sum(1)/2 if nx & 0x1 else 0.)
            )
        o[:, 8*i+6] = (
            w[:, vl, ds].sum((1, 2))
            + (w[:, vl, (ny//2)].sum(1)/2 if ny & 0x1 else 0.)
            )
        o[:, 8*i+7] = (
            w[:, vl, dn].sum((1, 2))
            + (w[:, vl, (ny//2)].sum(1)/2 if ny & 0x1 else 0.)
            )
        o[:, 8*i+8] = (
            n[:, vl, dw].sum((1, 2))
            + (n[:, vl, (nx//2)].sum(1)/2 if nx & 0x1 else 0.)
            )
    o[:, -1] = (
        n[:, st].sum((1, 2)) + e[:, st].sum((1, 2))
        + s[:, st].sum((1, 2)) + w[:, st].sum((1, 2))
        )
    return o


def sd2cov(sigma):
    r"""
    """
    cov = np.zeros((sigma.size, sigma.size))
    np.fill_diagonal(cov, sigma.flatten()**2)
    return cov


def check_memory():
    r"""
    """
    process = psutil.Process(os.getpid())
    rss = process.memory_info().rss / 1e6
    rssp = process.memory_percent()
    print(f'{rss:14.3f} MB\t{rssp:14} %')


def calc_mean_var(x):
    r"""Calculate variance of a mean
    """
    if np.array(x).size == 0:
        return np.nan
    elif np.isfinite(x).any():
        return (np.nansum(x)/np.isfinite(x).sum()**2)
    else:
        return np.nan


def regionQA2griddedQ(QA, area, region_grid):
    r"""
    """
    Q = np.zeros((QA.shape[0], *region_grid.shape))
    regions = np.unique(region_grid)
    for i, v in enumerate((QA/area).T):
        n = regions[i]
        Q[..., region_grid == n] = v.reshape(-1, 1)
    return Q


def calc_init_edge(chi0, d0i1, d0i2, chi0f, d0i1f, d0i2f, R_std):
    r"""
    d0i1[nx_tComp_tbSel]
    """
    # d0 (i, E)
    R0i1 = (d0i1 * d0i1f * 1.e-3 + 1) * R_std[1]
    R0i2 = (d0i2 * d0i2f * 1.e-3 + 1) * R_std[2]
    R0sum = 1 + R0i1 + R0i2
    # chi0 (i, E)
    chi0i0 = (chi0 * chi0f * (1. / R0sum))
    chi0i1 = (chi0 * chi0f * (R0i1 / R0sum))
    chi0i2 = (chi0 * chi0f * (R0i2 / R0sum))
    return chi0i0, chi0i1, chi0i2


def calc_init_site(chi0i0, chi0i1, chi0i2, E, idx_bin_site, R_std):
    r"""
    """
    i0 = (chi0i0 * E).sum(-1)
    i1 = (chi0i1 * E).sum(-1)
    i2 = (chi0i2 * E).sum(-1)
    chi0 = i0 + i1 + i2
    d0i1 = (i1 / i0 / R_std[1] - 1) * 1.e3
    d0i2 = (i2 / i0 / R_std[2] - 1) * 1.e3
    return chi0, d0i1, d0i2


def create_h(dim_tComp, idx, hdef, hlower, hupper):
    r"""
    """
    unc = np.concatenate([np.full([1, dim_tComp], i) for i in hdef], axis=0)
    unc[:, idx] = [[hlower], [hupper]]
    return unc


# =============================================================================
#   Settings
#       rmode
#           0: prior only
#           1: inversion
#           2: forward only
#       tmode
#           0: first half of the month
#           1: second half of the month
#           2: all from the month
#           3: daytime only from the month
#       Qtype
#           0: EDGAR + Natural
#           1: EDGAR scaled to UNFCCC + Natural
#           2: EDGAR (monthly scale) + Natural
#           3: EDGAR (monthly scale) scaled to UNFCCC + Natural
#       sce - scenario (pseudo observations)
#           0: Normal
#           1: Anthropogenic +50 % in UK-IE
#           2: Anthropogenic +50 % in West Europe exc. UK-IE
#           3: Anthropogenic -50 % in UK-IE
#           4: Anthropogenic -50 % all regions
# =============================================================================
print('Start Process')

# Argument Parser
parser = argparse.ArgumentParser()
parser.add_argument("-sfile", required=True)
parser.add_argument("-rmode", required=True, type=int)
parser.add_argument("-force", required=True, type=int)
parser.add_argument("-sce", required=True, type=int)
parser.add_argument("-date", required=True)
parser.add_argument("-tmode", required=True, type=int)
parser.add_argument("-Qtype", required=True, type=int)
parser.add_argument("-Qunc", type=int, default=0)
parser.add_argument("-idir", required=True)
parser.add_argument("-odir", required=True)
args = parser.parse_args()

sfile = args.sfile
if args.force:
    force_compute = True
else:
    force_compute = False
date = args.date
rmode = args.rmode
tmode = args.tmode
Qtype = args.Qtype
Qunc = args.Qunc
sce = args.sce
idir = args.idir
if idir is None and Qtype == 9:
    raise Exception('Input directory is required if Qtype is 9.')
odir = args.odir

# Run rmode
prior_only = False
inversion = False
if rmode == 0:
    prior_only = True
elif rmode == 1:
    inversion = True

# Obsertion sites and variables
with open(sfile) as f:
    iset = json.load(f)
    sites = iset['sites']
    y_vars = iset['y_vars']
    theta_vars = iset['theta_vars']
    baseline_files = iset['baseline_files']

# Read station info
field_dirs = OrderedDict()
mix_dirs = OrderedDict()
particle_dirs = OrderedDict()
obs_files = OrderedDict()
long_names = OrderedDict()
locations = OrderedDict()
field_prefix = OrderedDict()
mix_prefix = OrderedDict()
particle_prefix = OrderedDict()
met_file_old = OrderedDict()
met_dirs = OrderedDict()
met_prefix = OrderedDict()
for site in sites:
    with open(f'/home/ec5/name_qch4_couple/stations/{site}.json', 'r') as f:
        st_info = json.load(f)
        field_dirs[site] = OrderedDict(st_info['field_dir'])
        mix_dirs[site] = OrderedDict(st_info['mix_dir'])
        particle_dirs[site] = OrderedDict(st_info['particle_dir'])
        obs_files[site] = OrderedDict(st_info['obs_files'])
        long_names[site] = st_info['long_name']
        locations[site] = st_info['location']
        field_prefix[site] = OrderedDict(st_info['field_prefix'])
        mix_prefix[site] = OrderedDict(st_info['mix_prefix'])
        particle_prefix[site] = OrderedDict(st_info['particle_prefix'])
        if 'met_file_old' in st_info:
            met_file_old[site] = OrderedDict(st_info['met_file_old'])
        if 'met_dir' in st_info:
            met_dirs[site] = OrderedDict(st_info['met_dir'])
        if 'met_prefix' in st_info:
            met_prefix[site] = OrderedDict(st_info['met_prefix'])

# Inversion sample size
pm_tune = 1000#5000
pm_draws = 1500#10000
pm_cores = 8
pm_chains = 8
pm_seed = 10

# Constant to convert footprint mixing ratio to reported unit
chi_scale = 1.e3

# Legacy
p = 101325  # Pa
T = 288.15  # K

# UNFCCC emissions file
unfccc_file = r'/home/ec5/hpc-work/data_archive/unfccc/ch4.csv'

# Prior files
"""Emission Categories
0   combustion energy
    combustion other
    combustion industry
    industry production
    fossil fuel production
    road transport
    other transport
1   waste
2   agriculture
3   biomass
4   wetland
"""
Q_files = OrderedDict([
    (0, [
        ['/home/ec5/name_qch4_couple/priors/prior_edgar_v6_0_SNAP01.nc', 'CH4_emissions', '1M'],
        ['/home/ec5/name_qch4_couple/priors/prior_edgar_v6_0_SNAP02.nc', 'CH4_emissions', '1M'],
        ['/home/ec5/name_qch4_couple/priors/prior_edgar_v6_0_SNAP03.nc', 'CH4_emissions', '1M'],
        ['/home/ec5/name_qch4_couple/priors/prior_edgar_v6_0_SNAP04.nc', 'CH4_emissions', '1M'],
        ['/home/ec5/name_qch4_couple/priors/prior_edgar_v6_0_SNAP05.nc', 'CH4_emissions', '1M'],
        ['/home/ec5/name_qch4_couple/priors/prior_edgar_v6_0_SNAP07.nc', 'CH4_emissions', '1M'],
        ['/home/ec5/name_qch4_couple/priors/prior_edgar_v6_0_SNAP08.nc', 'CH4_emissions', '1M'],
        ]),
    (1, [
        ['/home/ec5/name_qch4_couple/priors/prior_edgar_v6_0_SNAP09.nc', 'CH4_emissions', '1M'],
        ]),
    (2, [
        ['/home/ec5/name_qch4_couple/priors/prior_edgar_v6_0_SNAP10.nc', 'CH4_emissions', '1M'],
        ]),
    (3, [
        ['/home/ec5/name_qch4_couple/priors/prior_gfed.nc', 'CH4_emissions', '1M'],
        ]),
    (4, [
        ['/home/ec5/name_qch4_couple/priors/prior_wetcharts.nc', 'CH4_emissions', '1M']
        ]),
    ])
Q_monthly_scalers = OrderedDict([
    (0, [
        ['/home/ec5/name_qch4_couple/priors/prior_edgar_v5_0_2015_monthly_SNAP01.nc', 'CH4_emissions', '1M'],
        ['/home/ec5/name_qch4_couple/priors/prior_edgar_v5_0_2015_monthly_SNAP02.nc', 'CH4_emissions', '1M'],
        ['/home/ec5/name_qch4_couple/priors/prior_edgar_v5_0_2015_monthly_SNAP03.nc', 'CH4_emissions', '1M'],
        ['/home/ec5/name_qch4_couple/priors/prior_edgar_v5_0_2015_monthly_SNAP04.nc', 'CH4_emissions', '1M'],
        ['/home/ec5/name_qch4_couple/priors/prior_edgar_v5_0_2015_monthly_SNAP05.nc', 'CH4_emissions', '1M'],
        ['/home/ec5/name_qch4_couple/priors/prior_edgar_v5_0_2015_monthly_SNAP07.nc', 'CH4_emissions', '1M'],
        ['/home/ec5/name_qch4_couple/priors/prior_edgar_v5_0_2015_monthly_SNAP08.nc', 'CH4_emissions', '1M'],
        ]),
    (1, [
        ['/home/ec5/name_qch4_couple/priors/prior_edgar_v5_0_2015_monthly_SNAP09.nc', 'CH4_emissions', '1M'],
        ]),
    (2, [
        ['/home/ec5/name_qch4_couple/priors/prior_edgar_v5_0_2015_monthly_SNAP10.nc', 'CH4_emissions', '1M'],
        ]),
    (3, [None]),
    (4, [None]),
    ])
# Prior signatures
prior_dQ = {
    0: [0.0, -40.0, -175.0],  # Menoud et al., 2020
    1: [0.0, -55.0, -293.0],  # Menoud et al., 2020
    2: [0.0, -68.0, -319.0],  # Menoud et al., 2020
    3: [0.0, -26.2, -211.0],  # Sherwood et al., 2017
    4: [0.0, -69.0, -330.0],  # Menoud et al., 2020
    }

prior_dQ_h = {
    0: [0.0, 10.7, 51.0],  # Sherwood et al., 2017
    1: [0.0, 7.6, 11.0],  # Sherwood et al., 2017
    2: [0.0, 6.7, 29.0],  # Sherwood et al., 2017
    3: [0.0, 4.8, 15.0],  # Sherwood et al., 2017
    4: [0.0, 5.4, 42.0],  # Sherwood et al., 2017
    }

# Select sectors for inversion
inv_s0_sel = {  # 0 = none, 1 = land only, 2 = full
    0: (2 if sce not in [21, 22] else 0), 1: 1, 2: 1, 3: 0, 4: 1
    }
# Network compatibility goals for measurement-model mismatch

#NCG = {'chi': 2., 'd_13C': 0.02, 'd_2H': 1}
NCG = {'chi': 5., 'd_13C': 0.2, 'd_2H': 5.}  # extended
NCG_mul = {'chi': 2., 'd_13C': 1., 'd_2H': 1.}

# =============================================================================
#   Preprocessing
# =============================================================================
print(f'Initialising')
print(f'  site  : {sites}')
print(f'  sce   : {sce}')
print(f'  date  : {date}')
print(f'  tmode : {tmode}')
print(f'  Qtype : {Qtype}')

# Constants
R_std = np.array([
    1.0,
    1.12372000e-02,
    6.23040000e-04,
    7.00122509e-06,
    1.45567066e-07
    ])
M = np.array([
    16.031300,
    17.034655,
    17.037577,
    18.040932,
    18.043854
    ])

# Dates
if tmode in [0, 1]:
    dates_tHourAll = pd.date_range(
        f"{date}-01" if not tmode else f"{date}-16",
        (
            pd.to_datetime(f"{date}-01") + pd.DateOffset(days=15)
            if not tmode else
            pd.to_datetime(f"{date}-01") + pd.DateOffset(months=1)
            ),
        freq='1H',
        closed='left'
        )
elif tmode in [2, 3]:
    dates_tHourAll = pd.date_range(
        f"{date}-01",
        pd.to_datetime(f"{date}-01") + pd.DateOffset(months=1),
        freq='1H',
        closed='left'
        )
if tmode not in [3]:
    dates_tHour = dates_tHourAll
elif tmode in [3]:
    dates_tHour = pd.date_range(
        f"{date}-01",
        pd.to_datetime(f"{date}-01") + pd.DateOffset(months=1),
        freq='1H',
        closed='left'
        ).to_series().between_time('0900', '1500').index
dates_tDay = dates_tHour.floor('D').unique()

print(f'{" ":8s}{"Memory 0":16s}', end='')
check_memory()

# Observations
print(f'{" ":8s}{"Observations":16s}', end='')
obs, obs_err = routines.read_obs(
    sites=sites,
    obs_files=obs_files,
    date_range=[dates_tDay[i].strftime('%Y-%m-%d') for i in [0, -1]],
    dates_tHour=dates_tHour
    )

# Monthly means for tropical ratio
chi0_ext = pd.read_csv(
    '/home/ec5/hpc-work/data_archive/agage/MHD-gcmd_mon.csv',
    skiprows=15, skipinitialspace=True,
    parse_dates={'datetime': ['year', 'month']}, index_col='datetime'
    ).loc[date, 'CH4'].values
chi0_tro = pd.read_csv(
    '/home/ec5/hpc-work/data_archive/agage/RPB-gcmd_mon.csv',
    skiprows=15, skipinitialspace=True,
    parse_dates={'datetime': ['year', 'month']}, index_col='datetime'
    ).loc[date, 'CH4'].values
d0i1_ext = name_qch4_couple.io.r_instaar(
    '/home/ec5/hpc-work/data_archive/noaa/trace_gases/'
    'ch4c13/ch4c13_mhd_surface-flask_1_sil_month.txt',
    'month'
    ).loc[date].values
d0i1_tro = name_qch4_couple.io.r_instaar(
    '/home/ec5/hpc-work/data_archive/noaa/trace_gases/'
    'ch4c13/ch4c13_mlo_surface-flask_1_sil_month.txt',
    'month'
    ).loc[date].values
d0i2_ext = np.array(-86.)
d0i2_tro = np.array(-82.)

# Footprint files
## Dosage
f_mix_paths = {
    site:
    pd.concat(
        [
            dates_tDay.strftime(
                os.path.join(v[k], mix_prefix[site][k])
                ).to_series(index=dates_tDay).apply(np.vectorize(
                    lambda x: x if os.path.exists(x) else False
                    ))
            for k in sorted(v)
            ],
        axis=1
        ).replace(False, np.nan)
    for site, v in mix_dirs.items()
    }
f_mix_path = {
    site:
    v.bfill(axis=1).iloc[:, 0].replace(np.nan, False)
    for site, v in f_mix_paths.items()
    }
f_field_paths = {
    site:
    pd.concat(
        [
           dates_tDay.strftime(
                os.path.join(v[k], field_prefix[site][k])
                ).to_series(index=dates_tDay).apply(np.vectorize(
                    lambda x: x if os.path.exists(x) else False
                    ))
            for k in sorted(v)
            ],
        axis=1
        ).replace(False, np.nan)
    for site, v in field_dirs.items()
    }
f_field_path = {
    site:
    v.bfill(axis=1).iloc[:, 0].replace(np.nan, False)
    for site, v in f_field_paths.items()
    }
f_foot_path = {
    site:
    pd.concat(
        [
            f_mix_path[site].replace(False, np.nan),
            f_field_path[site].replace(False, np.nan)
            ],
        axis=1
        ).bfill(axis=1).iloc[:, 0].replace(np.nan, False)
    for site in sites
    }
f_mix_or_field = {  # 1 = mix, 2 = field
    site:
    pd.concat(
        [
            (f_mix_path[site] != False) * 1,
            (f_field_path[site] != False) * 2
            ],
        axis=1
        ).replace(0, np.nan).bfill(axis=1).iloc[:, 0].replace(np.nan, False)
    for site in sites
    }
## Particles
f_particles_paths = {
    site:
    pd.concat(
        [
            dates_tDay.strftime(
                os.path.join(v[k], particle_prefix[site][k])
                ).to_series(index=dates_tDay).apply(np.vectorize(
                    lambda x: x if os.path.exists(x) else False
                    ))
            for k in sorted(v)
            ],
        axis=1
        ).replace(False, np.nan)
    for site, v in particle_dirs.items()
    }
f_particles_path = {
    site:
    v.bfill(axis=1).iloc[:, 0].replace(np.nan, False)
    for site, v in f_particles_paths.items()
    }
## Met files
f_met_paths = {
    site:
    pd.concat(
        [
            dates_tDay.strftime(
                os.path.join(v[k], met_prefix[site][k])
                ).to_series(index=dates_tDay).apply(np.vectorize(
                    lambda x: x if os.path.exists(x) else False
                    ))
            for k in sorted(v)
            ],
        axis=1
        ).replace(False, np.nan)
    for site, v in met_dirs.items()
    }
f_met_path = {
    site:
    v.bfill(axis=1).iloc[:, 0].replace(np.nan, False)
    for site, v in f_met_paths.items()
    }
f_met_old_path = {
    site:
    pd.Series(v).apply(np.vectorize(
        lambda x: x if os.path.exists(x) else False
        )).replace(False, np.nan).dropna()
    for site, v in met_file_old.items()
    }

# Date index
dates_foot = {
    site:
    pd.concat(
        [f_foot_path[site], f_particles_path[site]], axis=1
        ).replace(False, np.nan).dropna().index
    for site in sites
    }
dates_tObs = { #dates_tObs
    site: {
        k:
        v.loc[date:date].resample('H').mean().dropna().index.to_series().apply(
            np.vectorize(
                lambda x: x if x.date() in dates_foot[site].date else pd.NaT
                )
            ).reindex(index=dates_tHour).dropna().index
        for k, v in y.items()
        }
    for site, y in obs.items()
    }
dates_tSite = { #dates_tSite
    site: pd.DatetimeIndex(np.unique(np.concatenate([v for v in d.values()])))
    for site, d in dates_tObs.items()
    }
nx_tH_tObs = { #nx_tH_tObs
    site: {
        k: dates_tHour.get_indexer(v)
        for k, v in d.items()
        }
    for site, d in dates_tObs.items()
    }
nx_tH_tSite = { #nx_tH_tSite
    site: dates_tHour.get_indexer(d)
    for site, d in dates_tSite.items()
    }
nx_tHAll_tSite = { #nx_tHAll_tSite
    site: dates_tHourAll.get_indexer(d)
    for site, d in dates_tSite.items()
    }
dates_tComp = pd.DatetimeIndex( # dates_tComp
    np.unique(np.concatenate([d for d in dates_tSite.values()]))
    )
nx_tComp_tObs = { #nx_tComp_tObs
    site: {
        k: dates_tComp.get_indexer(v)
        for k, v in d.items()
        }
    for site, d in dates_tObs.items()
    }
nx_tComp_tSite = { #nx_tComp_tSite
    site: dates_tComp.get_indexer(d)
    for site, d in dates_tSite.items()
    }
nx_tH_tComp = dates_tHour.get_indexer(dates_tComp)
nx_tSite_tObs = {
    site: {
        k: dates_tSite[site].get_indexer(v)
        for k, v in d.items()
        }
    for site, d in dates_tObs.items()
    }
dim_tComp = dates_tComp.size

check_memory()

# Grid
print(f'{" ":8s}{"Grid":16s}', end='')
grid_info = routines.define_grid()
inv_reg_map0 = grid_info['inv_reg_map']
nlat = grid_info['nlat']
nlon = grid_info['nlon']
inv_lat0 = grid_info['inv_lat0']
inv_lat1 = grid_info['inv_lat1']
inv_lon0 = grid_info['inv_lon0']
inv_lon1 = grid_info['inv_lon1']
area = grid_info['area']
grid_vertex = grid_info['grid_vertex']
inv_reg_uniq = grid_info['inv_reg_uniq']
inv_mask = np.full((nlat, nlon), False)
inv_mask[inv_lat0:inv_lat1, inv_lon0:inv_lon1] = True
check_memory()

# Prior dictionary
def_state = {}
pri_state = {}

# Particles
print(f'{" ":8s}{"Particles":16s}', end='')
dim_bAll = 11
particle_end_18loc = {
    site:
    np.concatenate(
        [
            name_qch4_couple.name.cat_end_loc(
                name_qch4_couple.io.r_name_particles(f),
                grid_vertex, np.arange(24), 3.e3, 8.e3
                )[0]
            if f else
            np.zeros((24, 18))
            for f in fs
            ],
        axis=0
        )[nx_tHAll_tSite[site]]
    for site, fs in f_particles_path.items()
    }
particle_end_11loc = {
    site: np.zeros((v.shape[0], dim_bAll))
    for site, v in particle_end_18loc.items()
    }
for site, v in particle_end_18loc.items():
    particle_end_11loc[site][:, :8] = v[:, 1:9]
    particle_end_11loc[site][:, 8] = v[:, [9, 10, 15, 16]].sum(1)
    particle_end_11loc[site][:, 9] = v[:, [11, 12, 13, 14]].sum(1)
    particle_end_11loc[site][:, 10] = v[:, 17]
check_memory()

## Overlap between particle end locations and observations
idx0_site = {site: np.full((dates_tComp.size, dim_bAll), False) for site in sites}
for site, v in particle_end_11loc.items():
    idx0_site[site][nx_tComp_tSite[site]] = (v != 0)
idx0 = np.array([v for v in idx0_site.values()]).any(0)
dim_tbSel = idx0.sum()

nx_tComp_tbSel = np.tile(np.arange(dates_tComp.size).reshape(-1, 1), dim_bAll)[idx0]
nx_tbSel_tbSite = {  # index of site in E
    site: np.arange(dim_tbSel)[v[idx0]]
    for site, v in idx0_site.items()
    }
nx_tbSel_tSite = {  # time binned for bincount
    site: np.tile(np.arange(v.shape[0]).reshape(-1, 1), (1, dim_bAll))[v != 0]
    for site, v in particle_end_11loc.items()
    }
nx_bAll_tbSel = {
    site: (np.ones(v.shape, dtype=int)*np.arange(dim_bAll))[v != 0]
    for site, v in particle_end_11loc.items()
    }
E = {
    site: (v/v.sum(1, keepdims=True))
    for site, v in particle_end_11loc.items()
    }

# Prior Baseline
## Values
print(f'{" ":8s}{"Baseline":16s}', end='')
variables = ['chi', 'd_13C', 'd_2H']
baseline = pd.DataFrame(
    np.nan, index=dates_tHour,
    columns=[*variables, *[f'var_{i}' for i in variables]]
    )
f_baseline = f'/home/ec5/name_qch4_couple/baseline/mhd_all_{dates_tHour[0].year}.nc'
with xr.open_dataset(f_baseline) as ds_read:
    with ds_read.load() as ds:
        for k in [*variables, *[f'var_{i}' for i in variables]]:
            if not k in ds.variables:
                continue
            baseline[k].update(ds[k].to_pandas())
            if baseline[k].isnull().all():
                if dates_tHour[0] > ds[k].to_pandas().index[-1]:
                    idx = -1
                elif dates_tHour[-1] < ds[k].to_pandas().index[0]:
                    idx = 0
                else:
                    continue
                baseline[k] = ds[k].to_pandas().iloc[idx]
baseline.interpolate('linear', limit_direction='both', inplace=True)
for k in variables:
    if baseline[k].isnull().all():
        baseline[k] = {'chi': 1900.0, 'd_13C': -47.5, 'd_2H': -86.0}[k]
    if baseline[f'var_{k}'].isnull().all():
        baseline[f'var_{k}'] = NCG[k] * NCG_mul[k]
pri_state['v_chi0'] = np.array(baseline['chi']).flatten()[nx_tH_tComp]
pri_state['v_d0i1'] = np.array(baseline['d_13C']).flatten()[nx_tH_tComp]
pri_state['v_d0i2'] = np.array(baseline['d_2H']).flatten()[nx_tH_tComp]
pri_state['u_chi0'] = np.array(
    baseline['var_chi']**.5
    ).flatten()[nx_tH_tComp]
pri_state['u_d0i1'] = np.array(
    baseline['var_d_13C']**.5
    ).flatten()[nx_tH_tComp]
pri_state['u_d0i2'] = np.array(
    baseline['var_d_2H']**.5
    ).flatten()[nx_tH_tComp]
pri_state['h_chi0'] = pri_state['u_chi0'] * 0.5
pri_state['h_d0i1'] = pri_state['u_d0i1'] * 0.5
pri_state['h_d0i2'] = pri_state['u_d0i2'] * 0.5

## Scale for each particle end locations
chi0lf = np.ones(dim_bAll)
chi0lf[[2, 3, 4, 5, 9]] = chi0_tro / chi0_ext
chi0lf_u = np.zeros(dim_bAll)
chi0lf_u[[0, 1, 6, 7, 8]] = 1.e-3  # northern
chi0lf_u[[2, 3, 4, 5, 9]] = (1. - (chi0_tro / chi0_ext))*.5  # southern
chi0lf_u[10] = 1.e-2  # tropopause
pri_state['v_chi0f'] = chi0lf
pri_state['u_chi0f'] = chi0lf_u
d0i1lf = np.ones(dim_bAll)
d0i1lf[[2, 3, 4, 5, 9]] = d0i1_tro / d0i1_ext
d0i1lf_u = np.zeros(dim_bAll)
d0i1lf_u[[0, 1, 6, 7, 8]] = 5.e-4
d0i1lf_u[[2, 3, 4, 5, 9]] = (1. - (d0i1_tro / d0i1_ext))*.5
d0i1lf_u[10] = 2.e-3
pri_state['v_d0i1f'] = d0i1lf
pri_state['u_d0i1f'] = d0i1lf_u
d0i2lf = np.ones(dim_bAll)
d0i2lf[[2, 3, 4, 5, 9]] = d0i2_tro / d0i2_ext
d0i2lf_u = np.zeros(dim_bAll)
d0i2lf_u[[0, 1, 6, 7, 8]] = 5.e-3
d0i2lf_u[[2, 3, 4, 5, 9]] = (1. - (d0i2_tro / d0i2_ext))*.5
d0i2lf_u[10] = 2.e-2
pri_state['v_d0i2f'] = d0i2lf
pri_state['u_d0i2f'] = d0i2lf_u
# d0 (t, b)
pri_i_R0i1 = (
    pri_state['v_d0i1'][:, np.newaxis] * pri_state['v_d0i1f'] * 1.e-3 + 1.
    ) * R_std[1]
pri_i_R0i2 = (
    pri_state['v_d0i2'][:, np.newaxis] * pri_state['v_d0i2f'] * 1.e-3 + 1.
    ) * R_std[2]
pri_i_R0sum = 1. + pri_i_R0i1 + pri_i_R0i2
# d0 variance
pri_i_R0i1_var = (
    (pri_state['u_d0i1'] / pri_state['v_d0i1'])[:, np.newaxis]**2
    + (pri_state['u_d0i1f'] / pri_state['v_d0i1f'])**2
    ) * pri_i_R0i1**2
pri_i_R0i2_var = (
    (pri_state['u_d0i2'] / pri_state['v_d0i2'])[:, np.newaxis]**2
    + (pri_state['u_d0i2f'] / pri_state['v_d0i2f'])**2
    ) * pri_i_R0i2**2
pri_i_R0sum_var = pri_i_R0i1_var + pri_i_R0i2_var

# chi0, species specific (t, b)
pri_i_chi0fi0 = pri_state['v_chi0f'] * 1. / pri_i_R0sum
pri_i_chi0fi1 = pri_state['v_chi0f'] * pri_i_R0i1 / pri_i_R0sum
pri_i_chi0fi2 = pri_state['v_chi0f'] * pri_i_R0i2 / pri_i_R0sum
# chi0 variance, species specific
pri_i_chi0fi0_var = (
    (pri_state['u_chi0f'] / pri_state['v_chi0f'])**2
    + pri_i_R0sum_var / pri_i_R0sum**2
    ) * pri_i_chi0fi0**2
pri_i_chi0fi1_var = (
    (pri_state['u_chi0f'] / pri_state['v_chi0f'])**2
    + pri_i_R0i1_var / pri_i_R0i1**2
    + pri_i_R0sum_var / pri_i_R0sum**2
    ) * pri_i_chi0fi1**2
pri_i_chi0fi2_var = (
    (pri_state['u_chi0f']/pri_state['v_chi0f'])**2
    + pri_i_R0i2_var/pri_i_R0i2**2
    + pri_i_R0sum_var/pri_i_R0sum**2
    ) * pri_i_chi0fi2**2
# baseline, species specific
pri_bi0 = {
    site:
    pri_state['v_chi0'][nx_tComp_tSite[site]]
    * (pri_i_chi0fi0[nx_tComp_tSite[site]] * E[site]).sum(1)
    for site in sites
    }
pri_bi1 = {
    site:
    pri_state['v_chi0'][nx_tComp_tSite[site]]
    * (pri_i_chi0fi1[nx_tComp_tSite[site]] * E[site]).sum(1)
    for site in sites
    }
pri_bi2 = {
    site:
    pri_state['v_chi0'][nx_tComp_tSite[site]]
    * (pri_i_chi0fi2[nx_tComp_tSite[site]] * E[site]).sum(1)
    for site in sites
    }
pri_bi0_var = {
    site: (
        (pri_state['u_chi0']/pri_state['v_chi0'])[nx_tComp_tSite[site]]**2
        + (
            (pri_i_chi0fi0_var[nx_tComp_tSite[site]] * E[site]**2).sum(1)
            / (pri_i_chi0fi0[nx_tComp_tSite[site]] * E[site]).sum(1)**2
            )
        ) * pri_bi0[site]**2
    for site in sites
    }
pri_bi1_var = {
    site: (
        (pri_state['u_chi0']/pri_state['v_chi0'])[nx_tComp_tSite[site]]**2
        + (
            (pri_i_chi0fi1_var[nx_tComp_tSite[site]] * E[site]**2).sum(1)
            / (pri_i_chi0fi1[nx_tComp_tSite[site]] * E[site]).sum(1)**2
            )
        ) * pri_bi1[site]**2
    for site in sites
    }
pri_bi2_var = {
    site: (
        (pri_state['u_chi0']/pri_state['v_chi0'])[nx_tComp_tSite[site]]**2
        + (
            (pri_i_chi0fi2_var[nx_tComp_tSite[site]] * E[site]**2).sum(1)
            / (pri_i_chi0fi2[nx_tComp_tSite[site]] * E[site]).sum(1)**2
            )
        ) * pri_bi2[site]**2
    for site in sites
    }
pri_baseline = {}
pri_baseline_var = {}
for site in sites:
    pri_baseline.update({
        f'{site}_v_chi0':
            pri_bi0[site] + pri_bi1[site] + pri_bi2[site],
        f'{site}_v_d0i1':
            (pri_bi1[site] / pri_bi0[site] / R_std[1] - 1.) * 1.e3,
        f'{site}_v_d0i2':
            (pri_bi2[site] / pri_bi0[site] / R_std[2] - 1.) * 1.e3,
        })
    pri_baseline_var.update({
        f'{site}_v_chi0':
            pri_bi0_var[site] + pri_bi1_var[site] + pri_bi2_var[site],
        f'{site}_v_d0i1': (
            pri_bi1_var[site] / pri_bi1[site]**2
            + pri_bi0_var[site] / pri_bi0[site]**2
            ) * pri_baseline[f'{site}_v_d0i1']**2,
        f'{site}_v_d0i2': (
            pri_bi2_var[site] / pri_bi2[site]**2
            + pri_bi0_var[site] / pri_bi0[site]**2
            ) * pri_baseline[f'{site}_v_d0i2']**2,
        })
check_memory()

# Dimensions
dim_sAll = len(Q_files)
dim_yAll = grid_info['nlat']
dim_xAll = grid_info['nlon']
dim_ySel = grid_info['inv_nlat']
dim_xSel = grid_info['inv_nlon']
dim_cAll = dim_ySel * dim_xSel

# Prior Emissions
print(f'{" ":8s}{"Q":16s}', end='')
Q = np.zeros((len(Q_files), dim_yAll, dim_xAll))
for s, vs in Q_files.items():
    for v in vs:
        with xr.open_dataset(v[0]) as ds_read:
            with ds_read.load() as Q_in:
                t_Q = Q_in['time']
                if v[2] == '1Y':
                    t = np.datetime64(
                        dates_tDay[0].floor('d').replace(month=1, day=1)
                        )
                    t_in = min(t_Q, key=lambda x: abs(x - t))
                else:
                    t = np.datetime64(
                        dates_tDay[0].floor('d').replace(day=1)
                        )
                    t_in = min(
                        t_Q[t_Q.dt.month==dates_tDay[0].month],
                        key=lambda x: abs(x - t)
                        )
                Q[s] += Q_in[v[1]].sel(time=t_in).values * 1.e3  # kg -> g
'''
# replace EDGAR UK waste with the average between EDGAR and UNFCCC
Q[1, inv_lat0:inv_lat1, inv_lon0:inv_lon1][
    np.isin(
        inv_reg_map0,
        grid_info['inv_reg_uniq'][[
            n for n, i in enumerate(grid_info['inv_reg_codes'])
            if i.startswith('gb')
            ]]
        )
    ] *= 0.5 * (
        (
            unfccc.loc[
                unfccc.Party.isin(grid_info['reg_names']['gb'])
                & unfccc.Category.str.startswith(unfccc_s[1]),
                f'{dates_tDay[0].year}'
                ].sum() * 1.e6
            )
        / (
            (Q * area)[1, inv_lat0:inv_lat1, inv_lon0:inv_lon1][
                np.isin(
                    inv_reg_map0,
                    grid_info['inv_reg_uniq'][[
                        n for n, i in enumerate(grid_info['inv_reg_codes'])
                        if i.startswith('gb')
                        ]]
                    )
                ].sum() * 86400. * 365.25
            )
        + 1.
        )
'''
unfccc = name_qch4_couple.io.r_unfccc(
    '/home/ec5/name_qch4_couple/priors/unfccc_1990-2018_gb_ie.csv'
    )
unfccc_s = {
    0: ('1', '2'),
    1: ('5',),
    2: ('3', '4'),
    }
if Qtype in [1, 3]:
    r"""Scale to UNFCCC
    """
    unfccc_codes = ['gb', 'ie']
    for c in unfccc_codes:
        for s_e, s_u in unfccc_s.items():
            loc = np.isin(
                inv_reg_map0,
                grid_info['inv_reg_uniq'][[
                    n for n, i in enumerate(grid_info['inv_reg_codes'])
                    if i.startswith(c)
                    ]]
                )
            Q_u = unfccc.loc[
                unfccc.Party.isin(grid_info['reg_names'][c])
                & unfccc.Category.str.startswith(s_u),
                f'{dates_tDay[0].year}'
                ].sum() * 1.e6
            Q_e_grid = False
            for v in Q_files[s_e]:
                with xr.open_dataset(v[0]) as ds_read:
                    with ds_read.load() as Q_in:
                        t_Q_year = min(
                            set(Q_in.time.dt.year.values),
                            key=lambda x: abs(x - pd.to_datetime(date).year)
                            )
                        if Q_e_grid is False:
                            Q_e_grid = Q_in.CH4_emissions.sel(
                                time=[
                                    i.values for i in Q_in.time
                                    if i.dt.year==t_Q_year]
                                ).values * 1.e3
                        else:
                            Q_e_grid += Q_in.CH4_emissions.sel(
                                time=[
                                    i.values for i in Q_in.time
                                    if i.dt.year==t_Q_year]
                                ).values * 1.e3
            Q_e = (Q_e_grid.mean(0) * area)[
                inv_lat0:inv_lat1, inv_lon0:inv_lon1
                ][
                    loc
                    ].sum() * 86400 * 365.25
            if not np.isfinite([Q_u, Q_e]).all():
                continue
            elif Q_e == 0:
                (
                    Q[s_e, inv_lat0:inv_lat1, inv_lon0:inv_lon1][loc]
                    ) += Q_u / (loc*area).sum()
            else:
                (
                    Q[s_e, inv_lat0:inv_lat1, inv_lon0:inv_lon1][loc]
                    ) *= Q_u / Q_e

'''
if Qtype in [2, 3]:
    r"""Derive monthly for annual data
    """
    Q_monthly_scale_num = np.zeros((len(Q_files), dim_yAll, dim_xAll))
    Q_monthly_scale_den = np.zeros((len(Q_files), dim_yAll, dim_xAll))
    for s, vs in Q_monthly_scalers.items():
        if vs[0] is None:
            continue
        for v in vs:
            with xr.open_dataset(v[0]) as ds_read:
                with ds_read.load() as Q_in:
                    t_Q = Q_in['time']
                    t = np.datetime64(
                        dates_tDay[0].floor('d').replace(day=1)
                        )
                    t_in = min(
                        t_Q[t_Q.dt.month==dates_tDay[0].month],
                        key=lambda x: abs(x - t)
                        )
                    Q_monthly_scale_num[s] += Q_in[v[1]].sel(time=t_in).values * 1.e3
                    Q_monthly_scale_den[s] += Q_in[v[1]].mean(dim='time').values * 1.e3
    # if denominator is 0, scale = 1., i.e. use annual data
    Q_monthly_scale_num[Q_monthly_scale_den == 0] = 1.
    Q_monthly_scale_den[Q_monthly_scale_den == 0] = 1.
    Q_monthly_scale = Q_monthly_scale_num / Q_monthly_scale_den
    Q *= Q_monthly_scale
'''
if sce in [5]:  # Prior for the future
    """
    UK
        FF * 20 %
        WM * 50 %
        MA * 80 %
    Rest
        FF * 30 %
        WM * 60 %
        MA * 90 %
    """
    Q[0, inv_lat0:inv_lat1, inv_lon0:inv_lon1][np.isin(
        inv_reg_map0,
        grid_info['inv_reg_uniq'][np.array([
            i.startswith(('gb', 'ie'))
            for i in grid_info['inv_reg_codes']
            ])]
        )] *= 0.2
    Q[1, inv_lat0:inv_lat1, inv_lon0:inv_lon1][np.isin(
        inv_reg_map0,
        grid_info['inv_reg_uniq'][np.array([
            i.startswith(('gb', 'ie'))
            for i in grid_info['inv_reg_codes']
            ])]
        )] *= 0.5
    Q[2, inv_lat0:inv_lat1, inv_lon0:inv_lon1][np.isin(
        inv_reg_map0,
        grid_info['inv_reg_uniq'][np.array([
            i.startswith(('gb', 'ie'))
            for i in grid_info['inv_reg_codes']
            ])]
        )] *= 0.8
    Q[0, inv_lat0:inv_lat1, inv_lon0:inv_lon1][np.isin(
        inv_reg_map0,
        grid_info['inv_reg_uniq'][np.array([
            not i.startswith(('gb', 'ie'))
            for i in grid_info['inv_reg_codes']
            ])]
        )] *= 0.3
    Q[1, inv_lat0:inv_lat1, inv_lon0:inv_lon1][np.isin(
        inv_reg_map0,
        grid_info['inv_reg_uniq'][np.array([
            not i.startswith(('gb', 'ie'))
            for i in grid_info['inv_reg_codes']
            ])]
        )] *= 0.6
    Q[2, inv_lat0:inv_lat1, inv_lon0:inv_lon1][np.isin(
        inv_reg_map0,
        grid_info['inv_reg_uniq'][np.array([
            not i.startswith(('gb', 'ie'))
            for i in grid_info['inv_reg_codes']
            ])]
        )] *= 0.9

inv_Q_scale = (  # Scaling to avoid very small/large number
    1. if 'QA' in theta_vars else 1.e9
    )

inv_s_sel = sorted([k for k, v in inv_s0_sel.items() if v != 0])
dim_sSel = len(inv_s_sel)

# Prior Emission Signatures
dQ = np.array([prior_dQ[s] for s in range(len(prior_dQ))])
dQ_h = np.array([prior_dQ_h[s] for s in range(len(prior_dQ))])
dQi1 = dQ[:, 1].reshape(dim_sAll, 1, 1)
dQi2 = dQ[:, 2].reshape(dim_sAll, 1, 1)
def_state['def_v_dQi1'] = dQi1.flatten()
def_state['def_v_dQi2'] = dQi2.flatten()
pri_state['v_dQi1'] = dQi1.flatten()[inv_s_sel]
pri_state['v_dQi2'] = dQi2.flatten()[inv_s_sel]
pri_state['h_dQi1'] = dQ_h[inv_s_sel, 1] * np.array([1.0, 1.5]).reshape(2, 1)
pri_state['h_dQi2'] = dQ_h[inv_s_sel, 2] * np.array([1.0, 1.5]).reshape(2, 1)
pri_state['v_dQ'] = np.concatenate([pri_state['v_dQi1'], pri_state['v_dQi2']])
pri_state['h_dQ'] = np.concatenate(
    [pri_state['h_dQi1'], pri_state['h_dQi2']], axis=1
    ).reshape(2, -1)
check_memory()

# Dosage
print(f'{" ":8s}{"Dilution Matrix":16s}', end='')
inv_D = {}
dchi0_i0 = {}
dchi0_i1 = {}
dchi0_i2 = {}
dm0_s = {}
pri_y_i0 = {}
pri_y_i1 = {}
pri_y_i2 = {}
for site, fs in f_foot_path.items():
    metfile = (
        f'/home/ec5/name_qch4_couple/archive/met_invCH4/{site}_{date}.npy'
        )
    if os.path.exists(metfile):
        dosage_in = np.load(metfile, 'r')
    else:
        dosage_in = np.concatenate(
            [
                name_qch4_couple.name.txt2array(
                    name_qch4_couple.io.r_name(f),
                    [dim_yAll, dim_xAll], np.arange(24), 0.
                    )[:, np.newaxis]
                if f else
                np.zeros((24, 1, dim_yAll, dim_xAll))
                for i, f in enumerate(fs)
                ],
            axis=0
            )
            #)[nx_tHAll_tSite[site]]
        # g m-3 s -> ppm s
        if (f_mix_or_field[site] == 2).any():
            dosage_mf = pd.DataFrame(1., index=dates_tHourAll, columns=['rho'])
            for t, flag in f_mix_or_field[site].iteritems():
                if flag == 2.:
                    dosage_mf[dosage_mf.index.date == t.date()] = np.nan
            if site in f_met_path:
                for t, f in f_met_path[site].iteritems():
                    if f_mix_or_field[site].loc[t] != 2:  # skip since MixR
                        continue
                    if f:
                        met_in = name_qch4_couple.io.r_name_met(
                            f
                            )[['Temperature (C)', 'Pressure (Pa)']]
                        dosage_mf.update(
                            8.3145
                            * (met_in['Temperature (C)'] + 273.15)
                            / met_in['Pressure (Pa)']
                            * 1.e6
                            )
                    else:
                        dosage_mf.loc[t] = np.nan
            if site in f_met_old_path:
                for i, f in f_met_old_path[site].iteritems():
                    met_in = name_qch4_couple.io.r_name_met(
                        f
                        )[['Temperature (C)', 'Pressure (Pa)']]
                    dosage_mf.update(
                        pd.Series(
                            8.3145
                            * (met_in['Temperature (C)'] + 273.15)
                            / met_in['Pressure (Pa)']
                            * 1.e6,
                            name='rho'
                            ),
                        overwrite=False
                        )
            dosage_mf.replace(np.nan, 8.3145 * T / p * 1.e6)
            dosage_in *= np.array(
                dosage_mf.squeeze()
                #dosage_mf.squeeze()[nx_tHAll_tSite[site]]
                ).reshape(-1, 1, 1, 1)
            del dosage_mf, met_in
        np.save(metfile, dosage_in)
    dosage = dosage_in[nx_tHAll_tSite[site]]
    inv_D[site] = (
        dosage[:, 0, inv_lat0:inv_lat1, inv_lon0:inv_lon1]
        * area[inv_lat0:inv_lat1] / 3600. / inv_Q_scale * chi_scale
        )
    (
        pri_y_i0[site],
        pri_y_i1[site],
        pri_y_i2[site]
        ) = (
            name_qch4_couple.routines.forward_weight(
                M=M, R_std=R_std, p=p, T=T,
                p_chi0=pri_baseline[f'{site}_v_chi0'],
                p_d0i1=pri_baseline[f'{site}_v_d0i1'],
                p_d0i2=pri_baseline[f'{site}_v_d0i2'],
                p_QA=Q*area, p_dQi1=dQi1, p_dQi2=dQi2,
                p_dosage=dosage, chi_scale=chi_scale,
                select=np.s_[:], dosage_type=1
                )
            )
    (
        dchi0_i0[site],
        dchi0_i1[site],
        dchi0_i2[site]
        ) = (
            name_qch4_couple.routines.forward_weight(
                M=M, R_std=R_std, p=p, T=T,
                p_chi0=np.zeros(dates_tSite[site].size),
                p_d0i1=np.full(dates_tSite[site].size, -1.e3),
                p_d0i2=np.full(dates_tSite[site].size, -1.e3),
                p_QA=Q*area, p_dQi1=dQi1, p_dQi2=dQi2,
                p_dosage=dosage, chi_scale=chi_scale,
                select=~inv_mask, dosage_type=1
                )
            )
    dm0_s[site] = (Q * dosage * area)[..., ~inv_mask].sum(-1)
    del dosage, dosage_in
check_memory()

# Inversion Q and grid
print(f'{" ":8s}{"Inversion Grid":16s}', end='')
Q_abs_sigma_min = 5.e-11  # minimum uncertainty
Q_rel_sigma = np.ones(dim_sAll).reshape(-1, 1, 1)
if Qunc == 1:
    Q_rel_sigma[0] = 2.0
Q_hyper = 0.8
Q_invgrid = Q[..., inv_lat0:inv_lat1, inv_lon0:inv_lon1]
QA_invgrid = Q_invgrid * area[inv_lat0:inv_lat1, :]
"""
for c in ['gb', 'ie']:
    for k, v in unfccc_s.items():
        loc = np.isin(
            inv_reg_map0,
            grid_info['inv_reg_uniq'][[
                n for n, i in enumerate(grid_info['inv_reg_codes'])
                if i.startswith(c)
                ]]
            )
        Q_u = unfccc.loc[
            unfccc.Party.isin(grid_info['reg_names'][c])
            & unfccc.Category.str.startswith(v),
            f'{dates_tDay[0].year}'
            ].sum() * 1.e6
        Q_e = QA_invgrid[k, loc].sum() * 86400 * 365.25
        if not np.isfinite([Q_u, Q_e]).all():
            continue
        elif min(Q_u, Q_e) == 0.:
            Q_invgrid_h1[k, loc] = np.maximum(Q_invgrid, 5.e-11)[k, loc] * 0.9
        elif max(Q_u, Q_e) / min(Q_u, Q_e) >= 2.:
            Q_invgrid_h1[k, loc] = np.maximum(Q_invgrid, 5.e-11)[k, loc] * 0.9
"""
thr_freq = 5
thr_sens = 10.e0
inv_reg_maps = routines.div_regions(
    Q=(
        (Q_invgrid * Q_rel_sigma)
        if Qunc in [0, 1] else
        (np.tile(Q_invgrid.sum(0, keepdims=True), (5, 1, 1)))
        ) *inv_Q_scale / M[0],
    Q_min=Q_abs_sigma_min * inv_Q_scale / M[0],
    D=inv_D,
    regions_def=inv_reg_map0,
    thr_freq=thr_freq, thr_sens=thr_sens,
    s_sel=inv_s_sel,
    strip_ratio=4,
    verbose=False#True
    )
inv_reg_nos = np.unique(inv_reg_maps)
inv_reg_codes = np.array([
    grid_info['inv_reg_codes'][
        grid_info['inv_reg_uniq'] == inv_reg_map0[i == r][0]
        ][0]
    for i in inv_reg_maps for r in np.unique(i)]
    )
reg_code_land = grid_info['inv_reg_uniq'][
    np.any([
        ~np.char.startswith(grid_info['inv_reg_codes'], ('sea')),
        grid_info['inv_reg_codes'] == 'seaMdS'
        ], axis=0)
    ]
reg_map_sel = np.array([
    np.full(inv_reg_map0.shape, True)
    if inv_s0_sel[n] == 2 else
    np.full(inv_reg_map0.shape, False)
    if inv_s0_sel[n] == 0 else
    np.isin(inv_reg_map0, reg_code_land)
    for n in range(dim_sAll)
    ])
nx_rAll_cSel = inv_reg_maps[reg_map_sel]
nx_rAll_rSel = np.unique(inv_reg_maps[reg_map_sel])
nx_rAll_rSel_nots = np.unique(inv_reg_maps[~reg_map_sel])
nx_cSel_cAll = np.concatenate([
    np.arange(inv_reg_map0.size).reshape(inv_reg_map0.shape)[i]
    for i in reg_map_sel
    ])
nx_cSel_cAll_nots = np.concatenate([
    np.arange(inv_reg_map0.size).reshape(inv_reg_map0.shape)[~i]
    for i in reg_map_sel
    ])
nx_sAll_cSel = np.concatenate([
    i[i] * n for n, i in enumerate(reg_map_sel)
    ])
nx_sAll_cSel_nots = np.concatenate([
    i[i] * n for n, i in enumerate(~reg_map_sel)
    ])
nx_sSel_cSel = np.concatenate([
    i[i] * n for n, i in enumerate(reg_map_sel[inv_s_sel])
    ])
nx_sAll_rSel = np.concatenate([
    np.full(np.unique(i[reg_map_sel[n]]).size, n)
    for n, i in enumerate(inv_reg_maps)
    ])
nx_sAll_rSel_nots = np.concatenate([
    np.full(np.unique(i[~reg_map_sel[n]]).size, n)
    for n, i in enumerate(inv_reg_maps)
    ])
nx_sSel_rSel = np.concatenate([
    np.full(np.unique(i[reg_map_sel[inv_s_sel][n]]).size, n)
    for n, i in enumerate(inv_reg_maps[inv_s_sel])
    ])
inv_reg_maps_Qonly = np.full(inv_reg_maps.shape, -1)
for n, i in enumerate(nx_rAll_rSel):
    inv_reg_maps_Qonly[inv_reg_maps == i] = n
nx_rSel_cSel = inv_reg_maps_Qonly[reg_map_sel]
inv_Qbase = Q_invgrid[reg_map_sel] * inv_Q_scale
inv_QAbase = QA_invgrid[reg_map_sel] * inv_Q_scale
inv_reg_area = np.bincount(
    inv_reg_maps.flatten(),
    np.tile(
        area[np.newaxis, inv_lat0:inv_lat1],
        (dim_sAll, 1, inv_reg_map0.shape[-1])
        ).flatten()
    )
def_state['def_v_Q'] =  Q_invgrid.copy()
pri_state['v_Q'] = (
    np.bincount(inv_reg_maps[reg_map_sel], inv_QAbase)[nx_rAll_rSel]
    / inv_reg_area[nx_rAll_rSel]
    )
inv_Q_dist = inv_Qbase / pri_state['v_Q'][nx_rSel_cSel]
inv_Q_dist[~np.isfinite(inv_Q_dist)] = 1.
pri_state['u_Q'] = np.maximum(
    np.bincount(
        inv_reg_maps[reg_map_sel],
        (
            QA_invgrid
            if Qunc in [0, 1] else
            np.tile(QA_invgrid.sum(0, keepdims=True), (dim_sAll, 1, 1))
            )[reg_map_sel]
        )[nx_rAll_rSel]
    / inv_reg_area[nx_rAll_rSel] * Q_rel_sigma[nx_sAll_rSel].flatten(),
    Q_abs_sigma_min
    ) * inv_Q_scale
#pri_state['u_Q'] = np.full(
#    pri_state['v_Q'].shape,
#    np.maximum(
#        pri_state['v_Q'] * Q_rel_sigma[nx_sAll_rSel].flatten(),
#        Q_abs_sigma_min * inv_Q_scale
#        )
#    )
pri_state['h_Q'] = np.full(pri_state['v_Q'].shape, pri_state['u_Q']*Q_hyper)
inv_Df = {
    site:
    np.array([
        np.bincount(nx_rAll_cSel, inv_Q_dist * i)[nx_rAll_rSel]
        for i in (
            D /inv_reg_area if 'QA' in theta_vars else D
            ).reshape(-1, dim_cAll)[..., nx_cSel_cAll]
        ])
    for site, D in inv_D.items()
    }
dim_rSel = np.unique(nx_rSel_cSel).size
check_memory()
print(f'    No. of regions: {inv_reg_nos.size}')

# Contributions from regions in the inversion domain not optimised
dchi1_i0 = {}
dchi1_i1 = {}
dchi1_i2 = {}
for site in sites:
    (
        dchi1_i0[site],
        dchi1_i1[site],
        dchi1_i2[site]
        ) = (
            name_qch4_couple.routines.forward_weight(
                M=M, R_std=R_std, p=p, T=T,
                p_chi0=np.zeros(dates_tSite[site].size),
                p_d0i1=np.full(dates_tSite[site].size, -1.e3),
                p_d0i2=np.full(dates_tSite[site].size, -1.e3),
                p_QA=Q_invgrid[~reg_map_sel]*inv_Q_scale,
                p_dQi1=dQi1.flatten()[nx_sAll_cSel_nots],
                p_dQi2=dQi2.flatten()[nx_sAll_cSel_nots],
                p_dosage=(
                    inv_D[site].reshape(
                        (-1, 1, dim_cAll)
                        )[..., nx_cSel_cAll_nots]*3600.
                    ),
                chi_scale=1,
                select=np.s_[:], dosage_type=1
                )
            )
dm1_s = {
    site:
    np.array([
        np.bincount(nx_sAll_cSel_nots, i[..., ~reg_map_sel], dim_sAll)
        for i in Q_invgrid*inv_Q_scale*D[:, np.newaxis]
        ])
    for site, D in inv_D.items()
    }
dm2_s_pri = {
    site:
    np.array([
        np.bincount(nx_sAll_cSel, i[..., reg_map_sel], dim_sAll)
        for i in Q_invgrid*inv_Q_scale*D[:, np.newaxis]
        ])
    for site, D in inv_D.items()
    }
"""Test Only
dchi2_i0 = {}
dchi2_i1 = {}
dchi2_i2 = {}
for site in sites:
    (
        dchi2_i0[site],
        dchi2_i1[site],
        dchi2_i2[site]
        ) = (
            name_qch4_couple.routines.forward_weight(
                M=M, R_std=R_std, p=p, T=T,
                p_chi0=np.zeros(dates_tSite[site].size),
                p_d0i1=np.full(dates_tSite[site].size, -1.e3),
                p_d0i2=np.full(dates_tSite[site].size, -1.e3),
                p_QA=pri_state['v_Q'],
                p_dQi1=dQi1.flatten()[nx_sAll_rSel],
                p_dQi2=dQi2.flatten()[nx_sAll_rSel],
                p_dosage=inv_Df[site]*3600.,
                chi_scale=1,
                select=np.s_[:], dosage_type=1
                )
            )
"""

# Construnct observation vector
obs_v_y = {
    site: {
        k:
        np.array(
            v.loc[date:date].resample('H').mean().reindex(dates_tObs[site][k])
            ).flatten()
        for k, v in y.items()
        }
    for site, y in obs.items()
    }

# Pseudo-observations: from prior
if any([v for site, y in sites.items() for k, v in y.items()]):
    print('  Pseudo-observations from prior')
    for site, y in sites.items():
        for k, v in y.items():
            if v == 0:
                pass
            elif k == 'chi':
                obs_v_y[site][k][:] = (
                    pri_y_i0[site] + pri_y_i1[site] + pri_y_i2[site]
                    )[nx_tSite_tObs[site][k]]
            elif k == 'd_13C':
                obs_v_y[site][k][:] = (
                    (pri_y_i1[site] / pri_y_i0[site] / R_std[1] - 1.)
                    * 1.e3
                    )[nx_tSite_tObs[site][k]]
            elif k == 'd_2H':
                obs_v_y[site][k][:] = (
                    (pri_y_i2[site] / pri_y_i0[site] / R_std[2] - 1.)
                    * 1.e3
                    )[nx_tSite_tObs[site][k]]

# Pseudo-observations: from scenarios
## Create pseudo-observations
if sce:
    print('  Pseudo-observations: scenarios')
    print(f'{" ":8s}{"Scenario":16s}', end='')
    ## isotopologues
    sce_y_i0 = {}
    sce_y_i1 = {}
    sce_y_i2 = {}
    ## set RNG
    rng = np.random.Generator(np.random.PCG64(0))
if sce == 0:
    sce_codes = ()
    pass
elif sce == 1:  # gb, ie: (WM + MA) -> FF
    sce_state = {
        f'{k}': v if k != 'v_Q' else v.copy()
        for k, v in pri_state.items()
        }
    sce_codes = ('gb', 'ie')
    sec0 = 0
    nx_sel = np.unique(inv_reg_maps[sec0])
    id_sce = inv_reg_nos[[
        n for n, i in enumerate(inv_reg_codes)
        if i.startswith(sce_codes)
        ]]
    sce_state['v_Q'][
        ...,
        np.isin(nx_rAll_rSel, id_sce) & np.isin(nx_sSel_rSel, [sec0])
        ] = (
            np.bincount(
                inv_reg_maps[sec0].flatten(),#[reg_map_sel[sec0]],
                QA_invgrid[:3].sum(0).flatten()#[reg_map_sel[sec0]]
                * inv_Q_scale
                )[nx_sel][np.isin(nx_sel, id_sce)]
            / inv_reg_area[nx_rAll_rSel][
                np.isin(nx_rAll_rSel, id_sce) & np.isin(nx_sAll_rSel, [sec0])
                ]
            )
    sce_state['v_Q'][
        ...,
        np.isin(nx_rAll_rSel, id_sce) & np.isin(nx_sSel_rSel, [1, 2])
        ] *= 0.0
elif sce == 2:  # gb, ie: (FF + MA) -> WM
    sce_state = {
        f'{k}': v if k != 'v_Q' else v.copy()
        for k, v in pri_state.items()
        }
    sce_codes = ('gb', 'ie')
    sec0 = 1
    nx_sel = np.unique(inv_reg_maps[sec0])
    id_sce = inv_reg_nos[[
        n for n, i in enumerate(inv_reg_codes)
        if i.startswith(sce_codes)
        ]]
    sce_state['v_Q'][
        ...,
        np.isin(nx_rAll_rSel, id_sce) & np.isin(nx_sSel_rSel, [sec0])
        ] = (
            np.bincount(
                inv_reg_maps[sec0].flatten(),#[reg_map_sel[sec0]],
                QA_invgrid[:3].sum(0).flatten()#[reg_map_sel[sec0]]
                * inv_Q_scale
                )[nx_sel][np.isin(nx_sel, id_sce)]
            / inv_reg_area[nx_rAll_rSel][
                np.isin(nx_rAll_rSel, id_sce) & np.isin(nx_sAll_rSel, [sec0])
                ]
            )
    sce_state['v_Q'][
        ...,
        np.isin(nx_rAll_rSel, id_sce) & np.isin(nx_sSel_rSel, [0, 2])
        ] *= 0.0
elif sce == 3:  # gb, ie: (FF + WM) -> MA
    sce_state = {
        f'{k}': v if k != 'v_Q' else v.copy()
        for k, v in pri_state.items()
        }
    sce_codes = ('gb', 'ie')
    sec0 = 2
    nx_sel = np.unique(inv_reg_maps[sec0])
    id_sce = inv_reg_nos[[
        n for n, i in enumerate(inv_reg_codes)
        if i.startswith(sce_codes)
        ]]
    sce_state['v_Q'][
        ...,
        np.isin(nx_rAll_rSel, id_sce) & np.isin(nx_sSel_rSel, [sec0])
        ] = (
            np.bincount(
                inv_reg_maps[sec0].flatten(),#[reg_map_sel[sec0]],
                QA_invgrid[:3].sum(0).flatten()#[reg_map_sel[sec0]]
                * inv_Q_scale
                )[nx_sel][np.isin(nx_sel, id_sce)]
            / inv_reg_area[nx_rAll_rSel][
                np.isin(nx_rAll_rSel, id_sce) & np.isin(nx_sAll_rSel, [sec0])
                ]
            )
    sce_state['v_Q'][
        ...,
        np.isin(nx_rAll_rSel, id_sce) & np.isin(nx_sSel_rSel, [0, 1])
        ] *= 0.0
elif sce == 4:  # gb, ie: (0.2 * WM) -> FF
    sce_state = {
        f'{k}': v if k != 'v_Q' else v.copy()
        for k, v in pri_state.items()
        }
    sce_codes = ('gb', 'ie')
    sec_from = 2
    sec_to = 0
    sec_given = 0.1
    nx_sel = np.unique(inv_reg_maps[sec_to])
    id_sce = inv_reg_nos[[
        n for n, i in enumerate(inv_reg_codes)
        if i.startswith(sce_codes)
        ]]
    sce_state['v_Q'][
        ...,
        np.isin(nx_rAll_rSel, id_sce) & np.isin(nx_sSel_rSel, [sec_to])
        ] += (
            np.bincount(
                inv_reg_maps[sec_to].flatten(),
                QA_invgrid[sec_from].flatten()
                * inv_Q_scale
                )[nx_sel][np.isin(nx_sel, id_sce)]
            / inv_reg_area[nx_rAll_rSel][
                np.isin(nx_rAll_rSel, id_sce) & np.isin(nx_sAll_rSel, [sec_to])
                ]
            ) * sec_given
    sce_state['v_Q'][
        ...,
        np.isin(nx_rAll_rSel, id_sce) & np.isin(nx_sSel_rSel, [sec_from])
        ] *= (1.0 - sec_given)
elif sce == 5:  # gb, ie future
    sce_state = {
        f'{k}': v if k != 'v_Q' else v.copy()
        for k, v in pri_state.items()
        }
    sce_codes = ('gb', 'ie')
    id_sce = inv_reg_nos[[
        n for n, i in enumerate(inv_reg_codes)
        if i.startswith(sce_codes)
        ]]
    sce_state['v_Q'][
        ...,
        np.isin(nx_rAll_rSel, id_sce) & np.isin(nx_sSel_rSel, [0])
        ] *= 1.25  # 25 % (vs 20 %)
    sce_state['v_Q'][
        ...,
        np.isin(nx_rAll_rSel, id_sce) & np.isin(nx_sSel_rSel, [1])
        ] *= 1.1  # 55 % (vs 50 %)
    sce_state['v_Q'][
        ...,
        np.isin(nx_rAll_rSel, id_sce) & np.isin(nx_sSel_rSel, [2])
        ] *= 7/8  # 70 % (vs 80 %)
elif sce == 6:  # benllu, de, espt, fr MA x1.5
    sce_state = {
        f'{k}': v if k != 'v_Q' else v.copy()
        for k, v in pri_state.items()
        }
    sce_codes = ('benllu', 'de', 'espt', 'fr')
    id_sce = inv_reg_nos[[
        n for n, i in enumerate(inv_reg_codes)
        if i.startswith(sce_codes)
        ]]
    sce_state['v_Q'][
        ...,
        np.isin(nx_rAll_rSel, id_sce) & np.isin(nx_sSel_rSel, [0])
        ] *= 1.5
elif sce == 7:  # Scale gb, ie to UNFCCC
    sce_state = {
        f'{k}': v if k != 'v_Q' else v.copy()
        for k, v in pri_state.items()
        }
    sce_codes = ('gb', 'ie')
    id_sce = inv_reg_nos[[
        n for n, i in enumerate(inv_reg_codes)
        if i.startswith(sce_codes)
        ]]
    for c in sce_codes:
        for k, v in unfccc_s.items():
            id_c = inv_reg_nos[[
                n for n, i in enumerate(inv_reg_codes)
                if i.startswith((c))
                ]]
            QA_u = unfccc.loc[
                unfccc.Party.isin(grid_info['reg_names'][c])
                & unfccc.Category.str.startswith(v),
                f'{dates_tDay[0].year}'
                ].sum() * 1.e6
            QA_e = (pri_state['v_Q'] * inv_reg_area[nx_rAll_rSel])[
                np.isin(nx_rAll_rSel, id_c) & np.isin(nx_sSel_rSel, [k])
                ].sum() / inv_Q_scale * 86400. * 365.25
            if not np.isfinite([QA_u, QA_e]).all():
                continue
            elif QA_e == 0:
                sce_state['v_Q'][
                    np.isin(nx_rAll_rSel, id_c) & np.isin(nx_sSel_rSel, [k])
                    ] += (
                        QA_u
                        / inv_reg_area[nx_rAll_rSel[
                            np.isin(nx_rAll_rSel, id_sce) & np.isin(nx_sSel_rSel, [k])
                            ]].sum()
                        )
            else:
                sce_state['v_Q'][
                    np.isin(nx_rAll_rSel, id_c) & np.isin(nx_sSel_rSel, [k])
                    ] *= QA_u / QA_e
elif sce == 11: # gb, ie anthropogenic x0.5
    sce_state = {
        f'{k}': v if k != 'v_Q' else v.copy()
        for k, v in pri_state.items()
        }
    sce_codes = ('gb', 'ie')
    id_sce = inv_reg_nos[[
        n for n, i in enumerate(inv_reg_codes)
        if i.startswith(sce_codes)
        ]]
    sce_state['v_Q'][
        ...,
        np.isin(nx_rAll_rSel, id_sce) & np.isin(nx_sSel_rSel, [0, 1, 2])
        ] *= 0.5
elif sce == 12: # all regions anthropogenic x0.5
    sce_state = {
        f'{k}': v if k != 'v_Q' else v.copy()
        for k, v in pri_state.items()
        }
    sce_codes = tuple(grid_info['inv_reg_codes'])
    id_sce = inv_reg_nos[[
        n for n, i in enumerate(inv_reg_codes)
        if i.startswith(sce_codes)
        ]]
    sce_state['v_Q'][
        ...,
        np.isin(nx_rAll_rSel, id_sce) & np.isin(nx_sSel_rSel, [0, 1, 2])
        ] *= 0.5
elif sce == 21:  # gb, ie MA x1.5
    sce_state = {
        f'{k}': v if k != 'v_Q' else v.copy()
        for k, v in pri_state.items()
        }
    sce_codes = ('gb', 'ie')
    id_sce = inv_reg_nos[[
        n for n, i in enumerate(inv_reg_codes)
        if i.startswith(sce_codes)
        ]]
    sce_state['v_Q'][
        ...,
        np.isin(nx_rAll_rSel, id_sce) & np.isin(nx_sSel_rSel, [1])
        ] *= 1.5
elif sce == 22:  # benllu, de, espt, fr MA x1.5
    sce_state = {
        f'{k}': v if k != 'v_Q' else v.copy()
        for k, v in pri_state.items()
        }
    sce_codes = ('benllu', 'de', 'espt', 'fr')
    id_sce = inv_reg_nos[[
        n for n, i in enumerate(inv_reg_codes)
        if i.startswith(sce_codes)
        ]]
    sce_state['v_Q'][
        ...,
        np.isin(nx_rAll_rSel, id_sce) & np.isin(nx_sSel_rSel, [1])
        ] *= 1.5
if sce:
    r"""Create pseudo-observations
    Same calculation routine as the inversion for testing
    """
    # d0 (E,)
    sce_i_R0i1 = (
        sce_state['v_d0i1'][:, np.newaxis] * sce_state['v_d0i1f'] * 1.e-3 + 1.
        ) * R_std[1]
    sce_i_R0i2 = (
        sce_state['v_d0i2'][:, np.newaxis] * sce_state['v_d0i2f'] * 1.e-3 + 1.
        ) * R_std[2]
    sce_i_R0sum = 1. + sce_i_R0i1 + sce_i_R0i2
    # chi0 (E,)
    sce_i_chi0fi0 = sce_state['v_chi0f'] * 1. / sce_i_R0sum
    sce_i_chi0fi1 = sce_state['v_chi0f'] * sce_i_R0i1 / sce_i_R0sum
    sce_i_chi0fi2 = sce_state['v_chi0f'] * sce_i_R0i2 / sce_i_R0sum
    # Q
    sce_i_RQi1 = (sce_state['v_dQi1'][nx_sSel_rSel] * 1.e-3 + 1) * R_std[1]
    sce_i_RQi2 = (sce_state['v_dQi2'][nx_sSel_rSel] * 1.e-3 + 1) * R_std[2]
    sce_i_RQsum_M = 1*M[0] + sce_i_RQi1*M[1] + sce_i_RQi2*M[2]
    # intermediate
    sce_bi0 = {
        site:
        sce_state['v_chi0'][nx_tComp_tSite[site]]
        * (sce_i_chi0fi0[nx_tComp_tSite[site]] * E[site]).sum(1)
        for site in sites
        }
    sce_bi1 = {
        site:
        sce_state['v_chi0'][nx_tComp_tSite[site]]
        * (sce_i_chi0fi1[nx_tComp_tSite[site]] * E[site]).sum(1)
        for site in sites
        }
    sce_bi2 = {
        site:
        sce_state['v_chi0'][nx_tComp_tSite[site]]
        * (sce_i_chi0fi2[nx_tComp_tSite[site]] * E[site]).sum(1)
        for site in sites
        }
    sce_baseline = {}
    for site in sites:
        sce_baseline.update({
            f'{site}_v_chi0':
                sce_bi0[site] + sce_bi1[site] + sce_bi2[site],
            f'{site}_v_d0i1':
                (sce_bi1[site] / sce_bi0[site] / R_std[1] - 1.) * 1.e3,
            f'{site}_v_d0i2':
                (sce_bi2[site] / sce_bi0[site] / R_std[2] - 1.) * 1.e3,
            })
        sce_y_i0[site] = (
            (  # baseline
                sce_state['v_chi0'][nx_tComp_tSite[site]]
                * (sce_i_chi0fi0[nx_tComp_tSite[site]] * E[site]).sum(1)
                )
            + dchi0_i0[site]
            + dchi1_i0[site]
            + (sce_state['v_Q'] * 1 / sce_i_RQsum_M * inv_Df[site]).sum(1)
            )
        sce_y_i1[site] = (
            (  # baseline
                sce_state['v_chi0'][nx_tComp_tSite[site]]
                * (sce_i_chi0fi1[nx_tComp_tSite[site]] * E[site]).sum(1)
                )
            + dchi0_i1[site]
            + dchi1_i1[site]
            + (sce_state['v_Q'] * sce_i_RQi1 / sce_i_RQsum_M * inv_Df[site]).sum(1)
            )
        sce_y_i2[site] = (
            (  # baseline
                sce_state['v_chi0'][nx_tComp_tSite[site]]
                * (sce_i_chi0fi2[nx_tComp_tSite[site]] * E[site]).sum(1)
                )
            + dchi0_i2[site]
            + dchi1_i2[site]
            + (sce_state['v_Q'] * sce_i_RQi2 / sce_i_RQsum_M * inv_Df[site]).sum(1)
            )
        if 'chi' in obs_v_y[site]:
            obs_v_y[site]['chi'][:] = (
                sce_y_i0[site] + sce_y_i1[site] + sce_y_i2[site]
                )[nx_tSite_tObs[site]['chi']]
        if 'd_13C' in obs_v_y[site]:
            obs_v_y[site]['d_13C'][:] = (
                (sce_y_i1[site] / sce_y_i0[site] / R_std[1] - 1.) * 1.e3
                )[nx_tSite_tObs[site]['d_13C']]
        if 'd_2H' in obs_v_y[site]:
            obs_v_y[site]['d_2H'][:] = (
                (sce_y_i2[site] / sce_y_i0[site] / R_std[2] - 1.) * 1.e3
                )[nx_tSite_tObs[site]['d_2H']]
    dm2_s_sce = {
        site:
        np.array([
            np.bincount(nx_sAll_rSel, i, dim_sAll)
            for i in sce_state['v_Q']*D
            ])
        for site, D in inv_Df.items()
        }
    check_memory()

# Aggregate uncertainties for observation vector
obs_var_o_y = {  # Variance from measurement uncertainties
    site: {
        k:
        np.array(
            (
                v.loc[date:date].resample('H')
                ).apply(lambda x: (x**2).sum()/(x.size)**2
                    ).reindex(dates_tObs[site][k])
            ).flatten()
        if v is not None else
        None
        for k, v in y.items()
        }
    for site, y in obs_err.items()
    }
obs_var_a_y = {  # Variance from aggregation (hourly variance)
    site: {
        k:
        np.array(
            (
                v.loc[date:date].resample('H')
                ).var().replace(np.nan, 0.).reindex(dates_tObs[site][k])
            ).flatten()
        if v is not None else
        None
        for k, v in y.items()
        }
    for site, y in obs.items()
    }
obs_var_m_y = {  # Variance from meteorology (0.2 * daily max deviation from baseline)
    site: {  # From hourly means
        k:
        (
            0.2 * np.array(
                pd.Series(
                    np.abs(v-pri_baseline[
                        f'{site}_v_chi0' if k == 'chi' else
                        f'{site}_v_d0i1' if k == 'd_13C' else
                        f'{site}_v_d0i2'
                        ][nx_tSite_tObs[site][k]]),
                    index=dates_tObs[site][k]
                    ).resample('1D').max().replace(np.nan, 0.
                        ).reindex(dates_tHourAll).ffill().reindex(dates_tObs[site][k])
                ).flatten()
            )**2
        if v is not None else
        None
        for k, v in y.items()
        }
    for site, y in obs_v_y.items()
    }
obs_var_v_y = {  # Variance from atmospheric variability (daily variance)
    site: {  # From hourly means
        k:
        np.array(
            pd.Series(v, index=dates_tObs[site][k]
                ).resample('1D').var().replace(np.nan, 0.
                    ).reindex(dates_tHourAll).ffill().reindex(dates_tObs[site][k])
            ).flatten()
        if v is not None else
        None
        for k, v in y.items()
        }
    for site, y in obs_v_y.items()
    }
obs_var_b_y = {  # Variance from baseline
    site: {
        k:
        pri_baseline_var[
            f'{site}_v_chi0' if k == 'chi' else
            f'{site}_v_d0i1' if k == 'd_13C' else
            f'{site}_v_d0i2'
            ][nx_tSite_tObs[site][k]].flatten()
        if v is not None else
        None
        for k, v in y.items()
        }
    for site, y in obs_v_y.items()
    }
obs_var_y = {
    site: {
        k:
        (
            obs_var_o_y[site][k]
            + obs_var_a_y[site][k]
            + obs_var_m_y[site][k]
            + obs_var_v_y[site][k]
            + obs_var_b_y[site][k]
            )
        for k in y
        }
    for site, y in obs.items()
    }

if prior_only:
    sys.exit()

# =============================================================================
#   Main Processing
# =============================================================================
print('Main Processing')

# Decide whether to compute or read
compute = 1 if force_compute else 0
if not os.path.exists(odir):
    os.makedirs(odir)
    compute = 1
if not compute and os.path.exists(os.path.join(odir, 'constants.npz')):
    try:
        chi_scale = np.load(
            os.path.join(odir, 'constants.npz'), 'r'
            )['chi_scale']
    except:
        pass
    try:
        inv_Q_scale = np.load(
            os.path.join(odir, 'constants.npz'), 'r'
            )['inv_Q_scale']
    except:
        pass

if not inversion:
    pass
else:
    print('  Inversion')
    model = pm.Model()
    # =========================================================================
    # State
    # =========================================================================
    print('    Defining State')
    print(f'{" ":8s}{"State Vector":16s}', end='')
    with model:
        r"""State  vectors
        """
        # chi0 - baseline chi (t or b)
        if 'chi0' in theta_vars:
            u_chi0 = pm.TruncatedNormal(
                'u_chi0',
                mu=pri_state['u_chi0'],
                sigma=pri_state['h_chi0'],
                lower=pri_state['u_chi0']-pri_state['h_chi0'],
                upper=pri_state['u_chi0']+pri_state['h_chi0']*3,
                shape=(dim_tComp,)
                )
            v_chi0 = pm.Normal(
                'v_chi0',
                mu=pri_state['v_chi0'], sigma=u_chi0, shape=(dim_tComp,)
                )
            v_chi0f = pm.TruncatedNormal(
                'v_chi0f',
                mu=pri_state['v_chi0f'],
                sigma=pri_state['u_chi0f'],
                lower=pri_state['v_chi0f']*0.5,
                upper=pri_state['v_chi0f']*1.5,
                shape=(dim_bAll,)
                )
        else:
            v_chi0 = pri_state['v_chi0']
            v_chi0f = pri_state['v_chi0f']
        # d0i1 - baseline d_13CH4 (t or b)
        if 'd0i1' in theta_vars:
            u_d0i1 = pm.TruncatedNormal(
                'u_d0i1',
                mu=pri_state['u_d0i1'],
                sigma=pri_state['h_d0i1'],
                lower=pri_state['u_d0i1']-pri_state['h_d0i1'],
                upper=pri_state['u_d0i1']+pri_state['h_d0i1']*3,
                shape=(dim_tComp,)
                )
            v_d0i1 = pm.Normal(
                'v_d0i1',
                mu=pri_state['v_d0i1'], sigma=u_d0i1, shape=(dim_tComp,)
                )
            v_d0i1f = pm.TruncatedNormal(
                'v_d0i1f',
                mu=pri_state['v_d0i1f'],
                sigma=pri_state['u_d0i1f'],
                lower=pri_state['v_d0i1f']*0.5,
                upper=pri_state['v_d0i1f']*1.5,
                shape=(dim_bAll,)
                )
        else:
            v_d0i1 = pri_state['v_d0i1']
            v_d0i1f = pri_state['v_d0i1f']
        # d0i2 - baseline d_12CH3D (t or b)
        if 'd0i2' in theta_vars:
            u_d0i2 = pm.TruncatedNormal(
                'u_d0i2',
                mu=pri_state['u_d0i2'],
                sigma=pri_state['h_d0i2'],
                lower=pri_state['u_d0i2']-pri_state['h_d0i2'],
                upper=pri_state['u_d0i2']+pri_state['h_d0i2']*3,
                shape=(dim_tComp,)
                )
            v_d0i2 = pm.Normal(
                'v_d0i2',
                mu=pri_state['v_d0i2'], sigma=u_d0i2, shape=(dim_tComp,)
                )
            v_d0i2f = pm.TruncatedNormal(
                'v_d0i2f',
                mu=pri_state['v_d0i2f'],
                sigma=pri_state['u_d0i2f'],
                lower=pri_state['v_d0i2f']*0.5,
                upper=pri_state['v_d0i2f']*1.5,
                shape=(dim_bAll,)
                )
        else:
            v_d0i2 = pri_state['v_d0i2']
            v_d0i2f = pri_state['v_d0i2f']

        # Q (rSel)  ## scaled due to pymc3 refusing very small number
        if 'Q' in theta_vars:
            u_Q = pm.TruncatedNormal(
                'u_Q',
                mu=pri_state['u_Q'],
                sigma=pri_state['h_Q'],
                lower=pri_state['u_Q']*0.5,
                shape=pri_state['v_Q'].shape
                )
            """
            v_Q = pm.Lognormal(
                'v_Q',
                mu=np.log(np.maximum(pri_state['v_Q'], 1.e-6)),
                sigma=u_Q/pri_state['v_Q'],
                shape=pri_state['v_Q'].shape
                )
            """
            v_Q = pm.TruncatedNormal(
                'v_Q',
                mu=pri_state['v_Q'],
                sigma=u_Q,
                lower=0., shape=pri_state['v_Q'].shape
                )
        else:
            v_Q = pri_state['v_Q']
        # dQ - d_13CH4, d_12CH3D (2s)
        if 'dQ' in theta_vars:
            h_dQ = pm.Uniform(
               'h_dQi1',
               lower=pri_state['h_dQ'][0],
               upper=pri_state['h_dQ'][1],
               shape=(2*dim_sSel,)
               )
            h_dQcor = sd2cov(np.ones(2*dim_sSel))  # correlation matrix
            if dim_sSel > 1:
                np.fill_diagonal(h_dQcor[dim_sSel:], 0.10)
                np.fill_diagonal(h_dQcor[:, dim_sSel:], 0.10)
            v_dQ = pm.MvNormal(
                'v_dQ',
                mu=pri_state['v_dQ'],
                cov=h_dQcor*theano.tensor.outer(h_dQ, h_dQ),
                shape=(2*dim_sSel,)
                )
            v_dQi1 = v_dQ[(slice(0, dim_sSel))].reshape(pri_state['v_dQi1'].shape)
            v_dQi2 = v_dQ[(slice(dim_sSel, 2*dim_sSel))].reshape(pri_state['v_dQi1'].shape)
        else:
            # dQi1 - d_13CH4 (s, 1)
            if 'dQi1' in theta_vars:
                u_dQi1 = pm.Uniform(
                    'u_dQi1',
                    lower=pri_state['h_dQi1'][0],
                    upper=pri_state['h_dQi1'][1],
                    shape=pri_state['v_dQi1'].shape
                    )
                v_dQi1 = pm.Normal(
                    'v_dQi1',
                    mu=pri_state['v_dQi1'],
                    sigma=u_dQi1,
                    shape=pri_state['v_dQi1'].shape
                    )
            else:
                v_dQi1 = pri_state['v_dQi1']
            # dQi2 - d_13CH4 (s, 1)
            if 'dQi2' in theta_vars:
                u_dQi2 = pm.Uniform(
                    'u_dQi2',
                    lower=pri_state['h_dQi2'][0],
                    upper=pri_state['h_dQi2'][1],
                    shape=pri_state['v_dQi2'].shape
                    )
                v_dQi2 = pm.Normal(
                    'v_dQi2',
                    mu=pri_state['v_dQi2'],
                    sigma=u_dQi2,
                    shape=pri_state['v_dQi2'].shape
                    )
            else:
                v_dQi2 = pri_state['v_dQi2']

        # Dosage (t, r)
        p_Df = inv_Df
    check_memory()

    # =========================================================================
    # Calculations
    #   Calculating isotopologues separately
    # =========================================================================
    print('    Setting Calculation Up')
    print(f'{" ":8s}{"Isotopes":16s}', end='')
    with model:
        """
        """
        # d0 (t, b)
        mod_i_R0i1 = (v_d0i1[:, np.newaxis] * v_d0i1f * 1.e-3 + 1.) * R_std[1]
        mod_i_R0i2 = (v_d0i2[:, np.newaxis] * v_d0i2f * 1.e-3 + 1.) * R_std[2]
        mod_i_R0sum = 1. + mod_i_R0i1 + mod_i_R0i2
        # chi0f (t, b)
        mod_i_chi0fi0 = v_chi0f * 1. / mod_i_R0sum
        mod_i_chi0fi1 = v_chi0f * mod_i_R0i1 / mod_i_R0sum
        mod_i_chi0fi2 = v_chi0f * mod_i_R0i2 / mod_i_R0sum
        # dQ (s,)
        mod_i_RQi1 = (v_dQi1[nx_sSel_rSel] * 1.e-3 + 1.) * R_std[1]
        mod_i_RQi2 = (v_dQi2[nx_sSel_rSel] * 1.e-3 + 1.) * R_std[2]
        mod_i_RQsum_M = 1.*M[0] + mod_i_RQi1*M[1] + mod_i_RQi2*M[2]
    check_memory()

    # =========================================================================
    # Mixing ratios of isotopologues
    # =========================================================================
    print('    Intermediate')
    with model:
        """
        """
        # d0 (E,)
        mod_y_i0 = {}
        mod_y_i1 = {}
        mod_y_i2 = {}
        for site in sites:
            print(f'{" ":8s}{site:16s}', end='')
            mod_y_i0[site] = pm.Deterministic(
                f'mod_y_i0_{site}',
                (  # baseline
                    v_chi0[nx_tComp_tSite[site]]
                    * (mod_i_chi0fi0[nx_tComp_tSite[site]] * E[site]).sum(1)
                    )
                + dchi0_i0[site]  # dilution outside domain
                + dchi1_i0[site]  # dilution inside domain unoptimised
                + (  # dilution inside domain optimised
                    v_Q * 1 / mod_i_RQsum_M * p_Df[site]
                    ).sum(1)
                )
            mod_y_i1[site] = pm.Deterministic(
                f'mod_y_i1_{site}',
                (  # baseline
                    v_chi0[nx_tComp_tSite[site]]
                    * (mod_i_chi0fi1[nx_tComp_tSite[site]] * E[site]).sum(1)
                    )
                + dchi0_i1[site]
                + dchi1_i1[site]
                + (
                    v_Q * mod_i_RQi1 / mod_i_RQsum_M * p_Df[site]
                    ).sum(1)
                )
            mod_y_i2[site] = pm.Deterministic(
                f'mod_y_i2_{site}',
                (  # baseline
                    v_chi0[nx_tComp_tSite[site]]
                    * (mod_i_chi0fi2[nx_tComp_tSite[site]] * E[site]).sum(1)
                    )
                + dchi0_i2[site]
                + dchi1_i2[site]
                + (
                    v_Q * mod_i_RQi2 / mod_i_RQsum_M * p_Df[site]
                    ).sum(1)
                )
            check_memory()

    # =========================================================================
    # Modelled observations
    # =========================================================================
    print('    Defining Observations')
    with model:
        mod_u_obs = {site: {} for site in sites}
        mod_v_obs = {site: {} for site in sites}
        for site, vs in y_vars.items():
            # Using Network Compatibility (WMO GAW report Table 1.)
            print(f'{" ":6s}{site:16s}')
            k = 'chi'
            if k in vs and dates_tObs[site][k].size > 0:
                print(f'{" ":8s}{k:16s}')
                mod_u_obs[site][k] = pm.TruncatedNormal(
                    f'unc_{site}_{k}',
                    mu=obs_var_y[site][k]**0.5,
                    sigma=obs_var_y[site][k]**0.5*0.5,
                    lower=obs_var_y[site][k]**0.5*0.5,
                    shape=obs_v_y[site][k].shape
                    )
                mod_v_obs[site][k] = pm.Normal(
                    f'obs_{site}_{k}',
                    mu=(
                        mod_y_i0[site] + mod_y_i1[site] + mod_y_i2[site]
                        )[nx_tSite_tObs[site][k]],
                    sigma=mod_u_obs[site][k],
                    observed=obs_v_y[site][k]
                    )
            k = 'd_13C'
            if k in vs and dates_tObs[site][k].size > 0:
                print(f'{" ":8s}{k:16s}')
                '''
                mod_u_obs[site][k] = pm.Bound(
                    pm.Lognormal, lower=(obs_var_y[site][k]**0.5)*0.5)(
                        f'unc_{site}_{k}',
                        mu=np.log(obs_var_y[site][k]**0.5),
                        sigma=0.5,
                        shape=obs_v_y[site][k].shape
                        )
                '''
                mod_u_obs[site][k] = pm.TruncatedNormal(
                    f'unc_{site}_{k}',
                    mu=obs_var_y[site][k]**0.5,
                    sigma=obs_var_y[site][k]**0.5*0.5,
                    lower=obs_var_y[site][k]**0.5*0.5,
                    shape=obs_v_y[site][k].shape
                    )
                mod_v_obs[site][k] = pm.Normal(
                    f'obs_{site}_{k}',
                    mu=(
                        (mod_y_i1[site]/mod_y_i0[site]/R_std[1] - 1.) * 1.e3
                        )[nx_tSite_tObs[site][k]],
                    sigma=mod_u_obs[site][k],
                    observed=obs_v_y[site][k]
                    )
            k = 'd_2H'
            if k in vs and dates_tObs[site][k].size > 0:
                print(f'{" ":8s}{k:16s}')
                mod_u_obs[site][k] = pm.TruncatedNormal(
                    f'unc_{site}_{k}',
                    mu=obs_var_y[site][k]**0.5,
                    sigma=obs_var_y[site][k]**0.5*0.5,
                    lower=obs_var_y[site][k]**0.5*0.5,
                    shape=obs_v_y[site][k].shape
                    )
                mod_v_obs[site][k] = pm.Normal(
                    f'obs_{site}_{k}',
                    mu=(
                        (mod_y_i2[site]/mod_y_i0[site]/R_std[2] - 1.) * 1.e3
                        )[nx_tSite_tObs[site][k]],
                    sigma=mod_u_obs[site][k],
                    observed=obs_v_y[site][k]
                    )
            print(f'{" ":8s}{f"After {site}":16s}', end='')
            check_memory()

    # =========================================================================
    # Sample
    # =========================================================================
    with model:
        if compute:
            print('    Sampling Started')
            trace = pm.sample(
                pm_draws, cores=pm_cores, chains=pm_chains, tune=pm_tune,
                #target_accept=.8,
                random_seed=pm_seed,
                return_inferencedata=False
                )
            print('    Saving Results')
            result = pm.save_trace(trace, directory=odir, overwrite=True)
        else:
            print('    Reading Output')
            trace = pm.load_trace(odir)
    print(f'{" ":8s}{"After Inversion":16s}', end='')
    check_memory()

# =============================================================================
#   Post-processing
# =============================================================================
print('Post-Processing')
# Save constants
np.savez(
    os.path.join(odir, 'dates.npz'),
    **{
        **{f'dates_tSite_{site}': v for site, v in dates_tSite.items()},
        **{
            f'dates_tObs_{site}_{k}': v
            for site, y in dates_tObs.items()
            for k, v in y.items()
            },
        }
    )

# Plot parameters
plot_params = {
    'chi': {
        'ytick':
            [[1800., 3400.], 200.]
            if "cbw" in sites and sce in [0] else
            [[1800., 2300.], 100.],
        'ytick_diff': [[-20.0, 20.0], 5.0],
        'lbl': u'$\chi$ (CH$_4$) (ppb)',
        },
    'd_13C': {
        'ytick':
            [[-55., -45.], 2.]
            if 'cbw' in sites and sce in [0] else
            [[-50., -46.], 1.],
        'ytick_diff': [[-0.5, 0.5], 0.2],
        'lbl': u'$\delta$ $^{13}$CH$_4$ (\u2030)',
        },
    'd_2H': {
        'ytick':
            [[-180., -60.], 20.]
            if 'cbw' in sites and sce in [0] else
            [[-130., -70.], 10.],
        'ytick_diff': [[-10., 10], 4.],
        'lbl': u'$\delta$ $^{12}$CH$_3$D (\u2030)',
        },
    'v_chi0': {
        'ytick': [[1800., 2000.], 50.],
        'ytick_diff': [[-15., 15.], 3.],
        'lbl': u'Baseline $\chi$ (CH$_4$) (ppb)',
        },
    'v_d0i1': {
        'ytick': [[-51., -45.], 2.],
        'ytick_diff': [[-0.5, 0.5], 0.2],
        'lbl': u'Baseline $\delta$ $^{13}$CH$_4$ (\u2030)',
        },
    'v_d0i2': {
        'ytick': [[-140., -60.], 20.],
        'ytick_diff': [[-10., 10.], 4.],
        'lbl': u'Baseline $\delta$ $^{12}$CH$_3$D (\u2030)',
        },
    'v_Q': {  # Actual Q will be plotted instead
        'ytick':  [
            np.power(10., np.linspace(-12, -5, 15)),
            np.power(10., np.linspace(-12, -5, 8))
            ],
        'ytick_diff': [
            np.array([
                *-np.linspace(1.2, 0., 13)[:-1],
                *np.linspace(0., 1.2, 13)[1:]
                ]),
            np.array([
                *-np.linspace(1.2, 0., 4)[:-1],
                0.,
                *np.linspace(0., 1.2, 4)[1:]
                ])
            ],
        'lbl': u'Emission\n(g m$^{-2}$ s$^{-1}$)',
        },
    'v_Q_unc': {
        'ytick': [
            np.power(10., np.linspace(-12, -5, 15)),
            np.power(10., np.linspace(-12, -5, 8))
            ],
        'ytick_diff': [
            np.array([
                *-np.linspace(1.2, 0., 13)[:-1],
                *np.linspace(0., 1.2, 13)[1:]
                ]),
            np.array([
                *-np.linspace(1.2, 0., 4)[:-1],
                0.,
                *np.linspace(0., 1.2, 4)[1:]
                ])
            ],
        'lbl': u'Emission\nUncertainty\n(g m$^{-2}$ s$^{-1}$)',
        },
    'v_dQi1': {
        'ytick': [[-80., -10.], 10.],
        'ytick_diff': [[-1.0, 1.0], 0.4],
        'lbl': u'Emission $\delta$ $^{13}$CH$_4$ (\u2030)',
        },
    'v_dQi2': {
        'ytick': [[-450., -130.], 50.],
        'ytick_diff': [[-1.0, 1.0], 0.4],
        'lbl': u'Emission $\delta$ $^{12}$CH$_3$D (\u2030)',
        },
    }
colours = {
    0: '#FC8E50',  # observation
    1: '#000000',  # prior hres
    2: '#008DE1',  # posterior
    3: '#464E00',  # spare
    }
# Figure dict
plt.close('all')
fig_y = {}
fig_theta = {}

# Observations
## True observation
np.savez(
    os.path.join(odir, 'obs_y.npz'),
    **{
        **{
            f'{site}_{k}': v
            for site, y in obs_v_y.items()
            for k, v in y.items()
            },
        **{
            f'var_{site}_{k}': v
            for site, y in obs_var_y.items()
            for k, v in y.items()
            }
        }
    )
## Observation from posteriors
if inversion:
    pos_y = {
            f'{site}_{y}': np.full((3, dates_tHour.size), np.nan)
            for site, ys in sites.items()
            for y in ['chi', 'd_13C', 'd_2H']#ys
            }
    for site in sites:
        trace_i0 = trace.get_values(f'mod_y_i0_{site}')
        trace_i1 = trace.get_values(f'mod_y_i1_{site}')
        trace_i2 = trace.get_values(f'mod_y_i2_{site}')
        pos_y[f'{site}_chi'][:, nx_tH_tSite[site]] = (
            np.percentile(
                trace_i0 + trace_i1 + trace_i2,
                [16, 50, 84], axis=0
                )
            )
        pos_y[f'{site}_d_13C'][:, nx_tH_tSite[site]] = (
            np.percentile(
                (trace_i1 / trace_i0 / R_std[1] - 1.) * 1.e3,
                [16, 50, 84], axis=0
                )
            )
        pos_y[f'{site}_d_2H'][:, nx_tH_tSite[site]] = (
            np.percentile(
                (trace_i2 / trace_i0 / R_std[2] - 1.) * 1.e3,
                [16, 50, 84], axis=0
                )
            )
    np.savez(os.path.join(odir, 'pos_y.npz'), **pos_y)

## Observation from priors
pri_y = {
        f'{site}_{y}': np.full((dates_tHour.size,), np.nan)
        for site, ys in sites.items()
        for y in ['chi', 'd_13C', 'd_2H']
        }
for site in sites:
    pri_y[f'{site}_chi'][nx_tH_tSite[site]] = (
        pri_y_i0[site] + pri_y_i1[site] + pri_y_i2[site]
        )
    pri_y[f'{site}_d_13C'][nx_tH_tSite[site]] = (
        (pri_y_i1[site] / pri_y_i0[site] / R_std[1] - 1.) * 1.e3
        )
    pri_y[f'{site}_d_2H'][nx_tH_tSite[site]] = (
        (pri_y_i2[site] / pri_y_i0[site] / R_std[2] - 1.) * 1.e3
        )
np.savez(os.path.join(odir, 'pri_y.npz'), **pri_y)
## Plot
loc_y = {
    f'{site}_{k}': (y, x)
    for site, x in {site: x for x, site in enumerate(sites)}.items()
    for k, y in {k: y for y, k in enumerate(
        ['chi', 'd_13C', 'd_2H'] #set([k for v in sites.values() for k in v])
        )}.items()
    }
wdh = 2.0
hgt1 = 1.5
n_site = len(sites)
n_ky = len(set([k for v in sites.values() for k in v]))
fig_y['normal'] = plt.figure(figsize=(wdh*n_site, hgt1*n_ky), dpi=300)
fig_y['compare'] = plt.figure(figsize=(wdh*n_site, hgt1*n_ky), dpi=300)
fig_y['mismatch'] = plt.figure(figsize=(wdh*n_site, hgt1*n_ky), dpi=300)
for k, fig in fig_y.items():
    fig.clf()
for site in sites:
    for k in set([k for v in sites.values() for k in v]):
        if k not in sites[site]:
            continue
        key = f'{site}_{k}'
        # Normal
        name_qch4_couple.plot.generic(
            fig=fig_y['normal'],
            idata={
                'obs': [
                    'err',
                    [],
                    {
                        'x': dates_tObs[site][k], 'y': obs_v_y[site][k],
                        'yerr': NCG[k] * NCG_mul[k] if k != 'chi' else None,  #obs_u_y[site][k] if k != 'chi' else None,
                        'c': colours[0], 'fmt': '.',
                        'markersize': 2.0, 'elinewidth': 0.5,
                        'capsize': 1.0, 'capthick': 0.5,
                        'zorder': 1
                        }
                    ] if k in obs_v_y[site] else ['', [], {}],
                'prior': [
                    'err',
                    [],
                    {
                        'x': dates_tObs[site][k],
                        'y': pri_y[f'{site}_{k}'][nx_tH_tObs[site][k]],
                        'yerr': None,
                        'color': colours[1], 'fmt': '.',
                        'markersize': 2.0, 'elinewidth': 0.5,
                        'capsize': 1.0, 'capthick': 0.5,
                        'zorder': 2
                        }
                    ],
                'posterior': [
                    'err',
                    [],
                    {
                        'x': dates_tObs[site][k],
                        'y': pos_y[f'{site}_{k}'][1, nx_tH_tObs[site][k]],
                        'yerr': np.diff(
                            pos_y[f'{site}_{k}'][:, nx_tH_tObs[site][k]],
                            axis=0
                            ),
                        'color': colours[2], 'fmt': '.',
                        'markersize': 2.0, 'elinewidth': 0.5,
                        'capsize': 1.0, 'capthick': 0.5,
                        'zorder': 3
                        }
                    ] if inversion and k in sites[site] else ['', [], {}],
                },
            xlim=[
                np.datetime64(dates_tHour[0]) - np.timedelta64(1, 'D'),
                np.datetime64(dates_tHour[-1]) + np.timedelta64(1, 'D'),
                ],
            ylim=plot_params[k]['ytick'][0],
            texts=[],
            yticks=np.arange(
                plot_params[k]['ytick'][0][0],
                plot_params[k]['ytick'][0][1] + plot_params[k]['ytick'][1],
                plot_params[k]['ytick'][1]
                ),
            loc_plot=[
                (loc_y[f'{site}_{k}'][1]*wdh + 0.5)/wdh/n_site,
                ((n_ky-1-loc_y[f'{site}_{k}'][0])*hgt1 + 0.2)/hgt1/n_ky,
                (wdh-0.55)/wdh/n_site,
                (hgt1-0.25)/hgt1/n_ky
                ],
            tick_fontsize=6,
            xtick_params=[
                True,
                mdates.DateFormatter('%Y-%m-%d'),
                mdates.WeekdayLocator(
                    byweekday=6, interval=(1 if tmode in [0, 1] else 2)
                    ),
                ],
            )
        if k not in sites[site] or obs_v_y[site][k].size == 0:
            continue
        # Comparison (obs vs model)
        name_qch4_couple.plot.compare(
            fig=fig_y['compare'],
            idata={
                'prior': [
                    [
                        obs_v_y[site][k],
                        pri_y[f'{site}_{k}'][nx_tH_tObs[site][k]]
                        ],
                    {'s':1., 'c':colours[1]}
                    ],
                'posterior': [
                    [
                        obs_v_y[site][k],
                        pos_y[f'{site}_{k}'][1, nx_tH_tObs[site][k]]
                        ],
                    {'s':1., 'c':colours[2]}
                    ] if inversion else False,
                },
            lim=plot_params[k]['ytick'][0],
            err=NCG[k] * NCG_mul[k],  #obs_u_y[site][k].max(),
            texts=[],
            ticks=np.arange(
                plot_params[k]['ytick'][0][0],
                plot_params[k]['ytick'][0][1]
                + plot_params[k]['ytick'][1],
                plot_params[k]['ytick'][1]
                )[::2],
            loc_plot=[
                (loc_y[f'{site}_{k}'][1]*wdh + 0.5)/wdh/n_site,
                ((n_ky-1-loc_y[f'{site}_{k}'][0])*hgt1 + 0.2)/hgt1/n_ky,
                (hgt1-0.25)/wdh/n_site,
                (hgt1-0.25)/hgt1/n_ky
                ],
            fontsize=6,
            )
        # Mismatch (observation - model)
        name_qch4_couple.plot.generic(
            fig=fig_y['mismatch'],
            idata={
                'obs': [
                    'err',
                    [],
                    {
                        'x': dates_tObs[site][k],
                        'y': np.zeros(dates_tObs[site][k].size),
                        'yerr': NCG[k] * NCG_mul[k],  #obs_u_y[site][k] if k != 'chi' else None,
                        'c': colours[0], 'fmt': '.',
                        'markersize': 2.0, 'elinewidth': 0.5,
                        'capsize': 1.0, 'capthick': 0.5,
                        'zorder': 1
                        }
                    ] if k in obs_v_y[site] else ['', [], {}],
                'prior': [
                    'line',
                    [
                        dates_tObs[site][k],
                        obs_v_y[site][k]
                        - pri_y[f'{site}_{k}'][nx_tH_tObs[site][k]]
                        ],
                    {
                        'c': colours[1], 'ls': '', 'marker': 'o',
                        'ms': 2.0, 'mew': 0.,
                        'lw': 0.5, 'zorder': 2
                        }
                    ],
                'posterior': [
                    'err',
                    [],
                    {
                        'x': dates_tObs[site][k],
                        'y': (
                            obs_v_y[site][k]
                            - pos_y[key][1, nx_tH_tObs[site][k]]
                            ),
                        'yerr': (
                            np.diff(
                                pos_y[key][:, nx_tH_tObs[site][k]],
                                axis=0
                                )
                            ),
                        'color': colours[2], 'fmt': '.',
                        'markersize': 2.0, 'elinewidth': 0.5,
                        'capsize': 1.0, 'capthick': 0.5,
                        'zorder': 3
                        }
                    ] if inversion and k in sites[site] else ['', [], {}],
                },
            xlim=[
                np.datetime64(dates_tHour[0]) - np.timedelta64(1, 'D'),
                np.datetime64(dates_tHour[-1]) + np.timedelta64(1, 'D'),
                ],
            ylim=plot_params[k]['ytick_diff'][0],
            texts=[],
            yticks=np.arange(
                plot_params[k]['ytick_diff'][0][0],
                plot_params[k]['ytick_diff'][0][1]
                + plot_params[k]['ytick_diff'][1],
                plot_params[k]['ytick_diff'][1]
                ),
            loc_plot=[
                (loc_y[f'{site}_{k}'][1]*wdh + 0.5)/wdh/n_site,
                ((n_ky-1-loc_y[f'{site}_{k}'][0])*hgt1 + 0.2)/hgt1/n_ky,
                (wdh-0.55)/wdh/n_site,
                (hgt1-0.25)/hgt1/n_ky
                ],
            tick_fontsize=6,
            xtick_params=[
                True,
                mdates.DateFormatter('%Y-%m-%d'),
                mdates.WeekdayLocator(
                    byweekday=6, interval=(1 if tmode in [0, 1] else 2)
                    ),
                ],
            )
for k, fig in fig_y.items():
    for n, var in enumerate(['chi', 'd_13C', 'd_2H']):
        fig.text(**{
            'x': 0.05/wdh/n_site,
            'y': (n_ky - n - 0.45)/n_ky,
            's': plot_params[var]['lbl'],
            'fontsize': 6,
            'ha': 'left', 'va': 'center', 'rotation': 90
            })
    for site in sites:
        fig.text(**{
            'x': (
                (wdh*(loc_y[f'{site}_{[k for k in sites[site]][0]}'][1]+1) - 0.1)
                /wdh/n_site
                if k not in ['compare'] else
                (loc_y[f'{site}_{[k for k in sites[site]][0]}'][1] + 0.6)/n_site
                ),
            'y': (n_ky*hgt1 - 0.1)/hgt1/n_ky,
            's': site,
            'fontsize': 6,
            'ha': 'right', 'va': 'top', 'rotation': 0
            })
for k, fig in fig_y.items():
    fig.savefig(os.path.join(odir, f'inv_y_{k}.jpeg'), dpi=300)

# State
sector_names = {
    0: ['FF', 'Fossil Fuel'],
    1: ['WM', 'Waste Management'],
    2: ['MA', 'Anthropogenic Microbial'],
    3: ['BB', 'Biomass Burning'],
    4: ['MN', 'Natural Microbial'],
    }
focus_codes = {
    **({'sce': sce_codes} if sce else {}),
    'gbie': ('gb', 'ie'),
    'euw': ('benllu', 'de', 'espt', 'fr', 'gb', 'ie'),
    **{i: i for i in set([*sce_codes, 'gb'])}
    }
## Priors
pri_theta = {
    **def_state,
    **pri_state,
    **pri_baseline,
    **({f'sce_{k}': v for k, v in sce_state.items()} if sce else {}),
    **({f'sce_{k}': v for k, v in sce_baseline.items()} if sce else {}),
    'inv_Q_scale': inv_Q_scale,
    'chi_scale': chi_scale,
    'nx_rAll_rSel': nx_rAll_rSel,
    'inv_reg_maps': inv_reg_maps,
    'inv_reg_area': inv_reg_area,
    'inv_reg_codes': inv_reg_codes,
    'reg_map_sel': reg_map_sel,
    'inv_Q_dist': inv_Q_dist,
    **{
        f'id_{sreg}':
        inv_reg_nos[[
            n for n, i in enumerate(inv_reg_codes)
            if i.startswith(id_codes)
            ]]
        for sreg, id_codes in focus_codes.items()
        },
    **{f'dm0_s_{site}': i for site, i in dm0_s.items()},
    **{f'dm1_s_{site}': i for site, i in dm1_s.items()},
    **{f'dm2_s_{site}': i for site, i in dm2_s_pri.items()},
    **(
        {f'dm2_s_sce_{site}': i for site, i in dm2_s_sce.items()}
        if sce else {}
        )
    }
np.savez(os.path.join(odir, 'pri_theta.npz'), **pri_theta)
## Posteriors
if inversion:  # Process prior and posterior state
    pos_theta = {}
    pos_theta = {
        f'v_{k}': np.percentile(
            trace.get_values(f'v_{k}'),
            [16, 50, 84], axis=0
            )
        for k in [
            'Q', 'dQ', 'dQi1', 'dQi2'
            ]
        if k in theta_vars
        }
    if 'Q' in theta_vars:
        key = 'v_Q'
        for sreg, id_codes in focus_codes.items():
            id_c = inv_reg_nos[[
                n for n, i in enumerate(inv_reg_codes)
                if i.startswith(id_codes)
                ]]
            pos_theta[f'id_{sreg}'] = id_c
            pos_theta[f'QA_{sreg}'] = np.concatenate([
                (
                (
                    np.percentile(
                        (
                            trace.get_values(key)[
                                ..., np.isin(nx_rAll_rSel, id_c) & np.isin(nx_sAll_rSel, s)
                                ]
                            * (
                                inv_reg_area[nx_rAll_rSel[
                                    np.isin(nx_rAll_rSel, id_c) & np.isin(nx_sAll_rSel, s)
                                    ]]
                                )
                            ).sum(-1),
                        [16, 50, 84], axis=0
                        )
                    if s in inv_s_sel else
                    np.array([np.nan, 0., np.nan])
                    ).reshape(3, 1)
                + (
                    np.bincount(
                        inv_reg_maps[~reg_map_sel], QA_invgrid[~reg_map_sel]
                        )[nx_rAll_rSel_nots[
                            ...,
                            np.isin(nx_rAll_rSel_nots, id_c)
                            & np.isin(nx_sAll_rSel_nots, s)
                            ]] * inv_Q_scale
                    ).sum()
                ).reshape(3, 1)
                for s in range(dim_sAll)
                ], axis=1)
            pos_theta[f'QA_{sreg}_total'] = (
                (
                    np.percentile(
                        (
                            trace.get_values(key)[
                                ..., np.isin(nx_rAll_rSel, id_c)
                                ]
                            * (
                                inv_reg_area[nx_rAll_rSel[
                                    np.isin(nx_rAll_rSel, id_c)
                                    ]]
                                )
                            ).sum(-1),
                        [16, 50, 84], axis=0
                        )
                    ).reshape(3, 1)
                + (
                    np.bincount(
                        inv_reg_maps[~reg_map_sel], QA_invgrid[~reg_map_sel]
                        )[nx_rAll_rSel_nots[
                            ...,
                            np.isin(nx_rAll_rSel_nots, id_c)
                            ]] * inv_Q_scale
                    ).sum()
                ).reshape(3, 1)

        for site, D in inv_Df.items():
            pos_theta[f'dm2_s_{site}'] = (
                np.array([
                    np.bincount(nx_sAll_rSel, i, dim_sAll)
                    for i in pos_theta[key][1]*D
                    ])
                )
    if 'dQ' in theta_vars:
        pos_theta['v_dQi1'] = pos_theta['v_dQ'][:, :dim_sSel]
        pos_theta['v_dQi2'] = pos_theta['v_dQ'][:, dim_sSel:2*dim_sSel]
    flag = False
    if 'chi0' in theta_vars:
        pos_theta['v_chi0'] = np.percentile(
            trace.get_values(f'v_chi0'),
            [16, 50, 84], axis=0
            )
        pos_theta['v_chi0f'] = np.percentile(
            trace.get_values(f'v_chi0f'),
            [16, 50, 84], axis=0
            )
        flag = True
    if 'd0i1' in theta_vars:
        pos_theta['v_d0i1'] = np.percentile(
            trace.get_values(f'v_d0i1'),
            [16, 50, 84],axis=0
            )
        pos_theta['v_d0i1f'] = np.percentile(
            trace.get_values(f'v_d0i1f'),
            [16, 50, 84], axis=0
            )
        flag = True
    if 'd0i2' in theta_vars:
        pos_theta['v_d0i2'] = np.percentile(
            trace.get_values(f'v_d0i2'),
            [16, 50, 84], axis=0
            )
        pos_theta['v_d0i2f'] = np.percentile(
            trace.get_values(f'v_d0i2f'),
            [16, 50, 84], axis=0
            )
        flag = True
    if flag:
        pos_bi0, pos_bi1, pos_bi2 = calc_init_edge(
            chi0=(
                trace.get_values('v_chi0')
                if 'chi0' in theta_vars else
                pri_state['v_chi0'][np.newaxis]
                )[..., np.newaxis],
            d0i1=(
                trace.get_values('v_d0i1')
                if 'd0i1' in theta_vars else
                pri_state['v_d0i1'][np.newaxis]
                )[..., np.newaxis],
            d0i2=(
                trace.get_values('v_d0i2')
                if 'd0i2' in theta_vars else
                pri_state['v_d0i2'][np.newaxis]
                )[..., np.newaxis],
            chi0f=(
                trace.get_values('v_chi0f')
                if 'chi0' in theta_vars else
                pri_theta['v_chi0f'].mean(0, keepdims=True)
                )[:, np.newaxis],
            d0i1f=(
                trace.get_values('v_d0i1f')
                if 'd0i1' in theta_vars else
                pri_theta['v_d0i1f'].mean(0, keepdims=True)
                )[:, np.newaxis],
            d0i2f=(
                trace.get_values('v_d0i2f')
                if 'd0i2' in theta_vars else
                pri_theta['v_d0i2f'].mean(0, keepdims=True)
                )[:, np.newaxis],
            R_std=R_std,
            )
        for site in sites:
            (
                pos_theta[f'{site}_v_chi0'],
                pos_theta[f'{site}_v_d0i1'],
                pos_theta[f'{site}_v_d0i2']
                ) = (
                    np.percentile(v, [16, 50, 84], axis=0)
                    for v in calc_init_site(
                        chi0i0=pos_bi0[:, nx_tComp_tSite[site]],
                        chi0i1=pos_bi1[:, nx_tComp_tSite[site]],
                        chi0i2=pos_bi2[:, nx_tComp_tSite[site]],
                        E=E[site],
                        idx_bin_site=nx_tbSel_tSite[site],
                        R_std=R_std
                        )
                    )
    np.savez(os.path.join(odir, 'pos_theta.npz'), **pos_theta)
## Plot
wdh_Q = wdh * (3 if inversion else 1) + (0 if inversion else 0.5)
hgt2 = 1.2
fig_theta['Q_normal'] = plt.figure(figsize=(wdh_Q, hgt2*dim_sAll), dpi=300)
fig_theta['Q_unc'] = plt.figure(figsize=(wdh_Q, hgt2*dim_sAll), dpi=300)
fig_theta['dQ'] = plt.figure(figsize=(wdh*1, hgt1*2), dpi=300)
fig_theta['init'] = plt.figure(figsize=(wdh*n_site, hgt1*n_ky), dpi=300)
for k, fig in fig_theta.items():
    fig.clf()
for k, v in pri_theta.items():
    if k.endswith(('h_', 'u_')):  # Uncertainties and hyperparameters - skip
        continue
    elif k in ['v_chi0f', 'v_d0i1f', 'v_d0i2f']:  # Baseline factor - skip
        continue
    elif k in ['v_chi0', 'v_d0i1', 'v_d0i2']:  # Baseline
        if k == 'v_chi0':
            obs_key = 'chi'
        elif k == 'v_d0i1':
            obs_key = 'd_13C'
        else:
            obs_key = 'd_2H'
        for site in sites:
            name_qch4_couple.plot.generic(
                fig=fig_theta['init'],
                idata={
                    'obs': [
                        'err',
                        [],
                        {
                            'x': dates_tObs[site][obs_key],
                            'y': obs_v_y[site][obs_key],
                            'yerr': obs_var_y[site][obs_key]**.5,
                            'c': colours[0], 'fmt': '.',
                            'markersize': 2.0, 'elinewidth': 0.5,
                            'capsize': 1.0, 'capthick': 0.5,
                            'zorder': 1
                            }
                        ] if obs_key in obs_v_y[site] else ['', [], {}],
                    'prior': [
                        'err',
                        [],
                        {
                            'x': dates_tObs[site][obs_key],
                            'y': (
                                pri_theta[f'{site}_{k}']
                                )[nx_tSite_tObs[site][obs_key]],
                            'yerr': None,
                            'c': colours[1], 'ls': '', 'marker': 'o',
                            'ms': 2.0, 'mew': 0.,
                            'lw': 0.5, 'zorder': 2
                            }
                        ] if obs_key in obs_v_y[site] else ['', [], {}],
                    'posterior': [
                        'err',
                        [],
                        {
                            'x': dates_tSite[site],
                            'y': pos_theta[f'{site}_{k}'][1],
                            'yerr': np.diff(
                                pos_theta[f'{site}_{k}'], axis=0
                                ),
                            'color': colours[2], 'fmt': '.',
                            'markersize': 2.0, 'elinewidth': 0.5,
                            'capsize': 1.0, 'capthick': 0.5,
                            'zorder': 3
                            }
                        ] if inversion and k in pos_theta else ['', [], {}],
                    },
                xlim=[
                    np.datetime64(dates_tHour[0]) - np.timedelta64(1, 'D'),
                    np.datetime64(dates_tHour[-1]) + np.timedelta64(1, 'D'),
                    ],
                ylim=plot_params[k]['ytick'][0],
                texts=[],
                yticks=np.arange(
                    plot_params[k]['ytick'][0][0],
                    plot_params[k]['ytick'][0][1] + plot_params[k]['ytick'][1],
                    plot_params[k]['ytick'][1]
                    ),
                loc_plot=[
                    (loc_y[f'{site}_{obs_key}'][1]*wdh + 0.5)/wdh/n_site,
                    ((n_ky-1-loc_y[f'{site}_{obs_key}'][0])*hgt1 + 0.2)/hgt1/n_ky,
                    (wdh-0.55)/wdh/n_site,
                    (hgt1-0.25)/hgt1/n_ky
                    ],
                tick_fontsize=6,
                xtick_params=[
                    True,
                    mdates.DateFormatter('%Y-%m-%d'),
                    mdates.WeekdayLocator(
                        byweekday=6, interval=(1 if tmode in [0, 1] else 2)
                        ),
                    ],
                )
    elif k in ['v_Q']:  # Emissions
        for k1, v1 in {
                'prior': v / inv_Q_scale,
                'posterior':
                    pos_theta[k][1] / inv_Q_scale if inversion else None,
                'diff': (
                    (v - pos_theta[k][1]) / v
                    ) if inversion else None,
                'unc_prior': pri_state['u_Q'] / inv_Q_scale,
                'unc_posterior':
                    pos_theta[k][[0,2]].mean(0) / inv_Q_scale
                    if inversion else None,
                'unc_diff': (
                    (
                        pri_state['u_Q']
                        - pos_theta[k][[0,2]].mean(0)
                        ) / pri_state['u_Q']
                    ) if inversion else None,
                }.items():
            if v1 is None:
                continue
            if k1.startswith('unc'):
                plot_param = plot_params['v_Q_unc']
            else:
                plot_param = plot_params['v_Q']
            if k1.endswith('diff'):
                colour = 'RdBu_r'
                ctick = plot_param['ytick_diff']
            else:
                colour = 'viridis_r'
                ctick = plot_param['ytick']
            fig = fig_theta[
                'Q_unc' if k1.startswith('unc') else 'Q_normal'
                ]
            fx = (
                    0 if k1.endswith('prior') else
                    1 if k1.endswith('posterior') else
                    2 if k1.endswith('diff') else
                    3
                    )
            plotable = np.zeros(Q_invgrid.shape)
            if k1.endswith('diff'):
                plotable[:] = np.nan
                plotable[reg_map_sel] = v1[nx_rSel_cSel]
            else:
                plotable[:] = 0.
                plotable[reg_map_sel] = v1[nx_rSel_cSel] * inv_Q_dist
                plotable[~reg_map_sel] = Q_invgrid[~reg_map_sel]
            for s in range(dim_sAll):
                name_qch4_couple.plot.geographical(
                    fig=fig,
                    idata=plotable[s],
                    grid=[
                        grid_vertex[0][inv_lon0:inv_lon1+1],
                        grid_vertex[1][inv_lat0:inv_lat1+1]
                        ],
                    texts=[],
                    loc_plot=(
                        [
                            (0.14)/wdh_Q,
                            ((dim_sAll-s-1)*hgt2+0.05)/hgt2/dim_sAll,
                            1.6/wdh_Q,
                            (hgt2-0.1)/hgt2/dim_sAll
                            ]
                        if fx == 0 else
                        [
                            (0.14+fx*1.6+(fx-1)*0.53)/wdh_Q,
                            ((dim_sAll-s-1)*hgt2+0.05)/hgt2/dim_sAll,
                            1.6/wdh_Q,
                            (hgt2-0.1)/hgt2/dim_sAll
                            ]
                        ),
                    loc_bar=(
                        [
                            (0.14+(fx+1)*1.6+(fx-1)*0.53+0.02)/wdh_Q,
                            ((dim_sAll-s-1)*hgt2+0.1)/hgt2/dim_sAll,
                            (.1)/wdh_Q,
                            (hgt2-0.5)/hgt2/dim_sAll
                            ]
                        if fx != 0 and inversion else
                        [
                            (0.14+1.6+0.02)/wdh_Q,
                            ((dim_sAll-s-1)*hgt2+0.1)/hgt2/dim_sAll,
                            (.1)/wdh_Q,
                            (hgt2-0.5)/hgt2/dim_sAll
                            ]
                        if fx == 0 and not inversion else
                        False
                        ),
                    bbox=[
                        grid_vertex[0][inv_lon0], grid_vertex[0][inv_lon1],
                        grid_vertex[1][inv_lat0], grid_vertex[1][inv_lat1]
                        ],
                    tick_params = {
                        'labelsize': 6 if fx == 0 else 4,
                        'pad': 0.05
                        },
                    shapefiles=[
                        '/home/ec5/name_qch4_couple/shape_files/'
                        'CNTR_BN_10M_2016_4326.shp'
                        ],
                    colour=colour,
                    vlim=[ctick[0][0], ctick[0][-1], ctick[0], ctick[1]],
                    extend='both' if k1.endswith('diff') else 'max',
                    )
                if fx == 0 and not inversion:
                    fig.axes[-1].set_yticklabels(
                        [f'10$^{{{x:+1.0f}}}$' for x in np.log10(ctick[1])]
                        )
                elif fx == 1:
                    fig.axes[-1].set_yticklabels(
                        [f'10$^{{{x:+1.0f}}}$' for x in np.log10(ctick[1])]
                        )
                elif fx == 2:
                    fig.axes[-1].set_yticklabels(
                            [f'{x:4.2f}' for x in ctick[1]]
                        )
                if fx == 2:
                    fig.axes[-1].set_facecolor('#c0c0c0')
    elif k in ['v_dQi1', 'v_dQi2']:
        n = (1 if k == 'v_dQi1' else 0 if k == 'v_dQi2' else 2)
        hyper = (
            'h_dQi1' if k == 'v_dQi1' else
            'h_dQi2' if k == 'v_dQi2' else
            'h_dQi3'
            )
        plotable0 = (dQi1 if k == 'v_dQi1' else dQi2).copy().flatten()
        plotable1 = np.full((dim_sAll,), np.nan)
        if k.replace('v_', '') in theta_vars:
            plotable1[inv_s_sel] = pri_theta[hyper].mean(0).flatten()
        if inversion and k in pos_theta:
            plotable2 = np.full((3, dim_sAll), np.nan)
            plotable2[:, inv_s_sel] = pos_theta[k]
        name_qch4_couple.plot.generic(
            fig=fig_theta['dQ'],
            idata={
                'prior': [
                    'err',
                    [],
                    {
                        'x': np.arange(dim_sAll),
                        'y': plotable0,
                        'yerr': plotable1,
                        'c': colours[0], 'fmt': 'o',
                        'markersize': 2.0, 'elinewidth': 0.5,
                        'capsize': 2.0, 'capthick': 0.5,
                        'zorder': 1
                        }
                    ],
                'posterior': [
                    'err' if inversion and k in pos_theta else '',
                    [],
                    {
                        'x': np.arange(Q.shape[0]),
                        'y': plotable2[1],
                        'yerr': np.diff(plotable2, axis=0).reshape(2, -1),
                        'c': colours[2], 'fmt': 'o',
                        'markersize': 2.0, 'elinewidth': 0.5,
                        'capsize': 2.0, 'capthick': 0.5,
                        'zorder': 2
                        } if inversion and k in pos_theta else {}
                    ],
                },
            xlim=[-0.5, dim_sAll-0.5],
            ylim=plot_params[k]['ytick'][0],
            texts=[
                {
                    'x': 0.05/wdh/1 ,
                    'y': (n + 0.55) / 2,
                    's': plot_params[k]['lbl'],
                    'fontsize': 6,
                    'ha': 'left', 'va': 'center', 'rotation': 90
                    }, # y label
                    ],
            yticks=np.arange(
                plot_params[k]['ytick'][0][0],
                plot_params[k]['ytick'][0][1] + plot_params[k]['ytick'][1],
                plot_params[k]['ytick'][1]
                ),
            loc_plot=[
                0.5/wdh/1,
                (n*hgt1 + 0.2)/hgt1/2,
                (wdh-0.55)/wdh/1,
                (hgt1-0.25)/hgt1/2
                ],
            tick_fontsize=6,
            xtick_params=[
                False,
                np.arange(5),
                [sector_names[i][0] for i in range(dim_sAll)]
                ],
            )
for k, v in {'v_Q': 'Q_normal', 'v_Q_unc': 'Q_unc'}.items():
    for s in range(dim_sAll):
        for text in [
                {  # sector name
                    'x': 0.05/wdh_Q,
                    'y': (dim_sAll-s-0.5)/dim_sAll,
                    's': (
                        f'{sector_names[s][1]} ({sector_names[s][0]})'
                        ),
                    'ha': 'left', 'va': 'center', 'size': 6, 'rotation': 90
                    },
                {  # colorbar label 1
                    'x': (
                        (0.14+2*1.6+0*0.53+0.02)/wdh_Q
                        if inversion else
                        (0.14+1.6+0.02)/wdh_Q
                        ),
                    'y': ((dim_sAll-s)*hgt2-0.1)/hgt2/dim_sAll,
                    's': plot_params[k]['lbl'],
                    'ha': 'left', 'va': 'top', 'size': 6
                    },
                {  # colorbar label 2
                    'x': (
                        (0.14+3*1.6+1*0.53+0.02)/wdh_Q
                        if inversion else
                        (wdh+0.05)/wdh_Q
                        ),
                    'y': ((dim_sAll-s)*hgt2-0.1)/hgt2/dim_sAll,
                    's': u"Relative\nDifference",
                    'ha': 'left', 'va': 'top', 'size': 6
                    } if inversion else {}
                ]:
            if text:
                fig_theta[v].text(**text)
if 'init' in fig_theta:
    fig = fig_theta['init']
    for n, var in enumerate(['chi', 'd_13C', 'd_2H']):
        fig.text(**{
            'x': 0.05/wdh/n_site,
            'y': (n_ky - n - 0.45)/n_ky,
            's': plot_params[var]['lbl'],
            'fontsize': 6,
            'ha': 'left', 'va': 'center', 'rotation': 90
            })
    for site in sites:
        fig.text(**{
            'x': (
                (wdh*(loc_y[f'{site}_{[k for k in sites[site]][0]}'][1]+1) - 0.1)
                /wdh/n_site
                if k not in ['compare'] else
                (loc_y[f'{site}_{[k for k in sites[site]][0]}'][1] + 0.6)/n_site
                ),
            'y': (n_ky*hgt1 - 0.1)/hgt1/n_ky,
            's': site,
            'fontsize': 6,
            'ha': 'right', 'va': 'top', 'rotation': 0
            })
for k, fig in fig_theta.items():
    fig.savefig(os.path.join(odir, f'inv_theta_{k}.jpeg'))
print('The End')

