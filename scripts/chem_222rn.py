import numpy as np
import pandas as pd
import xarray as xr


def read_Q(timestamps):
    date = timestamps[0].strftime('%Y-%m')
    # European emissions
    Qfile = '/home/ec5/hpc-work/data_archive/rn_flux/noah.nc'
    with xr.open_dataset(Qfile) as ds_read:
        with ds_read.load() as Qin:
            t = pd.to_datetime(date, format='%Y-%m')
            t_Q = pd.to_datetime(np.array(Qin.time), format='%Y%m%d')
            tin = min(t_Q[t_Q.month==t.month], key=lambda x: abs(x - t))
        Q_Rn = np.array(Qin.rn_flux.sel(time=tin.strftime('%Y%m%d')))
    # Global emissions
    Qfile = '/home/ec5/hpc-work/data_archive/rn_flux_zhang2011/zhang2011_E3.nc'
    with xr.open_dataset(Qfile) as ds_read:
        with ds_read.load() as Qin:
            t = pd.to_datetime(date, format='%Y-%m')
            Q_Rn[~np.isfinite(Q_Rn)] = (
                np.array(Qin.rnemis[t.month-1])[~np.isfinite(Q_Rn)]
                * 1.e4*np.log(2)/3.822/86400*1e3
                )
    return Q_Rn


def read_obs(timestamps, site):
    date = timestamps[0].strftime('%Y-%m')
    io_date_range = [
        timestamps[0] - pd.to_timedelta('30min'),
        timestamps[-1],
        ]
    if site == 'HFD_100magl':
        obs_Rn_file = '/home/ec5/hpc-work/data_archive/decc/obs/hfd_radon.dat'
        obs_Rn_raw = (
            pd.read_csv(
                obs_Rn_file,
                sep='\t', skipinitialspace=True,
                parse_dates={'datetime': ['date', 'time']},
                date_parser=lambda x: datetime.strptime(x, '%y%m%d %H%M'),
                index_col='datetime'
                ).loc[io_date_range[0]:io_date_range[1]]
            if os.path.exists(obs_Rn_file) else
            pd.Series(np.nan, index=timestamps)
            )
        obs_Rn_deconv = pd.read_csv(
            '/hpc-work/ec5/data_archive/decc/obs/'
            'Rn_data_2021-09-15.csv',
            #'hfd_decRn_STP_202007-202104.csv',
            #'Heathfield_RT_corrected_radon_30min_preliminary.csv',
            sep=',', skipinitialspace=True,
            #date_parser=lambda x: pd.to_datetime(x, format='%Y/%m/%d %H:%M'),
            parse_dates={'Datetime': ['date']},
            index_col='Datetime'
            ).loc[io_date_range[0]:io_date_range[1]]
        obs = (
            obs_Rn_deconv['Rn_stp'].loc[date].rename()
            ).resample('1H').mean() * 1.e3
    else:
        obs = pd.Series(np.nan, index=timestamps)
    return obs


def read_baseline(timestamps, site):
    chi0 = pd.Series(0., index=timestamps)
    var_chi0 = pd.Series(0., index=timestamps)
    return chi0
