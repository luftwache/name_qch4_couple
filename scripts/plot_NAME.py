# Standard Library imports
import argparse
import gzip
import matplotlib.dates as mdates
import matplotlib.pyplot as plt
import numpy as np
import os
import pandas as pd
import re
import sys

# Third party imports 

# Semi-local imports
import name_qch4_couple.io
import name_qch4_couple.name
import name_qch4_couple.plot


# Parse arguments
parser = argparse.ArgumentParser()
parser.add_argument("-date", required=True)
parser.add_argument("-ifile", required=True)
parser.add_argument("-odir", required=True)
args = parser.parse_args()
date = args.date
ifile = args.ifile
odir = args.odir

# Grid
lon0 = -98.07600
lat0 = 10.61200
dlon = 0.3520000
dlat = 0.2340000
nlon = 391
nlat = 293
grid = [
    np.linspace(lon0 + dlon/2, lon0 + dlon*(nlon - 0.5), nlon),
    np.linspace(lat0 + dlat/2, lat0 + dlat*(nlat - 0.5), nlat)
    ]
grid2 = [
    np.linspace(lon0, lon0 + dlon*nlon, nlon+1),
    np.linspace(lat0, lat0 + dlat*nlat, nlat+1)
    ]
area = name_qch4_couple.name.calc_cell_area(
    grid2[1][:-1],
    grid2[1][1:],
    dlon
    )[:, np.newaxis]

# Read file
if ifile.endswith('gz'):
    dosage = name_qch4_couple.name.txt2array(
        name_qch4_couple.io.r_name(ifile),
        [nlat, nlon], np.arange(24), np.nan
        )
    dates = pd.date_range(
        f'{date} 00',
        np.datetime64(f'{date} 00') + np.timedelta64(1, 'D'),
        freq=f'{24 // dosage.shape[0]}H',
        closed='left'
        )
if np.datetime64(date).dtype == np.dtype('<M8[D]'):
    dates_idx = np.arange(dates.size)

fs = [5, 2.5]
fpd = 0.04
flp = [fpd, 0.24]
flb = [fs[0]-0.6, 0.24]
fbs = [0.1, 1.5]
fig = plt.figure(figsize=fs, dpi=300)
for n, i in enumerate(dates[dates_idx]):
    fig.clf()
    name_qch4_couple.plot.geographical(
        fig=fig,
        idata=dosage[n],
        grid=[grid2[0], grid2[1]],
        texts=[
            {
                'x': fpd/fs[0], 'y': (flp[1]-fpd)/fs[1],
                's': 'Release date',
                'ha': 'left', 'va': 'top', 'size': 8
                },
            {
                'x': (flb[0]-fpd)/fs[0], 'y': (flp[1]-fpd)/fs[1],
                's': f'{i}',
                'ha': 'right', 'va': 'top', 'size': 8
                },
            {
                'x': flb[0]/fs[0], 'y': (flb[1]+2)/fs[1],
                's': 'Dosage\n(ppm s)',
                'ha': 'left', 'va': 'top', 'size':8
                }
            ],
        loc_plot=[
            flp[0]/fs[0], flp[1]/fs[1],
            (flb[0]-flp[0]-fpd)/fs[0], (fs[1]-flp[1]-fpd)/fs[1]
            ],
        loc_bar=[
            flb[0]/fs[0], flb[1]/fs[1], 
            fbs[0]/fs[0], fbs[1]/fs[1]
            ],
        bbox=[
            grid2[0][0], grid2[0][-1],
            grid2[1][0], grid2[1][-1]
            ],
        tick_params={
            'labelsize': 6, 'pad': 0.05
            },
        shapefiles=[
            '/home/ec5/name_qch4_couple/shape_files'
            '/CNTR_BN_10M_2016_4326.shp'
            ],
        colour='viridis_r',
        vlim=[
            1.e-4, 1.e0,
            np.power(10, np.linspace(-4, 0, 9)),
            np.power(10, np.linspace(-4, 0, 5)),
            ],
        extend='both',
        )
    date_str = dates[n].strftime('%Y%m%d_%H')
    fig.savefig(os.path.join(odir, f'dosage_{date_str}.png'))

